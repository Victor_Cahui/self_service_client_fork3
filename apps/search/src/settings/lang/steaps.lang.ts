export const StepsLang = {
  Guarantees: 'GARANTIZA',
  PersonalSecurity: 'LA SEGURIDAD DE TU PERSONAL',
  Description: 'MEDIANTE ESTOS SIMPLES PASOS PODRÁS VERIFICAR LA VIGENCIA DE TU PÓLIZA',
  Step1: {
    Title: 'Primero, comprueba tu número de constancia',
    Description: `El número de constancia es el elemento identificador del período de vigencia de tu póliza. Lo encuentras en
    la parte superior izquierda del documento de aseguramiento.`
  },
  Step2: {
    Title: 'Segundo, ingresa un número de documento',
    Description: `Puedes elegir cualquier documento de uno de tus asegurados. Sin embargo,
    asegúrate de saber el tipo de documento (DNI, PASAPORTE, CARNET DE EXTRANJERÍA).`
  },
  Step3: {
    Title: 'Tercero, un clic y verifica la vigencia',
    Description: `Si tu constancia está vigente puedes certificar la cantidad de asegurados inscritos. En caso contrario
    puedes contactarte con:`
  }
};
