export const MAPFRE_PHONENUMBER_LIMA = '(01) 213-3333';
export const TITLE = 'Constancias SCTR';
export const APPLICATION_CODE = 'SCC';

export const GENERAL_MAX_LENGTH = {
  maxLengthRecord: 17,
  minLengthRecord: 7,
  maxLengthSearch: 20
};

export const REGEX = {
  record: /^[a-z]{2}\/\d{4}\/\d{7,9}/i
};
