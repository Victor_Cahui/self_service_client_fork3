import { APPLICATION_CODE } from '@mx-search/settings/constants/general-values';
import { environment } from '../../environments/environment';

export class RecordEndpoint {
  public static getRecords = `${environment.API_URL_SCTR}constancia/${APPLICATION_CODE}/{nroConstancia}/{tipoDocumento}/{documento}`;
  public static downloadRecord = environment.API_URL_DOWNLOAD_RECORD;
}
