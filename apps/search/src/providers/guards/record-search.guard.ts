import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable()
export class RecordSearchGuard implements CanActivate {
  constructor(private readonly router: Router) {}

  canActivate(next: ActivatedRouteSnapshot): boolean {
    const numberRecord: string = next.queryParamMap.get('numberRecord');
    const typeDocumentQuery: string = next.queryParamMap.get('typeDocument');
    const numberDocumentQuery: string = next.queryParamMap.get('numberDocument');

    if (numberRecord && typeDocumentQuery && numberDocumentQuery) {
      return true;
    }
    this.router.navigate(['/']);

    return false;
  }
}
