import { Injectable } from '@angular/core';
import { environment } from '@mx-search/environments/environment';
import { diffMinutesBetween } from '@mx/core/shared/helpers/util/date';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Subject } from 'rxjs';

export enum CAPTCHA_STORAGE_KEY {
  CAPTCHA = 'captcha',
  CAPTCHA_MAP = 'captcha_map'
}

export enum CAPCHAT_MODE {
  TIMER = 'tiempo',
  REQUEST = 'consultas'
}

@Injectable({
  providedIn: 'root'
})
export class CaptchaService {
  maxLifeCaptcha;
  onReset: Subject<any>;

  constructor(private readonly storageService: StorageService) {
    this.onReset = new Subject<any>();
  }

  handler(): void {
    const key = environment.RECAPTCHA.mode;
    this.maxLifeCaptcha = environment.RECAPTCHA.value || 3;

    switch (key) {
      case CAPCHAT_MODE.TIMER:
        this.handlerTime();
        break;
      case CAPCHAT_MODE.REQUEST:
        this.handlerRequest();
        break;
      default:
        break;
    }
  }

  private handlerRequest(): void {
    const def = { count: 0 };
    const requestMap = this.getCaptchaMap() || def;
    requestMap.count++;
    this.setCaptchaMap(requestMap);

    if (requestMap.count >= this.maxLifeCaptcha) {
      this.resetCaptcha();
    }
  }

  private handlerTime(): void {
    const def: any = { time: new Date().toISOString() };
    let requestMap = this.getCaptchaMap();
    if (!requestMap) {
      this.setCaptchaMap(def);
    }
    requestMap = requestMap || def;
    const minutes = diffMinutesBetween(new Date(requestMap.time), new Date(), false);

    if (minutes >= this.maxLifeCaptcha) {
      this.resetCaptcha();
    }
  }

  private resetCaptcha(): void {
    this.onReset.next();
    this.storageService.removeItem(CAPTCHA_STORAGE_KEY.CAPTCHA);
    this.storageService.removeItem(CAPTCHA_STORAGE_KEY.CAPTCHA_MAP);
  }

  setCaptchaResponse(response: string): void {
    this.storageService.setItem(CAPTCHA_STORAGE_KEY.CAPTCHA, response);
  }

  getCaptchaResponse(): string {
    return this.storageService.getItem(CAPTCHA_STORAGE_KEY.CAPTCHA);
  }

  setCaptchaMap(captchaMap): void {
    const tmp = btoa(JSON.stringify(captchaMap));
    this.storageService.setItem(CAPTCHA_STORAGE_KEY.CAPTCHA_MAP, tmp);
  }

  getCaptchaMap(): any {
    const tmp = this.storageService.getItem(CAPTCHA_STORAGE_KEY.CAPTCHA_MAP);

    return tmp ? JSON.parse(atob(tmp)) : null;
  }
}
