import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';

@Injectable({
  providedIn: 'root'
})
export class NavHelpers {
  constructor(@Inject(DOCUMENT) private readonly document: Document) {}

  routerToNavigate(element: string, enlace: string): void {
    const e3: HTMLElement = this.document.getElementById('activity');
    e3.classList.remove('g-font--bold');
    const el: HTMLElement = this.document.querySelector(element);
    const en: HTMLElement = this.document.querySelector(enlace);
    en.classList.add('g-font--bold');
    ScrollUtil.goTo(el.offsetTop - 68);
  }
}
