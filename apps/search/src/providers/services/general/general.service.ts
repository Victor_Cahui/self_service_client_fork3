import { Injectable } from '@angular/core';
import { GeneralEndpoint } from '@mx-search/endpoints/general.endpoint';
import { APPLICATION_CODE } from '@mx-search/settings/constants/general-values';
import { EMAIL_CONTAC_ACTPOL, EMAIL_CONTACT, PHONE_CONTACT_LIMA } from '@mx-search/settings/constants/key-values';
import { IErrorModal, IParameterApp } from '@mx-search/statemanagement/models/general.models';
import { TypeDocument } from '@mx-search/statemanagement/models/typedocument.interface';
import { ApiService } from '@mx/core/shared/helpers/api';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private errorDefault: IErrorModal;
  private readonly parameters: BehaviorSubject<Array<IParameterApp>>;
  private readonly typesDocument: BehaviorSubject<Array<TypeDocument>>;

  constructor(private readonly apiService: ApiService) {
    this.parameters = new BehaviorSubject<Array<IParameterApp>>(null);
    this.typesDocument = new BehaviorSubject<Array<TypeDocument>>(null);
  }

  getParametersApi(): Observable<Array<IParameterApp>> {
    return this.apiService.get(`${GeneralEndpoint.parameters}`, {});
  }

  getParameters(): BehaviorSubject<Array<IParameterApp>> {
    return this.parameters;
  }

  setParameters(parameters: Array<IParameterApp>): void {
    this.parameters.next(parameters);
  }

  getTypesDocumentApi(seccion: string = APPLICATION_CODE): Observable<Array<TypeDocument>> {
    return this.apiService.get(`${GeneralEndpoint.typesDocument}`, { params: { seccion } });
  }

  getTypesDocument(): BehaviorSubject<Array<TypeDocument>> {
    return this.typesDocument;
  }

  setTypesDocument(typesDocuments: Array<TypeDocument>): void {
    this.typesDocument.next(typesDocuments);
  }

  getPhoneNumber(): string {
    return this.getValueParams(PHONE_CONTACT_LIMA);
  }

  getEmailActivePolicy(): string {
    return this.getValueParams(EMAIL_CONTAC_ACTPOL);
  }

  getEmail(): string {
    return this.getValueParams(EMAIL_CONTACT);
  }

  getValueParams(key): string {
    if (!this.parameters) {
      return null;
    }
    const params = (this.parameters.value || []).filter(param => param.codigo === key);

    return params && params[0] ? params[0].valor : '';
  }

  setDefaultError(error: IErrorModal): void {
    this.errorDefault = error;
  }

  getDefaultError(): IErrorModal {
    return this.errorDefault;
  }
}
