import { Injectable } from '@angular/core';
import { RecordEndpoint } from '@mx-search/endpoints/record.endpoint';
import { APPLICATION_CODE } from '@mx-search/settings/constants/general-values';
import {
  IRecordDownloadRequest,
  IRecordDownloadResponde,
  IRecordRequest,
  IRecordResponse
} from '@mx-search/statemanagement/models/record.interface';
import { ApiService } from '@mx/core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecordService {
  constructor(private readonly apiService: ApiService) {}

  getRecords(params: IRecordRequest): Observable<IRecordResponse> {
    return this.apiService.get(RecordEndpoint.getRecords, { params, preloader: true });
  }

  downloadRecord(body: IRecordDownloadRequest): Observable<IRecordDownloadResponde> {
    body.codigoApp = APPLICATION_CODE;

    return this.apiService.post(RecordEndpoint.downloadRecord, body, {});
  }
}
