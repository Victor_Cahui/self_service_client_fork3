export enum RecordState {
  NotFound = 0,
  NotValid = 1,
  Found = 2
}
export interface IRecordResponse {
  numPoliza?: string;
  descripcionPoliza?: string;
  tipoDocumentoContratante?: string;
  numDocumentoContratante?: string;
  descripcionContratante?: string;
  nroConstancia?: string;
  fechaInicioVigenciaConstancia?: string;
  fechaFinVigenciaConstancia?: string;
  lugarActividad?: string;
  asegurados?: Array<IRecordInsuredResponse>;
  estado?: RecordState;
}

export interface IRecordInsuredResponse {
  tipoDocumento: string;
  numDocumento: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombre: string;

  fullName?: string;
}

export interface IRecordRequest {
  nroConstancia: string;
  tipoDocumento: string;
  documento: string;
}

export interface IRecordFilter {
  numberRecord: string;
  typeDocument: string;
  numberDocument: string;
}

export interface IRecordDownloadResponde {
  base64: string;
}

export interface IRecordDownloadRequest {
  codigoApp?: string;
  numPoliza: string;
  numeroConstancia: string;
}
