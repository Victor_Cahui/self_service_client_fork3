export const environment = {
  production: true,
  API_URL_COMUN: 'https://api.mapfre.com.pe/legacy/ascomun/v2/',
  API_URL_SCTR: '/api/v4/sctr/',
  API_URL_DOWNLOAD_RECORD: 'https://api.mapfre.com.pe/legacy/asweb/downloadConstancia',
  MAPFRE_QUOTE_SCTR: 'https://sctr.mapfre.com.pe',

  RECAPTCHA: {
    API_KEY: '6LdrmYcUAAAAAHX_QnP94k1xE9-OEXrvb4PmQU2X',
    mode: 'consultas', // tiempo (minutos) || consultas (#)
    value: 3
  }
};
