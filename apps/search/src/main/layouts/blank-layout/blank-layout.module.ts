import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterModule, HeaderModule } from '@mx-search/components';
import { MfButtonTopModule } from '@mx/core/ui/lib/components/button-top/button-top.module';
import { BlankLayoutComponent } from './blank-layout.component';

@NgModule({
  imports: [CommonModule, RouterModule, HeaderModule, FooterModule, MfButtonTopModule],
  declarations: [BlankLayoutComponent]
})
export class BlankLayoutModule {}
