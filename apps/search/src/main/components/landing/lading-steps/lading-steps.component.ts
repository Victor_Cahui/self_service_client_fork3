import { Component, OnInit } from '@angular/core';
import { GeneralService } from '@mx-search/services/general/general.service';
import { StepsLang } from '@mx-search/settings/lang/steaps.lang';

@Component({
  selector: 'search-lading-steps',
  templateUrl: './lading-steps.component.html'
})
export class LadingStepsComponent implements OnInit {
  lang = StepsLang;
  emailcontact: string;

  constructor(private readonly generalService: GeneralService) {}

  ngOnInit(): void {
    this.emailcontact = this.generalService.getEmailActivePolicy();
  }
}
