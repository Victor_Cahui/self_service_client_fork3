import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseFormQuery } from '@mx-search/components/shared/base-form-query';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { environment } from '../../../../environments/environment';
import { GeneralService } from '../../../../providers/services/general/general.service';

@Component({
  selector: 'search-landing-form',
  templateUrl: './landing-form.component.html',
  providers: [PlatformService]
})
export class LandingFormComponent extends BaseFormQuery implements OnInit {
  quote_url = environment.MAPFRE_QUOTE_SCTR;

  constructor(
    protected fb: FormBuilder,
    protected generalService: GeneralService,
    protected router: Router,
    protected platformService: PlatformService
  ) {
    super(fb, generalService, router, platformService);
  }

  ngOnInit(): void {
    this.initView();
  }

  onSubmit(): void {
    if (this.form.valid) {
      const numRecord = this.numberRecord.value.replace(/\//g, '_');
      this.router.navigate(['/search'], {
        queryParams: {
          numberRecord: numRecord,
          typeDocument: this.typeDocument.value,
          numberDocument: this.numberDocument.value
        }
      });
    }
  }

  getDefaultData(): void {
    this.setValues();
  }
}
