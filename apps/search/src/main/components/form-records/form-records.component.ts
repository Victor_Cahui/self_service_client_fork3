import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from '@mx-search/services/general/general.service';
import { IRecordFilter } from '@mx-search/statemanagement/models/record.interface';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { BaseFormQuery } from '../shared/base-form-query';

@Component({
  selector: 'search-form-records',
  templateUrl: './form-records.component.html',
  providers: [PlatformService]
})
export class FormRecordsComponent extends BaseFormQuery implements OnInit {
  @Output() search: EventEmitter<IRecordFilter>;
  @Output() clear: EventEmitter<any>;

  constructor(
    protected fb: FormBuilder,
    protected generalService: GeneralService,
    protected router: Router,
    private readonly activatedRoute: ActivatedRoute,
    protected platformService: PlatformService
  ) {
    super(fb, generalService, router, platformService);
    this.search = new EventEmitter<IRecordFilter>();
    this.clear = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.initView();
  }

  onSubmit(): void {
    if (this.form.valid) {
      const params = this.form.value;
      this.changingQueryParams(params);
      this.search.next(this.form.value);
    }
  }

  changingQueryParams(params: any): void {
    params.numberRecord = params.numberRecord.replace(/\//g, '_');

    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params,
      queryParamsHandling: 'merge'
    });
  }

  getDefaultData(): void {
    let numberRecord: string = this.activatedRoute.snapshot.queryParamMap.get('numberRecord') || '';
    let typeDocumentQuery: string = this.activatedRoute.snapshot.queryParamMap.get('typeDocument') || '';
    let numberDocumentQuery: string = this.activatedRoute.snapshot.queryParamMap.get('numberDocument') || '';

    numberRecord = numberRecord.replace(/_/g, '/');

    if (typeDocumentQuery && this.arrTypesDocuments.length) {
      const typedocument = this.getTypeDocumentByCode(typeDocumentQuery.toUpperCase());
      numberDocumentQuery = typedocument ? numberDocumentQuery : '';
      typeDocumentQuery = typedocument ? typeDocumentQuery.toUpperCase() : '';
    } else {
      typeDocumentQuery = '';
      numberDocumentQuery = '';
    }

    if (this.arrTypesDocuments && this.arrTypesDocuments.length) {
      this.setValues(numberRecord, typeDocumentQuery, numberDocumentQuery);
    }

    const search = {
      numberRecord: numberRecord.replace(/\//g, '_'),
      typeDocument: typeDocumentQuery,
      numberDocument: numberDocumentQuery
    };
    if (this.form.valid) {
      this.search.next(search);
    }
  }

  nextClear(): void {
    this.clear.next();
  }
}
