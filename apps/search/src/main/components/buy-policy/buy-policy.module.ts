import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonModule } from '@mx/core/ui';
import { BuyPolicyComponent } from './buy-policy.component';

@NgModule({
  imports: [CommonModule, MfButtonModule],
  declarations: [BuyPolicyComponent],
  exports: [BuyPolicyComponent]
})
export class BuyPolicyModule {}
