import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GeneralService } from '@mx-search/services/general/general.service';
import { RecordLang } from '@mx-search/settings/lang/record.lang';
import { IRecordResponse, RecordState } from '@mx-search/statemanagement/models/record.interface';

@Component({
  selector: 'search-record-detail',
  templateUrl: './record-detail.component.html'
})
export class RecordDetailComponent implements OnInit {
  @Input() record: IRecordResponse;
  @Input() showSpinner: boolean;
  @Output() download: EventEmitter<any>;

  lang = RecordLang;
  recordState = RecordState;
  emailcontact: string;

  constructor(private readonly generalService: GeneralService) {
    this.download = new EventEmitter<any>();
    this.emailcontact = this.generalService.getEmailActivePolicy();
  }

  ngOnInit(): void {}

  downloadRecord(): void {
    if (!this.showSpinner) {
      this.download.next();
    }
  }
}
