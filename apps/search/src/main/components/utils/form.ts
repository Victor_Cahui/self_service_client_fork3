import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { TYPE_DATA_NUMERIC, TYPE_DOCUMENT_RUC } from '@mx-search/settings/constants/types-documents';
import { AllValidationErrors, FormGroupControls } from '@mx-search/statemanagement/models/form.interface';
import { TypeDocument } from '@mx-search/statemanagement/models/typedocument.interface';
import { AlphanumericValidator, NumberValidator, RUCValidator, SelectListItem } from '@mx/core/ui';
import { isEmpty } from 'lodash-es';

export class FormComponent {
  static getNameOfItem(lstItems: Array<SelectListItem>, itemId: string): string {
    const filtereItems = lstItems.filter(item => item.value === itemId);
    const name = !isEmpty(filtereItems) ? filtereItems[0].text : '';

    return name;
  }
  static setControl(control: AbstractControl, value: any): void {
    if (!!value) {
      control.setValue(value);
    } else {
      control.setValue('');
      control.markAsPristine();
      control.markAsUntouched();
    }
  }
  static getMaxMinLength(type: string, list: any, value: any): string {
    const item = list.find(l => {
      return l.codigo === value;
    });

    if (type === 'maxlength') {
      return item.longitudMaxima;
    } else if (type === 'minlength') {
      return item.longitudMinima;
    }
  }

  static hasNumericValidator(list: Array<TypeDocument>, value: any): boolean {
    const item = list.find(l => {
      return l.codigo === value;
    });

    return TYPE_DATA_NUMERIC.toLowerCase() === item.tipoDato.toLowerCase();
  }

  static showBasicErrors(form: FormGroup, control: string): boolean {
    return (form.controls[control].dirty || form.controls[control].touched) && !isEmpty(form.controls[control].errors);
  }

  static getFormValidationErrors(controls: FormGroupControls): Array<AllValidationErrors> {
    let errors: Array<AllValidationErrors> = [];
    Object.keys(controls).forEach(key => {
      const control = controls[key];
      if (control instanceof FormGroup) {
        errors = errors.concat(FormComponent.getFormValidationErrors(control.controls));
      }
      const controlErrors: ValidationErrors = controls[key].errors;
      if (controlErrors !== null) {
        Object.keys(controlErrors).forEach(keyError => {
          errors.push({
            control_name: key,
            error_name: keyError,
            error_value: controlErrors[keyError]
          });
        });
      }
    });

    return errors;
  }

  static validateAllFormFields(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        FormComponent.validateAllFormFields(control);
      }
    });
  }

  static getValidatorTypeDocument(
    typeDocument: string,
    typesDocumentValidators: Array<TypeDocument>
  ): Array<ValidatorFn> {
    const validators: Array<ValidatorFn> = [];
    const isNumeric = FormComponent.hasNumericValidator(typesDocumentValidators, typeDocument);
    const maxlength = FormComponent.getMaxMinLength('maxlength', typesDocumentValidators, typeDocument);
    const minlength = FormComponent.getMaxMinLength('minlength', typesDocumentValidators, typeDocument);

    validators.push(Validators.minLength(Number(minlength)));
    validators.push(Validators.maxLength(Number(maxlength)));

    if (typeDocument === TYPE_DOCUMENT_RUC) {
      validators.push(RUCValidator.isValid());
    }

    validators.push(isNumeric ? NumberValidator.isValid : AlphanumericValidator.isValid);
    validators.push(Validators.required);

    return validators;
  }
}
