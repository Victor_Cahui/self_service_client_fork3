import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '@mx/core/ui';
import { InputSearchComponent } from './input-search.component';

@NgModule({
  imports: [CommonModule, FormsModule, DirectivesModule],
  declarations: [InputSearchComponent],
  exports: [InputSearchComponent]
})
export class InputSearchModule {}
