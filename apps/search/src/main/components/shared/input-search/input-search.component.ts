import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  Renderer2
} from '@angular/core';

@Component({
  selector: 'search-input-search',
  template: `
    <div class="d-flex g-b--1 g-bc--gray11 p-2 g-b-radius--5" [ngClass]="margin ? ['m-' + margin] : []">
      <span class="icon-mapfre_099_search g-font--21 pr-2 g-c--gray9"></span>
      <input
        type="text"
        value=""
        class="no-spinners w-10 text-truncate"
        [placeholder]="placeholder"
        [(ngModel)]="searchText"
        [maxlength]="maxLength"
        [Number]="numeric"
        [Alphanumeric]="alphanumeric"
      />
      <div class="g-ml--5" [ngClass]="{ 'g-none': (searchText || '').length === 0 }">
        <span class="icon-mapfre_121_close g-font--21 g-c--gray9 g-c--pointer" (click)="searchText = ''"></span>
      </div>
    </div>
  `
})
export class InputSearchComponent implements AfterViewInit {
  @HostBinding('class') class = `g-px-lg--0`;
  @Output() search: EventEmitter<string>;
  @Output() cancel: EventEmitter<any>;
  @Input() placeholder = 'Buscar';
  @Input() width = 35;
  @Input() full: boolean;
  @Input() margin: number;
  @Input() maxLength: string;
  @Input() alphanumeric: boolean;
  @Input() numeric: boolean;
  @Input()
  get searchText(): string {
    return this._searchText;
  }

  set searchText(searchText: string) {
    this._searchText = searchText;
    if (this._searchText) {
      this.search.next(this._searchText);
    } else {
      this.cancel.next();
    }
  }

  private _searchText: string;

  constructor(private readonly renderer2: Renderer2, private readonly elementRef: ElementRef) {
    this.search = new EventEmitter<string>();
    this.cancel = new EventEmitter<any>();
  }

  ngAfterViewInit(): void {
    if (!this.full) {
      this.renderer2.addClass(this.elementRef.nativeElement, `w-px-lg--${this.width}`);
    }
    this.renderer2.addClass(this.elementRef.nativeElement, `w-10`);
  }

  clearText(): void {
    this.searchText = '';
  }
}
