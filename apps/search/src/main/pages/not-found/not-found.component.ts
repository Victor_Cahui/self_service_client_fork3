import { Component, OnInit } from '@angular/core';
import { GeneralLang } from '../../../settings/lang/general.lang';

@Component({
  selector: 'search-not-found-component',
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent implements OnInit {
  title404 = GeneralLang.Titles.Title404;
  message404 = GeneralLang.Messages.Message404;

  ngOnInit(): void {}
}
