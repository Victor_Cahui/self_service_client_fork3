import { GeneralSearchRequest } from './general.models';

export interface IProcessOngoingRequest extends GeneralSearchRequest {
  codigoApp: string;
  tipoTramite: string;
  tipoSeguro?: string;
  registros?: number;
  orden?: string;
}

export interface IProcessOngoingResponse {
  identificador: string;
  tipoPoliza: string;
  fechaInicio: string;
  horaInicio: string;
  hexadecimalEstadoTramite: string;
  descripcionEstadoTramite: string;
  descripcionBeneficiario: string;
  descripcionTramite: string;
}
