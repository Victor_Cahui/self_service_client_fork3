export interface INotificationRequest {
  codigoApp: string;
  tipoNotificacion: string;
}
export interface INotificationHome {
  accion: string;
  codigo: number;
  descripcion: string;
  objeto: IObjNotification;
  tipoNotificacion: string;
}
export interface IObjNotification {
  descripcion: string;
  fecha: string;
}
