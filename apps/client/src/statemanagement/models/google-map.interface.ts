import { LatLngBounds } from '@agm/core';
import { MapTypeStyle } from '@agm/core/services/google-maps-types';

export interface ICoords {
  latitude?: number;
  longitude?: number;
}

export interface IAbrevCoords {
  lat?: number;
  lng?: number;
}

export interface IGoogleOptions extends ICoords {
  zoom?: number;
  minZoom?: number;
  disableDefaultUI?: boolean;
  zoomControl?: boolean;
  scrollwheel?: boolean;
  usePanning?: boolean;
  streetViewControl?: boolean;
  gestureHandling?: string;
  bounds?: LatLngBounds;
  styles?: Array<MapTypeStyle>;
  clickableIcons?: boolean;
}

export interface IItemMarker extends ICoords {
  id: string | number;
  pinUrl: string;
  openInfoWindow?: boolean;
  imageUrl: string;
  title: string;
  address: string;
  district: string;
  province: string;
  department: string;
  distance: number;
  phone: string;
  timetable: string;
  description?: string;
  isFavorite?: boolean;
  canScheduleAppointment: boolean;
  showFavoriteButton?: boolean;
  ranking?: number;
  payload?: any;
  citaCovid?: boolean;
}

export interface IUserMarker extends ICoords {
  pinUrl?: string;
}

export interface IFilterNotificacion {
  showNotification?: boolean;
  textNotification?: string;
  iconNotification?: string;
}

export interface IViewOnMapOptions {
  labelLink: string;
  urlLink: string;
  coverages: Array<string>;
  hidenLinkMap: boolean;
}

export interface ISearchPlace {
  coords?: ICoords;
  address?: string;
  addressComplete?: IAddress;
  valid?: boolean;
}

export interface IAddress {
  district?: string;
  province?: string;
  departament?: string;
  streetNumber?: number;
  postalCode?: number;
  refOne?: string;
  refTwo?: string;
}

export interface GoogleResultAutocomplete {
  description: string;
  id: string;
  matched_substrings: Array<Matchedsubstring>;
  place_id: string;
  reference: string;
  structured_formatting: Structuredformatting;
  terms: Array<Term>;
  types: Array<string>;
  active?: boolean;
}

interface Term {
  offset: number;
  value: string;
}

interface Structuredformatting {
  main_text: string;
  main_text_matched_substrings: Array<Matchedsubstring>;
  secondary_text: string;
}

interface Matchedsubstring {
  length: number;
  offset: number;
}
