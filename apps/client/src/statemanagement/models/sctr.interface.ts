import { ISctrPeriodResponse } from '@mx/statemanagement/models/policy.interface';
import { IPaginatorBaseRequest, IPaginatorBaseResponse } from './general.models';

export interface IRecordBaseRequest {
  codigoApp: string;
  numeroConstancia: string;
}

export interface IInsuredByRecordRequest extends IPaginatorBaseRequest, IRecordBaseRequest {
  nombreODocumento: string;
}

export interface IRecordDownloadRequest extends IRecordBaseRequest {
  numeroPoliza: string;
}

export interface IRecordsRequest {
  codigoApp: string;
  numeroPoliza: string;
  registros?: number;
  pagina?: number;
  numeroConstancia?: string;
  tipoDocumento?: string;
  documento?: string;
  fechaInicioVigencia?: string;
  fechaFinVigencia?: string;
  tipoMovimiento?: string;
}

export interface IRecordsPaginatorResponse extends IPaginatorBaseResponse {
  constancias: Array<IRecordResponse>;
}

export interface IInsuredsRecordPaginatorResponse extends IPaginatorBaseResponse {
  asegurados: Array<IInsuredRecordResponse>;
}

export interface IInsuredRecordResponse {
  apellidoMaterno: string;
  apellidoPaterno: string;
  codigoMonedaSueldo: number;
  fechaNacimiento: string;
  nombres: string;
  nombreCompleto: string;
  numeroDocumento: string;
  sueldo: number;
  tipoDocumento: string;
}

export interface IRecordResponse {
  numeroConstancia: string;
  tipoMovimiento: string;
  cantidadAsegurados: number;
  descripcionRamo: string;
  fechaEmision: string;
  fechaInicioVigencia: string;
  fechaFinVigencia: string;
}

export interface ICardPeriodPreview {
  month: string;
  year: string;
  periodStatus: string;
  workersQty: string;
  situationType: string;
  situationTypeRed: boolean;
  trafficLight: string;
  insuredGroup: string;
  dateStart: string;
  dateEnd: string;
  policies: Array<IPeriodDetails>;
  hasDeclaration: boolean;
  hasInclusion: boolean;
  canDeclare: boolean;
  canBreakdown: boolean;
  collapse: boolean;
  fullPeriodDate: string;
  payload: ISctrPeriodResponse;
}

export interface IPeriodDetails {
  policyType: string;
  policyDescription: string;
  customIcon: string;
  periodStatus: string;
  workersQty: string;
  hasOptions: boolean;
}

export interface IPeriodRiskItem {
  numRiesgo: number;
  descRiesgo: string;
}

export interface ICardPeriodRisks {
  riesgoSalud: Array<IPeriodRiskItem>;
  riesgoPension: Array<IPeriodRiskItem>;
}

export interface IOperationSummaryInfo {
  informacionOperacion: { centroCosto: string; centroTrabajo: string };
}

export interface IOperationSummaryRisk {
  number: string;
  categry: string;
  workersQty: string;
  payroll: string;
  bonus: string;
}

export interface IOperationSummaryRiskDetail {
  title: string;
  topSalary: string;
  showTopSalary: boolean;
  vigency: string;
  totalWorkers: string;
  totalPayroll: string;
  totalBonus: string;
  riskList: Array<IOperationSummaryRisk>;
}

export interface ICardOperationSummary {
  info: IOperationSummaryInfo;
  riskDetailList: Array<IOperationSummaryRiskDetail>;
  pendingPayroll?: {
    billingPeriod: string;
    pendingPayrollList: Array<IOperationSummaryRiskDetail>;
  };
}
