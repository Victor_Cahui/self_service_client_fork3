import { IClientInformationResponse } from './client.models';

export interface IProfileBasicInfo {
  fullName?: string;
  typeDocument?: string;
  document?: string;
  phone?: string;
  birthDay?: string;
  registrationDate?: string;
  email?: string;
  payload?: IClientInformationResponse;
  isEnterprise?: boolean;
}

export interface IJobBasicInfo {
  companyName?: string;
  companyAddress?: string;
  workstation?: string;
  admisionDate?: Date;
  oldWorking?: string;
  salaryRangeDescription?: string;
  phone?: string;
}
