import { TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { ACTION_SERVICE_DIGITAL_CLINIC } from '@mx/settings/constants/general-values';
import { GeneralRequest, GeneralSearchRequest } from './general.models';
import { IBeneficiary } from './policy.interface';

export interface IClinicsRequest extends GeneralSearchRequest, GeneralRequest {
  numeroPoliza: string;
  latitud?: any;
  longitud?: any;
  nombre?: string;
  descripcionUbigeo?: string;
  tipoCobertura?: string;
  tiposEstablecimiento?: string;
  departamentoId?: string;
  provinciaId?: string;
  distritoId?: string;
}

export interface IClinicFavoriteRequest extends GeneralRequest {
  clinicaId: string;
}

export interface IClinicCoverageRequest extends GeneralRequest {
  clinicaId: string;
  numeroPoliza: string;
}

export interface IPoliciesByClinicRequest extends GeneralRequest {
  identificadorClinica: string;
}

export interface IClinicCoverageResponse {
  nombreCobertura: string;
  codigoTipoDeducible: string;
  textoDeducibleCorto: string;
  textoDeducibleLargo: string;
  codigoMonedaCopago: number;
  montoCopago: number;
  cobertura: string;
}

export interface IClinicResponse {
  clinicaId: string;
  codigoSeps: string;
  esFavorito: boolean;
  puedeAgendarCita: boolean;
  nombre: string;
  descripcion: string;
  paginaWeb: string;
  latitud: number;
  longitud: number;
  direccion: string;
  distritoId: string;
  distritoDescripcion: string;
  provinciaId: string;
  provinciaDescripcion: string;
  departamentoId: string;
  departamentoDescripcion: string;
  distancia: number;
  redCodigo: number;
  redDescripcion: string;
  beneficioId: string;
  beneficioDescripcion: string;
  beneficioCopago: number;
  beneficioCopagoModalidad: string;
  codigoMonedaBeneficioCopago: number;
  beneficioCopagoMonedaDescripcion: string;
  telefono: string;
  horarioAtencion: string;
  imagenPortraitUrl: string;
  imagenLandscapeUrl: string;
}

export interface IClinicCoverage {
  coverageName: string;
  deductible: string;
  coveragePercent: string;
  description: string;
}

export interface IRefundPolicyListResponse {
  codCia: number;
  codRamo: number;
  codModalidad: number;
  descripcionPoliza: string;
  numeroPoliza: string;
  tipoPoliza: string;
  beneficiarios: Array<IBeneficiary>;
}

export interface IGetPdfRefundFormatRequest {
  codigoApp: string;
  codCia: number;
  codRamo: number;
  codModalidad: number;
}

export interface IRefundBeneficTypeRequest extends GeneralRequest {
  codCia: number;
  codRamo: number;
  codModalidad: number;
}

export interface IRefundBeneficTypeResponse {
  codigoBeneficio: number;
  descripcionBeneficio: string;
}

export interface IGetProviderRequest {
  codigoApp: string;
  numeroRuc: number;
}

export interface IProviderResponse {
  numeroRuc: number;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombres: string;
}

export interface IPreRegisterRefund {
  numeroPreSolicitud: number;
  numeroRuc: number;
  fechaIncidente: string;
  fechaTratamiento: string;
  codigoBeneficio: number;
  montoReembolso: number;
  tipoDocumentoBeneficiario: string;
  numeroDocumentoBeneficiario: string;
  tipoPoliza: string;
  numeroPoliza: string;
}

export interface IUploadFileRefundFormData {
  numeroPreSolicitud: number;
  tipoArchivo?: IFileTypeRefund;
  numeroRuc?: number;
  numeroArchivo?: number;
}

export interface IRemoveFileRefund extends GeneralRequest {
  numeroArchivos: Array<number>;
}

export enum IFileTypeRefund {
  REQUEST_FORMAT = 'FORMATO_SOLICITUD',
  PAYMENT_VOUCHER = 'COMPROBANTE_PAGO',
  ATTACHMENTS = 'OTROS_ADJUNTOS'
}

export interface IRefundHealth {
  numberPreRequest?: number;
  hoursConfirm?: number;
  policy?: {
    policyNumber: string;
    codCia: number;
    codRamo: number;
    codModality: number;
    description: string;
    type: string;
  };
  beneficiary?: {
    documentType: string;
    document: string;
    name: string;
    type: string;
  };
  attention?: {
    ruc: number;
    social: string;
    verify: boolean;
    incidenceDate: string;
    treatmentDate: string;
    benefitTypeCode: number;
    benefitType: string;
    amount: number;
  };
  documents?: {
    formatRequest?: Array<IRefundImageStorage>;
    payments?: Array<IRefundImageStorage>;
    attachments?: Array<IRefundImageStorage>;
  };
}

export interface ISearchProvider {
  ruc: number;
  social: string;
  verify: boolean;
}

export interface IRefundImageStorage {
  name: string;
  size: number;
  type: string;
  tmpKey: number;
  numberFile: number;
  ruc?: number;
  social?: string;
}

export interface IRefundRequestResponse {
  enCurso?: Array<IRefundRequestItemResponse>;
  historial?: Array<IRefundRequestItemResponse>;
}

export interface IRefundRequestItemResponse {
  fechaSolicitud: string;
  numeroSolicitud: number;
  nombreBeneficiario: string;
  nombreBeneficio: string;
  descripcion: string;
  estadoActual: string;
  montoReembolsoAprobado: number;
  codigoMoneda: number;
  estadosReembolso?: Array<{
    estado: string;
    descripcion: string;
    fecha: string;
  }>;
}

export interface IRefundDetailRequest {
  codigoApp: string;
  numeroSolicitud: number;
}

export interface IRefundDetailResponse {
  tipo: string;
  esProximo: boolean;
  fecha: string;
  informacion: {
    nombreBeneficiario: string;
    nombreBeneficio: string;
    telefonoContacto: string;
  };
  descargas: Array<{
    archivo: string;
  }>;
}

export interface IConfirmRefundRequest {
  numeroPreSolicitud: number;
}

export interface IDownLoadDocumentRequest {
  codigoApp: string;
  numeroSolicitud: number;
  identificadorDocumento: number;
}

export interface IDownLoadDocumentResponse {
  documento: string;
}

export interface IEpsPendingInvoicesResponse {
  nroFactura: string;
  nroPreFactura: string;
  codPlan: number;
  descPlan: string;
  fechaEmision: string;
  fechaExpiracion: string;
  codigoMoneda: number;
  montoTotal: number;
  semaforo: string;
  estado: string;
  tipoSituacion: string;
  urlIcono: string;
}

export interface IHomeDoctorRequestSummary {
  specialty: string;
  patient: string;
  telephone: string;
  address: string;
  referenceAddress: string;
  price: string;
  currencySymbol: string;
}

export interface IEpsPendingInvoicesView {
  invoiceNumber: string;
  preInvoiceNumber: string;
  planCode: number;
  planDescription: string;
  dateEmission: string;
  dateExpiration: string;
  currencyCode: number;
  totalAmount: number;
  totalAmountFormatted: string;
  trafficLightColor: TRAFFIC_LIGHT_COLOR;
  bulletColor: string;
  status: string;
  situationType: string;
  situationDescription: string;
  iconUrl: string;
  payload: IEpsPendingInvoicesResponse;
}

export interface IServiceDigitalClinic {
  id: string;
  key: string;
  description: string;
  icon: string;
  route: string;
  isNew: boolean;
  action: ACTION_SERVICE_DIGITAL_CLINIC;
}
