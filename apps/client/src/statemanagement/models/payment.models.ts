import { PaymentMode } from '@mx/components/shared/utils/payment';
import {
  COVERAGE_STATUS,
  POLICY_QUOTA_TYPE,
  SITUATION_TYPE,
  TRAFFIC_LIGHT_COLOR
} from '@mx/components/shared/utils/policy';
import { EPaymentProcess } from '@mx/settings/constants/general-values';
import { IContractor } from './client.models';
import { GeneralRequest, GeneralSearchRequest } from './general.models';
import { IBeneficiary, IContractingSoat } from './policy.interface';
import { IEmitPaymentResponse, IInspection, IInspectionRequestResponse, IVehicleQuoteSoat } from './vehicle.interface';

// Tipos de Pagos
export interface IPaymentTypeResponse {
  tiposPagosId: number;
  tiposPagosLlave: string;
  tiposPagosDescripcion: string;
}

// Pagos Pendientes
export interface IPaymentRequest extends GeneralSearchRequest {
  codigoApp: string;
  estadoPago?: string;
  tipoSeguro?: string;
  tipoProducto?: string;
  pSeccion?: string;
}

export interface IGeneralPaymentResponse {
  beneficiarios?: Array<IBeneficiary>;
  codCia: number;
  codModalidad: string;
  codRamo: number;
  codigoMoneda: number;
  cuota: string;
  descripcionPoliza: string;
  fechaEmision?: string;
  fechaExpiracion?: string;
  montoTotal?: number;
  numRecibo?: string;
  numeroAviso: string;
  numeroPoliza: string;
  tieneCobertura?: boolean;
  tipoDocumentoPago: POLICY_QUOTA_TYPE;
  tipoPoliza?: string;
  tipoSituacion: SITUATION_TYPE;
}

export interface IPaymentResponse extends IGeneralPaymentResponse {
  estado?: string;
  estadoCobertura?: COVERAGE_STATUS;
  fechaDebitoRenovacion?: string; // stay
  listaRecibosAsociados: Array<IAssociatedReceipt>;
  semaforo?: TRAFFIC_LIGHT_COLOR; // stay
  codigoEstado?: string; // ?
  descripcionEstado?: string; // ?
  fechaLimite?: string; // ?
  numeroBeneficiario?: string; // ?
  datosPoliza?: IGeneralPolicyData;
  iconoRamo?: string;
  nroPreFactura?: string;
  glosa?: string;
  tienePagoPendiente: boolean;
  puedeHacerDebitoAutomatico: boolean;
}

// inicio de interfaces para listas

export interface IPaymentInsured {
  tipoDocumento: string;
  documento: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombre: string;
  sexo: string;
  relacionId: number;
  relacionDescripcion: string;
  tipoAseguradoId: number;
  tipoAseguradoDescripcion: string;
  beneficio: string;
  sueldo: string;
  fechaNacimiento: string;
  telefono: string;
  correoElectronico: string;
}

export interface IPaymentBeneficiary {
  nombreBeneficiario: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  tipoDocumento: string;
  documento: string;
}

export interface IPaymentConstancy {
  numeroConstancia: string;
  tipoMovimiento: string;
  cantidadAsegurados: number;
  fechaEmision: string;
  fechaInicioVigencia: string;
  fechaFinVigencia: string;
}

export interface IPaymentRisk {
  marca: string;
  modelo: string;
  anio: string;
  descripcion: string;
  direccionHogar: string;
  departamento: string;
  provincia: string;
  distrito: string;
}

// fin de interfaces para listas

export interface IGeneralPolicyData {
  descripcionPoliza: string;
  listaAsegurados: Array<IPaymentInsured>;
  listaBeneficiarios: Array<IPaymentBeneficiary>;
  listaConstancias: Array<IPaymentConstancy>;
  listaRiesgos: Array<IPaymentRisk>;
  numeroPoliza: string;
  tipoPoliza: string;
}

export interface IAssociatedReceipt {
  descripcionPoliza: string;
  tipoPoliza: string;
  numeroPoliza: string;
  montoTotal: number;
  codigoMoneda: number;
  datosPoliza: IGeneralPolicyData;
  totalAmount?: string;
}

export interface IPaymentsGroupByMonth {
  titleMonth: string;
  subtitleMonth?: string;
  paymentResume: string;
  payments: Array<IPaymentResponse>;
  year?: number;
  collapse: boolean;
}

// Recibos de Pagos
export interface IReceiptRequest extends GeneralSearchRequest {
  codigoApp: string;
  tipoSeguro?: string;
}

export interface IReceiptResponse {
  codCia: number;
  codRamo: number;
  codModalidad: string;
  descripcionPoliza: string;
  tipoPoliza?: string;
  numeroPoliza: string;
  numRecibo: string;
  cuota: string;
  montoTotal: number;
  codigoMoneda: number;
  monedaDescripcion: string;
  monedaSimbolo: string;
  tiposPagosId: string;
  tiposPagosLlave: string;
  fechaPago: string;
  fechaExpiracion: string;
  fechaLimite: string;
  tieneCobertura: boolean;
  beneficiarios?: Array<IBeneficiary>;
}

export interface IGroupReceiptMonth {
  customIcon?: string;
  periodo: string;
  periodoDesc: string;
  cantidadPagos: number;
  montoSoles: number;
  montoDolares: number;
  subtitle?: string;
  textRight?: string;
  collapse?: boolean;
  payments?: Array<IMonthReceipt>;
  loading?: boolean;
  isNotice?: boolean;
  isInvoice: boolean;
  associatedReceipts: Array<IAssociatedReceipt>;
  paymentReceiptList: Array<IPaymentItemDetailView>;
}

export interface IGroupReceiptResponse {
  lista: Array<IGroupReceiptMonth>;
  flagVerMas: boolean;
}

export interface IMonthReceipt {
  customIcon?: string;
  codCia: number;
  codRamo: number;
  codModalidad: number;
  descripcionPoliza: string;
  tipoPoliza: string;
  numeroPoliza: string;
  numRecibo: string;
  cuota: string;
  montoTotal: number;
  codigoMoneda: number;
  tiposPagosLlave: string;
  tiposPagosId: string;
  fechaPago: string;
  fechaExpiracion: string;
  tieneCobertura: string;
  beneficiarios: Array<IBeneficiary>;
  periodo: string;
  totalAmount?: string;
  tiposPagosNombre?: string;
  isNotice: boolean;
  isInvoice: boolean;
  numeroAviso?: string;
  collapse?: boolean;
  associatedReceipts?: Array<IAssociatedReceipt>;
  paymentReceiptList?: Array<IPaymentItemDetailView>;
  paymentNumberLabels: Array<string>;
  paymentDetailLabel: string;
}

export interface IReceiptView extends IReceiptResponse {
  codigoEstado?: string;
  descripcionEstado?: string;
  tiposPagosNombre?: string;
  anio?: string;
  totalAmount?: string;
}

export interface ISendReceiptRequest {
  codigoApp: string;
  numeroRecibo: string;
}

export interface ICardPaymentPendingView {
  paymentLabel: string;
  tooltipLabel: string;
  policy: string;
  policyNumber: string;
  billNumber: string;
  policyStatusLabel: string;
  expired: boolean;
  expiredDate: string;
  totalAmount: string;
  quota: string;
  bulletColor?: string;
  semaforo?: TRAFFIC_LIGHT_COLOR;
  coverageStatus?: string;
  isWithoutCoverage: boolean;
  showQuota?: boolean;
  isNotice: boolean;
  isInvoice: boolean;
  noticeNumber: string;
  associatedReceipts: Array<IAssociatedReceipt>;
  response: IPaymentResponse;
}

export interface ICardPaymentGroupByMonthView {
  titleMonth: string;
  paymentResume: string;
  payments: Array<ICardPaymentDetailView>;
  year?: number;
  collapse: false;
}

export interface ICardPaymentDetailView {
  customIcon?: string;
  beneficiaries?: Array<{ fullName: string }>;
  billNumber: string;
  bulletColor?: string;
  coverage?: boolean;
  coverageStatus?: string;
  isWithoutCoverage: boolean;
  currencySymbol: string;
  deadline: string;
  debitRenovation?: any;
  expired: boolean;
  startDate: string;
  expiredDate: string;
  month: string;
  policy: string;
  policyNumber: string;
  policyStatus?: string;
  policyStatusLabel: string;
  policyType: string;
  quota: string;
  showQuota?: boolean;
  response: IPaymentResponse;
  semaforo?: string;
  total: number;
  totalAmount: string;
  isNotice: boolean;
  isInvoice: boolean;
  invoiceGloss: string;
  noticeNumber: string;
  associatedReceipts: Array<IAssociatedReceipt>;
  policyData: Array<IGeneralPolicyData>;
  collapse?: boolean;
  paymentReceiptList: Array<IPaymentItemDetailView>;
  paymentNumberLabels: Array<string>;
  paymentDetailLabel: string;
  hasPendingPayments?: boolean;
  canMakeAutomaticPayment?: boolean;
}

export interface IPaymentItemDetailView {
  tipoPoliza: string;
  customIcon: string;
  descripcionPoliza: string;
  descripcionPolizaDetalle?: string;
  numeroPoliza: string;
  totalAmount: string;
  listaAsegurados?: Array<IPaymentInsured>;
  listaRiesgos?: Array<IPaymentRisk>;
  listaConstancias?: Array<IPaymentConstancy>;
  listaBeneficiarios?: Array<IPaymentBeneficiary>;
  isInvoice: boolean;
}

export interface IPaymentConfigurationResponse extends IPaymentResponse {
  pago: IPaymentTypeResponse;
  fechaRenovacion: string;
  tipoRenovacionId: number;
  tipoRenovacionDescripcion: string;
  prima: number;
  usarMapfreDolares: boolean;
  unBeneficiario: boolean;
  puedeCambiarTipoRenovacion: boolean;
  puedeDesafiliarDebito: boolean;
  puedeHacerCargoCuenta: boolean;
  puedeHacerDebitoAutomatico: boolean;
  puedeHacerPagoManual: boolean;
  puedeRefinanciar: boolean;
  puedeUsarMapfreDolaresRenovacion: boolean;
  tieneTipoPago: boolean;
  fechaFinVigencia: string;
  fechaInicioVigencia: string;
  tarjeta: ITarjeta;
}

// Cotizacion SOAT
export interface ISoatQuoteRequest extends GeneralRequest {
  numeroPoliza: string;
}

export interface ISoatQuoteResponse {
  datosVehiculo: {
    fechaActual: string;
    tieneDatosCompletos: boolean;
    vehiculo: IVehicleQuoteSoat;
    soat: {
      numeroPoliza: string;
      numeroDocumento: number;
      numeroCotizacion: string;
      fechaInicioVigencia: string;
      fechaFinVigencia: string;
      fechaFormateada: string;
      diponibleCompraSoat: boolean;
      motivoNoDisponibleCompra: string;
      codigoModalidad: number;
    };
  };
  contratante: IContractingSoat;
  cotizacionConMPD: IQuote;
  cotizacionSinMPD: IQuote;
}

export interface IQuote {
  monto: number;
  montoNeto: number;
  montoFinal: number;
  montoNetoFinal: number;
  codigoMoneda: number;
  codigoModalidad: string;
  codigoZona: string;
  numeroCotizacion: string;
  usoMapfreDolares: boolean;
  montoMapfreDolaresUsado: number;
  montoMapfreDolaresRestantes: number;
  tasaCambio: number;
  montoAhorro: number;
  codigoMonedaAhorro: number;
}

// Emision SOAT
export interface ISoatIssuanceRequest extends GeneralRequest {
  DatosEmisionSOAT?: IDataPayment;
}

export interface ISoatIssuanceResponse {
  numeroRecibo?: string;
}

export interface IDataPayment {
  vehiculo: IVehicleQuoteSoat;
  contratante: IContractingSoat;
  cotizacion: IQuote;
  vigencia: {
    fechaInicio: string;
    fechaFin: string;
  };
  pago: {
    tipoPago: string;
    monto: number;
    tipoMoneda: number;
  };
  tarjeta?: ICardPaymentRequest;
}

export interface ICardPaymentRequest {
  numero: string;
  nombre: string;
  cvv: string;
  correoElectronico: string;
  mesVencimiento: string;
  anioVencimiento: string;
  tipo: string;
  token?: string;
}

// View Cotizacion SOAT
export interface IPaymentQuoteCard {
  typePayment?: EPaymentProcess;
  policy?: string;
  policyNumber?: string;
  policyType?: string;
  billNumber?: string;
  currencySymbol?: string;
  currency?: number;
  total?: number;
  comesSoat?: boolean;
  withMPD?: boolean;
  totalAmount?: string;
  quota?: string;
  documentType?: string;
  document?: string;
  email?: string;
  paymentMode?: string;
  startDate?: string;
  expiredDate?: string;
  endDate?: string;
  confirm?: boolean;
  dataQuote?: ISoatQuoteResponse;
  dataQuoteVehicle?: IPolicyQuoteResponse;
  modalitySelected?: {
    company?: number;
    branch?: number;
    modality?: string;
    quoteNumber?: number;
    textPaymentFrecuency?: string;
    fractionationCode?: string;
    quotas?: number;
    years?: number;
  };
  contractor?: IContractor;
  inspection?: IInspection;
  emitResponse?: IEmitPaymentResponse;
  inspectionResponse?: IInspectionRequestResponse;
  isNotice?: boolean;
  noticeNumber?: string;
  associatedReceipts?: Array<IAssociatedReceipt>;
  response?: any;
  confirmFormData?: any;
  canMakeAutomaticPayment?: boolean;
  hasPendingPayments?: boolean;
}

// Card lateral pagos
export interface IPaymentQuote {
  year: string;
  mapfreDolars: string;
  discount: string;
  finalPrice: string;
  quota?: string;
}

export interface IPaymentMethod {
  paymentMode: PaymentMode;
  cardExpiredMonth?: string;
  cardExpiredYear?: string;
  cardNumber?: string;
  cardType?: string;
  cvv?: string;
  email?: string;
  ownerName?: string;
  cardToken?: string;
}

export interface IReceiptPayRequest {
  modalidadPago: PaymentMode;
  tarjeta: ICardPaymentRequest;
  recibo: IReceiptResponse;
}

export interface IPaymentRenewalRequest {
  numeroPoliza: string;
  tipoRenovacionId: number;
  usarMapfreDolares: boolean;
}

export interface IPaymentRefinancingRequest {
  codigoApp: string;
  numeroPoliza: string;
}

export interface IPaymentRefinancingResponse {
  fechaUltimoRefinanciamiento: string;
  enProcesoRefinanciamiento: boolean;
  montoRefinanciar: number;
  codigoMonedaMontoRefinanciar: number;
  cuotaVigente: ICurrentQuotaResponse;
  cuotasRefinanciamiento: Array<IRefinancingQuotaResponse>;
}

export interface ICurrentQuotaResponse {
  tipoCuota: string;
  cuota: number;
  tieneInteres: boolean;
  interes: number;
  montoCuota: number;
  codigoMonedaMonto: number;
  descripcionFrecuenciaPago: string;
  cuotasPagadas: number;
  cuotasFaltantes: number;
  montoCuotasFaltantes: number;
  codigoMonedaMontoFaltante: number;
}

export interface IRefinancingQuotaResponse {
  tipoCuotaNueva: string;
  tieneInteres: boolean;
  interes: number;
  montoCuota: number;
  codigoMonedaMonto: number;
  descripcionFrecuenciaPago: string;
  permiteRefinanciar: boolean;
}

export interface IPaymentRefinancingRequestRequest {
  cuotaActual: number;
  montoRefinanciar: number;
  codigoMonedaMontoRefinanciar: number;
  tipoCuotaNueva: string;
  tieneInteres: boolean;
  interes: number;
  montoCuota: number;
  codigoMonedaMonto: number;
}

// Datos para compra
export interface IPolicyQuoteResponse {
  codigoCompania: number;
  codigoRamo: number;
  codigoModalidad: string;
  cotizacionConMPD: IQuoteWithQuota;
  cotizacionSinMPD: IQuoteWithQuota;
}

export interface IQuoteWithQuota extends IQuote {
  montoCuota: number;
  montoCuotaNeto: number;
  // tslint:disable-next-line: max-file-line-count
}

// EPS Card pagos - Concepto
export interface IEpsPaymentConceptItem {
  codConcepto: string;
  descConcepto: string;
  codigoMoneda: number;
  monto: number;
  price?: string;
}

// EPS Card pagos - Concepto de pago
export interface IEpsPaymentConcept {
  codPlan: number;
  descPlan: string;
  conceptos: Array<IEpsPaymentConceptItem>;
}

// Tarjetas guardadas
export interface ITarjeta {
  numero: string;
  tipo?: string;
  tarjetaToken?: string;
  fechaVencimiento?: string;
}

export interface ItooltipPM {
  label?: string;
  link?: string;
  title?: string;
  text?: string;
  icon?: string;
}

// tslint:disable-next-line: max-file-line-count
