import { IGaPropertie } from '@mx/core/shared/helpers/util/google';

export interface ITabItem {
  name: string;
  route?: string; // ruta al seleccionar tab
  exact?: boolean;
  visible?: boolean;
  value?: any; // tab seleccionado en el mismo componente
  ga?: Array<IGaPropertie>; // configuración de ga
}
