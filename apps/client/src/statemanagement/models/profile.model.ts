export interface IProfileSensibleDataRequest {
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  nombre?: string;
  fechaNacimiento?: string;
  tipoDocumentoModificado?: string;
  numeroDocumentoModificado?: string;
}

export interface IProfileNonSensibleDataRequest {
  telefonoMovil: string;
}

export interface IContactsDataRequest {
  codigoApp: string;
}

export interface IContactsDataResponse {
  titulo: string;
  nombre: string;
  telefono: string;
  email: string;
}

export interface IProfessionsDataRequest {
  codigoApp: string;
}

export interface IProfessionsDataResponse {
  id: number;
  descripcion: string;
}
