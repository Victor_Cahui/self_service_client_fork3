import { GeneralRequest, GeneralSearchRequest } from './general.models';

export interface ISinistersRequest extends GeneralRequest, GeneralSearchRequest {
  tipo: string;
}

export interface ISinister {
  descripcionCausa: string;
  descripcionEstado: string;
  descripcionPoliza: string;
  descripcionRiesgo: string;
  fechaDeclaracion: string;
  fechaDenuncia: string;
  fechaOcurrencia: string;
  numeroPoliza: string;
  numeroSiniestro: number;
  tipoExpediente: string;
  tipoPoliza?: string;
  tipo?: string;
  labelTipoSinistro?: string;
  riskIcon?: string;
  hasDeail?: boolean;
}

export interface ISinistersResponse {
  siniestros: Array<ISinister>;
  totalPaginas: number;
  totalRegistros: number;
}
