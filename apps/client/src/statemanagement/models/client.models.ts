import { GeneralSearchRequest } from './general.models';

export interface IVerificacionDocumentoRequest {
  codigoApp: string;
  documento: string;
}

export interface IVerificacionDocumentoResponse {
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombres: string;
  nombreRapido: string;
  tipoUsuario: number;
  correoElectronico: string;
}

export interface IObtenerConfiguracionRequest {
  codigoApp: string;
  seccion?: string;
  esClinicaDigital?: string;
}

export interface IObtenerConfigurationCarouselResponse {
  rutaPagina?: string;
  listaBanners?: IObtenerConfiguracionAdvertisingResponse[];
}

export interface IObtenerConfiguracionAdvertisingResponse {
  mobile?: string;
  tablet?: string;
  desktop?: string;
  desktopFull?: string;
  enlaceUrl?: string;
}

export interface IObtenerConfiguracionActionResponse {
  llave: string;
  titulo: string;
}

export interface IObtenerConfiguracionLinkResponse {
  titulo: string;
  enlace: string;
}

export interface IUpdateDataWorkerRequest {
  lugarTrabajo: string;
  profesionId: number;
  profesionDescripcion: string;
  rangoSalarialId: number;
  identificadorDepartamento: number;
  identificadorProvincia: number;
  identificadorDistrito: number;
  direccion: string;
  telefono: string;
}

export interface IObtenerConfiguracionResponse {
  carruseles?: Array<IObtenerConfigurationCarouselResponse>;
  acciones: Array<IObtenerConfiguracionActionResponse>;
  enlaces: Array<IObtenerConfiguracionLinkResponse>;
}

export interface IClientAddressRequest {
  /**
   * Values: AUTOS; SALUD; VIDA; HOGAR; DECESOS; SOAT; SCTR
   */
  tipoPoliza: string;
}

export interface IClientCorrespondenceAddressRequest {
  tipoDocumento: string;
  documento: string;
}

export interface IClientAddressResponse {
  identificadorDireccion: number;
  codCia: number;
  codRamo: number;
  numeroPoliza: string;
  descripcionProducto: string;
  identificadorDepartamento: number;
  departamento: string;
  identificadorDistrito: number;
  distrito: string;
  identificadorProvincia: number;
  provincia: string;
  direccion: string;
  tipoPoliza?: string;
  todasLasPolizas?: boolean;
}

export interface IClientInformationRequest extends GeneralSearchRequest {
  tipoDocumento: string;
  documento: string;
}

export interface IClientJobInformationResponse {
  nombreEmpresa: string;
  fechaInicio: string;
  antiguedadTrabajo: string;
  direccionTrabajo: string;
  ingresosClienteId: number;
  ingresosClienteDescripcion: string;
  telefonoTrabajo: string;
  departamentoId: string;
  distritoId: string;
  provinciaId: string;
  departamentoDescripcion: string;
  distritoDescripcion: string;
  provinciaDescripcion: string;
  profesionDescripcion: string;
  profesionId: number;
}

export interface IClientInformationResponse {
  apellidoPaternoCliente: string;
  apellidoMaternoCliente: string;
  nombreCliente: string;
  aceptoTerminosPoliza?: boolean;
  nombreCompletoCliente: string;
  razonSocial?: string;
  emailCliente: string;
  tipoUsuario: string;
  enlaceTipoUsuario: string;
  telefonoFijo: string;
  telefonoMovil: string;
  fechaNacimiento: string;
  fechaAsegurado: string;
  tieneAgente?: boolean;
  trabajo: IClientJobInformationResponse;
  tipoDocumento?: string;
  relacionId?: number;
  documento?: string;
}

export interface IClientMapfreDolarResponse {
  mapfreDolares: number;
}

export interface IBodySendDataContact {
  telefono: string;
  rangoHorario: string;
  motivoContacto: string;
  submotivoContacto?: string;
}

export interface IAddress {
  departmentId: number;
  department: string;
  provinceId: number;
  province: string;
  districtId: number;
  district: string;
  address: string;
  reference?: string;
  useDefault?: boolean;
}

export interface IContractor extends IAddress {
  documentType: string;
  document: string;
  email: string;
  phone: string;
}

export interface IEpsContractCard {
  icon: string;
  title: string;
  number: string;
  vigencyStart: string;
  lastRenewal: string;
  vigencyEnd: string;
}

export interface IContractorCard {
  icon: string;
  title: string;
  contractor: string;
  document: string;
  phone: string;
  address: string;
}

// Contacto Referidos
export interface IReferredContactRequest {
  landingID: string;
  code: string;
}

export interface ICodeRequest {
  numeroDocumento: string;
  tipoDocumento: string;
  correo: string;
  telefono: string;
  codigoCompania: number;
  codigoRamo: number;
  codigoModalidad: string;
  numeroPoliza: string;
}

export interface IReferredContactResponse {
  codigo?: string;
  mensaje?: string;
}

export interface IContactData {
  company?: number;
  branch?: number;
  modality?: string;
  policyNumber?: string;
  subject?: string;
  description?: string;
  landingId?: string;
}
