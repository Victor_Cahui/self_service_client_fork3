import { appConstants, dev } from '@mx/core/shared/providers/constants';

export const envBase = {
  ACTIVATE_ENABLE_USER_LINK: true,
  ACTIVATE_FORGOT_PASSWORD_LINK: true,
  API_URL_AUTH: `${appConstants.apis.seguridad}/api/`,
  API_URL_CLIENT: `${appConstants.apis.autoservicios}/areaPrivada/`,
  API_URL_COMUN: `${appConstants.apis.comun}/`,
  OIM_LOGIN_URL: `https://oim.mapfre.com.pe/login/#/`,
  production: true,
  VERSION: dev.version
};
