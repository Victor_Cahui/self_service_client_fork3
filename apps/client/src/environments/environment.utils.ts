import { localStore } from '@mx/core/shared/helpers';
import { KEY_PERMISSIONS } from '@mx/settings/auth/auth-values';

const permissions = localStore().get(KEY_PERMISSIONS) || [];

export function decorateEnvironment(environment): void {
  permissions.forEach(p => {
    environment[p.codigo] = p.valor;
  });
}
