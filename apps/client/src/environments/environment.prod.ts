import { appConstants } from '@mx/core/shared/providers/constants';
import { envBase } from './environment.base';
import { decorateEnvironment } from './environment.utils';

export const environment: any = {
  ...envBase,
  WEB_OIM_URL: `https://oim.mapfre.com.pe`,
  lyraPublicKeySoles: '98895953:publickey_Bu6PFkXyf87o6ztffOK9U5gajHSl7j5c9VIe2uSnqQUb7',
  lyraPublicKeyDolares: '63105764:publickey_SoDg6bqz1IUJybqVPk3ixmfpS3NHa2g0KXXDbq52b6Mpo',
  urlWebSocketApi: `${appConstants.apis.autoservicios}/websockets`
};

decorateEnvironment(environment);
