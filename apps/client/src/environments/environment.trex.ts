import { appConstants } from '@mx/core/shared/providers/constants';
import { envBase } from './environment.base';
import { decorateEnvironment } from './environment.utils';

export const environment: any = {
  ...envBase,
  API_URL_COMUN: `${appConstants.apis.comun}/mapfrePeru/utilitarios/v2/`,
  WEB_OIM_URL: 'http://dev.oim.mapfre.com.pe:6777',
  lyraPublicKeySoles: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ',
  lyraPublicKeyDolares: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ',
  urlWebSocketApi: `${appConstants.apis.autoservicios}/websockets`
};

decorateEnvironment(environment);
