import { DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { GestureConfig } from '@angular/material';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CardPendingPaymentModule } from '@mx/components/payment/card-pending-payment/card-pending-payment.module';
import { DirectivesModule } from '@mx/core/shared/common/directives/directives.module';
import { HelperModule } from '@mx/core/shared/helpers/helper.module';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StateModule } from '@mx/core/shared/state';
import { MfLoaderModule, MfModalAlertModule } from '@mx/core/ui';
import { LoginLayoutModule, MainLayoutModule } from '@mx/layouts/modules';
import { RoutesModule } from '@mx/routes/routes.module';
import { ProvidersModule } from '../providers/providers.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CardPendingPaymentModule,
    HelperModule,
    LoginLayoutModule,
    MainLayoutModule,
    MfLoaderModule,
    MfModalAlertModule,
    ProvidersModule,
    RoutesModule,
    StateModule,
    DirectivesModule
  ],
  providers: [PlatformService, { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }, DatePipe],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: []
})
export class AppModule {}
