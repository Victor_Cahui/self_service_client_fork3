import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { SkipInsterceptorJsonHeader } from '@mx/core/shared/helpers/interceptors/content-type-json-interceptor';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { HealthEndpoint } from '@mx/endpoints/health.endpoint';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import {
  IConfirmRefundRequest,
  IDownLoadDocumentRequest,
  IDownLoadDocumentResponse,
  IGetPdfRefundFormatRequest,
  IGetProviderRequest,
  IPreRegisterRefund,
  IProviderResponse,
  IRefundBeneficTypeRequest,
  IRefundBeneficTypeResponse,
  IRefundDetailRequest,
  IRefundDetailResponse,
  IRefundHealth,
  IRefundPolicyListResponse,
  IRefundRequestResponse,
  IUploadFileRefundFormData
} from '@mx/statemanagement/models/health.interface';
import { Observable } from 'rxjs';

export enum KEY_STORAGE_HEALTH {
  REFUND = 'health_refund'
}

@Injectable({
  providedIn: 'root'
})
export class HealthRefundService {
  constructor(private readonly apiService: ApiService, private readonly storageService: StorageService) {}

  setRefund(refund: IRefundHealth): void {
    this.storageService.saveStorage(KEY_STORAGE_HEALTH.REFUND, refund);
  }

  getRefund(): IRefundHealth {
    return this.storageService.getStorage(KEY_STORAGE_HEALTH.REFUND);
  }

  validPolicyAndBeneficy(): boolean {
    const refund = this.getRefund();
    const hasPolicy = refund && refund.policy && refund.policy.policyNumber && refund.policy.type;
    const hasBenefy = refund && refund.beneficiary && refund.beneficiary.document && refund.beneficiary.documentType;

    return !!hasPolicy && !!hasBenefy;
  }

  validAttention(): boolean {
    const refund = this.getRefund();

    return !!(
      refund &&
      refund.attention &&
      refund.attention.amount &&
      refund.attention.benefitType &&
      refund.attention.incidenceDate &&
      refund.attention.treatmentDate &&
      refund.attention.social &&
      refund.attention.ruc
    );
  }

  validDocumentation(): boolean {
    const refund = this.getRefund();

    return !!(
      refund &&
      refund.documents &&
      refund.documents.formatRequest &&
      refund.documents.formatRequest.length &&
      refund.documents.payments &&
      refund.documents.payments.length
    );
  }

  validSummary(): boolean {
    const refund = this.getRefund();

    return !!(refund && refund.hoursConfirm);
  }

  getRefundPolicyList(params: GeneralRequest): Observable<Array<IRefundPolicyListResponse>> {
    return this.apiService.get(HealthEndpoint.RequestRefundPolicyList, { params, default: [] });
  }

  getPDFRefundFormat(params: IGetPdfRefundFormatRequest): Observable<{ base64: string }> {
    return this.apiService.get(HealthEndpoint.DownloadRefunsFormat, { params, default: {} });
  }

  getBenefitType(params: IRefundBeneficTypeRequest): Observable<Array<IRefundBeneficTypeResponse>> {
    return this.apiService.get(HealthEndpoint.BeneficType, { params, default: [] });
  }

  getProvider(params: IGetProviderRequest): Observable<IProviderResponse> {
    return this.apiService.get(HealthEndpoint.GetProvider, { params, default: {} });
  }

  preRegister(params: GeneralRequest, body: IPreRegisterRefund): Observable<{ numeroPreSolicitud: number }> {
    return this.apiService.post(HealthEndpoint.PreRegister, body, { params, default: {}, preloader: true });
  }

  addFile(params: GeneralRequest, body: IUploadFileRefundFormData, file: File): Observable<{ numeroArchivo: number }> {
    let headers = new HttpHeaders();
    headers = headers.set(SkipInsterceptorJsonHeader, '');

    return this.apiService.postDataAndFile(HealthEndpoint.AddFile, body, [{ name: 'archivo', native: file }], {
      params,
      default: {},
      headers
    });
  }

  removeFile(params: GeneralRequest, body: Array<number>): Observable<{ numeroArchivo: number }> {
    return this.apiService.post(HealthEndpoint.RemoveFile, body, { params, default: {} });
  }

  getRefundRequestList(params: GeneralRequest): Observable<IRefundRequestResponse> {
    return this.apiService.get(HealthEndpoint.RefundRequestList, { params, default: {} });
  }

  getRefundRequestDetail(params: IRefundDetailRequest): Observable<Array<IRefundDetailResponse>> {
    return this.apiService.get(HealthEndpoint.RefundRequestDetail, { params, default: [] });
  }

  getRefundPendingRequest(params: GeneralRequest): Observable<{ numeroSolicitudesPendientes: number }> {
    return this.apiService.get(HealthEndpoint.RefundPendingRequest, { params, default: {} });
  }

  confirmRefund(params: GeneralRequest, body: IConfirmRefundRequest): Observable<{ horasConfirmacion: number }> {
    return this.apiService.post(HealthEndpoint.ConfirmRefundRequest, body, { params, default: {}, preloader: true });
  }

  downLoadDocument(params: IDownLoadDocumentRequest): Observable<IDownLoadDocumentResponse> {
    return this.apiService.get(HealthEndpoint.DownLoadDocument, { params, default: {} });
  }
}
