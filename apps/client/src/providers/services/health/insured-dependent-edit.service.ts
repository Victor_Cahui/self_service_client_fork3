import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { IPolicyInsuredEditDataRequest, IPolicyInsuredResponse } from '@mx/statemanagement/models/policy.interface';
import { Relation } from '@mx/statemanagement/models/realtion.interface';
import { TypeDocument } from '@mx/statemanagement/models/typedocument.interface';

export interface IChangesDataInsured {
  key: string;
  currentValue: any;
  oldValue: any;
  label: string;
  isSensible?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class InsuredDependentEditService {
  originInsured: IPolicyInsuredResponse;
  buffInsured: IPolicyInsuredResponse;
  changesInInsured: Array<IChangesDataInsured>;
  changesObjectInsured: IPolicyInsuredEditDataRequest;
  relations: Array<Relation>;
  typesDocuments: Array<TypeDocument>;
  maxDay: number;
  maxSize: number;
  success: boolean;
  successStep1: boolean;
  openModal: boolean;
  public appCode: string;
  public numberPolice: string;
  public namePolice: string;
  constructor(private readonly storageService: StorageService) {
    this.openModal = false;
    this.reset();
    let local = this.storageService.getItem('insured');
    if (!!local) {
      local = JSON.parse(atob(local));
      this.originInsured = local.insured;
      this.buffInsured = local.insured;
      this.appCode = local.appCode;
      this.numberPolice = local.numberPolice;
      this.namePolice = local.namePolice;
    }
  }

  reset(): void {
    this.buffInsured = null;
    this.originInsured = null;
    this.changesInInsured = [];
    this.relations = [];
    this.typesDocuments = [];
    this.maxDay = null;
    this.maxSize = null;
    this.appCode = null;
    this.numberPolice = null;
    this.namePolice = null;
    this.success = false;
    this.successStep1 = false;
  }

  removeLocalStorage(): void {
    this.storageService.removeItem('insured');
  }

  setPolicyInsuredResponse(
    data: IPolicyInsuredResponse,
    appCode: string,
    numberPolice: string,
    namePolice: string
  ): void {
    this.originInsured = { ...data };
    this.buffInsured = { ...data };
    this.appCode = appCode;
    this.numberPolice = numberPolice;
    this.namePolice = namePolice;
    this.storageService.setItem('insured', btoa(JSON.stringify({ insured: data, appCode, numberPolice, namePolice })));
  }

  getPolicyInsuredResponse(): IPolicyInsuredResponse {
    return !this.originInsured ? null : { ...this.originInsured };
  }

  getBuffInsured(): IPolicyInsuredResponse {
    return !this.buffInsured ? null : { ...this.buffInsured };
  }

  getTypeDocuments(): Array<TypeDocument> {
    return [...this.typesDocuments];
  }

  getRelations(): Array<Relation> {
    return [...this.relations];
  }

  setBuffInsured(newInsured: IPolicyInsuredResponse): void {
    this.buffInsured = { ...newInsured };
  }

  setArr(arrDoc: Array<TypeDocument>, arrRe: Array<Relation>): void {
    this.typesDocuments = [...arrDoc];
    this.relations = [...arrRe];
  }

  setParameters(maxDay: number, maxSize: number): void {
    this.maxDay = maxDay;
    this.maxSize = maxSize;
  }

  getMaxDay(): number {
    return this.maxDay;
  }

  getMaxSize(): number {
    return this.maxSize;
  }

  setChangesInInsured(arr: Array<IChangesDataInsured>): void {
    this.changesInInsured = [...arr];
  }

  setChangesObjectInsured(data: IPolicyInsuredEditDataRequest): void {
    this.changesObjectInsured = { ...data };
  }

  getChangesInInsured(): Array<IChangesDataInsured> {
    return [...this.changesInInsured];
  }

  getChangesObjectInsured(): IPolicyInsuredEditDataRequest {
    return { ...this.changesObjectInsured };
  }

  setSuccess(bool: boolean): void {
    this.success = bool;
  }

  getSuccess(): boolean {
    return this.success;
  }

  setSuccessStep1(bool: boolean): void {
    this.successStep1 = bool;
  }

  getSuccessStep1(): boolean {
    return this.successStep1;
  }

  setOpenModal(bool: boolean): void {
    this.openModal = bool;
  }

  getOpenModal(): boolean {
    return this.openModal;
  }
}
