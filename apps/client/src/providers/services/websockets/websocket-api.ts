import { Injectable } from '@angular/core';
import { environment } from '@mx/environments/environment';
import { Subject } from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import { channelWebSockets } from './channel-websockets';
@Injectable()
export class WebSocketAPI {
  public onMaintenance$ = new Subject();
  webSocketEndPoint = environment.urlWebSocketApi;
  stompClient: Stomp.Client;

  _connect(): void {
    if (!this.stompClient) {
      this.stompClient = Stomp.over(new SockJS(this.webSocketEndPoint));
      // this._disableDebugWebSocket();
      this._createObservable();
    } else if (!this.stompClient.connected) {
      this._createObservable();
    }
  }

  _disconnect(): void {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
  }

  private _createObservable(): void {
    this.stompClient.connect({}, () => {
      this.stompClient.subscribe(
        channelWebSockets.WE_ARE_IN_MAINTENANCE,
        message => {
          this.onMaintenance$.next(message);
        },
        error => {
          this.onMaintenance$.error(error);
        }
      );
    });
  }

  private _disableDebugWebSocket(): void {
    this.stompClient.debug = null;
  }
}
