import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api/api.service';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { appConstants } from '@mx/core/shared/providers/constants';
import { GeneralEndpoint } from '@mx/endpoints/general.endpoint';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { EMAIL_CONTAC_ACTPOL, EMAIL_CONTACT, PHONE_CONTACT_LIMA } from '@mx/settings/constants/key-values';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { TypeDocument } from '@mx/statemanagement/models/typedocument.interface';
import { BehaviorSubject, Observable } from 'rxjs';
import { KEYS } from '../policy/policy-quote.service';

export const STORAGE_KEYS = {
  PARAMETERS: 'parameters_app',
  DOCUMENT_TYPES: 'document_types'
};
@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private parameters: Array<IParameterApp>;
  private parametersSubject: BehaviorSubject<Array<IParameterApp>>;
  private documentTypesSubject: BehaviorSubject<Array<TypeDocument>>;
  public editAppointmentsBody: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public rescheduleAppointment: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public showChatGenesys: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor(
    private readonly apiService: ApiService,
    private readonly storageService: StorageService,
    private readonly datePipe: DatePipe
  ) {
    this.initParametersData();
  }

  // Inicializar Subject y Storage
  initParametersData(): void {
    if (!this.parametersSubject) {
      this.parametersSubject = new BehaviorSubject<Array<IParameterApp>>(null);
      this.apiService.get(`${GeneralEndpoint.parameters}`, {}).subscribe((parameters: Array<IParameterApp>) => {
        this.setParameters(parameters);
      });
    }
    if (!this.documentTypesSubject) {
      this.documentTypesSubject = new BehaviorSubject<Array<TypeDocument>>(null);
      this.apiService
        .get(`${GeneralEndpoint.typesDocument}${APPLICATION_CODE}`, {})
        .subscribe((types: Array<TypeDocument>) => {
          this.setDocumentTypes(types);
        });
    }
  }

  // Parametros del Sistema
  getParameters(): Observable<Array<IParameterApp>> {
    this.parameters = this.getParametersData();
    if (!this.parameters || (this.parameters && !this.parameters.length)) {
      return this.apiService.get(`${GeneralEndpoint.parameters}`, { default: [] });
    }
    this.parametersSubject.next(this.parameters);

    return this.parametersSubject.asObservable();
  }

  getParametersSubject(): Observable<Array<IParameterApp>> {
    return this.parametersSubject;
  }

  getParametersData(): Array<IParameterApp> {
    return this.parameters || this.storageService.getStorage(STORAGE_KEYS.PARAMETERS);
  }

  setParameters(data: Array<IParameterApp>): void {
    if (data) {
      this.storageService.saveStorage(STORAGE_KEYS.PARAMETERS, data);
      this.parameters = data;
      this.parametersSubject.next(data);
    }
  }

  getEmailActivePolicy(): string {
    return this.getValueParams(EMAIL_CONTAC_ACTPOL);
  }

  getEmail(): string {
    return this.getValueParams(EMAIL_CONTACT);
  }

  getPhoneNumber(key?: string): string {
    if (!key) {
      // tslint:disable-next-line: no-parameter-reassignment
      key = PHONE_CONTACT_LIMA;
    }

    return this.getValueParams(key);
  }

  getValueParams(key): string {
    this.parameters = this.getParametersData();
    if (!this.parameters) {
      return null;
    }
    const params = (this.parameters || []).filter(param => param.codigo === key);

    return params && params[0] ? params[0].valor : '';
  }

  // Tipo de Documentos
  getDocumentTypes(section: string = APPLICATION_CODE): Observable<Array<TypeDocument>> {
    const documentTypes = this.getDocumentTypesData();
    if (!documentTypes || (documentTypes && !documentTypes.length)) {
      return this.apiService.get(`${GeneralEndpoint.typesDocument}${section}`, { default: [] });
    }
    this.documentTypesSubject.next(documentTypes);

    return this.documentTypesSubject.asObservable();
  }

  getDocumentTypesData(): Array<TypeDocument> {
    return this.storageService.getStorage(STORAGE_KEYS.DOCUMENT_TYPES) || [];
  }

  setDocumentTypes(data: Array<TypeDocument>): void {
    this.documentTypesSubject.next(data);
    this.storageService.saveStorage(STORAGE_KEYS.DOCUMENT_TYPES, data);
  }

  // Rango Salarial
  getSalaryRange(): Observable<any> {
    return this.apiService.get(`${GeneralEndpoint.salaryRange}`, { default: [] });
  }

  // Direccion de Correspondencia para Contratante e Inspeccion
  getCorrespondenceAddress(): IClientAddressResponse {
    return this.storageService.getStorage(KEYS.ADDRESS);
  }

  setCorrespondenceAddress(data: IClientAddressResponse): void {
    this.storageService.saveStorage(KEYS.ADDRESS, data);
  }

  clearCorrespondenceAddress(): void {
    this.storageService.removeItem(KEYS.ADDRESS);
  }

  // Tipo de Relación con titular de poliza
  getTypesRelation(): Observable<any> {
    return this.apiService.get(`${GeneralEndpoint.typesRelation}`, { default: [] });
  }

  ObtenerContactoPais(reqOptions?: any): Observable<any> {
    return this.apiService.get(`${GeneralEndpoint.countriesPhone}`, { ...reqOptions });
  }

  getDateWithStringFormat(date: string): string {
    const stringDate = date.split('-');
    const day = stringDate[2];
    const month = parseInt(stringDate[1], 10);
    let stringMonth;
    switch (month) {
      case 1:
        stringMonth = 'Enero';
        break;
      case 2:
        stringMonth = 'Febrero';
        break;
      case 3:
        stringMonth = 'Marzo';
        break;
      case 4:
        stringMonth = 'Abril';
        break;
      case 5:
        stringMonth = 'Mayo';
        break;
      case 6:
        stringMonth = 'Junio';
        break;
      case 7:
        stringMonth = 'Julio';
        break;
      case 8:
        stringMonth = 'Agosto';
        break;
      case 9:
        stringMonth = 'Septiembre';
        break;
      case 10:
        stringMonth = 'Octubre';
        break;
      case 11:
        stringMonth = 'Noviembre';
        break;
      default:
        stringMonth = 'Diciembre';
        break;
    }

    return `${day} de ${stringMonth}`;
  }

  getPeriodOfTime(time: string): string {
    const hourTime = this.getTimeOnSplit(time)[0];
    const dateTime = this.datePipe.transform(
      new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
        this.getTimeOnSplit(time)[0],
        this.getTimeOnSplit(time)[1]
      ),
      'hh:mm a'
    );

    if (hourTime < 12) {
      return dateTime;
    }

    return dateTime;
  }

  getTimeOnSplit(time: string): any {
    const dateArr: any[] = time.split(':');
    dateArr.forEach(parseInt);

    return dateArr;
  }

  saveUserMovements(modulo: string, proceso: string, descripcion: string): void {
    const bodyReq = {
      modulo,
      proceso,
      descripcion
    };

    this.apiService
      .post(`${appConstants.apis.comun}/areaPrivada/eventos/movimientos`, bodyReq, {
        params: { codigoApp: APPLICATION_CODE }
      })
      .subscribe(
        res => {
          return res;
        },
        err => {
          console.error(err);
        }
      );
  }

  action(param: string): string {
    return this.getValueParams(param);
  }
}
