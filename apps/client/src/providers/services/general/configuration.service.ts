import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { IObtenerConfiguracionResponse } from '@mx/statemanagement/models/client.models';
import { BehaviorSubject, Subject, timer } from 'rxjs';

export const STORAGE_KEYS = {
  CONFIG_HOME: 'config_home',
  HAS_DIGITAL_CLINIC: 'hasDigitalClinic'
};

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  configurationClient: BehaviorSubject<IObtenerConfiguracionResponse>;
  hideServicesIcons: Subject<boolean>;

  constructor(private readonly storageService: StorageService) {
    this.configurationClient = new BehaviorSubject<IObtenerConfiguracionResponse>(null);
    this.hideServicesIcons = new Subject<boolean>();
  }

  // Configuracion por Cliente
  getConfigurationByType(type?: string): IObtenerConfiguracionResponse {
    if (type) {
      return this.storageService.getStorage(`config_${type.toLowerCase()}`);
    }

    return this.storageService.getStorage(STORAGE_KEYS.CONFIG_HOME);
  }

  // Cliente con Clinica Digital
  getConfigurationDigitalClinic(): string {
    return this.storageService.getStorage(STORAGE_KEYS.HAS_DIGITAL_CLINIC);
  }

  getConfiguration(): BehaviorSubject<IObtenerConfiguracionResponse> {
    return this.configurationClient;
  }

  setConfiguration(configuration: IObtenerConfiguracionResponse, type?: string): void {
    if (type) {
      this.storageService.saveStorage(`config_${type.toLowerCase()}`, configuration);
    } else {
      this.storageService.saveStorage(STORAGE_KEYS.CONFIG_HOME, configuration);
    }

    this.configurationClient.next(configuration);
  }

  // Ocultar Iconos de Servicios
  getHideServicesIcons(): Subject<boolean> {
    return this.hideServicesIcons;
  }

  setHideServicesIcons(hide: boolean): void {
    const timer$ = timer(0).subscribe(() => {
      this.getHideServicesIcons().next(hide);
      timer$.unsubscribe();
    });
  }
}
