import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api/api.service';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { AuthEndpoint } from '@mx/endpoints/auth.endpoint';
import {
  AdminUserResponse,
  IChangePasswordRequest,
  IChangePasswordResponse,
  IChangePasswordToken,
  IUpdatePasswordRequest
} from '@mx/statemanagement/models/auth.interface';
import { GeneralRequestEn } from '@mx/statemanagement/models/general.models';
import { Observable } from 'rxjs';

export const KEYS = {
  EMAIL: 'email',
  PASSWORD: 'pwd'
};

@Injectable({
  providedIn: 'root'
})
export class UserAdminService {
  constructor(private readonly apiService: ApiService, private readonly storageService: StorageService) {}

  // Habilitar Usuario
  enableUser(params: GeneralRequestEn): Observable<AdminUserResponse> {
    return this.apiService.post(AuthEndpoint.enableUser, params, { default: {} });
  }
  // Recuperar password
  recoverUser(params: GeneralRequestEn): Observable<AdminUserResponse> {
    return this.apiService.post(AuthEndpoint.recoverUser, params, { default: {} });
  }
  // Cambiar password
  changePassword(params: IChangePasswordRequest): Observable<IChangePasswordResponse> {
    return this.apiService.post(AuthEndpoint.changePassword, params, { default: {} });
  }
  // Cambiar password
  updatePassword(body: IUpdatePasswordRequest): Observable<IChangePasswordResponse> {
    return this.apiService.post(AuthEndpoint.updatePassword, body, { default: {} });
  }

  // Gestión de email en storage
  getEmailConfirm(): string {
    return this.storageService.getStorage(KEYS.EMAIL);
  }

  setEmailConfirm(data: string): void {
    this.storageService.saveStorage(KEYS.EMAIL, data);
  }

  clearEmailConfirm(): void {
    this.storageService.removeItem(KEYS.EMAIL);
  }

  // Gestión de cambio de password
  getChangePasswordConfirm(): IChangePasswordToken {
    return this.storageService.getStorage(KEYS.PASSWORD);
  }

  setChangePasswordConfirm(data: IChangePasswordToken): void {
    this.storageService.saveStorage(KEYS.PASSWORD, data);
  }

  clearChangePasswordConfirm(): void {
    this.storageService.removeItem(KEYS.PASSWORD);
  }
}
