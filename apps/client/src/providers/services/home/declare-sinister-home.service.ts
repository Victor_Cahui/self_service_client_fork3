import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { FileItem } from '@mx/core/ui/lib/directives/ng-drop/file-item.class';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import {
  IDeclareSinister,
  IIncidentReason,
  IListAddress,
  IListHoursResponse,
  IListIncidentTypesResponse
} from '@mx/statemanagement/models/home.interface';
import { BehaviorSubject } from 'rxjs';

export interface IStepsSinester {
  step1: boolean;
  step2: boolean;
  step3: boolean;
  step4: boolean;
}
@Injectable({
  providedIn: 'root'
})
export class DeclareSinisterHomeService {
  dayAfterIncident: string;
  maximumSizeFileMB: number;
  assistanceNumber: number;
  listIncidentHours: Array<IListHoursResponse>;
  listAddress: Array<IListAddress>;
  listIncidentTypes: Array<IListIncidentTypesResponse>;
  sinester: IDeclareSinister;
  subSteps: BehaviorSubject<IStepsSinester>;
  finalize: boolean;
  private steps: IStepsSinester;
  constructor(private readonly storageService: StorageService) {
    this.reset();
    const vFinalize = this.storageService.getItem('finalize');
    const sinester = this.storageService.getItem('sinester');
    if (!!sinester) {
      if (!!vFinalize) {
        this.finalize = JSON.parse(atob(vFinalize)).value;
        const bSinester: IDeclareSinister = JSON.parse(atob(sinester));
        const steps: IStepsSinester = { step1: true, step2: true, step3: true, step4: true };
        this.setSteeps(steps);
        this.sinester = { ...bSinester, files: [] };
      } else {
        const bSinester: IDeclareSinister = JSON.parse(atob(sinester));
        this.checkSteps(bSinester);
        this.sinester = { ...bSinester, files: [] };
      }
    }
  }

  successSteep1(address: IListAddress, date: Date, hour: IListHoursResponse, hourId: number): void {
    this.sinester.address = { ...address };
    this.sinester.date = date;
    this.sinester.hour = { ...hour };
    this.sinester.hourId = hourId;
    this.setSteep(1, !!address && !!date && !!hour);
    this.storageService.setItem('sinester', btoa(JSON.stringify(this.getSinesterStorage())));
  }

  successSteep2(incidentId: number, reasonId: number): void {
    this.sinester.idIncident = incidentId;
    this.sinester.idReason = reasonId;
    this.setSteep(2, !!incidentId && !!reasonId);
    this.storageService.setItem('sinester', btoa(JSON.stringify(this.getSinesterStorage())));
  }

  successSteep3(files: Array<FileItem>): void {
    this.sinester.files = [...files];
    this.setSteep(3, files.length > 0 && files.length < 4);
  }

  successSteep4(assistanceNumber: number): void {
    this.assistanceNumber = assistanceNumber;
    this.setSteep(4, !!assistanceNumber);
  }

  stepSeen(): void {
    this.storageService.setItem('finalize', btoa(JSON.stringify({ value: true })));
  }

  getFinalize(): boolean {
    return this.finalize;
  }

  setSteep(index: number, bool: boolean): void {
    const steps: IStepsSinester = this.getSteeps();
    switch (index) {
      case 1:
        steps.step1 = bool;
        break;
      case 2:
        steps.step2 = bool;
        break;
      case 3:
        steps.step3 = bool;
        break;
      case 4:
        steps.step4 = bool;
        break;
      default:
        break;
    }
    this.setSteeps(steps);
  }

  setSteeps(steps: IStepsSinester): void {
    this.steps = { ...steps };
    this.subSteps.next({ ...steps });
  }

  getSteeps(): IStepsSinester {
    return { ...this.steps };
  }

  checkSteps(sinester: IDeclareSinister): void {
    this.setSteep(1, !!sinester.address && !!sinester.date && !!sinester.hour);
    this.setSteep(2, !!sinester.idIncident && !!sinester.idReason);
    this.setSteep(3, !!sinester.files && sinester.files.length > 0 && sinester.files.length < 4);
  }

  setConstantsRequired(
    day: string,
    maxSizeFileMB: number,
    hours: Array<IListHoursResponse>,
    address: Array<IClientAddressResponse>,
    types: Array<IListIncidentTypesResponse>
  ): void {
    this.dayAfterIncident = day;
    this.maximumSizeFileMB = maxSizeFileMB;
    this.listIncidentHours = [...hours];
    this.listAddress = [...address].map((obj: IClientAddressResponse, index: number) => {
      const buff: any = obj;
      buff.id = index + 1;

      return buff;
    });
    this.listIncidentTypes = [...types];
  }

  getDayAfterIncident(): string {
    return this.dayAfterIncident;
  }

  getMaxSizeFileMB(): number {
    return this.maximumSizeFileMB;
  }

  getListIncidentHours(): Array<IListHoursResponse> {
    return [...this.listIncidentHours];
  }

  getListAddress(): Array<IListAddress> {
    return [...this.listAddress];
  }

  getListIncidentTypes(): Array<IListIncidentTypesResponse> {
    return [...this.listIncidentTypes];
  }

  getListIncidentReason(id: number): Array<IIncidentReason> {
    const listTypes = this.getListIncidentTypes();
    const reasons = listTypes.find((obj: IListIncidentTypesResponse) => obj.identificadorIncidente === id);

    return [...reasons.motivos];
  }

  getIndexListIncident(id: number): number {
    const listTypes = this.getListIncidentTypes();
    const index = listTypes.findIndex((obj: IListIncidentTypesResponse) => obj.identificadorIncidente === id);

    return index;
  }

  getAssistanceNumber(): number {
    return this.assistanceNumber;
  }

  getSinester(): IDeclareSinister {
    return { ...this.sinester };
  }

  getSinesterStorage(): any {
    const sinester = this.getSinester();
    delete sinester.files;

    return { ...sinester };
  }

  getAddressReview(): string {
    const sinister = this.getSinester();

    return `${sinister.address.direccion}, ${sinister.address.distrito}, ${sinister.address.provincia}, ${sinister.address.departamento}`;
  }

  getIncidentReview(): string {
    const sinister = this.getSinester();
    const incidentType = this.getListIncidentTypes().find(
      (obj: IListIncidentTypesResponse) => obj.identificadorIncidente === sinister.idIncident
    );

    return incidentType.descripcionIncidente;
  }

  getReasonReview(): string {
    const sinister = this.getSinester();
    const reason = this.getListIncidentReason(sinister.idIncident).find(
      (obj: IIncidentReason) => obj.identificadorMotivo === sinister.idReason
    );

    return reason.descripcionMotivo;
  }

  reset(): void {
    this.dayAfterIncident = null;
    this.assistanceNumber = null;
    this.maximumSizeFileMB = null;
    this.listIncidentHours = [];
    this.listAddress = [];
    this.listIncidentTypes = [];
    this.finalize = false;
    this.steps = { step1: false, step2: false, step3: false, step4: false };
    this.sinester = this.newSinester();
    this.subSteps = new BehaviorSubject<IStepsSinester>(this.steps);
  }

  removeLocalStorage(): void {
    this.storageService.removeItem('sinester');
    this.storageService.removeItem('finalize');
  }

  newSinester(): IDeclareSinister {
    return {
      address: null,
      date: null,
      hour: null,
      hourId: null,
      idIncident: null,
      idReason: null,
      files: []
    };
  }
}
