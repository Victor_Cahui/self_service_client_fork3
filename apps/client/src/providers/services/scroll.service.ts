import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class ScrollService {
  constructor(private readonly activatedRoute: ActivatedRoute) {}

  checkScrollTo(fragmentTarget: string): void {
    this.activatedRoute.fragment.subscribe(fragment => {
      setTimeout(() => {
        const object = document.getElementById(fragmentTarget);
        if (object && fragment === fragmentTarget) {
          object.scrollIntoView({ behavior: 'smooth' });
        }
      });
    });
  }
}
