import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api/api.service';
import { NotificationsEndpoint } from '@mx/endpoints/notifications.endpoint';
import { INotificationHome, INotificationRequest } from '@mx/statemanagement/models/notification.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificactionsService {
  constructor(private readonly apiService: ApiService) {}

  getNotificationsforHome(notificactionRequest: INotificationRequest): Observable<Array<INotificationHome>> {
    return this.apiService.get(`${NotificationsEndpoint.notification}`, { params: notificactionRequest, default: [] });
  }
}
