import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import {
  IAgent,
  ICardPoliciesView,
  IPoliciesByClientResponse,
  IPolicyList
} from '@mx/statemanagement/models/policy.interface';
import { BehaviorSubject, Observable } from 'rxjs';

export enum KEYS {
  POLICY_LIST = 'policy_list'
}

@Injectable({
  providedIn: 'root'
})
export class PolicyListService {
  policyList: IPolicyList;
  validPoliciesSub: BehaviorSubject<boolean>;

  constructor(private readonly storageService: StorageService) {
    this.policyList = this.newPolicyList();
    this.validPoliciesSub = new BehaviorSubject(true);
  }

  getPolicyListLocal(): IPolicyList {
    return this.storageService.getStorage(KEYS.POLICY_LIST);
  }

  getPolicyType(): string {
    const data = this.getPolicyListLocal();

    return data ? data.policyType : '';
  }

  getPolicySelected(): string {
    const data = this.getPolicyListLocal();

    return data ? data.policySelected : '';
  }

  getAgent(): IAgent {
    const data = this.getPolicyListLocal();

    return data ? data.agente : null;
  }

  getDataPolicies(): Array<ICardPoliciesView> {
    const data = this.getPolicyListLocal();

    return data ? data.list : null;
  }

  getDataPolicy(policyNumber): IPoliciesByClientResponse {
    const policies = this.getPolicyListData();
    if (!policies) {
      return null;
    }
    const data = policies.list.filter(item => item.policyNumber === policyNumber);

    return data.length ? data[0].response : null;
  }

  getPolicyListData(): IPolicyList {
    const data = this.getPolicyListLocal();

    return data ? data : null;
  }

  getPolicyList(): IPolicyList {
    return this.policyList;
  }

  setPolicyListLocal(): void {
    this.storageService.saveStorage(KEYS.POLICY_LIST, this.getPolicyList());
  }

  setPolicySelected(policy: string): void {
    this.policyList = this.getPolicyListData();
    if (this.policyList) {
      this.policyList.policySelected = policy;
      this.setPolicyListLocal();
    }
  }

  setAgent(agent: IAgent): void {
    this.policyList = this.getPolicyListData();
    if (this.policyList) {
      this.policyList.agente = agent;
      this.setPolicyListLocal();
    }
  }

  setPolicyListData(data: IPolicyList): void {
    this.policyList = data;
    this.setPolicyListLocal();
  }

  removePolicyListLocal(): void {
    this.storageService.removeItem(KEYS.POLICY_LIST);
  }

  newPolicyList(): IPolicyList {
    return {
      policyType: '',
      policySelected: '',
      typeDocument: '',
      document: '',
      list: []
    };
  }

  /*******************
   * Polizas Válidas
   *******************/
  setValidPolicies(valid: boolean): void {
    this.validPoliciesSub.next(valid);
  }

  getValidPolicies(): Observable<boolean> {
    return this.validPoliciesSub;
  }
}
