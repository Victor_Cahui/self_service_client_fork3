import { GoogleMapsAPIWrapper } from '@agm/core';
import { Injectable } from '@angular/core';
import { IItemMarker, ISearchPlace, IUserMarker } from '@mx/statemanagement/models/google-map.interface';
import { BehaviorSubject } from 'rxjs';

export enum GoogleMapsEvents {
  VIEW_DETAILS = 'view_detail',
  CHANGE_MARKERS = 'change_markers',
  RESET_MARKERS = 'reset_markers',
  SET_START_MARKER = 'set_start_marker',
  SET_END_MARKER = 'set_end_marker',
  DEFAULT_MAP = 'default_map',
  CONFIRM_PLACE = 'confirm_place',
  APPLY_BOUNCE = 'apply_bounce',
  MAP_CLICK = 'map_click',
  CENTER_CHANGED = 'center_changed',
  DRAG_END = 'dragend',
  DRAG_START = 'dragstart',
  ZOOM_CONTROL = 'zoom_control',
  READY = 'ready',
  CHANGE_OPTIONS_MAPS = 'change_options_map'
}

@Injectable({
  providedIn: 'root'
})
export class GoogleMapService {
  private readonly itemMaker: BehaviorSubject<IItemMarker> = new BehaviorSubject<IItemMarker>(null);
  private readonly temporalPosition: BehaviorSubject<IUserMarker> = new BehaviorSubject<IUserMarker>(null);
  private readonly searchMaker: BehaviorSubject<ISearchPlace> = new BehaviorSubject<ISearchPlace>(null);
  private readonly events: BehaviorSubject<{ event: GoogleMapsEvents; data: any }> = new BehaviorSubject<any>(null);
  private map: GoogleMapsAPIWrapper;
  private searchPlaceTitle: string;
  private useFocusedSearch: boolean;
  private searchMakerDefault: ISearchPlace;
  private searchPlaceEventMarker: GoogleMapsEvents;

  setItemMarker(item: IItemMarker): void {
    this.itemMaker.next(item);
  }

  getItemMarker(): BehaviorSubject<IItemMarker> {
    return this.itemMaker;
  }

  emitTemporalPosition(): void {
    this.temporalPosition.next(null);
  }

  onTemporalPosition(): BehaviorSubject<IUserMarker> {
    return this.temporalPosition;
  }

  eventEmit(event: GoogleMapsEvents, data?: any): void {
    this.events.next({ event, data });
  }

  onEvent(): BehaviorSubject<{ event: GoogleMapsEvents; data?: any }> {
    return this.events;
  }

  setMap(map: GoogleMapsAPIWrapper): void {
    this.map = map;
  }

  getMap(): GoogleMapsAPIWrapper {
    return this.map;
  }

  // Search place
  setSearchPlaceTitle(searchPlaceTitle: string): void {
    this.searchPlaceTitle = searchPlaceTitle;
  }

  getSearchPlaceTitle(): string {
    return this.searchPlaceTitle;
  }

  setSearchPlaceEventMarker(searchPlaceEventMarker: GoogleMapsEvents): void {
    this.searchPlaceEventMarker = searchPlaceEventMarker;
  }

  getSearchPlaceEventMarker(): GoogleMapsEvents {
    return this.searchPlaceEventMarker;
  }

  setUseFocusedSearch(useFocusedSearch: boolean): void {
    this.useFocusedSearch = useFocusedSearch;
  }

  getUseFocusedSearch(): boolean {
    return this.useFocusedSearch;
  }

  setSearchMarker(searchMaker: ISearchPlace): void {
    this.searchMaker.next(searchMaker);
  }

  getSearchMarker(): BehaviorSubject<ISearchPlace> {
    return this.searchMaker;
  }

  setSearchMarkerDefault(searchMakerDefault: ISearchPlace): void {
    this.searchMakerDefault = searchMakerDefault;
  }

  getSearchMarkerDefault(): ISearchPlace {
    return this.searchMakerDefault;
  }
}
