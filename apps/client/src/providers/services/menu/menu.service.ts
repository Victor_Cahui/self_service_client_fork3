import { Injectable } from '@angular/core';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { isExpired } from '@mx/core/shared/helpers/util/date';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { CLINICS_SEARCH_OPTION, GET_MENU_LIST } from '@mx/settings/constants/menu';
import { HEALTH_CLINIC_SEARCH, POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { IMenuItem } from '@mx/statemanagement/models/general.models';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { BehaviorSubject, Subject, timer } from 'rxjs';

export enum KEYS {
  ROUTES_BY_MENU = 'routes_by_menu'
}
@Injectable({
  providedIn: 'root'
})
export class MenuService {
  modalContact: Subject<boolean>;
  pendingPayments: BehaviorSubject<string>;

  constructor(
    private readonly storageService: StorageService,
    private readonly authService: AuthService,
    private readonly _Autoservicios: Autoservicios
  ) {
    this.modalContact = new Subject<boolean>();
    this.pendingPayments = new BehaviorSubject<string>('');
    this.getNumberPendingPayments();
  }

  setRoutesByMenu(menuList: Array<IMenuItem>): void {
    const routesByMenu: Array<string> = [];
    menuList.forEach(itemMenu => {
      if (itemMenu.route) {
        routesByMenu.push(itemMenu.route);
      }
      if (itemMenu.children) {
        itemMenu.children.forEach(itemSubMenu => {
          if (itemSubMenu.route) {
            routesByMenu.push(itemSubMenu.route);
          }
        });
      }
    });
    this.storageService.saveStorage(KEYS.ROUTES_BY_MENU, routesByMenu);
  }

  getRoutesByMenu(): Array<string> {
    return this.storageService.getStorage(KEYS.ROUTES_BY_MENU);
  }

  /**
   * Deprecated!
   * generar el menu a partir de las polizas del cliente, servicio: buscarPorCliente
   * @param policiesResponse Array de polizad por cliente.
   */
  generateMenu(policiesResponse: Array<IPoliciesByClientResponse>): Array<IMenuItem> {
    let menuList = GET_MENU_LIST();
    // tslint:disable-next-line: no-parameter-reassignment
    policiesResponse = policiesResponse || [];
    // Filtra no visibles
    menuList = menuList.filter(item => item.visible);

    const newMenu: Array<IMenuItem> = menuList.filter(itemMenu => {
      // verifica si existen codes de polizas por menu
      if (!itemMenu.policies) {
        return itemMenu;
      }

      // filtra los codes de polizas del menu con respecto al policiesResponse
      itemMenu.policies = itemMenu.policies.filter(itemCode => {
        return policiesResponse.find(policyResponse => {
          return policyResponse.tipoPoliza === itemCode;
        });
      });

      // filtra buscador de clinicas
      if (itemMenu.name === CLINICS_SEARCH_OPTION) {
        itemMenu.policies = itemMenu.policies.filter(itemCode => {
          return policiesResponse.find(policyResponse => {
            return (
              !isExpired(policyResponse.fechaFin) &&
              (itemCode === POLICY_TYPES.MD_EPS.code ||
                (itemCode === POLICY_TYPES.MD_SALUD.code &&
                  (policyResponse.codRamo === HEALTH_CLINIC_SEARCH.COD_RAMO[0] ||
                    policyResponse.codRamo === HEALTH_CLINIC_SEARCH.COD_RAMO[1])) ||
                PolicyUtil.isSctrHealth(itemCode, policyResponse.codCia, policyResponse.codRamo))
            );
          });
        });
      }

      // verifica si existen subMenus
      if (itemMenu.children) {
        // Filtra no visibles
        itemMenu.children = itemMenu.children.filter(item => item.visible);

        itemMenu.children = itemMenu.children.filter(itemSubMenu => {
          // verifica si existen codes de polizas por subMenu
          if (!itemSubMenu.policies) {
            return itemSubMenu;
          }

          // filtra los codes de polizazs del subMenu con respecto a los codes filtrados en el menu
          itemSubMenu.policies = itemSubMenu.policies.filter(itemSubMenuCode => {
            return itemMenu.policies.find(itemCode => {
              return itemSubMenuCode === itemCode;
            });
          });

          // retorna el subMenu si existen codes
          return itemSubMenu.policies.length;
        });
      }

      // retorna el menu si existen codes
      return itemMenu.policies.length;
    });

    this.setRoutesByMenu(newMenu);

    return newMenu;
  }

  getModalContact(): Subject<boolean> {
    return this.modalContact;
  }

  setModalContact(exist: boolean): void {
    const timer$ = timer(0).subscribe(() => {
      this.getModalContact().next(exist);
      timer$.unsubscribe();
    });
  }

  getNumberPendingPayments(): void {
    const params = {
      codigoApp: APPLICATION_CODE
    };
    this._Autoservicios.ObtenerNumeroPagosPendientes(params).subscribe(response => {
      response && this.pendingPayments.next(`${response.numeroPendientes}`);
    });
  }
}
