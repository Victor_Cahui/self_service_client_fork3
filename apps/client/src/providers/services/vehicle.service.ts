import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { SkipInsterceptorJsonHeader } from '@mx/core/shared/helpers/interceptors/content-type-json-interceptor';
import {
  VehicleAccesoryEndpoint,
  VehicleAccidentEndpoint,
  VehicleAssistanceEndpoint,
  VehicleBuyEndpoint,
  VehicleEndpoint,
  VehicleQuoteEndpoint
} from '@mx/endpoints/vehicle.endpoint';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IListHoursResponse } from '@mx/statemanagement/models/home.interface';
import { IPolicyQuoteResponse } from '@mx/statemanagement/models/payment.models';
import {
  ICompareDetailsResponse,
  ICompareHeaderResponse,
  ICoveragesPolicyRequest
} from '@mx/statemanagement/models/policy.interface';
import * as v from '@mx/statemanagement/models/vehicle.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  constructor(private readonly apiService: ApiService) {}

  getDetailVehicle(params: v.IVehicleRequest): Observable<v.IClientVehicleDetail> {
    return this.apiService.get(VehicleEndpoint.getVehicle, { params, default: {} });
  }

  getClientVehicle(params: v.IClientVehicleRequest): Observable<v.IClientVehicleDetail> {
    return this.apiService.get(VehicleEndpoint.clientVehicle, { params, default: {} });
  }

  // Lista de Vehículos de una Póliza
  getVehiclesList(params: v.IVehiclesListRequest): Observable<Array<v.IClientVehicleDetail>> {
    return this.apiService.get(VehicleEndpoint.vehiclesList, { params, default: [] });
  }

  updateVehiclePlate(
    params: v.IVehicleUpdateRequest,
    poliza: v.IClientVehicleUpdateRequest
  ): Observable<v.IClientVehicleUpdateResponse> {
    return this.apiService.put(VehicleEndpoint.updateVehiclePlate, poliza, { params, default: {} });
  }

  // Accesorios
  getAccesoriesType(): Observable<Array<v.IVehicleAccesoryType>> {
    return this.apiService.get<Array<v.IVehicleAccesoryType>>(VehicleAccesoryEndpoint.getAccesoriesType, {
      default: []
    });
  }

  addAccesory(params: v.IVehicleAccesoryRequestPost, body: v.IVehicleAccesory): Observable<v.IVehicleAccesory> {
    return this.apiService.post(VehicleAccesoryEndpoint.addAccesory, body, { params, default: {} });
  }

  editAccesory(params: v.IVehicleAccesoryRequestPut, body: v.IVehicleAccesory): Observable<v.IVehicleAccesory> {
    return this.apiService.put(VehicleAccesoryEndpoint.editOrDeleteAccesory, body, { params, default: {} });
  }

  deleteAccesory(params: v.IVehicleAccesoryRequestPut): Observable<any> {
    return this.apiService.del(VehicleAccesoryEndpoint.editOrDeleteAccesory, { params, default: {} });
  }

  // Accidentes registradas
  getAccidents(params: GeneralRequest): Observable<v.IVehicleAssistsResponse> {
    return this.apiService.get(VehicleAccidentEndpoint.getAssists, { params, default: {} });
  }

  getWorkshopsPDF(params: GeneralRequest): Observable<v.IWorkshopPDFResponse> {
    return this.apiService.get(VehicleAccidentEndpoint.getWorkshopsPDF, { params, default: {} });
  }

  // Robos registrados
  getStolens(params: GeneralRequest): Observable<v.IVehicleAssistsResponse> {
    return this.apiService.get(VehicleEndpoint.getStolens, { params, default: {} });
  }

  // Data para Cotizacion
  getBrands(params: v.IVehicleQuoteRequest): Observable<Array<v.IVehicleBrandsResponse>> {
    return this.apiService.get(VehicleQuoteEndpoint.getBrands, { params, default: [] });
  }

  getModels(params: v.IVehicleQuoteRequest): Observable<Array<v.IVehicleModelsResponse>> {
    return this.apiService.get(VehicleQuoteEndpoint.getModels, { params, default: [] });
  }

  getType(params: v.IVehicleQuoteRequest): Observable<Array<v.IVehicleTypeResponse>> {
    return this.apiService.get(VehicleQuoteEndpoint.getType, { params, default: [] });
  }

  getValues(params: v.IVehicleQuoteRequest): Observable<v.IVehicleValuesResponse> {
    return this.apiService.get(VehicleQuoteEndpoint.getValues, { params, default: {} });
  }

  getUse(params: v.IVehicleQuoteRequest): Observable<Array<v.IVehicleUseResponse>> {
    return this.apiService.get(VehicleQuoteEndpoint.getUse, { params, default: [] });
  }

  getCity(params: v.IVehicleQuoteRequest): Observable<Array<v.IVehicleCityResponse>> {
    return this.apiService.get(VehicleQuoteEndpoint.getCity, { params, default: [] });
  }

  verifyGPSRequired(params: v.IVehicleQuoteRequest): Observable<v.IVerifyGPSRequiredResponse> {
    return this.apiService.get(VehicleQuoteEndpoint.getGPSRequired, { params, default: {} });
  }

  verifyPlate(params: v.IVerifyPlateRequest): Observable<v.IVerifyPlateResponse> {
    return this.apiService.get(VehicleQuoteEndpoint.verifyPlate, { params, default: {} });
  }

  // Coberturas para cotizacion
  getCompareHeader(
    data: ICoveragesPolicyRequest,
    body: v.ICompareVehicleRequest
  ): Observable<Array<ICompareHeaderResponse>> {
    return this.apiService.post(`${VehicleQuoteEndpoint.PoliciesCompareHeader}`, body, { params: data, default: [] });
  }

  getCompareDetails(
    data: ICoveragesPolicyRequest,
    body: v.ICompareVehicleRequest
  ): Observable<ICompareDetailsResponse> {
    return this.apiService.post(`${VehicleQuoteEndpoint.PoliciesCompareDetails}`, body, { params: data, default: {} });
  }

  // Validadores para compra de póliza vehicular
  searchChassisMotor(params: v.ISearchChassisMotorRequest): Observable<v.ISearchChassisMotorResponse> {
    return this.apiService.get(VehicleBuyEndpoint.SearchChassisMotor, { params, default: {} });
  }

  verifyPolicyChassisMotor(
    params: v.IVerifyPolicyChassisMotorRequest
  ): Observable<v.IVerifyPolicyChassisMotorResponse> {
    return this.apiService.get(VehicleBuyEndpoint.VerifyPolicyChassisMotor, { params, preloader: true, default: {} });
  }

  getColors(params: v.IGetColorsRequest): Observable<Array<v.IGetColorsResponse>> {
    return this.apiService.get(VehicleBuyEndpoint.GetColors, { params, default: [] });
  }

  verifyInspection(params: v.IVerifyInspectionRequest): Observable<v.IVerifyInsprectionResponse> {
    return this.apiService.get(VehicleBuyEndpoint.VerifyInspection, { params, preloader: true, default: {} });
  }

  getRangeHoursInspection(rangeHoursInspectionRequest: GeneralRequest): Observable<Array<IListHoursResponse>> {
    return this.apiService.get(VehicleBuyEndpoint.RangeHoursInspection, {
      params: rangeHoursInspectionRequest,
      default: []
    });
  }

  // Compra Vehiculo
  getQuoteData(data: GeneralRequest, body: v.IVehicleBuyRequest): Observable<IPolicyQuoteResponse> {
    return this.apiService.post(`${VehicleBuyEndpoint.QuoteData}`, body, { params: data, default: {} });
  }

  getPersonalDataClause(params: GeneralRequest): Observable<{ descripcionClausula: string }> {
    return this.apiService.get(VehicleBuyEndpoint.PersonalDataClause, { params, default: {} });
  }

  getInspectionConditions(params: v.IInspectionConditionsRequest): Observable<{ descripcionCondicion: string }> {
    return this.apiService.get(VehicleBuyEndpoint.InspectionConditions, { params, default: {} });
  }

  getIndication(params: GeneralRequest): Observable<{ descripcionPolizaElectronica: string }> {
    return this.apiService.get(VehicleBuyEndpoint.Indication, { params, default: {} });
  }

  getUrlConditions(params: v.IVehicleGetUrlContionsBuy): Observable<{ detalleCondicionesProducto: string }> {
    return this.apiService.get(VehicleBuyEndpoint.Conditions, { params, default: {} });
  }

  emitPayment(params: GeneralRequest, body: v.IEmitPaymentBody): Observable<v.IEmitPaymentResponse> {
    return this.apiService.post(VehicleBuyEndpoint.EmitPayment, body, { params, default: {} });
  }

  inspectionRequest(params: GeneralRequest, body: v.IInspectionRequestBody): Observable<v.IInspectionRequestResponse> {
    return this.apiService.post(VehicleBuyEndpoint.InspectionRequest, body, { params, default: {} });
  }

  // Asistencias
  getAccidentDetail(params: v.IAssistanceDetailRequest): Observable<any> {
    return this.apiService.get(VehicleAssistanceEndpoint.AccidentDetail, { params, default: {} });
  }

  getStolenDetail(params: v.IAssistanceDetailRequest): Observable<any> {
    return this.apiService.get(VehicleAssistanceEndpoint.StolenDetail, { params, default: {} });
  }

  rateWorkshop(body: v.IRateWorkshopRequestBody): Observable<any> {
    return this.apiService.post(VehicleAssistanceEndpoint.RateWorkshop, body, { default: {} });
  }

  addLetter(params: GeneralRequest, file: FormData): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set(SkipInsterceptorJsonHeader, '');

    return this.apiService.post(VehicleEndpoint.addLetter, file, { params, headers });
  }
}
