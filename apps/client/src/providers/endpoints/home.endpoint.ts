import { environment } from '@mx/environments/environment';

const params = '{codigoApp}/{numeroPoliza}/{numeroSuplemento}/{numeroRiesgo}/{codRamo}';
export class HomeEndpoint {
  public static ListIncidentHours = `${environment.API_URL_CLIENT}hogar/incidentes/rangosHorarios/{codigoApp}`;
  public static ListIncidentTypes = `${environment.API_URL_CLIENT}hogar/incidentes/tipos/{codigoApp}`;
  public static RegisterIncident = `${environment.API_URL_CLIENT}hogar/incidentes/registrar`;
  public static ListCoverage = `${environment.API_URL_CLIENT}hogar/coberturas/${params}`;
  public static ListDeductibles = `${environment.API_URL_CLIENT}hogar/deducibles/${params}`;

  public static ListSublimits = `${environment.API_URL_CLIENT}hogar/coberturas/sublimites/{codigoApp}/{numeroPoliza}`;
  public static ListDeductibleDescriptions = `${environment.API_URL_CLIENT}hogar/coberturas/descripciones/{codigoApp}/{numeroPoliza}`;

  // Comparacion de Pólizas
  public static PoliciesCompareHeader = `${environment.API_URL_CLIENT}hogar/comparacion/productos/{codigoApp}`;
  public static PoliciesCompareDetails = `${environment.API_URL_CLIENT}hogar/comparacion/coberturas/{codigoApp}`;
}
