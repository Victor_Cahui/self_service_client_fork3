import { environment } from '@mx/environments/environment';
export class OfficeEndpoint {
  public static officeList = `${environment.API_URL_CLIENT}oficinas/{codigoApp}`;
}
