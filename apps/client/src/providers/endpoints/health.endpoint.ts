// tslint:disable: max-line-length
import { environment } from '@mx/environments/environment';

export class HealthEndpoint {
  public static ClinicsList = `${environment.API_URL_CLIENT}salud/clinicas/{codigoApp}/{numeroPoliza}`;
  public static ClinicFavorite = `${environment.API_URL_CLIENT}salud/clinicas/favorito/{codigoApp}/{clinicaId}`;
  public static ClinicCoverage = `${environment.API_URL_CLIENT}salud/clinicas/coberturas/{codigoApp}/{clinicaId}/{numeroPoliza}`;

  public static ContractingAddress = `${environment.API_URL_CLIENT}salud/eps/datosContratante/{codigoApp}/{numeroContrato}`;
  public static GetPolicyByClinic = `${environment.API_URL_CLIENT}salud/clinicas/polizas/{codigoApp}/{identificadorClinica}`;

  // Reembolsos - Refunding
  public static RequestRefundPolicyList = `${environment.API_URL_CLIENT}salud/reembolso/listarPolizasSalud/{codigoApp}`;
  public static DownloadRefunsFormat = `${environment.API_URL_CLIENT}salud/reembolso/formatoSolicitud/{codigoApp}/{codCia}/{codRamo}/{codModalidad}`;
  public static BeneficType = `${environment.API_URL_CLIENT}salud/reembolso/listarBeneficios/{codigoApp}`;
  public static GetProvider = `${environment.API_URL_CLIENT}salud/reembolso/obtenerProveedor/{codigoApp}/{numeroRuc}`;
  public static PreRegister = `${environment.API_URL_CLIENT}salud/reembolso/registrar/{codigoApp}`;
  public static AddFile = `${environment.API_URL_CLIENT}salud/reembolso/cargarDocumento/{codigoApp}`;
  public static RemoveFile = `${environment.API_URL_CLIENT}salud/reembolso/eliminarDocumento/{codigoApp}`;
  public static RefundRequestList = `${environment.API_URL_CLIENT}salud/reembolso/buscarPorCliente/{codigoApp}`;
  public static RefundRequestDetail = `${environment.API_URL_CLIENT}salud/reembolso/detalleSolicitud/{codigoApp}`;
  public static RefundPendingRequest = `${environment.API_URL_CLIENT}salud/reembolso/obtenerCantidadPendientes/{codigoApp}`;
  public static ConfirmRefundRequest = `${environment.API_URL_CLIENT}salud/reembolso/confirmar/{codigoApp}`;
  public static DownLoadDocument = `${environment.API_URL_CLIENT}salud/reembolso/documento/{codigoApp}/{numeroSolicitud}/{identificadorDocumento}`;
}
