import { environment } from '@mx/environments/environment';

export class UbigeoEndpoint {
  public static departamentos = `${environment.API_URL_COMUN}ubigeo/departamentos`;
  public static provincias = `${environment.API_URL_COMUN}ubigeo/provincias`;
  public static distritos = `${environment.API_URL_COMUN}ubigeo/distritos`;
}
