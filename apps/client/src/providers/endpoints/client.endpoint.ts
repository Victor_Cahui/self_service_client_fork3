import { environment } from '@mx/environments/environment';

export class ClientEndpoint {
  public static GetClientMenu = `${environment.API_URL_CLIENT}clientes/menu/{codigoApp}`;
  public static DocumentVerification = `${environment.API_URL_CLIENT}clientes/verificacionDocumento/{codigoApp}`;
  public static GetInformation = `${environment.API_URL_CLIENT}clientes/perfil/datosPersonales`;
  public static GetConfiguration = `${environment.API_URL_CLIENT}clientes/perfil/obtenerConfiguracion/{codigoApp}`;
  public static GetAddress = `${environment.API_URL_CLIENT}clientes/perfil/direcciones`;
  public static GetCorrespondenceAddress = `${environment.API_URL_CLIENT}clientes/perfil/direccionCorrespondencia`;
  public static UpdateCorrespondenceAddress = `${environment.API_URL_CLIENT}clientes/perfil/direccionCorrespondencia`;
  public static UpdateDataWork = `${environment.API_URL_CLIENT}clientes/perfil/datosTrabajo`;
  public static updateClientEmail = `${environment.API_URL_CLIENT}clientes/perfil/correoElectronico`;
  public static AgentContact = `${environment.API_URL_CLIENT}clientes/perfil/contactoAgente/{codigoApp}`;
  public static PutProfileNonSensibleData = `${environment.API_URL_CLIENT}clientes/perfil/datosPersonalesNoSensibles`;
  public static PutProfileSensibleData = `${environment.API_URL_CLIENT}clientes/perfil/datosPersonalesSensibles`;
  public static GetContactData = `${environment.API_URL_CLIENT}clientes/perfil/datosContactos/{codigoApp}`;
  public static GetProfessions = `${environment.API_URL_CLIENT}clientes/perfil/profesiones/{codigoApp}`;
  public static ReferredContact = `${environment.API_URL_CLIENT}clientes/perfil/contactoReferidos/{codigoApp}`;
}
