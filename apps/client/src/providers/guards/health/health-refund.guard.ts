import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { HealthRefundService } from '@mx/services/health/health-refund.service';

@Injectable()
export class HealthRefundGuard implements CanActivate {
  constructor(private readonly router: Router, private readonly healthRefundService: HealthRefundService) {}

  canActivate(): boolean {
    const success = this.healthRefundService.validPolicyAndBeneficy();

    if (!success) {
      this.router.navigate([this.router.url]);
    }

    return !!success;
  }
}
