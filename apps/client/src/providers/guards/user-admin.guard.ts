import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserAdminService } from '@mx/services/auth/user-admin.service';

@Injectable()
export class UserAdminGuard implements CanActivate {
  constructor(private readonly router: Router, private readonly adminService: UserAdminService) {}

  canActivate(): boolean {
    if (this.adminService.getEmailConfirm() || this.adminService.getChangePasswordConfirm()) {
      this.router.navigate(['/login']);

      return false;
    }

    return true;
  }
}
