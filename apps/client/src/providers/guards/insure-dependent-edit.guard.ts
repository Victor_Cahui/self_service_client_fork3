import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';

@Injectable()
export class InsureDependentEditGuard implements CanActivate {
  constructor(private readonly service: InsuredDependentEditService) {}

  canActivate(): boolean {
    const flag =
      !this.service.getPolicyInsuredResponse() ||
      !this.service.appCode ||
      !this.service.numberPolice ||
      !this.service.namePolice;

    return !flag;
  }
}
