import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { AuthService } from '@mx/core/shared/common/services/auth.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { AuthService as AuthServiceInt } from '@mx/services/auth/auth.service';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(
    private readonly _AuthService: AuthService,
    private readonly router: Router,
    private readonly localStorage: LocalStorageService,
    private readonly _authServiceInt: AuthServiceInt
  ) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // HACK: sin el timeout, se vuelven a cargar los js de la ultima vista
    setTimeout(() => {
      const canReload = localStorage.getItem('canReload');
      if (canReload) {
        localStorage.setItem('canReload', '');
        // tslint:disable-next-line: deprecation
        location.reload(true);
      }
    }, 1000);

    const logout: string = route.queryParams.logout;
    if (logout) {
      this._authServiceInt.logout();

      return true;
    }

    if (this._AuthService.getAuthorizationToken()) {
      this.router.navigate(['/home']);

      return false;
    }
    if (this.localStorage.getData(LOCAL_STORAGE_KEYS.USER)) {
      this.router.navigate(['/welcome']);

      return false;
    }

    return true;
  }
}
