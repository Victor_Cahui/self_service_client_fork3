import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PaymentService } from '@mx/services/payment.service';

@Injectable()
export class PaymentQuoteConfirmGuard implements CanActivate {
  constructor(private readonly paymentService: PaymentService, private readonly router: Router) {}

  canActivate(): boolean {
    const quote = this.paymentService.getCurrentQuote();
    const success = quote && quote.confirm;
    if (!success) {
      this.router.navigate([this.router.url]);
    }

    return success;
  }
}
