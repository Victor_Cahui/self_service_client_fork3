import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { DeclareSinisterHomeService, IStepsSinester } from '@mx/services/home/declare-sinister-home.service';

@Injectable()
export class DeclareSinesterFinalizeGuard implements CanActivate {
  constructor(
    private readonly declareSinesterHomeService: DeclareSinisterHomeService,
    private readonly router: Router
  ) {}

  canActivate(): boolean {
    const steps: IStepsSinester = this.declareSinesterHomeService.getSteeps();
    if (!steps.step1 || !steps.step2 || !steps.step3 || !steps.step4) {
      this.router.navigate([`/household/hurt-and-steal/declare-sinister-home/step/3`]);
    }

    return steps.step1 && steps.step2 && steps.step3 && steps.step4;
  }
}
