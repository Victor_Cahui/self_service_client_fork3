import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';

@Injectable()
export class VehicleBuyStepFourGuard implements CanActivate {
  constructor(
    private readonly policiesQuoteService: PoliciesQuoteService,
    private readonly paymentService: PaymentService,
    private readonly router: Router
  ) {}

  canActivate(): boolean {
    const hasVehicle = this.policiesQuoteService.isVehicleDataCompleted();
    const hasContractor = this.paymentService.isContractorCompleted();
    const needInspection = this.policiesQuoteService.requiresInspection();
    const quote = this.paymentService.getCurrentQuote();
    const hasInspection = !!(
      quote &&
      quote.inspection &&
      (quote.inspection.selfInspection || (quote.inspection.data && quote.inspection.data.date))
    );

    if (!hasVehicle) {
      this.router.navigate(['vehicles/quote/buy/step1']);
    } else if (!hasContractor) {
      this.router.navigate(['vehicles/quote/buy/step2']);
    } else if (needInspection && !hasInspection) {
      this.router.navigate(['vehicles/quote/buy/step3']);

      return false;
    }

    return hasVehicle && hasContractor;
  }
}
