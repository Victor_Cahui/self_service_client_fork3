import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';

@Injectable()
export class VehicleBuyStepThreeGuard implements CanActivate {
  constructor(
    private readonly policiesQuoteService: PoliciesQuoteService,
    private readonly paymentService: PaymentService,
    private readonly router: Router
  ) {}

  canActivate(): boolean {
    const hasVehicle = this.policiesQuoteService.isVehicleDataCompleted();
    const hasContractor = this.paymentService.isContractorCompleted();
    const needInspection = this.policiesQuoteService.getVehicleData().requiereInspeccion;

    if (!hasVehicle) {
      this.router.navigate(['vehicles/quote/buy/step1']);
    } else if (!hasContractor) {
      this.router.navigate(['vehicles/quote/buy/step2']);
    }

    return hasVehicle && hasContractor && needInspection;
  }
}
