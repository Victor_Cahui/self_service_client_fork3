import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { EPaymentProcess } from '@mx/settings/constants/general-values';

@Injectable()
export class VehicleBuyGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly policiesQuoteService: PoliciesQuoteService,
    private readonly paymentService: PaymentService
  ) {}

  canActivate(): boolean {
    const vehicleData = this.policiesQuoteService.getVehicleData();
    const quote = this.paymentService.getCurrentQuote();
    const success = vehicleData && vehicleData.placa && quote && quote.typePayment === EPaymentProcess.BUY_VEHICLE;

    if (!success) {
      this.router.navigate([this.router.url]);
    } else if (quote && quote.confirm) {
      this.router.navigate(['vehicles/vehicularInsurance']);
    }

    return !!success;
  }
}
