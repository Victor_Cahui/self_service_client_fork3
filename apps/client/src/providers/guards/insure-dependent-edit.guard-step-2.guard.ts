import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';

@Injectable()
export class InsureDependentEditGuardStep2 implements CanActivate {
  constructor(private readonly service: InsuredDependentEditService, private readonly router: Router) {}

  canActivate(): boolean {
    const success = this.service.getSuccessStep1();
    if (!success) {
      this.router.navigate(['/health/health-insurance/insured-dependent/edit/1']);
    }

    return success;
  }
}
