import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor, ContentTypeJsonInterceptor, SessionInterceptor } from '@mx/core/shared/helpers/interceptors';

export const HttpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: SessionInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ContentTypeJsonInterceptor,
    multi: true
  }
];
