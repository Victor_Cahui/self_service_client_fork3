export const MEDIKTOR_URLS_REDIRECT = {
  WhatsAppText: 'Quiero chatear con un doctor ahora',
  WhatsAppTextVirtualWithDoctor: 'Necesito crear mi cuenta de MAPFRE DOC',
  PharmacyDelivery: 'http://farmaciadelivery.mapfre.com.pe/',
  MapfreDoc: 'https://mapfredoc.pe/login',
  MapfreTel: '2133358',
  MediktorDiagnostic: 'https://d37nt7og99q49p.cloudfront.net'
};
