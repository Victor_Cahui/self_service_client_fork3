import { IStepperItem } from '@mx/core/ui/lib/components/stepper/stepper.component';

export const VEHICLES_QUOTE_STEP: Array<IStepperItem> = [
  {
    name: 'Vehículo',
    value: 1,
    visible: true,
    route: 'vehicles/quote/detail/step1'
  },
  {
    name: 'Cotización',
    value: 2,
    visible: true,
    route: 'vehicles/quote/detail/step2'
  }
];

export const OPTIONAL_STEP = 2; // Posicion

export const VEHICLES_QUOTE_BUY_STEP: Array<IStepperItem> = [
  {
    name: 'Vehículo',
    value: 1,
    visible: true,
    route: 'vehicles/quote/buy/step1'
  },
  {
    name: 'Contratante',
    value: 2,
    visible: true,
    route: 'vehicles/quote/buy/step2'
  },
  {
    name: 'Inspección',
    value: 3,
    visible: false,
    route: 'vehicles/quote/buy/step3'
  },
  {
    name: 'Pago',
    value: 4,
    visible: true,
    route: 'vehicles/quote/buy/step4'
  }
];

// HEALTH
export const HEALTH_REFUND_STEP: Array<IStepperItem> = [
  {
    name: 'Atención',
    value: 1,
    visible: true,
    route: 'health/refund/request/step1'
  },
  {
    name: 'Documentación',
    value: 2,
    visible: true,
    route: 'health/refund/request/step2'
  }
];

export const SCHEDULE_QUOTE_STEP: Array<IStepperItem> = [
  {
    name: 'MODALIDAD',
    value: 1,
    visible: true,
    route: 'health/schedule/steps/1'
  },
  {
    name: 'PACIENTE',
    value: 2,
    visible: true,
    route: 'health/schedule/steps/2'
  },
  {
    name: 'HORARIO',
    value: 3,
    visible: true,
    route: 'health/schedule/steps/3'
  },
  {
    name: 'CONFIRMACIÓN',
    value: 4,
    visible: true,
    route: 'health/schedule/steps/4'
  }
];

export const SCHEDULE_COVID_QUOTE_STEP: Array<IStepperItem> = [
  {
    name: 'TIPO DE PRUEBA',
    value: 1,
    visible: true,
    route: 'health/schedule-covid/steps/0'
  },
  {
    name: 'PACIENTE',
    value: 2,
    visible: true,
    route: 'health/schedule-covid/steps/1'
  },
  {
    name: 'HORARIO',
    value: 3,
    visible: true,
    route: 'health/schedule-covid/steps/2'
  },
  {
    name: 'CONFIRMACIÓN',
    value: 4,
    visible: true,
    route: 'health/schedule-covid/steps/3'
  }
];

export const HOME_DOCTOR_STEP: Array<IStepperItem> = [
  {
    name: 'Ubicación',
    value: 1,
    visible: true,
    route: 'mapfre-services/home-doctor/step1'
  },
  {
    name: 'Datos',
    value: 2,
    visible: true,
    route: 'mapfre-services/home-doctor/step2'
  },
  {
    name: 'Confirmación',
    value: 3,
    visible: true,
    route: 'mapfre-services/home-doctor/step3'
  }
];
