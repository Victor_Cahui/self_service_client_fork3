import { environment } from '@mx/environments/environment';
import { IMenuItem } from '@mx/statemanagement/models/general.models';
import { APPS_ICON_OUT, HEART_ICON, HELMET_ICON, SHIELD_CHECK_ICON, TRAVEL_OUT_ICON } from './images-values';

export const CLINICS_SEARCH_OPTION = 'RED DE CLÍNICAS';

export function GET_MENU_LIST(): Array<IMenuItem> {
  return [
    {
      name: 'MIS PÓLIZAS',
      icon: SHIELD_CHECK_ICON,
      route: '/myPolicies',
      visible: true
    },
    {
      policies: ['MD_AUTOS', 'MD_SOAT', 'MD_SOAT_ELECTRO'],
      name: 'VEHÍCULOS',
      icon: 'icon-mapfre_022_car2',
      visible: true,
      collapse: true,
      hideIcon: true,
      children: [
        {
          policies: ['MD_AUTOS'],
          name: 'Seguro Vehicular',
          route: '/vehicles/vehicularInsurance',
          visible: true
        },
        {
          policies: ['MD_SOAT', 'MD_SOAT_ELECTRO'],
          name: 'SOAT',
          route: '/vehicles/soat',
          visible: true
        },
        {
          policies: ['MD_AUTOS'],
          name: 'Accidentes',
          route: '/vehicles/accidents',
          visible: true
        },
        {
          policies: ['MD_AUTOS'],
          name: 'Robos',
          route: '/vehicles/stolen',
          visible: true
        },
        {
          policies: ['MD_AUTOS'],
          name: 'Talleres',
          route: '/vehicles/shop',
          visible: false
        },
        {
          policies: ['MD_AUTOS'],
          name: 'Cotización',
          route: '/vehicles/quote',
          visible: true
        }
      ]
    },
    {
      policies: ['MD_SALUD', 'MD_EPS'],
      name: 'SALUD',
      icon: 'icon-mapfre_023_cross2',
      visible: true,
      collapse: true,
      hideIcon: true,
      children: [
        {
          name: 'Seguro de Salud',
          route: '/health/health-insurance',
          visible: true
        },
        {
          name: 'Cotización',
          route: '/health/quote',
          visible: true
        },
        {
          name: 'Cartas de Garantía',
          route: '/health/guarantee-letters',
          visible: true
        },
        {
          name: 'Reembolsos',
          route: '/health/refund',
          visible: environment.ACTIVATE_REFUND_HEALTH
        }
      ]
    },
    {
      policies: ['MD_HOGAR'],
      name: 'HOGAR',
      icon: 'icon-mapfre_024_house2',
      visible: true,
      collapse: true,
      hideIcon: true,
      children: [
        {
          name: 'Seguro de Hogar',
          route: '/household/household-insurance',
          visible: true
        },
        {
          name: 'Daños y robos',
          route: '/household/hurt-and-steal',
          visible: true
        },
        {
          name: 'Cotización',
          route: '/household/quote',
          visible: true
        }
      ]
    },
    {
      policies: ['MD_VIDA'],
      name: 'VIDA Y AHORROS',
      icon: HEART_ICON,
      visible: true,
      collapse: true,
      hideIcon: true,
      children: [
        {
          name: 'Seguro de vida/ahorro',
          route: '/life/life-insurance',
          visible: true
        },
        {
          name: 'Cotización',
          route: '/life/quote',
          visible: true
        }
      ]
    },
    {
      policies: ['MD_VIAJES'],
      name: 'VIAJES',
      icon: TRAVEL_OUT_ICON,
      visible: true,
      collapse: true,
      route: '/travel/quote',
      hideIcon: true
      // TODO: el servicio muestra/oculta las opciones si el usuario es o no contratante de MD_VIAJES
      // children: [
      //   {
      //     name: 'Seguro de viajes',
      //     route: '/travel/travel-insurance',
      //     visible: true
      //   },
      //   {
      //     name: 'Cotización',
      //     route: '/travel/quote',
      //     visible: true
      //   }
      // ]
    },
    {
      policies: ['MD_SCTR'],
      name: 'SCTR',
      icon: HELMET_ICON,
      route: '/sctr/sctr-insurance',
      hideIcon: true,
      visible: true
    },
    {
      policies: ['MD_DECESOS'],
      name: 'DECESOS',
      icon: 'icon-mapfre_149_bird_out',
      visible: true,
      collapse: true,
      hideIcon: true,
      children: [
        {
          name: 'Seguro de decesos',
          route: '/death/death-insurance',
          visible: true
        },
        {
          name: 'Cotización',
          route: '/death/quote',
          visible: true
        }
      ]
    },
    {
      name: 'APLICACIONES',
      icon: APPS_ICON_OUT,
      route: '/apps',
      visible: false
    },
    {
      policies: ['MD_SCTR', 'MD_AUTOS', 'MD_SALUD', 'MD_HOGAR', 'MD_VIDA', 'MD_DECESOS', 'MD_VIAJES'],
      name: 'MIS PAGOS',
      icon: 'icon-mapfre_028_pagos',
      route: '/payments',
      visible: true
    },
    {
      name: 'SERVICIOS MAPFRE',
      icon: 'icon-mapfre_151_services_out',
      route: '/mapfre-services',
      visible: true
    },
    {
      name: 'RED DE OFICINAS',
      icon: 'icon-mapfre_029_location-pin2',
      route: '/offices',
      visible: true
    },
    {
      policies: ['MD_SALUD', 'MD_EPS', 'MD_SCTR'],
      name: CLINICS_SEARCH_OPTION,
      icon: 'icon-mapfre_214_hospital_out',
      route: '/clinics',
      visible: true
    }
  ];
}
