import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import {
  BIRD_ICON,
  CAR_ICON,
  CROSS_ICON,
  HEART_OUT_ICON,
  HELMET_OUT_ICON,
  HOUSE_ICON,
  MAPFRE_CIRCLE_ICON,
  PERSONAL_ACCIDENT_ICON,
  SOAT_ICON,
  TRAVEL_ICON
} from './images-values';

export const POLICY_TYPES = {
  MD_AUTOS: {
    code: 'MD_AUTOS',
    title: 'SEGURO VEHICULAR',
    description: 'Vehiculos',
    icon: CAR_ICON,
    dropDownTitle: 'vehículos',
    dropDownSubTitle: 'Vehículo',
    path: '/vehicles/vehicularInsurance',
    staticBanners: '/assets/images/banners/vehicles'
  },
  MD_SALUD: {
    code: 'MD_SALUD',
    title: 'SEGURO DE SALUD',
    description: 'Salud',
    icon: CROSS_ICON,
    dropDownTitle: 'asegurados',
    dropDownSubTitle: 'Asegurado',
    path: '/health/health-insurance',
    staticBanners: '/assets/images/banners/health',
    staticBannersRefund: '/assets/images/banners/health/refund',
    subject: 'SALUD'
  },
  MD_VIDA: {
    code: 'MD_VIDA',
    title: 'SEGURO DE VIDA Y AHORRO',
    description: 'Vida y Ahorro',
    icon: HEART_OUT_ICON,
    dropDownTitle: 'beneficiarios',
    dropDownSubTitle: 'Beneficiario',
    path: '/life/life-insurance',
    staticBanners: '/assets/images/banners/life'
  },
  MD_HOGAR: {
    code: 'MD_HOGAR',
    title: 'SEGURO DE HOGAR',
    description: 'Hogar',
    icon: HOUSE_ICON,
    dropDownTitle: 'direcciones',
    dropDownSubTitle: 'Dirección',
    path: '/household/household-insurance',
    staticBanners: '/assets/images/banners/household',
    subject: 'HOGAR'
  },
  MD_DECESOS: {
    code: 'MD_DECESOS',
    title: 'SEGURO DE DECESOS',
    description: 'Decesos',
    icon: BIRD_ICON,
    dropDownTitle: 'asegurados',
    dropDownSubTitle: 'Asegurado',
    staticBanners: '/assets/images/banners/death',
    subject: 'DECESOS',
    path: '/death/death-insurance'
  },
  MD_SOAT: {
    code: 'MD_SOAT',
    title: 'SOAT',
    icon: SOAT_ICON,
    dropDownTitle: 'vehículos',
    dropDownSubTitle: 'Vehículo',
    path: ''
  },
  MD_SOAT_ELECTRO: {
    code: 'MD_SOAT_ELECTRO',
    title: 'SOAT',
    icon: SOAT_ICON,
    dropDownTitle: 'vehículos',
    dropDownSubTitle: 'Vehículo',
    path: ''
  },
  MD_SCTR: {
    code: 'MD_SCTR',
    title: 'SEGURO COMPLEMENTARIO DE TRABAJO DE RIESGO',
    description: 'SCTR',
    icon: HELMET_OUT_ICON,
    dropDownTitle: 'asegurados',
    dropDownSubTitle: 'Asegurado',
    path: '/sctr/sctr-insurance',
    subject: 'SCTR'
  },
  MD_EPS: {
    code: 'MD_EPS',
    title: 'SEGURO DE SALUD',
    icon: CROSS_ICON,
    dropDownTitle: 'asegurados',
    dropDownSubTitle: 'Asegurado',
    path: '/health/health-insurance',
    subject: 'EPS'
  },
  MD_OTROS: {
    code: 'MD_OTROS',
    title: 'Otros',
    icon: MAPFRE_CIRCLE_ICON,
    dropDownTitle: 'asegurados',
    dropDownSubTitle: 'Asegurado',
    path: '/health/health-insurance',
    subject: 'OTROS'
  },
  MD_VIAJES: {
    code: 'MD_VIAJES',
    title: 'SEGURO DE VIAJES',
    description: 'Viajes',
    icon: TRAVEL_ICON,
    dropDownTitle: 'asegurados',
    dropDownSubTitle: 'Asegurado',
    path: '/travel/travel-insurance',
    subject: 'VIAJES',
    staticBanners: '/assets/images/banners/travel'
  },
  MD_ACCIDENTES: {
    code: 'MD_ACCIDENTES',
    icon: PERSONAL_ACCIDENT_ICON
  }
};

export const INSURED_TYPE = {
  INSURED_CODE: 2,
  DEPENDENT_CODE: 3,
  BENEFICIARY_CODE: 4
};

export const ACCIDENTES_PERSONALES = {
  COD_RAMO: 630
};

export const NECESIDAD_FUTURA = {
  COD_RAMO: 401
};

export const NECESIDAD_INMEDIATA = {
  COD_RAMO: 400
};

// Para identificar tipo de SCTR
export const SCTR_PENSION = {
  COD_CIA: 2,
  COD_RAMO: 701
};

export const SCTR_HEALTH = {
  COD_CIA: 3,
  COD_RAMO: 702
};

// Ramos de Salud que requieren opcion Buscar Clínicas
export const HEALTH_CLINIC_SEARCH = {
  COD_RAMO: [114, 116]
};

// Tipo de coberturas Salud
export const TYPE_AMBULATORY_CARE = 'AT_AMBU';
export const TYPE_HOSPITAL_CARE = 'AT_HOSP';
export const TYPE_MATERNITY = 'AT_MATE';

// Tipo de poliza para cuadro comparacion
export enum POLICY_INDICATOR {
  MY_POLICY = 'MIPOLIZA',
  OTHER = 'NORMAL',
  RECOMMENDED = 'RECOMENDAMOS'
}

export const CURRENT_OWN_POLICY = 'POLIZA_VIGENTE_PROPIA';
export const CURRENT_POLICY = 'POLIZA_VIGENTE_EXTERNA';

export const STATIC_BANNERS: IPathImageSize = {
  xs: '/banner-xs.png',
  sm: '/banner-sm.png', // static-banners
  md: '/banner-md.png',
  lg: '/banner-lg.png', // static-banners
  xl: '/banner-xl.png' // static-banners
};

export const LANDING_ID = {
  HOME_CHANGE: 'HOGAR_COMPARACION_POLIZA_CAMBIO',
  HOME_MULTIRISK: 'HOGAR_COMPARACION_POLIZA_MULTIRIESGO'
};

export const RELATION_TYPES = {
  RELATION_HEADLINE: {
    relacionId: 10,
    relacionDescripcion: 'TITULAR'
  }
};
