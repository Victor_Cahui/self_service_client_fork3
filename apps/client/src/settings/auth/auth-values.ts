export const CONTENT_TYPE = 'application/x-www-form-urlencoded';
export const CLIENT_ID = '099153c2625149bc8ecb3e85e03f0022';
export const GRANT_TYPE = 'password';
export const GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';

export const GROUP_TYPE_ID = 5;
export const GROUP_TYPE_ID_PERSON = 5;
export const GROUP_TYPE_ID_COMPANY = 2;
export const GROUP_TYPES = [GROUP_TYPE_ID_PERSON, GROUP_TYPE_ID_COMPANY];

export const GROUP_TYPE = [
  {
    text: 'Empresa',
    value: GROUP_TYPE_ID_COMPANY
  },
  {
    text: 'Persona',
    value: GROUP_TYPE_ID_PERSON
  }
];

export const KEY_USER_TOKEN = 'user_token';
export const KEY_EXPIRES_IN = 'expires_in';
export const KEY_PERMISSIONS = 'user_permissions';
export const KEY_USER_REFRESH_TOKEN = 'user_refresh_token';
export const KEY_DOC_TYPE = 'doc_type';
export const KEY_DOC_NUMBER = 'doc_number';
export const KEY_RUC_NUMBER = 'rucNumber';
export const KEY_GROUP_TYPE_ID = 'groupTypeId';
export const KEY_DOC_NUMBER_CLIENT = 'documentNumber';
export const KEY_DOC_TYPE_AUTH = 'doc_type_auth';
export const KEY_DOC_NUMBER_AUTH = 'doc_number_auth';
export const KEY_GROUP_TYPE_AUTH = 'group_key_auth';

export const KEY_ACTIONS = {
  ENABLE: 'HABILITACION',
  RECOVER: 'RECUPERACION'
};
