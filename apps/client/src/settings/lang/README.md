# SETTING LANGUAGE

## Archivos

* **auth.lang.ts**: textos para vista de autenticación del usuario.

* **client.lang.ts**: textos para edición de datos del cliente.

* **general.lang.ts**: configuración de textos generales de titulos y labels de cards, botones y mensajes para vistas de Home y Perfil.

## Estructura
 export const nameLang = {
    Buttons: {},
    Labels: {},
    Texts: {},
    Titles: {},
    Messages: {}
 }


