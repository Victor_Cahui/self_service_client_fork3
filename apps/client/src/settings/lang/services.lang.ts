import { IModalHowWorks } from '@mx/statemanagement/models/general.models';
import { PolicyLang } from './policy.lang';

export const ServicesLang = {
  Buttons: {},
  Labels: {
    DaysLeft: 'días restantes'
  },
  Texts: {
    NoService: 'Ningún servicio solicitado',
    OneService: 'Servicio solicitado',
    Services: 'Servicios solicitados'
  },
  Titles: {
    ServicesRequest: '¿Qué necesitas?',
    AmbulanceServices: '¿Necesitas una ambulancia?',
    ServicesInProgress: 'Atenciones Agendadas',
    ServicesHistory: 'Atenciones Pasadas',
    SubstituteDriverRequestSuccess: 'SOLICITUD ENVIADA'
  },
  Messages: {
    ServicesRequest: 'Pide lo que quieras, estamos en confianza',
    AmbulanceServices: 'No te preocupes, nosotros nos encargamos de ayudarte',
    AmbulanceServicesHelp: `<div class="g-font--medium g-font-lg--20 g-font--18 g-line--10 mb-xs--1 text-center text-md-center
            text-lg-left g-font--medium g-c--gray8">
            Llama a nuestra central de asistencia 24 horas
        </div>
        <div class="g-line--10 g-font--light text-center text-md-center text-lg-left ">
          Te indicaremos que hacer a continuación y enviaremos ayuda si es necesario.
        </div>`,
    AmbulanceServicesHelpMsgWhatsapp: 'Necesito una ambulancia',
    ServiceNotFound: 'Has ingresado el nombre de un servicio que no existe',
    ServicesNotAssociated: 'No cuenta con ningún servicio asociado a sus pólizas.',
    AppNotFound: 'Has ingresado el nombre de una aplicación que no existe',
    NoRegister: 'No se registran atenciones en curso',
    NoRegisterHistory: 'No se registran atenciones anteriores',
    NoRegisterAssists: 'No se registra ninguna atención',
    SubstituteDriverRequestSuccess:
      'Hemos registrado tu solicitud y en breve nos contactaremos para confirmar el servicio.',
    InvalidDate: `El servicio se debe reservar con un mínimo de 24 horas de antelación. Por favor modifica la fecha y el
      horario de la solicitud del servicio.<br>(Hora actual: {{dateTime}}).`
  },
  Utils: {}
};

export const ServiceFormLang = {
  NeedNow: 'Necesito el servicio con urgencia.',
  Request: 'Solicitar',
  Benefits: 'Beneficios',
  Symptom: 'Síntomas',
  Policy: PolicyLang.Labels.Policy,
  InsertTheInformation: 'Ingresa la siguiente información.',
  Patient: 'Paciente',
  Symptoms: 'Síntomas',
  TypeSpecialty: 'Tipo de medicina',
  VehicleType: 'Tipo de vehículo',
  Unlimited: 'Ilimitado',
  Free: '¡GRATIS!',
  WithDiscountMapfre: 'Con descuento MAPFRE',
  ContactPhone: 'Teléfono de contacto',
  AvailableWithBenefit: {
    first: 'Disponibles',
    second: 'con beneficio'
  },
  CashPayment: 'Realice el pago en efectivo directamente al chofer.',
  CashPaymentDoctor: 'Realice el pago en efectivo directamente al médico.',
  RequestSubstituteDriver: 'Solicitar chofer de reemplazo',
  RequestHomeDoctor: 'UBICACIÓN DEL SERVICIO',
  StartPoint: 'Punto de partida',
  LocateStartPoint: 'Ubica el punto de partida',
  ReferenceStartPoint: 'Referencia del punto de partida',
  LocateEndPoint: 'Ubica el punto de destino',
  ReferenceEndPoint: 'Número de interior / departamento (opcional)',
  EndPoint: 'Punto de destino',
  Questions: {
    first: '1. ¿Cuándo y dónde requieres el servicio?',
    secondDriver: '2. Selecciona el vehículo para el servicio',
    secondDoctor: '2. Selecciona el paciente e indícanos el tipo de especialidad'
  },
  WarningText: `El servicio solicitado fuera de las áreas marcadas tendrá un costo adicional, además de una previa evaluación.`,
  SummaryTitle: 'RESUMEN DE TU SOLICITUD',
  Scheduled: 'Atención',
  Driver: 'Chofer',
  Doctor: 'Médico',
  From: 'Desde',
  To: 'Hasta',
  Reference: 'Referencia',
  Address: 'Escribe una dirección',
  MessageCashPayment: 'Pago en efectivo: {{price}}',
  AttendedService: 'SERVICIO ATENDIDO',
  RejectedService: 'SERVICIO RECHAZADO',
  ToBeAssigned: 'POR ASIGNAR',
  ToBeConfirmed: 'Por confirmar',
  Unconfirmed: 'Sin confirmar',
  MessageConfirmServiceFree: 'Servicio Gratuito',
  BtnCancel: 'Cancelar',
  BtnConfirmation: 'Confirmar',
  BtnConfirmationLocation: 'Confirmar Ubicación'
};

export const SubstituteDriverLang = {
  ScheduledService: 'CHOFER PROGRAMADO',
  WarningDetails: `Para editar o cancelar el servicio de chofer de reemplazo solicitado por favor comunícate
      con el SI24 ({{phone}}).`,
  MessageConfirmService: `El chofer llegará a recogerte en el lugar y hora acordada`,
  MessageConfirmServicePrice: `, recuerda contar con {{price}} en efectivo.`,
  MessageConfirmServiceFree: `. En esta ocasión el servicio es gratuito.`
};

export const HomeDoctorLang = {
  ScheduledService: 'MÉDICO PROGRAMADO',
  WarningDetails: `Para editar o cancelar el servicio de médico a domicilio solicitado por favor comunícate con el SI24 ({{phone}}).`,
  MessageConfirmService: `El médico se presentará en tu domicilio a la hora acordada`,
  MessageConfirmServicePrice: `, recuerda contar con {{price}} en efectivo.`,
  MessageConfirmServiceFree: `. En esta ocasión el servicio es gratuito.`,
  SelectOnMap: 'Seleccionar destino en mapa'
};

export const SearchPlaceLang = {
  Address: 'Dirección',
  WarmingPlace: 'No se encontraron resultados.',
  InvalidZone: 'Fuera de zona permitida.'
};

// Modales
export const HowSubstituteDriverWorks: IModalHowWorks = {
  link: ' ¿Cómo funciona?',
  title: 'CÓMO FUNCIONA EL SERVICIO DE CHOFER DE REEMPLAZO',
  warning: 'El servicio se debe reservar con un mínimo de {{hours}} horas de anticipación.',
  body: {
    text: `Si no puedes conducir, por ser cliente MAPFRE puedes disfrutar del servicio de chofer de reemplazo. Cada póliza
    tiene asignado un número de servicios por vigencia, que se irán descontando a medida que los utilices.`,
    alignClass: 'text-lg-left'
  },
  steps: {
    title: {
      text: 'Pasos para utilizar el servicio:',
      alignClass: 'text-lg-left'
    },
    showStepNumber: true,
    items: [
      {
        icon: 'assets/images/services/icon-tech-track.svg',
        text: 'Indícanos dónde y cuándo necesitarás el servicio.'
      },
      {
        icon: 'assets/images/services/icon-document-aprove.svg',
        text: 'Te asignaremos un chofer que irá a recogerte en el día y hora indicados.'
      },
      {
        icon: 'assets/images/services/icon-mobile-alert.svg',
        text: 'Te llamaremos unas horas antes para confirmar el servicio.'
      },
      {
        icon: 'assets/images/services/icon-car-aprove.svg',
        text: 'El chofer te llevará a la dirección que indicaste en tu reserva.'
      }
    ]
  }
};

export const HowHomeDoctorWorks: IModalHowWorks = {
  link: ' ¿Cómo funciona?',
  title: 'CÓMO FUNCIONA EL SERVICIO DE MÉDICO A DOMICILIO',
  warning: `El servicio puede ser solicitado con {{hours}} horas de anticipación. Además, se encuentra circunscrito a ciertas zonas y
            distritos especificados en su plan de salud.`,
  body: {
    text: `Este servicio ofrece atención médica a domicilio de las especialidades Medicina General y Pediatría. Mientras en Provincias
          se podrá recibir la atención de Medicina General. El deducible es único (Según el plan de salud) y la cobertura de medicamentos
          (Productos de salud y platino: 5 días / Hogar: 1 día) y servicios de apoyo al diagnóstico al 100%.`,
    alignClass: 'text-lg-left'
  },
  steps: {
    title: {
      text: 'Pasos para utilizar el servicio:',
      alignClass: 'text-lg-left'
    },
    showStepNumber: true,
    items: [
      {
        icon: 'assets/images/services/icon-tech-track.svg',
        text: 'Indícanos quién, dónde y cuándo se necesitará el servicio.'
      },
      {
        icon: 'assets/images/services/icon-document-doctor.svg',
        text: 'Te asignaremos un médico para atenderte en la fecha y hora indicada.'
      },
      {
        icon: 'assets/images/services/icon-mobile-alert.svg',
        text: 'Te llamaremos unas horas antes para confirmar el servicio.'
      },
      {
        icon: 'assets/images/services/icon-doctor.svg',
        text: 'El médico te atenderá en la dirección que indicaste en tu solicitud.'
      }
    ]
  }
};

export const HowAutomaticDebitWorks: IModalHowWorks = {
  title: 'DÉBITO AUTOMÁTICO',
  body: {
    text: `Al afiliarte se cargará automáticamente a tu tarjeta el pago de tus pólizas y podrás disfrutar de más beneficios.`
  },
  steps: {
    title: { text: 'Ventajas de afiliarte al débito automático:' },
    showStepNumber: false,
    items: [
      {
        icon: 'assets/images/services/icon-lock.svg',
        title: 'Mayor seguridad',
        text: 'No tienes que preocuparte por retirar dinero en efectivo.'
      },
      {
        icon: 'assets/images/services/icon-time.svg',
        title: 'Ahorro de tiempo',
        text: 'No pierdas tu tiempo en el banco haciendo largas filas.'
      },
      {
        icon: 'assets/images/services/icon-document-payment.svg',
        title: 'Pagos al día',
        text: 'Realiza tus pagos automáticamente y evita retrasarte.'
      }
    ]
  }
};
