export const MapLang = {
  CLINIC: {
    Title: 'listado de clínicas',
    defaultCoords: {
      latitude: -12.128891,
      longitude: -77.027102
    }
  },
  OFFICE: {
    Title: 'listado de oficinas',
    defaultCoords: {
      latitude: -12.128491,
      longitude: -77.025102
    }
  }
};
