import { HEALTH_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import {
  Content,
  ContentHowtoWork,
  ContentInsurancePlans,
  IModalHowWorks,
  PaymentsOptions
} from '@mx/statemanagement/models/general.models';
import { MONEY_ICON, TABLET_PLUS_ICON, TECH_CHECK_ICON, TIME_ICON } from '../constants/images-values';
import { PolicyLang } from './policy.lang';

export const HealthLang = {
  Buttons: {
    FormatDownload: 'Descargar formato',
    RequestRefund: 'Solicitar reembolso'
  },
  Links: {
    Send: 'COTIZA AHORA',
    ViewMap: 'Ver en el mapa',
    Here: 'aquí.'
  },
  Labels: {
    HoureChange: 'Horarios pueden variar',
    PolicySelection: PolicyLang.Labels.Policy,
    PatientSelection: 'Paciente',
    InvoiceNumber: 'Nro. de factura',
    PreInvoiceNumber: 'Nro. de prefactura',
    DateEmission: 'Fecha de emisión',
    SeePaymentPlaces: 'Ver sitios de pago'
  },
  Texts: {
    Cotizar: 'Estás a un paso de encontrar el seguro con la cobertura más completa para ti y tu familia.',
    SearchPlaceholder: 'Buscar por coberturas',
    Coverages: 'Coberturas',
    Coverage: 'Cobertura',
    Deductible: 'Deducible',
    CoverageInformation: 'Información de cobertura',
    RefundPdfFile: 'formato',
    PaymentPlaces:
      'Realiza el pago de tu factura mediante tu banco de preferencia o mediante su plataforma digital (web o app).'
  },
  Titles: {
    clinicsList: 'LISTADO DE CLÍNICAS',
    Cotizar: 'Cotiza y compra tu seguro',
    RefundForm: 'Solicitud de reembolso',
    AccessToClinics: '¿Deseas acceder de nuestra red de Clínicas?',
    PendingInvoices: 'Facturas pendientes',
    PaymentPlaces: 'Realiza tu pago mediante el banco'
  },
  Messages: {
    exclusionsOneInsuredTitular: '<strong>{{nameInsured}}</strong> tienes las siguientes exclusiones:',
    observationsOneInsuredTitular: '<strong>{{nameInsured}}</strong> tienes las siguientes observaciones:',
    exclusionsOneInsured: '<strong>{{nameInsured}}</strong> tiene las siguientes exclusiones:',
    observationsOneInsured: '<strong>{{nameInsured}}</strong> tiene las siguientes observaciones:',
    withOutExclusions: 'El asegurado no tiene exclusiones.',
    withOutExclusionsTitular: '<strong>{{nameInsured}}</strong> no tienes exclusiones.',
    withOutExclusionsOneInsured: '<strong>{{nameInsured}}</strong> no tiene exclusiones.',
    withOutObservations: 'El asegurado no tiene observaciones.',
    withOutObservationsTitular: '<strong>{{nameInsured}}</strong> no tienes observaciones.',
    withOutObservationsOneInsured: '<strong>{{nameInsured}}</strong> no tiene observaciones.',
    AccessToClinics: `Te ofrecemos diversas opciones para mantenerte a ti y a tu familia protegidos y tranquilos en su
      día a dia. Goza de nuestras coberturas y servicios adquiriendo nuestros productos de Salud`
  }
};

export const HOW_IT_WORKS: ContentHowtoWork = {
  title: '¿CÓMO FUNCIONA?',
  items: [
    'Solicita que un agente te contacte para que te pueda dar más información',
    'Contrata la opción que más te convenga',
    '¡Y listo! Recibe tu póliza por correo electrónico'
  ]
};

export const HOW_REFUND_WORKS: IModalHowWorks = {
  title: '¿CÓMO FUNCIONA?',
  body: {
    text: `Los gastos deberán presentarse dentro de los {days} días de su facturación y con todo
      comprobante de pago emitido a nombre del paciente adjunto.`,
    alignClass: 'w-10 g-max-w-lg--69 mx-auto g-px--25'
  },
  steps: {
    showStepNumber: true,
    items: [
      {
        icon: TECH_CHECK_ICON,
        text: 'Ingresa la información en el formulario de reembolso'
      },
      {
        icon: MONEY_ICON,
        text: 'Adjunta la copia de la boleta o factura de la atención'
      },
      {
        icon: TABLET_PLUS_ICON,
        text: 'Si tienes exámenes médicos también adjúntalos a la solicitud'
      },
      {
        icon: TIME_ICON,
        text: 'En plazo de 48 horas te daremos una respuesta sobre tu solicitud'
      }
    ]
  }
};

export const PAYMENTS_OPTIONS: PaymentsOptions = {
  title: 'Pago 100% seguro',
  url: './assets/images/banks/',
  images: ['card-visa.svg', 'card-mastercard.svg', 'card-american.svg', 'card-dinners.svg']
};

export const INSURANCE_TOP: ContentInsurancePlans = {
  title: 'TOP SEGUROS SALUD',
  textBtnGetQuote: 'CONTRATAR',
  urlGetQuote: 'https://salud.mapfre.com.pe/',
  plans: [
    {
      title: 'VIVA SALUD',
      subtitle: 'EL MÁS ECONÓMICO',
      content: [
        {
          title: 'Beneficio Máximo Anual',
          price: { amount: 'S/ 2’000,000', desc: 'por persona' }
        },
        {
          title: 'Coberturas',
          items: [
            {
              text:
                'Cobertura Integral: Atención Ambulatoria, Hospitalaria y por Maternidad en una amplia red de clínicas afiliadas.'
            },
            {
              text:
                'Red de Centros Médicos MAPFRE, 2 redes de clínicas afiliadas en Lima y una completa red de atención en Provincias.'
            },
            { text: 'Cobertura Oncológica al 100% (incluye terapia biológica).' },
            { text: 'Coberturas de Emergencias Accidentales y Médicas con opción a traslado en ambulancia.' },
            { text: 'Programa de Enfermedades Crónicas "Vivir en Salud".' },
            { text: 'Médico a Domicilio.' },
            { text: 'Cobertura de Enfermedades Congénitas del Recién Nacido.' }
          ]
        }
      ],
      price: 'desde S/ 1,610 al año*',
      conditionPrice: '*Tarifa referencial para titular hasta 30 años. Precio incluye IGV.'
    },
    {
      title: 'VIVA SALUD PLUS',
      subtitle: 'SOLO LO QUE NECESITAS',
      content: [
        {
          title: 'Beneficio Máximo Anual',
          price: { amount: 'S/ 2’000,000', desc: 'por persona' }
        },
        {
          title: 'Coberturas',
          items: [
            { text: '3 redes de atención en Lima. Incluye la Clínica Delgado en la Red 3.' },
            { text: 'Atención por Reembolso.' },
            { text: 'Programa de Chequeo Médico Anual cubierto al 100%.' }
          ]
        }
      ],
      price: 'desde S/ 2,184 al año*',
      conditionPrice: '*Tarifa referencial para titular hasta 30 años. Precio incluye IGV. '
    },
    {
      title: 'TRÉBOL SALUD',
      subtitle: 'EL MÁS COMPLETO',
      content: [
        {
          title: 'Beneficio Máximo Anual',
          price: { amount: 'S/ 5’000,000', desc: 'por persona' }
        },
        {
          title: 'Coberturas',
          items: [
            {
              text:
                'La más completa red de clínicas afiliadas. 5 redes en Lima, incluye la Clínica Delgado en la Red 3.'
            },
            { text: 'Programa de Asistencia en viaje.' },
            { text: 'Cobertura de Enfermedades Congénitas No Conocidas y Trasplante de órganos.' }
          ]
        }
      ],
      price: 'desde S/ 2,540 al año*',
      conditionPrice: '*Tarifa referencial para titular hasta 30 años. Precio incluye IGV.'
    }
  ]
};

// Titulos - textos
export const MYPOLICY: Content = {
  title: HEALTH_COVERAGE_TABS[0].name.toUpperCase(),
  content: 'Tu póliza {{typeInsured}} te cubre a ti y a tus asegurados en las siguientes coberturas:'
};

export const AMBULATORY_CARE: Content = {
  title: HEALTH_COVERAGE_TABS[1].name.toUpperCase(),
  // TODO: Estos textos están desactualizados, por el momento es mejor ocultarlos para que sea facil su actualización futura. (AM-65)
  // content: `Cubre atenciones ambulatorias relativas a prestaciones de capa simple y/o compleja.`,
  content: '',
  action: {
    text: 'Ver en el mapa',
    routeLink: '/clinics/search'
  },
  table: {
    columns: ['CLÍNICAS', 'DEDUCIBLE', 'COBERTURA']
  }
};

export const HOSPITAL_CARE: Content = {
  title: HEALTH_COVERAGE_TABS[2].name.toUpperCase(),
  // TODO: Estos textos están desactualizados, por el momento es mejor ocultarlos para que sea facil su actualización futura. (AM-65)
  // content: `Cubre atenciones hospitalarias relativas a prestaciones de capa simple y/o compleja.`,
  content: '',
  warning: 'Recuerda que el período de carencia para Atención Hospitalaria es de 30 días.',
  action: {
    text: 'Ver en el mapa',
    routeLink: '/clinics/search'
  },
  table: {
    columns: ['CLÍNICAS', 'DEDUCIBLE', 'COBERTURA']
  }
};

export const MATERNITY: Content = {
  title: HEALTH_COVERAGE_TABS[3].name.toUpperCase(),
  content: `En caso de parto natural y/o múltiple, control pre-natal, post-natal y control de niño sano y circunsición en recién
  nacidos únicamente mientras la madre se encuentre hospitalizada a causa de parto.`,
  warning: 'Recuerda que la cobertura maternidad solo sirve para titular o cónyuge mujer.',
  table: {
    columns: ['CLÍNICAS', 'DEDUCIBLE', 'COBERTURA']
  }
};

export const WAITING_PERIOD: Content = {
  title: HEALTH_COVERAGE_TABS[4].name.toUpperCase(),
  content: `Los períodos de carencia y espera mostrados corresponden a las condiciones generales del producto contratado
  y aplican para asegurados nuevos al sistema. En caso de haber migrado desde otro plan de salud con continuidad, estos plazos
  podrían variar. Revisa las observaciones y/o condiciones especiales de tu póliza para mayor información.`
};

export const CONDITIONED: Content = {
  title: HEALTH_COVERAGE_TABS[5].name.toUpperCase(),
  content: 'Descarga el condicionado para revisar las condiciones y exclusiones particulares de tu póliza.'
};

export const FOIL_ATTACHED: Content = {
  title: HEALTH_COVERAGE_TABS[6].name.toUpperCase(),
  content: ''
};

export const EXCLUSSIONS: Content = {
  title: HEALTH_COVERAGE_TABS[7].name.toUpperCase(),
  content: 'Selecciona un asegurado para revisar sus exclusiones.'
};

export const OBSERVATIONS: Content = {
  title: HEALTH_COVERAGE_TABS[8].name.toUpperCase(),
  content: 'Selecciona un asegurado para revisar sus observaciones.'
};

export const TOOLTIPS_HEALTH: Content = {
  title: 'Suma asegurada',
  content: 'Es el beneficio máximo anual por persona.'
};

export const REFUND_HEALT_LANG = {
  SearchProvider: {
    RUC: 'Número de RUC',
    Social: 'Razón social',
    Verify: 'verificada',
    NotFound: 'no verificada',
    Error: 'Por favor, ingrésala manualmente'
  },
  StepOne: {
    Title: 'DATOS DE LA ATENCIÓN',
    Description:
      'Para iniciar por favor ingresa el número de RUC del establecimiento donde recibiste la atención médica.',
    Labels: {
      IncidenceDate: 'Fecha de incidencia',
      TreatmentDate: 'Fecha de tratamiento',
      BenefitType: 'Tipo de beneficio',
      Amount: 'Importe (S/)'
    }
  },
  StepTwo: {
    Title: 'DOCUMENTACIÓN',
    Description: `Para concluir con la solicitud necesitaremos que nos adjuntes la siguiente documentación (formatos
      compatibles JPEG, PNG o PDF):`,
    DescriptionModal: `Para adjuntar un comprobante es necesario validar el RUC, por favor verifica que el número en
      el archivo corresponda al ingresado (puedes adjuntar varios recibos a un número de RUC).`,
    TitleModal: 'ADJUNTAR COMPROBANTES DE PAGO ',
    One: {
      Title: '2.1 Formato de solicitud',
      Description: 'Adjunta la hoja de formato de solicitud (máx. 2):',
      FormatQuestion: '¿No tienes el formato?',
      FormatDownload: 'Descargar formato'
    },
    Two: {
      Title: '2.2 Comprobantes de pago',
      Description: 'Adjunta como mínimo un comprobante con RUC (máx. 10):',
      Button: 'Subir comprobante(s)'
    },
    Three: {
      Title: '2.3 Documentos opcionales',
      Description: '(máx. 10):'
    }
  },
  StepThree: {
    Title: 'RESUMEN DE SOLICITUD DE REEMBOLSO',
    Description: 'Antes de enviar tu solicitud por favor verifica que los datos sean correctos.'
  },
  FnishRequest: {
    Title: 'SOLICITUD REPORTADA',
    Desciption1: 'Nos contactaremos contigo en un plazo máximo de',
    Desciption2: 'horas para comunicarte el estado de tu solicitud.'
  },
  RequestList: {
    DetailTitle: 'Detalle de tu solicitud',
    NoRegisterRequest: 'No se registra ninguna solicitud',
    NoRegisterHistoryRequest: 'No se registra solicitudes anteriores',
    NoRegisterProcessRequest: 'No se registra solicitudes pendientes',
    OnePendingRequest: 'Tienes {{number}} solicitud pendiente',
    PendingRequest: 'Tienes {{number}} solicitudes pendientes',
    RequestLabel: 'Solicitud',
    PatientLabel: 'Paciente',
    RefundLabel: 'Reembolso',
    TracingMessage: 'En menos de 48 horas podrás hacer el seguimiento de tu solicitud.',
    ForInformation: 'Si tienes consultas puedes comunicarte al 213 3333',
    DownLoadRequest: 'Ver archivos adjuntos',
    ContactAgent: 'Para recibir tu reembolso un agente se contactará contigo para realizar la transacción.'
  }
};

export const HOME_DOCTOR_LANG = {
  StepOne: {
    Title: 'DATOS DE LA ATENCIÓN',
    Description:
      'Para iniciar por favor ingresa el número de RUC del establecimiento donde recibiste la atención médica.',
    Labels: {
      IncidenceDate: 'Fecha de incidencia',
      TreatmentDate: 'Fecha de tratamiento',
      BenefitType: 'Tipo de beneficio',
      Amount: 'Importe (S/)'
    }
  },
  StepTwo: {
    Title: 'DATOS DEL SERVICIO',
    Labels: {
      Policy: 'Seleccione póliza*',
      Patient: 'Seleccione paciente*',
      Specialty: 'Especialidad*',
      Telephone: 'Teléfono de contacto*',
      ReasonForConsultation: 'Motivo de consulta'
    }
  },
  StepThree: {
    Title: 'RESUMEN DE SOLICITUD',
    Labels: {
      Specialty: 'Especialidad',
      Patient: 'Paciente',
      Telephone: 'Teléfono de contacto',
      Address: 'Dirección',
      ReferenceAddress: 'Referencia de la ubicación',
      PayPerConsultation: 'Pago por consulta'
    },
    Texts: {
      SendRequestHomeDoctor: 'La solicitud del servicio de Médico a Domicilio se ha enviado con éxito',
      IndicationAfterConfirming:
        'Despues de confirmar esta solicitud <strong>un asesor te contactará vía telefónica lo más pronto posible</strong> para confirmar fecha y hora de la consulta.',
      ContactAdviser:
        '<strong>Un asesor te contactará vía telefónica lo más pronto posible para confirmar los detalles de la solicitud</strong>'
    }
  }
};
