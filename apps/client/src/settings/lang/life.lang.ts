import { LIFE_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import {
  Content,
  ContentHowtoWork,
  ContentInsurancePlans,
  PaymentsOptions
} from '@mx/statemanagement/models/general.models';

export const LifeLang = {
  Links: {
    Send: 'COTIZA AHORA'
  },
  Labels: {
    TypeInsurance: 'Seleccione un seguro',
    LifetimePolicy: 'Póliza vitalicia'
  },
  Texts: {},
  Titles: {
    Form: 'Cotiza y compra tu seguro',
    Subtitle: '¿Qué seguro necesitas?'
  },
  Messages: {
    messageSent: ''
  }
};

// Contenido de Landing de Vida
export const INSURANCE_TOP: ContentInsurancePlans = {
  title: 'TOP SEGUROS',
  textBtnGetQuote: 'CONTRATAR',
  plans: [
    {
      title: 'FONDO UNIVERSITARIO',
      subtitle: 'PERFECTO PARA AHORRAR',
      content: [
        {
          text: 'Ahorra para la universidad de tus hijos o para lo que tu quieras.',
          title: 'Coberturas',
          items: [
            { text: 'Capital de vida: Recibe tu dinero en el plazo que elijas.' },
            {
              text:
                'Capital de fallecimiento: Tus beneficiarios reciben una renta anual durante 5 años. Suma asegurada + bonos anuales.'
            }
          ]
        },
        {
          title: 'Beneficios',
          items: [
            { text: 'Acceso a prestamos y rescates.' },
            { text: 'Elige tu suma asegurada y el plazo.' },
            { text: 'Fracciona los pagos como quieras.' },
            { text: 'Recibe bonos anuales.' }
          ]
        }
      ],
      price: 'desde S/ 100 al mes'
    },
    {
      title: 'CON VIDA',
      subtitle: 'EL MÁS ECONÓMICO',
      content: [
        {
          text: 'Protege a tu familia asegurando su futuro.',
          title: 'Coberturas',
          items: [
            {
              text: 'Capital de fallecimiento: Tus beneficiarios reciben el capital asegurado en caso de fallecimiento.'
            },
            { text: 'Plazo fijo o renovable anual (temporal).' }
          ]
        },
        {
          title: 'Beneficios',
          items: [
            { text: 'Aseguramos a tus seres queridos desde el primer día.' },
            { text: 'Elige tu suma asegurada y el plazo.' },
            { text: 'Fracciona los pagos como quieras.' }
          ]
        }
      ],
      price: 'desde S/ 116 al mes'
    },
    {
      title: 'MUJER INDEPENDIENTE',
      subtitle: 'El más completo',
      content: [
        {
          text: 'La mejor protección para toda la vida.',
          title: 'Coberturas',
          items: [
            {
              text: 'Capital de fallecimiento: Tus beneficiarios reciben el capital asegurado en caso de fallecimiento.'
            },
            { text: 'Coberturas adicionales: muerte accidental, invalidez total permanente, diagnostico de cáncer.' }
          ]
        },
        {
          title: 'Beneficios',
          items: [
            { text: 'Aseguramos a tus seres queridos desde el primer día.' },
            { text: 'Recibe bonos anuales.' },
            { text: 'Fracciona los pagos como quieras.' }
          ]
        }
      ],
      price: 'desde S/ 127 al mes'
    }
  ]
};

export const HOW_TO_WORK: ContentHowtoWork = {
  title: '¿CÓMO FUNCIONA?',
  items: [
    'Ingresa los datos de la persona a asegurar',
    'Contrata la opción que más te convenga',
    '¡Y listo! Recibe tu póliza por correo electrónico'
  ]
};

export const PAYMENTS_OPTIONS: PaymentsOptions = {
  title: 'Pago 100% seguro',
  url: './assets/images/banks/',
  images: ['card-visa.svg', 'card-mastercard.svg', 'card-american.svg', 'card-dinners.svg']
};

export const TYPE_INSURANCE_LIST = [
  { text: 'Vida', value: 'Vida', selected: false, disabled: false },
  { text: 'Ahorro', value: 'Ahorro', selected: false, disabled: false },
  { text: 'Vida y Ahorro', value: 'Todos', selected: false, disabled: false }
];

// Titulos - textos
export const COVERAGE: Content = {
  title: LIFE_COVERAGE_TABS[0].name,
  content: 'Tu póliza {{typeInsured}} te cubre en los siguientes casos:'
};

export const EXCLUSIONS: Content = {
  title: LIFE_COVERAGE_TABS[1].name,
  content: `
    <p>Los siniestros ocasionados como consecuencia de guerra civil o internacional (declarada o no) o servicio militar
    o policial de cualquier índole.</p>
    <p>El homicidio doloso cometido en calidad de autor o cómplice por un BENEFICIARIO de esta póliza o quien pudiera reclamar la
    indemnización. Esta exclusión será aplicable solo a tal(es) autor(es) o cómplice(s), dejando a salvo el derecho de los demás
    BENEFICIARIOS o herederos legales a recibir la cobertura garantizada en función a los porcentajes de beneficio establecidos en
    la póliza para cada uno. El porcentaje del beneficiario(s) excluido(s) será distribuido para ser pagado a los demás BENEFICIARIOS.</p>
    <p>Suicidio ocurrido, salvo que el contrato haya estado en vigencia ininterrumpidamente por dos (02) años.</p>
    <p>A consecuencia de la participación del asegurado como conductor o acompañante en carreras o ensayos de velocidad o resistencia de
    automóviles, motocicletas, lanchas a motor, avionetas, incluyendo carreras de entretenimiento.</p>
    <p>Para mayor detalle, el CONTRATANTE y/o ASEGURADO podrán remitirse a lo establecido en el artículo 5° de las Cláusulas Generales de
    Contratación comunes a los seguros de Vida Individuales.</p>
  `
};

export const FOIL_ATTACHED: Content = {
  title: LIFE_COVERAGE_TABS[2].name,
  content: ''
};
