export const GeneralLang = {
  Buttons: {
    allAffiliates: 'VER LISTA COMPLETA DE AFILIADOS',
    goToAppointment: 'Agendar cita',
    goToListAppointment: 'Ir a mis citas médicas',
    Nullify: 'Anular',
    Search: 'Buscar',
    Clear: 'Limpiar',
    Download: 'Descargar',
    AccountStatus: 'Estado de cuenta',
    Editing: 'Continuar editando',
    Confirm: 'Aceptar',
    Agree: 'Confirmar',
    Cancel: 'Cancelar',
    Yes: 'Si',
    No: 'No',
    Compare: 'Comparar',
    Edit: 'Editar',
    Update: 'Modificar',
    Purchase: 'Comprar',
    Next: 'Continuar',
    Send: 'Enviar',
    Save: 'Guardar',
    Payment: 'PAGAR',
    Understood: 'Entendido',
    ReNew: 'Renovar',
    Filter: 'Filtrar',
    Back: 'Regresar',
    Select: 'Seleccionar',
    UnFiltered: 'Quitar filtro',
    CleanFilter: 'Limpiar filtro',
    RestoreFilter: 'Restablecer filtro',
    MoreFaq: 'VER MÁS PREGUNTAS FRECUENTES',
    ViewAll: 'Ver todos',
    ViewMore: 'Ver más',
    ViewDetail: 'Ver detalle',
    Reschedule: 'Reagendar',
    goToStart: 'Ir a inicio',
    WhatDoYouWantToDo: [
      { description: 'Modificar mis datos' },
      { description: 'Ver mis siniestros' },
      { description: 'Modificar mis datos' },
      { description: 'Modificar los datos de mi póliza' },
      { description: 'Configurar mis tipos de pago' },
      { description: 'Hacer una reclamación' }
    ],
    AllMyServices: 'Todos mis servicios',
    FilterBy: 'Filtrar por:',
    AttachFile: 'Adjuntar archivo(s)',
    AttachOneFile: 'Adjuntar archivo',
    Attach: 'Adjuntar',
    EditCard: 'Editar tarjeta',
    ViewServices: 'Ver mis servicios',
    ViewLastServices: 'Ver servicios pasados',
    From: 'Desde:',
    To: 'Hasta:',
    TakeOutInsurance: 'Contratar',
    SeeHealthPlans: 'Ver planes de salud'
  },
  Labels: {
    Birth: 'Nacimiento',
    PolizaVitalicia: 'Póliza Vitalicia',
    PolizaVigente: 'Vigente',
    Salary: 'Sueldo',
    InsuranceType: 'Tipo de seguro',
    SinisterStatus: 'Estado de siniestro',
    PaymentType: 'Medio de pago',
    PaymentChannel: 'Canal de pago',
    Plan: 'Nombre de Plan',
    Period: 'Periodo activo',
    FilterBy: 'Filtrar por',
    DAA: 'Deducible Agregado Anual (DAA)',
    AppointmentPay: 'Pago por consulta',
    AppointmentPayCovid: 'Pago por prueba',
    NoCurrentPolicy: 'Mostrar pólizas no vigentes',
    BeatenSoat: 'Mostrar SOAT vencidos',
    HiringYear: 'Año de contratación',
    Select: 'Seleccione',
    Year: 'Año',
    Date: 'Fecha',
    Hour: 'Hora',
    MinValue: 'Valor mínimo',
    MaxValue: 'Valor máximo',
    PriceRange: 'Rango de precio',
    Price: 'Precio',
    HowToGetThere: 'CÓMO LLEGAR',
    Services: 'Servicios',
    FoilAttached: 'Anexos',
    Companies: 'Ver empresas',
    From: 'Desde',
    To: 'Hasta',
    Driver: 'Chofer',
    Scheduled: 'Agendado',
    Patient: 'Paciente',
    DateTime: 'Fecha y hora',
    Old: 'Antiguo',
    Specialty: 'Especialidad',
    DoctorByDate: 'Doctor de turno',
    New: 'Nuevo',
    MyBenefits: 'Conocer mis beneficios',
    MyPromotions: 'Mis promociones',
    MyBenefitsCheck: 'Mis beneficios',
    MyInsurance: 'Mis seguros',
    Affiliate: 'Afiliado',
    Clinic: 'Clínica:',
    SolicitudeDate: 'Fecha y hora de solicitud:',
    ApprovalDate: 'Fecha y hora de aprobación:',
    Affiliates: 'Afiliados',
    AppointmentVirtual: 'Cita médica Virtual',
    Appointment: 'Cita médica #{{nroCita}}'
  },
  Texts: {
    By: 'Por',
    AppointmentPay: 'El pago se hace a la hora de registrarse en módulo de admisión del Centro médico MAPFRE.',
    ElectronicPolicy: `Para reducir el consumo de papel y ayudar al medio ambiente, MAPFRE desea enviar su póliza
    por correo electrónico, usando certificados de conexión segura y con la validación de un tercer fedatario que
    certifica que la póliza le ha sido entregada y leída por usted. Igualmente, la póliza se archivará
    electrónicamente en un entorno seguro encontrándose a su disposición de forma inmediata y permanente.`,
    WhatIsMD: `Son un beneficio que tus clientes reciben por comprar y/o renovar seguros MAPFRE. Con ellos pueden acceder a
    descuentos directos al momento de la renovación o contratación de un nuevo seguro.`,
    HowGenerateMD: `Los M$ se generan al final de tu mes de aniversario con una vigencia por 4 años o adquirir nuevas
    pólizas o si renuevas las que tienes contratadas en seguros de Autos, Hogar, Salud y generales.`,
    Add: 'AGREGAR',
    Edit: 'EDITAR',
    WithOutInformation: 'Sin información',
    WithOutAddress: 'Sin dirección',
    WithOutProfession: 'Sin información de profesión',
    WithOutEmail: 'Sin información',
    WithOutPhone: 'Sin información',
    Well: '¡Vaya!',
    PolicyNumber: 'Nro. de póliza',
    ContractNumber: 'Nro. de contrato',
    NumberAbrev: 'Nro.',
    ChangePolicy: 'Cambiar póliza',
    SearchByDocumentPlaceholder: 'Buscar por nombre de paciente o documento',
    AppointmentSuccessTitle: 'TU CITA HA SIDO REGISTRADA EXITÓSAMENTE',
    AppointmentCovidSuccessTitle: 'TU PRUEBA HA SIDO REGISTRADA EXITÓSAMENTE',
    AppointmentRescheduleSuccessTitle: 'TU CITA HA SIDO AGENDADA EXITÓSAMENTE',
    AppointmentEditSuccessTitle: 'TU CITA HA SIDO MODIFICADA EXITÓSAMENTE',
    AppointmentSuccessSubTitle1: 'Te hemos enviado un correo con los datos de la cita médica al correo:',
    AppointmentSuccessSubTitleCovid1: 'Te hemos enviado un correo con los datos de la prueba al correo:',
    AppointmentSuccessSubTitle2: 'Correo electrónico:',
    AppointmentSuccessSubTitle3: `<p>Recuerda que el paciente debe acercarse con su documento 30 minutos antes al Centro
    médico MAPFRE. Podrás modificar o anular la cita hasta con 3 horas de anticipación de la hora agendada.</p>`,
    AppointmentSuccessSubTitle4: `<ol class="g-ol-decimal">
                                    <li>Recibirás un correo de confirmación</li>
                                    <li>El día de la cita recibirás un SMS con un link para ingresar.</li>
                                    <li>Asegurate de tener una buena calidad de internet y estar iluminado.</li>
                                  </ol>`,
    All: 'Todos',
    termsAndConditions: [
      'Los cargos se realizarán de acuerdo a su cronograma de pagos.',
      'Si el cliente cuenta con cuotas atrasadas, MAPFRE Perú o MAPFRE Perú Vida podrán realizar' +
        'tantos intentos de cobro como necesarios sin tener que informar previamente esta acción.',
      'En caso de reemplazo de la tarjeta por pérdida, robo o vencimiento, el Banco emisor de la' +
        'tarjeta puede informar el nuevo número de la tarjeta a MAPFRE Perú o MAPFRE Perú Vida y continuar con el' +
        'cargo automático; sin perjuicio de ello, es obligación del tarjetahabiente informar el cambio del número de' +
        'la tarjeta, sino se realiza dicha comunicación, MAPFRE Perú o MAPFRE Perú Vida no serán responsables por la' +
        'falta de cargo de la prima, pudiendo quedar suspendida la cobertura del seguro.',
      'Las solicitudes de suspensión temporal, modificación y/o desafiliación, deberán ser' +
        'presentadas con una anticipación no menor de 72 horas a la fecha del cobro. El titular del seguro es' +
        'responsable del seguimiento de los pagos del seguro a fin de evitar la suspensión de la cobertura del mismo.'
    ],
    NextService: 'Tu próximo servicio',
    HaveAppointment: 'Tienes 1 servicio agendado',
    HasAppointments: 'Tienes {{n}} servicios agendados',
    DontHaveAppointment: 'No tienes servicios agendados',
    CdmBenefitsQuoteFirst: 'Al ser cliente de un Seguro de Salud o EPS MAPFRE,',
    CdmBenefitsQuoteSecond: 'Accede a todos los beneficios que ofrece la clínica digital mapfre.',
    CdmBenefitsQuoteThird: 'Recibe atención en las <b>mejores clinicas</b> y asegura el bienestar de tu familia',
    RequestReceived: 'Hemos recibido tu solicitud',
    CdmPlanContact:
      'Uno de nuestros agentes te contactara al celular <b> {{nroCelular}} </b> para que contrates tu plan Clínica Digital.',
    TestResultCDM: '<p>El resultado de su prueba estará disponible en un <b>lapso no mayor a {{time}}.</b></p>',
    MolecularResults: 'Resultados en 24 horas.',
    AntigenoResults: 'Resultados en 1 hora.'
  },
  Titles: {
    ControlAffiliates: 'CONTROL DE AFILIADOS',
    Movements: 'Movimientos',
    MDUsed: 'Usados',
    MDCurrent: 'Tienes actualmente',
    MDWon: 'Ganados',
    MDDateWon: 'Desde',
    ViewMovements: 'Ver mis movimientos',
    MDNOBeaten: 'No tienes MAPFRE dólares por vencer',
    EditTimeOut: 'Sesión expirada',
    WhatIsMD: '¿Qué son los MAPFRE Dólares?',
    HowGenerateMD: '¿Cómo se generan?',
    ElectronicPolicy: 'CONSENTIMIENTO DE PÓLIZA ELECTRÓNICA',
    MyProceduresOnGoing: 'SINIESTROS EN CURSO',
    Sinister: 'Siniestros',
    MyRecordProcedures: 'HISTORIAL DE SINIESTROS',
    WhatDoYouWantToDo: '¿QUÉ QUIERES HACER?',
    TermsModal: 'TÉRMINOS Y CONDICIONES',
    Terms: `CLÁUSULA DE CONSENTIMIENTO DE TRATAMIENTO DE DATOS
    PERSONALES EN WEB`,
    Title404: 'Página no encontrada',
    TitleUnderConstruction: 'Página en construcción',
    Faq: 'Preguntas frecuentes',
    InsuredTypes: {
      Insured: 'ASEGURADO',
      Insureds: 'ASEGURADOS',
      Beneficiary: 'BENEFICIARIOS',
      Dependent: 'ASEGURADOS',
      MyInsureds: 'MIS ASEGURADOS',
      Households: 'VIVIENDAS',
      Vehicles: 'VEHÍCULOS',
      Persons: 'PERSONAS',
      Constancies: 'CONSTANCIAS'
    },
    AppDownload: 'Descarga el App de MAPFRE',
    DownLoadFiles: 'ARCHIVOS ADJUNTOS',
    RequestAssistance: 'SOLICITAR ASISTENCIA',
    Applications: 'Aplicaciones',
    MyFees: 'CUOTAS DE MI PÓLIZA',
    Periods: 'Periodos',
    PeriodsAdditionals: 'Periodos Adicionales',
    GuaranteeLetters: 'Cartas de Garantía',
    PpfmContact: 'Activar Beneficio',
    Headline: 'TITULAR',
    SelectedSavedCards: 'Selecciona la tarjeta con la cual deseas realizar el pago',
    TermsAndConditions: 'Términos y condiciones de la afiliación al pago automático',
    DocumentNotAvailable: 'Documento no disponible',
    QuoteYourPlan: 'Cotiza tu plan de Clínica Digital'
  },
  Messages: {
    EstimatedUser: 'Estimado Usuario',
    AreYouSureToDelete: '¿Estás seguro que deseas anular la cita?',
    EditTimeOut: 'El tiempo de sesión ha expirado. Actualiza para cargar nuevamente la pantalla.',
    DeleteData: 'Al aceptar, se perderá el agendamiento de la cita médica ¿Deseas continuar?',
    RollbackEdit: 'Al aceptar, se perderá la modificación de la cita médica ¿Deseas continuar?',
    Done: '¡Listo!',
    OneScheduledService: 'solicitado',
    ScheduledServices: 'Tienes % servicios agendados',
    SuccessUpdate: 'La información se ha actualizado correctamente.',
    SuccessAdd: 'La información se ha registrado correctamente.',
    SentDataSuccess: 'Datos enviados correctamente',
    ErrorUpdateAndAdd: 'La información registrada no ha sido guardada correctamente.',
    Fail: '¡Oops!',
    FailUpdate: 'Ha ocurrido un error al enviar la información. Inténtelo más tarde.',
    WithOutMD: 'No se registra ningún movimiento',
    WithOutMDWon: 'No tienes MAPFRE Dólares ganados',
    WithOutMDUsed: 'No tienes MAPFRE Dólares usados',
    WithOutMDBeaten: 'No tienes MAPFRE Dólares vencidos',
    WithoutInProgressSinisters: '¡No tienes siniestros en curso!',
    WithoutHistoricalSinisters: '¡Tu historial está vacío!',
    Message404: 'No hay nada por aquí.',
    MessageUnderConstruction: '',
    Loading: 'Cargando...',
    LoadingWithoutDots: 'Cargando',
    messageDocumentNew: 'Sube una foto de ambas caras de tu documento de identidad para poder hacer el cambio',
    ErrorImageFormat: 'Solo puedes adjuntar imágenes con formato JPEG o PNG',
    ErrorXLSFormat: 'Solo puedes adjuntar archivos con formato XLS o XLSX',
    ItemNotFound: 'No se encontraron resultados',
    InitPolicyItemNotFound: 'No se registra ninguna póliza',
    InitSinisterItemNotFound: 'No se registra ningún siniestro',
    FiltersActived: 'Has activado los filtros.',
    ErrorMaxMB: {
      start: 'El archivo ha superado el límite máximo de subida (',
      end: 'MB)'
    },
    AppDownload: 'Para disfrutar de los servicios de tu seguro vayas donde vayas.',
    WorkshopRating: 'Ayúdanos a mejorar calificando al taller',
    WorkshopRated: 'Gracias por tu calificación',
    DownLoadFiles: 'Has adjuntado los siguientes archivos:',
    ActionNotAvailable: 'La acción requerida no esta disponible en estos momentos.',
    WithErrorPeriods: 'Sus períodos no están disponibles.',
    WithoutPeriods: 'No tiene períodos pendientes.',
    WithoutInsureds: 'No se encontraron asegurados.',
    GuaranteeLettersItemNotFound: 'No se registran documentos disponibles',
    DownloadNotAvailable: 'Lo sentimos, el archivo no está disponible para la descarga.',
    OppsItemNotFound: 'Opps! No se encontraron resultados que coincidan con tu búsqueda',
    PpfmContactByPhone:
      'Para activar este beneficio es necesario que contactes a tu sectorista:<br><br><strong>agentName</strong><br><br><strong>Correo electrónico:</strong><br>agentEmail<br><br><strong>Línea de Whatsapp:</strong><br>agentPhone',
    PpfmContactByEmail:
      'Para activar este beneficio es necesario que contactes a tu sectorista:<br><br><strong>agentName</strong><br><br><strong>Correo electrónico:</strong><br>agentEmail'
  },
  Alert: {
    titleSuccess: '¡Listo!',
    titleFail: '¡Oops!',
    updateWork: {
      success: {
        title: '¡Listo!',
        message: 'La información se ha actualizado correctamente.'
      },
      error: {
        title: '¡Oops!',
        message: 'La acción realizada no se ha completado. Por favor, inténtalo más tarde.'
      }
    },
    errorDownloadPDF: {
      error: {
        title: '¡Oops!',
        message: 'En estos momentos no es posible descargar el archivo. Por favor, inténtalo más tarde.'
      }
    },
    errorDuplicateImage: {
      error: {
        message: 'No puedes usar la misma imagen dos veces'
      }
    },
    errorMaxImages: {
      error: {
        message: 'Solo puede subir un máximo de #number imágenes'
      }
    },
    sendData: {
      success: {
        message: 'La información se ha enviado correctamente.'
      },
      error: {
        message: 'Ha ocurrido un error al enviar la información. Inténtelo más tarde.'
      }
    },
    changeTermsAndConditions:
      'Hemos actualizado nuestros términos y condiciones, debes aceptarlos para continuar usando los servicios de Clínica Digital.'
  }
};
