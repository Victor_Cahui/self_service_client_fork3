import { IContactedByAgent } from '@mx/statemanagement/models/general.models';

export const MODAL_CONTACT = {
  title: 'MI MAPFRE',
  subTitle: '¿Necesitas ayuda?',
  description: 'Contacta con nosotros y estaremos encantados de atenderte.',
  officesUs: 'Nuestras oficinas',
  urlOffices: '/offices'
};

export const CONTACTED_BY_AGENT: IContactedByAgent = {
  title: 'Quiero que me contacte un agente',
  titleMobile: '',
  phoneNumber: 'Teléfono de contacto',
  hoursRange: 'Rango de horas',
  hoursRangeList: [
    { text: '08:00 a.m. - 10:00 a.m.', value: '08:00 a.m. - 10:00 a.m.', selected: false, disabled: false },
    { text: '10:00 a.m. - 12:00 p.m.', value: '10:00 a.m. - 12:00 p.m.', selected: false, disabled: false },
    { text: '12:00 p.m. - 02:00 p.m.', value: '12:00 p.m. - 02:00 p.m.', selected: false, disabled: false },
    { text: '02:00 p.m. - 04:00 p.m.', value: '02:00 p.m. - 04:00 p.m.', selected: false, disabled: false },
    { text: '04:00 p.m. - 06:00 p.m.', value: '04:00 p.m. - 06:00 p.m.', selected: false, disabled: false }
  ],
  email: 'Correo electrónico',
  titleShortSent: 'Te contactará un agente',
  titleSent: 'Solicitud enviada correctamente',
  messageSent: 'Un agente te contactará en un breve período de tiempo.',
  labelHoursRangeWithColons: 'Rango de horas:',
  labelPhoneNumberWithColons: 'Teléfono de contacto:',
  labelPhoneNumber: 'Teléfono de contacto',
  sendButton: 'ENVIAR',
  okButton: 'ACEPTAR'
};
