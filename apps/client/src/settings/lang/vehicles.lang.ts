import { CAR_PHOTO_ICON, DOCUMENT_CHECK_ICON, RADIO_EDIT_ICON } from '@mx/settings/constants/images-values';
import {
  Content,
  ContentHowtoWork,
  ContentInsurancePlans,
  Faq,
  PaymentsOptions
} from '@mx/statemanagement/models/general.models';

export const VehiclesLang = {
  Buttons: {
    AddPlateNumber: 'Añadir placa',
    AddAccesory: 'Añadir accesorio',
    Quote: 'COTIZA AHORA'
  },
  Labels: {
    Use: 'Uso',
    UseVehicle: 'Uso de vehículo',
    Color: 'Color',
    ChassisNumber: 'Nro. de chasis',
    ChassisNumberLg: 'Número de chasis',
    MotorNumber: 'Nro. de motor',
    MotorNumberLg: 'Número de motor',
    AccessoriesNumber: 'Nro. de accesorios',
    AccessoriesNumberLg: 'Número de accesorios adicionales',
    PlateNumber: 'Número de placa',
    EnterYourPlate: 'Ingresa tu placa',
    Plate: 'Placa',
    Model: 'Modelo',
    Brand: 'Marca',
    Value: 'Valor sugerido',
    Type: 'Tipo de vehículo',
    TypeAccesory: 'Tipo de accesorio',
    ModelAccesory: 'Marca y modelo',
    FindYourVehicle: 'Busca tu vehículo',
    YearProduction: 'Año de fabricación',
    NoPlate: 'Aún no tengo placa',
    City: 'Ciudad',
    checkIsNew: 'Es 0 KM',
    BrandModelYear: 'Marca/modelo/año',
    attention: '¿Dónde deseas atenderte?',
    MedicalCenter: 'Centro Médico de atención'
  },
  Texts: {
    InProcess: 'Placa en trámite',
    WorkshopsPDF: 'Lista de Talleres',
    VirtualMeeting: 'Agenda tu Cita Virtual',
    Appointment: 'Agenda tu cita Presencial',
    VirtualMeetingDescription: `Realiza las videoconsultas que necesites desde tu casa y atiéndete con nuestros mejores especialistas.`,
    AppointmentDescription: 'Acércate a un centro médico MAPFRE y atiéndete con nuestros mejores especialistas.'
  },
  Titles: {
    MyVehicles: 'MIS VEHÍCULOS',
    MyAccesories: 'extras',
    AddPlateNumber: 'AÑADIR PLACA',
    DataVehicle: 'DATOS DEL VEHÍCULO',
    DataMedicalAppointment: 'DATOS DE LA CITA MÉDICA',
    DataMedicalAppointmentStepOne: '¿COMO DESEAS LLEVAR TU CITA MÉDICA?',
    AddAccesory: 'AÑADIR ACCESORIO',
    EditAccesory: 'EDITAR ACCESORIO',
    EditPlate: 'EDITAR PLACA',
    VehiclesAccidents: '¿Has tenido un accidente?',
    VehiclesStolen: '¿Te han robado?',
    QuoteAndPurchase: 'Cotiza y compra tu seguro de AUTO',
    DataYourVehicle: 'Datos de tu vehículo',
    QuoteContact: 'Contáctanos para obtener una cotización personalizada',
    DataMedicalAppointmentPatient: 'DATOS DEL PACIENTE'
  },
  Messages: {
    AgentWillContactYou: 'Te contactará un agente',
    NotFoundVehicles: 'Has ingresado el nombre de una placa o modelo que no existe',
    CallMe: `Para modificar estos datos llámanos al {{phone}}`,
    DeleteAccesory: '¿Seguro que deseas eliminar el accesorio extra? ',
    VehiclesAccidents: 'Tranquilízate, no estás solo.',
    VehiclesAccidentsHelp: `<div class="g-font--medium g-font-lg--20 g-font--18 g-line--10 mb-xs--1 text-center text-md-center
              text-lg-left g-font--medium g-c--gray8">
              Llama a nuestra central de asistencia 24 horas
            </div>
            <div class="g-line--10 g-font--light text-center text-md-center text-lg-left ">
                Si hay lesionados no intentes moverlos del lugar, nosotros nos encargaremos.
            </div>`,
    VehiclesStolen: 'No te preocupes, nosotros nos encargamos de ayudarte.',
    VehiclesStolenHelp: `<div class="g-font--medium g-font-lg--20 g-font--18 g-line--10 mb-xs--1 text-center text-md-center
                text-lg-left g-font--medium g-c--gray8">
                Comunícate inmediatamente a nuestra central de asistencia 24 horas
            </div>
            <div class="g-line--10 g-font--light text-center text-md-center text-lg-left ">
                Realiza la denuncia policial en la comisaría más cercana.
            </div>`,
    VehicleErrorEditAccesoryTitle: '¡Este accesorio necesita una inspección!',
    VehicleErrorEditAccesory: 'Para modificar por favor contacte con un asesor MAPFRE o llámenos al ',
    VehicleErrorEditOrCreate: 'Lo sentimos. Su accesorio ya ha sido registrado',
    VehicleErrorActionAll: 'La acción realizada no se ha completado. Por favor, inténtalo más tarde.',
    VehicleNewQuote: `Para el modelo de tu auto es necesaria la instalación de un GPS. Hasta que el GPS esté
      instalado no tendrás cobertura ante robos. {{gpsCompanies}} autorizadas para la instalación.`,
    QuoteContact: 'Por favor, registra tus datos para que uno de nuestros asesores se ponga en contacto contigo.',
    WorkshopsNotification: {
      title: 'Sabías que al utilizar un taller preferente:',
      benefit1: 'Se da seguimiento a tu asistencia',
      benefit2: 'Tiene mejores precios en repuestos',
      moreInfo: 'Más información:',
      lima: 'Lima',
      provinces: 'Provincias'
    },
    // Redirección temporal al cotizador de mapfre (aut-1094)
    QuoteExternalLink: 'Estás a un paso de encontrar el seguro con la cobertura más completa para tu vehículo.'
  },
  Icon: {
    fail: 'icon--fail',
    error: 'icon--error'
  },
  Urls: {
    VehicularInsure: 'https://seguro-vehicular.mapfre.com.pe/'
  }
};

/***
 *  Contenido de Landing
 */
export const VEHICLE_INSURANCE_FAQ: Array<Faq> = [
  {
    title: '¿Puedo reportar el accidente o robo al día siguiente de ocurrido?',
    content: `Es necesario reportar el accidente o robo en el más breve plazo posible
    de haber ocurrido para que nuestros asesores lo evalúen en el lugar de ocurrencia.`,
    landing: true
  },
  {
    title: '¿Cuánto tiempo tomará realizar la reparación de mi vehículo en el taller?',
    content: `El tiempo de reparación promedio de un accidente vehicular de daños medios
    es de 12 días, el cual puede variar dependiendo de la disponibilidad de repuestos.`,
    landing: true
  },
  {
    title: '¿Se utilizarán repuestos originales en las reparaciones?',
    content: `Se podrán utilizar repuestos originales o alternativos de acuerdo a la
    evaluación de nuestros peritos. Los repuestos alternativos cumplen con los mismos
    estándares de calidad de los repuestos originales y cuentan con 1 año de garantía.`,
    landing: true
  },
  {
    title: '¿Mi deducible es el mismo en cualquier taller?',
    content: `Los deducibles varían de acuerdo a la red donde se encuentre el taller.
     Contamos con 4 redes: preferente 1, preferente 2, concesionario 1 y concesionario
      2. Para conocer el deducible que aplica revisa tu póliza. Los talleres no afiliados
       a ninguna red cuentan con deducibles mayores.`,
    landing: true
  }
];

export const INSURANCE_TOP: ContentInsurancePlans = {
  title: 'TOP SEGUROS VEHICULARES',
  textBtnGetQuote: 'CONTRATAR',
  plans: [
    {
      title: 'DAÑO TOTAL',
      subtitle: 'EL MÁS ECONÓMICO',
      content: [
        {
          items: [
            { text: 'Daño Total a valor comercial.' },
            { text: 'Daños a terceros hasta $ 150,000.' },
            { text: 'Cobertura de lunas.' },
            { text: 'Grúa y auxilio mecánico hasta 2 servicios mensuales.' }
          ]
        }
      ],
      price: 'desde S/ 85 al mes'
    },
    {
      title: 'FULL COBERTURA',
      subtitle: 'SOLO LO QUE NECESITAS',
      content: [
        {
          items: [
            { text: 'Daño Parcial y Total hasta valor comercial.' },
            { text: 'Daños a terceros hasta $ 150,000.' },
            { text: 'Chofer de reemplazo ilimitado con copago.' },
            { text: 'Vehículo de reemplazo ilimitado con copago.' },
            { text: 'Grúa y auxilio mecánico hasta 2 servicios mensuales.' },
            { text: 'Atención exclusiva en red preferente 2.' }
          ]
        }
      ],
      price: 'desde S/ 105 al mes'
    },
    {
      title: 'FULL COBERTURA PREMIUM X 2',
      subtitle: 'EL MÁS COMPLETO',
      content: [
        {
          items: [
            { text: 'Daño Parcial y Total hasta valor pactado.' },
            { text: 'Daños a terceros hasta $ 150,000.' },
            { text: 'Chofer de reemplazo ilimitado con copago.' },
            { text: 'Vehículo de reemplazo hasta 30 días por robo y 15 por choque.' },
            { text: 'Grúa y auxilio mecánico hasta 2 servicios mensuales.' },
            { text: 'SOAT Gratis.' },
            { text: 'Financiamiento a 12 cuotas sin intereses.' },
            { text: 'Canasta familiar y renta educativa hasta <span>$ 3,000.</span>' },
            { text: 'Atención en cualquier red afiliada a MAPFRE.' }
          ]
        }
      ],
      price: 'desde S/ 120 al mes'
    }
  ]
};

export const HOW_TO_WORK: ContentHowtoWork = {
  title: '¿CÓMO FUNCIONA?',
  items: [
    'Ingresa los datos de tu vehículo y cotiza',
    'Contrata la opción que más te convenga',
    '¡Y listo! Recibe tu póliza por correo electrónico'
  ]
};

export const PAYMENTS_OPTIONS: PaymentsOptions = {
  title: 'Pago 100% seguro',
  url: './assets/images/banks/',
  images: ['card-visa.svg', 'card-mastercard.svg', 'card-american.svg', 'card-dinners.svg']
};

export const COVERAGES_FOIL_ATTACHED: Content = {
  title: 'ANEXOS',
  content: 'En este documento encontrarás detalles exclusivos sobre tu póliza.',
  button: 'Descargar anexos',
  iconBtn: 'icon-mapfre_093_pdf'
};

export const COVERAGES_BY_POLICY_VEHICLE = {
  startCopy: 'Selecciona un vehículo de la póliza',
  endCopy: 'para visualizar su cobertura: ',
  selectLabel: 'Selecciona tu vehículo'
};

// Titulos - textos
export const EXCLUSIONS: Content = {
  title: 'EXCLUSIONES GENERALES',
  content: `
    <p>Este seguro no cubre siniestros debido a:</p>
    <ol class="g-list-alpha">
      <li>Deficiencias en el mantenimiento y/o en el uso del vehículo.</li>
      <li>Desgaste y/o deterioro u otro motivo vinculado al uso del vehículo, o por factores climáticos.</li>
      <li>Actos intencionales o negligentes del ASEGURADO y/o del conductor del vehículo; entendiéndose como actos negligentes a:
        <p>
          <ul class="g-list-bullet">
            <li>Conducir el vehículo en estado de somnolencia.</li>
            <li>Distraer la atención en la conducción por el uso de teléfonos móviles, radios u otros dispositivos,
            salvo que se utilice el sistema “manos libres”.</li>
            <li>Conducir el vehículo en exceso de las jornadas máximas de conducción establecidas en el Reglamento Nacional de
            Administración de Transporte o la norma que la sustituya, independientemente que el asegurado se encuentre regulado
            por el señalado reglamento.</li>
            <li>Conducir el vehículo sin utilizar las dos manos en el volante.</li>
            <li>Continuar conduciendo el vehículo luego de un impacto, agravando el daño causado. En este caso únicamente se excluye
            el daño producto de la agravación.</li>
            <li>Circular por lugares en los que la unidad infrinja los límites de altura y/o peso.</li>
            <li>Dejar el vehículo estacionado sin activar los seguros de las puertas y/o los dispositivos de alarma de los que esté
            provisto el vehículo.</li>
            <li>Dejar el vehículo estacionado en terrenos fangosos, pantanosos o movedizos.</li>
            <li>Dejar el vehículo estacionado sin activar el freno de mano o no tomar medidas a fin de evitar que terceros
            lo desactiven.</li>
          </ul>
        </p>
      </li>
      <li>La participación del ASEGURADO o conductor del vehículo en riñas, grescas, alteración del orden público u otros hechos que
      pudieran provocar agresiones de terceros.</li>
      <li>Cortocircuito originado en el vehículo, así como los daños que este pueda ocasionar a terceros.</li>
      <li>Otras expresamente nombradas en las Condiciones Generales del Producto.</li>
    </ol>
  `
};

export const RECORDS = {
  Filter: 'Filtrar',
  TitleAccident: 'Historial de Accidentes',
  TitleStolen: 'Historial de Robos',
  Label: {
    Assistance: 'Asistencia',
    Assists: 'Asistencias',
    CboYear: 'Elegir año'
  },
  Breadcrumb: {
    Accident: {
      SubTitle: 'ACCIDENTES',
      Title: 'HISTORIAL'
    },
    Stolen: {
      SubTitle: 'ROBOS',
      Title: 'HISTORIAL'
    }
  }
};

export const ASSISTANCE = {
  Title: 'En proceso',
  History: 'Historial',
  Filter: 'Filtrar',
  SelectYear: 'Elegir año',
  Assistance: 'Asistencia',
  AssistanceNumber: 'Nro. de Accidente:',
  StolenNumber: 'Nro. de Robo:',
  PolicyNumber: 'Nro. de póliza:',
  SinisterNumber: 'Nro. de Siniestro:',
  Attorney: 'Procurador:',
  ViewDetail: 'Ver detalle',
  NoRegister: 'No se registran asistencias pendientes',
  NoRegisterHistory: 'No se registran asistencias anteriores',
  NoRegisterAssists: 'No se registra ninguna asistencia',
  Delivery: 'Entrega',
  DetailTitle: 'DETALLE DE TU ASISTENCIA',
  Remember: 'Recuerda realizar el pago del deducible en el taller',
  Labels: {
    DeclaredPlace: 'Lugar del siniestro declarado',
    ViewReport: 'Ver informe',
    ReportFileName: 'informe_procurador',
    ViewTotalLost: 'Ver carta de declaración de pérdida total',
    TotalLostFileName: 'carta_perdida_total',
    Place: 'Lugar de atención',
    Cost: 'Costo',
    Deductible: 'Deducible',
    YouPay: 'Tú pagas',
    Phase: 'Fase',
    Delivery: 'ENTREGADO',
    AnotherBudget: 'Se inicio una ampliación ({{n}}) y el taller emitió un nuevo presupuesto.',
    SinisterExecutive: 'Ejecutivo de siniestro',
    RequestedDocuments: 'Documento(s) solicitado(s)',
    AddressIssue: '¿No encuentras la dirección?',
    GoToMap: 'Ir a Google Maps'
  },
  Texts: {
    NotaryDocuments: `Para recibir tu indemnización tendrás que acercarte a una notaría para firmar el
      traspaso de titularidad del vehículo y adjuntar los siguientes documentos:`
  }
};

export const VehicleQuoteBuyLang = {
  Step1: {
    title: 'Datos del vehículo',
    Tooltips: {
      ChassisText: 'Puedes encontrar el número en el sticker de tu parabrisa en una de estas ubicaciones:',
      MotorText: 'Puedes encontrar el número de motor en tu tarjeta de propiedad:',
      AccessoryText1: 'Número de accesorios:',
      AccessoryText2: `Son accesorios adicionales a los de fábrica. Si tienes más de tres accesorios en tu
       auto deberás solicitar una inspección presencial.`
    },
    CancelBuy: '¿Seguro de cancelar el proceso de compra?',
    Errors: {
      motor: 'Ingrese otro número de motor',
      chassis: 'Ingrese otro número de chasis'
    },
    Modal: {
      PolicyRegistered: 'Ya existe una póliza de seguro registrada para los siguientes campos:',
      PleaseChangeData: 'Por favor, ingrese otros datos o si crees que es un error puedes contactar con un asesor.',
      Contact: 'Contactar con un asesor'
    }
  },
  Step2: {
    title: 'Datos del contratante',
    ContactData: 'Datos de contacto',
    PolicyAddress: 'Dirección asociada a la póliza',
    Clause: {
      start: 'He leído y acepto la',
      end: 'Cláusula de consentimiento de tratamiento de datos personales.'
    }
  },
  Step4: {
    Condition: {
      start: 'He leído y acepto las',
      end: 'Condiciones del producto'
    },
    SendEmail: {
      start: 'y el envío por correo de la',
      end: 'Póliza Electrónica.'
    }
  },
  StepSelectInspection: {
    Title: 'Datos de la inspección',
    Detail: `Para poder concluir con la contratación, necesitamos hacer una inspección de tu auto. Puedes solicitar
      que un agente te contacte o hacerlo tu mismo descargando nuestra App.`,
    Reminder: 'Recuerda, no tendrás cobertura hasta que realices el pago y luego completes la inspección.',
    Options: {
      onsiteInspection: { Id: 1, Title: 'Inspección presencial', Label: 'Programa la visita del inspector' },
      selfInspection: { Id: 2, Title: 'Autoinspección', Label: 'Mediante la App MAPFRE' }
    },
    Agreement: { Label: 'He leído y acepto las', Link: 'Condiciones de inspección' }
  },
  StepOnsiteInspection: {
    Datetime: {
      Title: 'Fecha y hora de la inspección',
      Detail: 'Una vez realizado el pago, te llamaremos para coordinar la hora exacta.'
    },
    Address: 'Dirección para la inspección',
    UseCorrespondenceAddress: 'Utilizar la dirección de correspondencia',
    AnotherPerson: 'Otra persona estará presente en la inspección (opcional).',
    AnotherPersonName: 'Nombre de contacto',
    AnotherPersonPhone: 'Teléfono de contacto'
  },
  StepSelfinspection: {
    Title: 'Has seleccionado la modalidad de Autoinspección',
    Detail:
      'Para realizar la inspección, completa el pago de la póliza. Luego ingresa a la App de MAPFRE y realiza los siguientes pasos:',
    Steps: [
      {
        Title: 'Primero',
        Description: `Toma una fotografía de ambas caras de tu tarjeta de propiedad.`,
        Icon: DOCUMENT_CHECK_ICON
      },
      { Title: 'Segundo', Description: 'Captura cinco ángulos de tu vehículo.', Icon: CAR_PHOTO_ICON },
      { Title: 'Tercero', Description: 'Si deseas puedes agregar accesorios.', Icon: RADIO_EDIT_ICON }
    ]
  }
};

export const VehicleTimelineLang = {
  Compensation: {
    titile2: 'El cheque ha sido emitido',
    content: `Tienes un promedio de 12 días útiles para el recojo del monto de la indemnización.
     Si tienes una carta de no adeudamiento, por favor adjúntala.`,
    content2: `Tienes un promedio de 12 días útiles para el recojo del monto de la indemnización.`,
    action: {
      text: '¿Ya cancelaste tu deuda?',
      textLink: 'Adjuntar documento'
    },
    action2: {
      text: 'Has adjuntado tu carta de no adeudo.',
      textLink: 'Ver archivo'
    },
    tooltips: {
      title: `Propietario del vehículo:`,
      text: 'Propietario registrado en SUNARP',
      items: [
        `La indeminzación de tu vehículo se realizará con el propiertario registrado en la
        Superintendencia Nacional de los Registros Públicos.`
      ]
    },
    modal: {
      atachFile: 'ADJUNTAR DOCUMENTO',
      description: 'Puedes adjuntar la carta de no adeudo o levantamiento de garantía de tu entidad bancaria.',
      successTitle: '¡Documentación enviada!',
      successDescription:
        'Nos contactaremos contigo en un breve período de tiempo para comunicarte el estado de tu solicitud.'
    }
  }
  // tslint:disable-next-line: max-file-line-count
};
