import { ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import { Content, IModalHowWorks } from '@mx/statemanagement/models/general.models';

// Titulos - textos
export const FUNERAL_EXPENSES: Content = {
  title: 'GASTOS DE SEPELIO',
  content: `En caso de fallecimiento del “ASEGURADO” como consecuencia inmediata de un accidente de trabajo o enfermedad
      profesional amparado por este seguro o mientras se encuentre gozando de subsidios a cargo de ESSALUD, “LA COMPAÑÍA”
      reembolsará los gastos de sepelio a la persona natural o jurídica que los hubiera efectivamente sufragado.`
};

export const DISABILITY: Content = {
  title: 'PENSIÓN POR INVALIDEZ',
  content: 'Tu póliza SCTR Pensión te cubre en los siguientes casos de invalidez:'
};

export const SURVIVAL: Content = {
  title: 'PENSIÓN DE SOBREVIVENCIA',
  content: 'Tu póliza SCTR Pensión te cubre en los siguientes casos:',
  table: {
    title: 'FALLECIMIENTO DEL ASEGURADO',
    columns: ['BENEFICIARIO', 'PRESTACIÓN ECONÓMICA'],
    rows: [
      ['Cónyuge sin hijo(s)', 'Pensión vitalicia igual al 50% de la Remuneración.'],
      ['Cónyuge con uno o más hijos', 'Pensión vitalicia igual al 35% de la Remuneración.'],
      ['Hijos', '14% de la Remuneración por cada uno hasta los 18 años o vitalicia si es inválido.'],
      [
        'Un sólo hijo con derecho (si no existe cónyuge o conviviente con derecho)',
        '42% de la Remuneración hasta los 18 años o vitalicia si es inválido.'
      ],
      [
        'Dos o más hijos con derecho (si no existe cónyuge o conviviente con derecho)',
        '42% de la Remuneración + 14% por cada hijo, en partes iguales, hasta los 18 años o vitalicia si es inválido.'
      ],
      [
        'Padres mayores de 60 años y dependientes del causante o inválidos en más de un 50%',
        'Hasta 14% de la Remuneración por cada uno, y sólo si quedara algún remanente.'
      ]
    ]
  }
};

export const EXCLUSIONS_PENSION: Content = {
  title: 'EXCLUSIONES',
  content: `<p>No constituye accidente de trabajo:</p>
    <ul class="g-list-alpha">
      <li>El que se produce en el trayecto de ida o retorno al centro de trabajo, aunque el transporte sea realizado por cuenta de
      la Entidad Empleadora.</li>
      <li>El provocado intencionalmente por el trabajador o por su participación en riñas, peleas u otra acción ilegal.</li>
      <li>El que se produzca como consecuencia del incumplimiento del trabajador de una orden escrita específica del empleador.</li>
      <li>El que se produzca con ocasión de actividades recreativas, deportivas o culturales aunque se realicen dentro de la
      jornada laboral o en el centro de trabajo.</li>
      <li>El producido durante los permisos, licencias, vacaciones o cualquier otra forma de suspensión del contrato de trabajo.</li>
      <li>Los que se produzcan como consecuencia del uso de sustancias alcohólicas o estupefacientes por parte del 	asegurado.</li>
      <li>Los producidos en caso de guerra civil o internacional, declarada o no, dentro o fuera del Perú, motín, conmoción contra
      el orden público o terrorismo.</li>
      <li>Los que se produzcan por efecto de terremoto, maremoto, erupción volcánica o cualquier otra convulsión de la naturaleza.</li>
      <li>Los que se produzcan como consecuencia de fisión o fusión nuclear por efecto de la combustión de cualquier combustible nuclear,
      salvo cobertura especial expresa.</li>
    </ul>
    <p>Están excluidos del Seguro:</p>
    <ul class="g-list-alpha">
      <li>La Invalidez configurada antes del inicio de la vigencia del seguro cuyas prestaciones serán amparadas por la “ASEGURADORA”
      que otorgó la cobertura al tiempo de la configuración de invalidez.</li>
      <li>Muerte o invalidez causadas por lesiones voluntarias o autoinfligidas o autoeliminación o su tentativa.</li>
      <li>La muerte o invalidez de trabajadores asegurables no declarados por el “CONTRATANTE” que serán de cargo de la ONP de conformidad
      con lo indicado en el artículo 88° del Decreto Supremo N° 009-97-SA.</li>
      <li>La muerte del “ASEGURADO” mientras percibe subsidios por incapacidad temporal a cargo de ESSALUD por causas distintas al accidente
      de trabajo o enfermedad profesional que ocasionó dicho subsidio.</li>
     </ul>
    <p><strong>NOTA:</strong><p/>
    <p>Los trabajadores que tienen la calidad de afiliados regulares al Seguro Social de Salud, siempre se encuentran protegidos, pues
    en caso que un siniestro no califique para ser cubierto por el SCTR, el mismo deberá ser cubierto por Essalud (la atención médica)
    y/o la ONP o AFP donde esté afiliado el trabajador (las pensiones).</p>
    <p>Sobre esto el Artículo 4° del Decreto Supremo N° 003-98-SA (Normas Técnicas del SCTR) establece lo siguiente:</p>
    <p><i>“Todo accidente que no sea calificado como accidente de trabajo con arreglo a las normas del presente Decreto Supremo, así
    como toda enfermedad que no merezca la calificación de enfermedad profesional, serán tratados como accidente o enfermedad comunes
    sujetos al régimen general del Seguro Social en Salud y al sistema pensionario al que se encuentre afiliado el trabajador.”</i></p>
    `
};

export const EXCLUSIONS_HEALTH: Content = {
  title: 'EXCLUSIONES',
  content: `<p>No constituye accidente de trabajo:</p>
    <ul class="g-list-alpha">
      <li>El que se produce en el trayecto de ida o retorno al centro de trabajo, aunque el transporte sea realizado por cuenta de la
      Entidad Empleadora.</li>
      <li>El provocado intencionalmente por el trabajador o por su participación en riñas, peleas u otra acción ilegal.</li>
      <li>El que se produzca como consecuencia del incumplimiento del trabajador de una orden escrita específica del 	empleador.</li>
      <li>El que se produzca con ocasión de actividades recreativas, deportivas o culturales aunque se realicen dentro de la jornada
      laboral o en el centro de trabajo.</li>
      <li>El producido durante los permisos, licencias, vacaciones o cualquier otra forma de suspensión del contrato de trabajo.</li>
      <li>Los que se produzcan como consecuencia del uso de sustancias alcohólicas o estupefacientes por parte del asegurado.</li>
      <li>Los producidos en caso de guerra civil o internacional, declarada o no, dentro o fuera del Perú, motín, conmoción contra
      el orden público o terrorismo.</li>
      <li>Los que se produzcan por efecto de terremoto, maremoto, erupción volcánica o cualquier otra convulsión de la naturaleza.</li>
      <li>Los que se produzcan como consecuencia de fisión o fusión nuclear por efecto de la combustión de cualquier combustible nuclear,
      salvo cobertura especial expresa.</li>
    </ul>
    <p>Están excluidos del Seguro:</p>
    <ul class="g-list-alpha">
      <li>Lesiones voluntariamente autoinfligidas o derivadas de tentativa de autoeliminación.</li>
      <li>Accidente de trabajo o enfermedad profesional de los trabajadores asegurables que no hubieren sido declarados por La Entidad
      Empleadora; cuyas lesiones se mantendrán amparadas por el Seguro Social de Salud a cargo del 	IPSS de acuerdo con el Art. 88 del
      Decreto Supremo Nº 009-97-SA.</li>
      <li>Procedimientos o terapias que no contribuyen a la recuperación o rehabilitación del paciente de naturaleza cosmética, estética
      o suntuaria, cirugías electivas (no recuperativas ni rehabilitadoras) cirugía plástica, odontología 	de estética, tratamiento de
      periodoncia y ortodoncia; curas de reposo y del sueño, lentes de contacto. Sin embargo, 	serán obligatoriamente cubiertos los
      tratamientos de cirugía plástica reconstructiva o reparativa exigibles como consecuencia de un accidente de trabajo o una enfermedad
      profesional.</li>
    </ul>
    <p><strong>NOTA:</strong></p>
    <p>Los trabajadores que tienen la calidad de afiliados regulares al Seguro Social de Salud, siempre se encuentran protegidos, pues
    en caso que un siniestro no califique para ser cubierto por el SCTR, el mismo deberá ser cubierto por Essalud (la atención médica)
    y/o la ONP o AFP donde esté afiliado el trabajador (las pensiones).</p>
    <p>Sobre esto el Artículo 4° del Decreto Supremo N° 003-98-SA (Normas Técnicas del SCTR) establece lo siguiente:</p>
    <p><i>“Todo accidente que no sea calificado como accidente de trabajo con arreglo a las normas del presente Decreto Supremo, así
    como toda enfermedad que no merezca la calificación de enfermedad profesional, serán tratados como accidente o enfermedad comunes
    sujetos al régimen general del Seguro Social en Salud y al sistema pensionario al que se encuentre afiliado el trabajador.”</i></p>`
};

export const ASSISTANCE: Content = {
  title: 'ASISTENCIA Y ASESORAMIENTO',
  content: 'Tu póliza SCTR Salud te cubre en los siguientes casos:'
};
export const FOIL_ATTACHED: Content = {
  title: 'ANEXOS',
  content: ''
};

// Coberturas
export const COVERAGES_DISABILITY: Array<ICoverageItem> = [
  {
    title: 'Invalidez parcial entre 20% y 50%',
    key: '',
    icon: 'icon-mapfre_153_disability_hand',
    description: `Pago único de 24 mensualidades calculadas en forma proporcional
                  a la que correspondería a una invalidez permanente total.`,
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: ''
  },
  {
    title: 'Invalidez parcial entre 50% y 66.67%',
    key: '',
    icon: 'icon-mapfre_152_disability',
    description: 'Pensión vitalicia igual al 50% de la remuneración mensual.',
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: ''
  },
  {
    title: 'Total permanente',
    key: '',
    icon: 'icon-mapfre_141_disability',
    description:
      'Invalidez de grado igual o superior al 66.67%. Pensión vitalicia igual al 50% de la remuneración mensual.',
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: ''
  },
  {
    title: 'Gran Invalidez',
    key: '',
    icon: 'icon-mapfre_154_bed',
    description: `Invalidez de grado igual o superior al 66.67% y requiera el auxilio de otra persona
                  para las funciones esenciales para la vida. Pensión vitalicia igual al 100% de la remuneración mensual.`,
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: ''
  }
];

export const COVERAGES_ASSISTANCE: Array<ICoverageItem> = [
  {
    title: 'Atención médica al 100%',
    key: 'COB_SAL_ATENCION_MEDICA',
    icon: 'icon-mapfre_199_medical_care',
    description: `Atención médica, farmacológica, hospitalaria y quirúrgica sin límite
      hasta la recuperación total o parcial, o el fallecimiento del asegurado.`,
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: '',
    visible: false
  },
  {
    title: 'Rehabilitación y readaptación laboral',
    key: 'COB_SAL_REHAB_READAP',
    icon: 'icon-mapfre_198_rehabilitation',
    description: '',
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: '',
    visible: false
  },
  {
    title: 'Aparatos de prótesis y ortopédicos',
    key: 'COB_SAL_APARATOS_PROTESIS',
    icon: 'icon-mapfre_177_surgical_prosthesis',
    description: '',
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: '',
    visible: false
  },
  {
    title: 'Servicio de Prevención de Riesgos Gratuito',
    key: 'COB_SAL_SERV_PREVENCION',
    icon: 'icon-mapfre_200_prevention',
    description: 'Asistencia y asesoramiento preventivo en salud ocupacional para los empleadores y asegurados.',
    question: '',
    questionBool: false,
    routeKey: '',
    iconKey: '',
    visible: false
  }
];

export const RECORD_LANG = {
  ViewInsureds: 'Ver asegurados',
  DateEmissionAbrev: 'F. de emisión',
  StartValid: 'Inicio vigencia',
  EndValid: 'Fin vigencia',
  MyRecords: 'MIS CONSTANCIAS',
  Record: 'CONSTANCIA',
  ItemNotFound: 'Has ingresado un número de constancia que no existe.',
  ItemNotFoundInsured: 'Has ingresado un nombre o documento que no existe.',
  NotFound: {
    Title: '¡No tienes ninguna constancia!',
    Message: 'Por el momento no has declarado ninguna constancia.'
  },
  placeholderSearch: 'Buscar (Ejm: MP/2019/1234567)',
  placeholderSearchInsured: 'Buscar por nro. de documento o nombre',
  ViewAllRecords: 'Ver todas las constancias'
};

export const PERIODS_LANAG = {
  Period: 'Periodo',
  Periods: 'Periodos',
  DetailPeriod: '¿Qué periodo deseas ver a detalle?',
  SelectPeriod: 'Seleccionar periodo',
  Payroll: 'Planilla',
  ViewPayroll: 'Ver planilla',
  ViewReceipts: 'Ver recibos',
  Continue: 'Continuar',
  Declare: 'Declarar',
  Include: 'Incluir',
  DeclarationPath: 'declaration',
  InclusionPath: 'inclusion',
  DeclarationPeriod: 'Periodo de declaración',
  InclusionPeriod: 'Periodo de inclusión',
  StartPeriod: 'Inicio de periodo',
  EndPeriod: 'Fin de periodo',
  GenerateDeclaration: 'Generar declaración',
  GenerateInclusion: 'Generar inclusión',
  Declaration: 'Declaración',
  Inclusion: 'Inclusión',
  RisksToDeclare: '¿Que riesgo quieres declarar?',
  RisksToInclude: '¿En que riesgo deseas incluir personal?',
  RiskInfo:
    'Estos son los riesgos que incluye tu póliza. Asegúrate de indicar correctamente a que trabajador corresponde el tipo de riesgo.',
  IncludeWorker: 'Incluir trabajador',
  ViewAllPeriods: 'Ver todos los periodos',
  ViewAllAdditionalPeriods: 'Ver todos los periodos adicionales',
  DownloadTemplateText:
    '<strong>Descarga el formato</strong> y completa el excel con la información de los trabajadores.',
  DownloadTemplate: 'Descargar formato',
  Errors: 'Errores',
  Comments: 'Observaciones',
  WorkersDeclaredFoud: 'Se han encontrado trabajadores ya declarados',
  DownloadErrors: 'Descargar errores',
  DownloadComments: 'Descargar observaciones',
  UploadTemplate: 'Subir planilla en Excel',
  AddTemplate: 'Agrega o arrastra tu archivo',
  FileSize: 'Tamaño de archivo',
  LastEdited: 'Última modificación',
  DeclarationSummaryTitle: 'RESUMEN DE LA DECLARACIÓN',
  InclusionSummaryTitle: 'RESUMEN DE LA INCLUSIÓN',
  EditDeclaration: 'Modificar declaración',
  EditInclusion: 'Modificar inclusión',
  AdditionalInfo: 'Información adicional',
  AdditionalInfo1: 'Datos de obra o centro de trabajo',
  AdditionalInfo2: 'Centro de costos',
  TopSalary: 'Tope de remuneración mensual',
  Vigency: 'Vigencia de aplicación',
  StartValid: 'Inicio de vigencia',
  EndValid: 'Fin de vigencia',
  DeclarationIssued: 'Declaración emitida',
  InclusionIssued: 'Inclusión emitida',
  DeclarationSuccess: 'La declación fue emitida con éxito',
  InclusionSuccess: 'La inclusión fue emitida con éxito',
  DeclarationNumber: 'Número de declaración',
  InclusionNumber: 'Número de inclusión',
  ProofNumber: 'Número de constancia',
  WorkersDeclared: 'Trabajadores declarados',
  WorkersIncluded: 'Trabajadores incluidos',
  OperationDate: 'Fecha de operación',
  GotoPolicyDetail: 'Ir al detalle de la póliza',
  BillingTitle: 'Facturación',
  BillingDescription:
    '¿Deseas recibos por esta inclusión o prefieres que se cargue en el recibo de la próxima vigencia?',
  Billed: 'Facturada',
  NonBilled: 'No facturada',
  RiskDetail: 'Este es tu <strong>detalle de riesgos</strong>',
  PendingPayroll: 'Esta es tu <strong>planilla pendiente</strong>',
  BilledPerdiod: 'Se facturará en el periodo',
  DownloadPendingPayroll: 'Descargar planilla pendiente',
  Receipt: 'Recibo',
  Receipts: 'Recibos',
  Insured: 'asegurado',
  Insureds: 'asegurados',
  minLengthText: 'El número mínimo de caracteres es',
  DownloadConstancy: 'Descargar constancia',
  DownloadConstancyHelp:
    'Completa los datos de obra o centro de trabajo, verifica la información y presiona en el botón "Descargar Constancia"'
};

export const HowToDeclarePeriodLang: IModalHowWorks = {
  link: ' ¿Cómo funciona?',
  title: 'CÓMO FUNCIONA LA DECLARACIÓN DE SCTR',
  warning: '',
  body: {
    text: `Para declarar de manera correcta en la plataforma de SCTR verifica tus riesgos en la primera parte de la
    pantalla, a continuación, descarga el archivo excel y completa los datos de los trabajadores a declarar
    requeridos por el Excel.`,
    alignClass: 'text-lg-left'
  },
  steps: {
    title: {
      text: 'Pasos para completar el formato Excel:',
      alignClass: 'text-lg-left'
    },
    showStepNumber: true,
    items: [
      {
        icon: 'assets/images/services/icon-steps-checked.svg',
        text: 'Identifica los riesgos que tiene tu póliza.'
      },
      {
        icon: 'assets/images/services/icon-file-download.svg',
        text: 'Descarga el formato Excel del Portal Cliente.'
      },
      {
        icon: 'assets/images/services/icon-file-xls.svg',
        text: 'Indica el Riesgo y colócalo en el excel.'
      },
      {
        icon: 'assets/images/services/icon-file-upload.svg',
        text: 'Sube tu archivo Excel al Portal Cliente.'
      }
    ]
  }
};

export const HowToIncludePeriodLang: IModalHowWorks = {
  link: ' ¿Cómo funciona?',
  title: 'CÓMO FUNCIONA LA INCLUSIÓN DE SCTR',
  warning: '',
  body: {
    text: `Para incluir de manera correcta en la plataforma de SCTR verifica tus riesgos en la primera parte de la
    pantalla, a continuación, descarga el archivo excel y completa los datos de los trabajadores a incluir
    requeridos por el Excel.`,
    alignClass: 'text-lg-left'
  },
  steps: HowToDeclarePeriodLang.steps
};
