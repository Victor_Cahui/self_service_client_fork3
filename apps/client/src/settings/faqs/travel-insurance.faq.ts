import { Faq } from '@mx/statemanagement/models/general.models';

export const TRAVEL_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Qué es un seguro de viajes?',
    content: `Un Seguro de viajes como cualquier otro seguro, es un contrato entre la Aseguradora (MAPFRE) y el
    asegurado (cliente). El Seguro de viajes está diseñado para asistir al asegurado con cualquier imprevisto que
    este pueda presentar durante un viaje (desde la pérdida de su equipaje, hasta una asistencia u emergencia médica).
    Brindamos protección personalizada para distintos tipos de viajes y estilos de vida las 24 horas del día,
    los 365 días del año.`,
    landing: true
  },
  {
    title: '¿Qué tipos de seguro de viajes existen y qué me cubren?',
    content: `
    <p>Conoce los distintos planes que ofrecemos para que puedas hacer tu viaje por vacaciones, trabajo o estudios
    sin preocupaciones:</p>
    <ul class= "g-list-bullet">
    <li><strong>Gold:</strong> Seguro de viaje premium con validez en todo el mundo.</li>
    <li><strong>Euroschengen:</strong> Seguro internacional con coberturas tanto en euros (€) (para cuando el asegurado se
    encuentre dentro de la zona Euro) como en dólares americanos ($) (para cuando el cliente se encuentra en cualquier
    destino fuera de Europa).</li>
    <li><strong>Silver:</strong> Seguro internacional económico, valido en todo el mundo excluyendo Europa.</li>
    <li><strong>Nacional:</strong> Seguro de viajes para viajar dentro del Perú.</li>
    <li><strong>Student:</strong> Seguro de viajes de larga estadía. Este plan es ideal para personas que requieren visitar
    un país por un periodo prolongado.</li>
    </ul>
    <p>Todos los seguros de viajes a excepción del Plan Nacional, excluyen viajes del asegurado dentro del país de
    residencia.</p>`,
    landing: false
  },
  {
    title: '¿Me puedo atender en cualquier lugar o ustedes me indican a dónde tengo que ir?',
    content: `No. Una vez realizada la llamada a la central telefónica, el operador verificará la red de clínicas o
    centros médicos a los cuales puede acudir para recibir atención. El pasajero pierde la cobertura del seguro en
    caso se atienda en una clínica o centro de salud que no ha sido autorizado por el operador.`,
    landing: true
  },
  {
    title: '¿Qué pasa si mi viaje se suspende?',
    content: `En caso se suspenda el viaje, el pasajero debe informar con 48 horas de anticipación para que se pueda
    realizar el cambio de fechas de la póliza. Una vez entrada en vigencia el seguro, no se pueden realizar cambios. `,
    landing: true
  },
  {
    title: '¿Con cuánto tiempo de anticipación debo comunicar la cancelación del seguro?',
    content: `<p>Para los casos de cancelación de la póliza, se aplica una penalidad del 12% sobre el monto pagado
    y no aplica devolución; salvo causas justificadas como: </p>
    <ul class= "g-list-bullet">
      <li>Fallecimiento, accidente o enfermedad grave del beneficiario o familiar directo.</li>
      <li>Convocatoria como parte, testigo o jurado de un tribunal.</li>
      <li>Daños que por incendio, robo o por la fuerza de la naturaleza en su residencia habitual o en sus locales
      profesionales, los hacen inhabitables y justifican ineludiblemente su presencia.</li>
      <li>Cuarentena médica como consecuencia de suceso accidental.</li>
      <li>Si la persona que ha de acompañar al beneficiario en el viaje, se entiende por acompañante la persona que
      comparte la misma habitación de hotel o la misma cabina de crucero, o un familiar directo: Padre, Madre,
      Cónyuge, Hijo, Hermano, también poseedora de una Tarjeta de Asistencia en las mismas condiciones.</li>
    </ul>`,
    landing: false
  },
  {
    title: '¿Qué pasa si me automedico y sufro algún efecto adverso? ¿Pierdo la cobertura?',
    content: `Bajo ninguna circunstancia el pasajero debe auto medicarse. De ser así, se perdería automáticamente la
    cobertura del seguro. Antes de tomar alguna acción, debe comunicarse con la central telefónica para que lo pueda
    derivar con un especialista.`,
    landing: true
  }
];
