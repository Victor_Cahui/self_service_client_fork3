import { Faq } from '@mx/statemanagement/models/general.models';

export const DEATH_INSURANCE_FAQS_FOR_LANDING: Array<Faq> = [
  {
    title: '¿Cuál es la cobertura del seguro?',
    content: `<p>Nuestro seguro cubre todos los gastos que se deriven de los servicios de sepelio de cada integrante de la póliza,
      detallados a continuación: </p>
      <u>Servicio Funerario</u><br>
      <ul class="g-list-bullet">
        <li>Ataúd o urna de cremación</li>
        <li>Limusina o servicio de carroza, de coche de flores y movilidad</li>
        <li>Capilla Ardiente</li>
        <li>Arreglo floral</li>
        <li>Aviso de defunción en medio local</li>
        <li>Trámites de sepelio e instalación del servicio</li>
      </ul>
      <u>Servicio de Sepultura</u><br>
      <ul class="g-list-bullet">
        <li>Tumba bajo césped en espacio individual compartido en el camposanto MAPFRE seleccionado
        (nicho letra “E” en el camposanto MAPFRE Pisco)
          <ul class= "g-list-bullet">
            <li>Ceremonia de inhumación</li>
            <li>Sarcófago o módulo de concreto</li>
            <li>Lápida de mármol</li>
          </ul>
        </li>
        <li>Servicio de cremación en el camposanto MAPFRE Huachipa (próximamente Piura)
          <ul class= "g-list-bullet">
            <li>Urna</li>
            <li>Responso</li>
          </ul>
        </li>
      </ul>
      <p>Indemnizaciones en efectivo por muerte natural del titular y muerte accidental de los integrantes, de acuerdo a lo
      señalado en las Condiciones Particulares de la Póliza.</p>
      <p><strong>Adicionalmente el asegurado titular cuenta con un Seguro Complementario de Accidentes Personales gratuito
      durante el primer año de vigencia.</strong></p>`
  },
  {
    title: '¿Cuáles son los requisitos para tomar el seguro?',
    content: `<p>1.  Como titular, ser mayor de 18 años y menor de 61 años de edad y como integrante, ser mayor de 16 años y menor
    de 61 años de edad.<br>
    2.  Gozar de buena salud.</p>
    <p>El seguro puede estar conformado por un máximo de 5 personas: un asegurado titular y cuatro integrantes. <strong>El titular y
    los integrantes permanecerán invariables en el tiempo</strong>, no pudiendo ser reemplazados por otros. Uno de los integrantes podrá
    ser mayor de 61 años y menor de 66 años de edad a la fecha de inicio del seguro y será igualmente incluido en la póliza.</p>
    <p>Se considera gozar de buena salud, el no tener diagnóstico de enfermedad preexistente; entendiéndose como tal, cualquier
    condición de alteración del estado de salud diagnosticada por un profesional médico colegiado, conocida por el titular y
    no resuelta a la fecha de suscripción de la Solicitud de Seguro.</p>`,
    landing: true,
    order: 0
  },
  {
    title: '',
    content: ``,
    landing: true,
    order: 1
  },
  {
    title: '¿Dónde puedo realizar los pagos?',
    content: `<p>Nuestros ejecutivos de cobranza te visitarán en la dirección que señales o puedes acercarte a cualquiera de nuestras
    oficinas ubicadas en Lima y provincias, detalladas en nuestra página web.</p>
    La cobranza en el domicilio es un servicio prestado por la compañía, mas no una obligación de ésta; por lo tanto, es tu obligación
    como contratante cumplir con el pago oportuno de la prima del seguro.`,
    landing: true,
    order: 2
  },
  {
    title: '¿Dónde puedo solicitar el servicio de sepelio?',
    content: `<p>Acércate a nuestra amplia red de oficinas a nivel nacional o comunícate a nuestra central telefónica <strong>SI24
    </strong>.</p>  <p><strong>Si necesitas realizar una consulta</strong>, comunícate con nosotros al 213-3333 (Lima) y 0801-1-1133
    (provincias) de lunes a viernes de 08:30am. a 06:00pm., o los sábados y feriados de 08:30am. a 01:00pm.</p> <p><strong>En caso de
    emergencia y/o asistencia</strong>, llámanos al 213-3333 (Lima) y 0801-1-1133 (provincias) las 24 horas del día.</p>`
  },
  {
    title: '¿Puedo tener los servicios de sepultura en el Camposanto MAPFRE?',
    content: `Sí, sólo debes solicitarlo al momento que se presente el siniestro.`
  },
  {
    title: '¿Qué es el Período de Carencia?',
    content: `Es el período en que la cobertura del servicio de sepelio e indemnizaciones se encuentra suspendida por muerte natural
    del titular e integrantes de la póliza. En caso de muerte accidental serán aplicadas las coberturas del seguro que correspondan.`,
    landing: true,
    order: 3
  },
  {
    title: '¿Qué es el Seguro Complementario de Accidentes Personales?',
    content: `<p>Este seguro presenta las coberturas que se detallan a continuación:</p>
    <div class="g-mb--20">
      <img class="d-block d-md-none mx-auto" src="./assets/images/tables/accidentes_personales_responsive.png">
      <img class="d-none d-md-block" src="./assets/images/tables/accidentes_personales_desktop.png">
    </div>
    <p>En caso de producirse el Desamparo Familiar Súbito, la compañía pagará la indemnización indicada en las Condiciones Particulares
    de la póliza por cada hijo del asegurado que fuese menor de 18 años de edad (ambos padres deben estar protegidos por el Seguro
    Complementario de Accidentes Personales). La máxima responsabilidad de la compañía con respecto a esta cobertura será la que
    corresponda a cuatro hijos.</p>
    <p>Las indemnizaciones derivadas de la presente Cláusula Adicional se otorgan siempre que el asegurado sea menor de 75 años de edad
    a la fecha del fallecimiento por accidente.</p>
    <p>Este seguro es gratuito para el asegurado titular durante el primer año de vigencia de la póliza. Sin embargo, puede ser contratado
    para los demás integrantes pagando una prima adicional al seguro principal.</p>`
  },
  {
    title: '¿Qué es el Seguro Complementario de Vida Integral?',
    content: `<p>Este seguro presenta las coberturas que se detallan a continuación: </p>
    <div class="g-mb--20">
      <img class="d-block d-md-none mx-auto" src="./assets/images/tables/vida_integral_responsive.png">
      <img class="d-none d-md-block" src="./assets/images/tables/vida_integral_desktop.png">
    </div>
    <p>En caso de producirse el Desamparo Familiar Súbito, la compañía pagará la indemnización indicada en las Condiciones Particulares
    de la Póliza por cada hijo del asegurado que fuese menor de 18 años de edad (ambos padres deben estar protegidos por el Seguro
    Complementario de Vida Integral). La máxima responsabilidad de la compañía con respecto a esta cobertura será la que corresponda a
    cuatro hijos.</p>
    <p>Las indemnizaciones derivadas de la Cláusula Adicional se otorgan siempre que el asegurado sea menor de 70 años de edad
    a la fecha del fallecimiento por accidente.</p>`
  }
];

export const DEATH_INSURANCE_FAQS_INSIDE: Array<Faq> = [
  {
    title: '¿Cuál es la cobertura del seguro?',
    content: `<p>Nuestro seguro cubre todos los gastos que se deriven de los servicios de sepelio de cada integrante
    de la póliza y que se detalla a continuación:</p>
      <u>Servicio Funerario</u><br>
      <ul class="g-list-bullet">
        <li>Ataúd o urna de cremación</li>
        <li>Limusina o servicio de carroza, de coche de flores y movilidad</li>
        <li>Capilla Ardiente</li>
        <li>Arreglo floral</li>
        <li>Aviso de defunción en medio local</li>
        <li>Trámites de sepelio e instalación del servicio</li>
      </ul>
      <u>Servicio de Sepultura</u><br>
      <ul class="g-list-bullet">
        <li>Tumba bajo césped en espacio individual compartido en el camposanto MAPFRE seleccionado
        (nicho letra “E” en el camposanto MAPFRE Pisco)
          <ul class= "g-list-bullet">
            <li>Ceremonia de inhumación</li>
            <li>Sarcófago o módulo de concreto</li>
            <li>Lápida de mármol</li>
          </ul>
        </li>
        <li>Servicio de cremación en el camposanto MAPFRE Huachipa (próximamente Piura)
          <ul class= "g-list-bullet">
            <li>Urna</li>
            <li>Responso</li>
          </ul>
        </li>
      </ul>
      <p>Indemnizaciones en efectivo por muerte natural del titular y muerte accidental de los integrantes, de acuerdo a lo
      señalado en las Condiciones Particulares de la Póliza.</p>
      <p><strong>Adicionalmente el asegurado titular cuenta con un Seguro Complementario de Accidentes Personales gratuito
      durante el primer año de vigencia.</strong></p>`
  },
  {
    title: '¿Cuáles son los requisitos para tomar el seguro?',
    content: `<p>Puede estar conformado por un máximo de 5 personas de los cuales uno es El ASEGURADO Titular:
    Persona mayor de 18 años y menor de 61 años de edad, 4 Integrantes: Personas mayores de 16 años y menores de
    61 años de edad.</p>
    <p><strong>El titular y los Integrantes permanecerán invariables en el tiempo,</strong> no pudiendo
    ser reemplazados por otros. Uno de los Integrantes podrá ser mayor de 61 años y menor de 66 años de edad a la
    fecha de inicio del seguro y será igualmente incluido en la Póliza.</p>

    <p>Se considera gozar de buena salud, el no tener diagnóstico de enfermedad preexistente, entendiéndose como tal,
    cualquier condición de alteración del estado de salud diagnosticada por un profesional médico colegiado, conocida
    por el titular y no resuelta a la fecha de suscripción de la Solicitud de Seguro.</p>`
  },
  {
    title: '¿Dónde puedo realizar los pagos?',
    content: `<p>A nuestros Ejecutivos de Cobranza que lo visitaran en la dirección que Ud. Señale.</p>
    <p>Se deja expresa Constancia que la cobranza en el domicilio es un servicio prestado por La Compañía, mas no una
    obligación de ésta, por lo tanto, es obligación del Contratante el cumplir con el pago oportuno de la prima del
    seguro en cualquiera de las oficinas de La Compañía ubicadas en Lima y Provincias, detalladas en nuestra página
    web /contáctanos.</p>`
  },
  {
    title: '¿Dónde recurrimos para solicitar el servicio de sepelio?',
    content: `<p>Acercándose a nuestra amplia red de oficinas a nivel nacional o Comunicándose a nuestra Central
    de telefónica SI24:</p>
    <p>Si necesitas realizar una consulta, comunícate con nosotros al 213-3333 (Lima) y 0801-1-1133 (provincias) de
    lunes a viernes de 08:30am. a 06:00pm., o los sábados y feriados de 08:30 am. a 01:00pm.</p>
    <p>De presentarse una emergencia y/o asistencia: llámanos al 213-3333 (Lima) y 0801-1-1133 (Provincias) las 24
    horas del día.</p>`
  },
  {
    title: '¿Puedo tener los servicios de sepultura en el Camposanto MAPFRE?',
    content: `Si, sólo debe solicitarlo al momento que se presente el siniestro.`
  },
  {
    title: '¿Qué es periodo de Carencia?',
    content: `Es el periodo en que la cobertura del servicio de sepelio e indemnizaciones se encuentra suspendida por
    muerte natural del titular e integrantes de la póliza, salvo por muerte accidental en cuyo caso serán de aplicación
    las coberturas del seguro que correspondan.`
  },
  {
    title: '¿Qué es el Seguro Complementario de Accidentes Personales?',
    content: `<p>Este seguro presenta las coberturas que se detallan a continuación:</p>
    <div class="g-mb--20">
      <img class="d-block d-md-none mx-auto" src="./assets/images/tables/accidentes_personales_responsive.png">
      <img class="d-none d-md-block" src="./assets/images/tables/accidentes_personales_desktop.png">
    </div>
    <p>En caso de producirse el Desamparo Familiar Súbito, LA COMPAÑÍA pagará la indemnización indicada en las
    Condiciones Particulares de la Póliza por cada hijo de El ASEGURADO que fuese menor de dieciocho (18) años de
    edad (ambos padres deben estar protegidos por el Seguro Complementario de Accidentes Personales).
    La máxima responsabilidad de LA COMPAÑÍA con respecto a esta cobertura será la que corresponda a cuatro (4)
    hijos.</p>
    <p>Las indemnizaciones derivadas de la presente Cláusula Adicional se otorgan siempre que EL ASEGURADO sea menor
    de setenta y cinco (75) años de edad a la fecha del fallecimiento por accidente.</p>
    <p>Este seguro es gratuito para el Asegurado <strong>Titular</strong> durante el primer año de vigencia de la
    póliza, también puede ser contratado para los demás integrantes, pagando una prima adicional al del
    seguro principal.</p>`
  },
  {
    title: '¿Qué es el Seguro Complementario de Vida Integral?',
    content: `<p>Este seguro presenta las coberturas que se detallan a continuación: </p>
    <div class="g-mb--20">
      <img class="d-block d-md-none mx-auto" src="./assets/images/tables/vida_integral_responsive.png">
      <img class="d-none d-md-block" src="./assets/images/tables/vida_integral_desktop.png">
    </div>
    <p>En caso de producirse el Desamparo Familiar Súbito, LA COMPAÑÍA pagará la indemnización indicada en las
    Condiciones Particulares de la Póliza por cada hijo de El ASEGURADO que fuese menor de dieciocho (18) años de
    edad (ambos padres deben estar protegidos por el Seguro Complementario de Vida Integral). La máxima responsabilidad
    de LA COMPAÑÍA con respecto a esta cobertura será la que corresponda a cuatro (4) hijos.</p>
    <p>Las indemnizaciones derivadas de la presente Cláusula Adicional se otorgan siempre que EL ASEGURADO sea menor
    de setenta (70) años de edad a la fecha del fallecimiento por accidente.</p>`
  }
];
