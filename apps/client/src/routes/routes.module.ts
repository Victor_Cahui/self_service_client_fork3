import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@mx/guards/auth.guard';
import { AuthorizationByMenuGuard } from '@mx/guards/authorization-by-menu.guard';
import { LoginGuard } from '@mx/guards/login.guard';
import { SessionGuard } from '@mx/guards/session.guard';
import { UserAdminGuard } from '@mx/guards/user-admin.guard';
import { WelcomeGuard } from '@mx/guards/welcome.guard';
import { MainLayoutComponent } from '@mx/layouts/components';
import { LoginLayoutComponent } from '@mx/layouts/login-layout/login-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        canActivate: [LoginGuard],
        loadChildren: '../main/views/authentication/login/login.module#LoginModule'
      },
      {
        path: 'login-type',
        canActivate: [LoginGuard],
        loadChildren: '../main/views/authentication/login-type/login-type.module#LoginTypeModule'
      },
      {
        path: 'welcome',
        canActivate: [WelcomeGuard],
        loadChildren: '../main/views/authentication/login-recurrent/login-recurrent.module#LoginRecurrentModule'
      },
      {
        canActivate: [UserAdminGuard],
        path: 'recover-user',
        loadChildren: '../main/views/authentication/recover-user/recover-user.module#RecoverUserModule'
      },
      {
        canActivate: [UserAdminGuard],
        path: 'enable-user',
        loadChildren: '../main/views/authentication/enable-user/enable-user.module#EnableUserModule'
      },
      {
        path: 'send-success',
        loadChildren: '../main/views/authentication/send-email-success/send-email-success.module#SendEmailSuccessModule'
      },
      {
        path: 'restore-password',
        loadChildren: '../main/views/authentication/restore-password/restore-password.module#RestorePasswordModule'
      },
      {
        path: 'confirm-restore-password',
        loadChildren:
          '../main/views/authentication/restore-password-success/restore-password-success.module#RestorePasswordSuccessModule'
      },
      {
        path: 'expired-token',
        loadChildren:
          '../main/views/authentication/restore-password-fail/restore-password-fail.module#RestorePasswordFailModule'
      }
    ]
  },
  {
    path: 'validate-token',
    loadChildren: '../main/views/authentication/validate-token/validate-token.module#ValidateTokenModule'
  },
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    canActivateChild: [SessionGuard],
    children: [
      {
        path: 'home',
        loadChildren: '../main/views/home/home.module#HomeModule'
      },
      {
        path: 'not-found',
        loadChildren: '../main/pages/not-found/not-found.module#NotFoundModule'
      },
      {
        path: 'profile',
        loadChildren: '../main/views/profile/profile.module#ProfileModule'
      },
      {
        path: 'myPolicies',
        loadChildren: '../main/views/my-policies/my-policies.module#MyPoliciesModule'
      },
      {
        path: 'apps',
        canActivate: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/apps/apps.module#AppsModule'
      },
      {
        path: 'sinistersInProgress',
        loadChildren: '../main/views/sinisters-in-progress/sinisters-in-progress.module#SinistersInProgressModule'
      },
      {
        path: 'payments',
        loadChildren: '../main/views/payments/payments.module#PaymentsModule'
      },
      {
        path: 'vehicles',
        canActivate: [AuthorizationByMenuGuard],
        canActivateChild: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/vehicles/vehicles.module#VehiclesModule'
      },
      {
        path: 'health',
        canActivate: [AuthorizationByMenuGuard],
        canActivateChild: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/health/health.module#HealthModule'
      },
      {
        path: 'digital-clinic',
        loadChildren: '../main/views/digital-clinic/digital-clinic.module#DigitalClinicModule'
      },
      {
        path: 'household',
        canActivate: [AuthorizationByMenuGuard],
        canActivateChild: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/household/household.module#HouseholdModule'
      },
      {
        path: 'life',
        canActivate: [AuthorizationByMenuGuard],
        canActivateChild: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/life/life.module#LifeModule'
      },
      {
        path: 'travel',
        canActivate: [AuthorizationByMenuGuard],
        canActivateChild: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/travel/travel.module#TravelModule'
      },
      {
        path: 'death',
        canActivate: [AuthorizationByMenuGuard],
        canActivateChild: [AuthorizationByMenuGuard],
        loadChildren: '../main/views/death/death.module#DeathModule'
      },
      {
        path: 'sctr',
        loadChildren: '../main/views/sctr/sctr.module#SctrModule'
      },
      {
        path: 'mapfre-services',
        loadChildren: '../main/views/mapfre-services/mapfre-services.module#MapfreServicesModule'
      },
      {
        path: 'offices',
        loadChildren: '../main/views/offices/offices.module#OfficesModule'
      },
      {
        path: 'clinics',
        loadChildren: '../main/views/clinics/clinics.module#ClinicsModule'
      },
      {
        path: 'ppfm',
        loadChildren: '../main/views/ppfm/ppfm-benefit.module#PpfmBenefitModule'
      },
      {
        path: 'mapfre-dollars',
        loadChildren: '../main/views/mapfre-dollars/mapfre-dollars.module#MapfreDollarsModule'
      },
      {
        path: 'autoevaluador',
        loadChildren: '../main/views/autoevaluador/autoevaluador.module#AutoevaluadorModule'
      },
      {
        path: '**',
        redirectTo: '/not-found',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false, paramsInheritanceStrategy: 'always' })
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutesModule {}
