import { NgModule } from '@angular/core';
import { UnderConstructionModule } from '@mx/pages/under-construction/under-construction.module';
import { VehiclesShopComponent } from './vehicles-shop.component';
import { VehiclesShopRoutingModule } from './vehicles-shop.routing';

@NgModule({
  imports: [VehiclesShopRoutingModule, UnderConstructionModule],
  exports: [],
  declarations: [VehiclesShopComponent],
  providers: []
})
export class VehiclesShopModule {}
