import { NgModule } from '@angular/core';
import { VehiclesInsuranceRoutingModule } from './vehicles-insurance.routing';

@NgModule({
  imports: [VehiclesInsuranceRoutingModule]
})
export class VehiclesInsuranceModule {}
