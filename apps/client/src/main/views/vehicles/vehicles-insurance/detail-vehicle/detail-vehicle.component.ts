import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE, SOLES_SYMBOL_CURRENCY } from '@mx/settings/constants/general-values';
import { ADD_ICON } from '@mx/settings/constants/images-values';
import { PHONE_UPDATE_VEHICLE } from '@mx/settings/constants/key-values';
import { DETAIL_VEHICLE_HEADER } from '@mx/settings/constants/router-titles';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IClientVehicleDetail, IVehicleRequest } from '@mx/statemanagement/models/vehicle.interface';
import { Observable, Subscription } from 'rxjs';
import { AddNumberPlateComponent } from './add-number-plate/add-number-plate.component';

@Component({
  selector: 'client-detail-vehicle',
  templateUrl: './detail-vehicle.component.html'
})
export class DetailVehicleComponent implements OnInit, OnDestroy {
  @ViewChild(AddNumberPlateComponent) addNumberPlateComponent: AddNumberPlateComponent;

  vehicleDetail: IClientVehicleDetail;
  routeSub: Subscription;
  parameterSub: Subscription;
  identity: IdDocument;

  vehiclesLang = VehiclesLang;

  motorNumber: string;
  callMe: string;
  chassis: string;
  policyNumber: string;
  numberPhoneErrorEditAccesory: string;
  vehicleSub: Subscription;
  parametersSubject: Observable<Array<IParameterApp>>;
  addIcon = ADD_ICON;
  warningStatus = NotificationStatus.INFO;

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly route: ActivatedRoute,
    private readonly authService: AuthService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly generalService: GeneralService
  ) {}

  ngOnInit(): void {
    this.getIdentity();
    this.getParameters();
    this.routeSub = this.route.params.subscribe(params => {
      this.motorNumber = params['motorNumber'];
      this.chassis = params['chassis'];
      this.policyNumber = params['policyNumber'];
      this.getClientVehicle();
    });
  }

  // Parametros Generales
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.numberPhoneErrorEditAccesory = this.generalService.getValueParams(PHONE_UPDATE_VEHICLE) || '';
      this.callMe = this.vehiclesLang.Messages.CallMe.replace('{{phone}}', this.generalService.getPhoneNumber());
    });
  }

  getClientVehicle(): void {
    const data: IVehicleRequest = {
      chasis: this.chassis,
      nroMotor: this.motorNumber,
      numeroPoliza: this.policyNumber,
      codigoApp: APPLICATION_CODE
    };
    this.vehicleSub = this.vehicleService.getDetailVehicle(data).subscribe(
      (clientVehicle: IClientVehicleDetail) => {
        clientVehicle.monedaValorSugerido = clientVehicle.codigoMonedaValorSugerido
          ? StringUtil.getMoneyDescription(clientVehicle.codigoMonedaValorSugerido)
          : SOLES_SYMBOL_CURRENCY;
        this.vehicleDetail = clientVehicle;
        this.setHeaderTitle();
      },
      null,
      () => {
        this.vehicleSub.unsubscribe();
      }
    );
  }

  setHeaderTitle(): void {
    const vehicleHeader = { ...DETAIL_VEHICLE_HEADER };
    const title = `${
      this.vehicleDetail.placaEnTramite ? 'PLACA EN TRÁMITE' : this.vehicleDetail.placa
    } - ${this.complexModel()}`;
    vehicleHeader.title = title;
    this.headerHelperService.set(vehicleHeader);
  }

  getIdentity(): void {
    this.identity = this.authService.retrieveEntity();
  }

  addNumberPlate(): void {
    this.addNumberPlateComponent.open();
  }

  onUpdateVehicle(placa: string): void {
    this.vehicleDetail.placa = placa;
  }

  complexModel(): string {
    return `${this.vehicleDetail.marca}-${this.vehicleDetail.modelo}-${this.vehicleDetail.anio}`;
  }

  ngOnDestroy(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.vehicleSub) {
      this.vehicleSub.unsubscribe();
    }
  }
}
