import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { AuthService } from '@mx/services/auth/auth.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IClientVehicleDetail, IClientVehicleUpdateResponse } from '@mx/statemanagement/models/vehicle.interface';

@Component({
  selector: 'client-add-number-plate',
  templateUrl: './add-number-plate.component.html'
})
export class AddNumberPlateComponent implements OnInit {
  @Input() policyNumber: string;
  @Input() vehicleDetail: IClientVehicleDetail;
  @Output() numberPlateUpdated: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalMessage) mfModalMessage: MfModalMessage;
  validRegex = /^[A-Z0-9-]*$/;
  numberPlate: AbstractControl;
  identity: IdDocument;

  generalMaxLength = GENERAL_MAX_LENGTH;
  doneUpdate: boolean;
  doneMessage = GeneralLang.Messages.Done;
  messageSuccessOrError = GeneralLang.Messages.SuccessAdd;
  title = VehiclesLang.Titles.AddPlateNumber;
  titleMobile = VehiclesLang.Titles.DataVehicle;
  secondTitleMobile = VehiclesLang.Titles.AddPlateNumber;
  labelPlateNumber = VehiclesLang.Labels.PlateNumber;
  showLoading: boolean;

  constructor(private readonly vehicleService: VehicleService, private readonly authService: AuthService) {}

  ngOnInit(): void {
    this.identity = this.authService.retrieveEntity();
  }

  setFormControl(): void {
    this.numberPlate = new FormControl(
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern(this.validRegex),
        Validators.minLength(6),
        Validators.maxLength(6)
      ])
    );
  }

  open(): void {
    this.setFormControl();
    this.mfModal.open();
  }

  saveForm(confirm: boolean): void {
    if (confirm && this.numberPlate.valid) {
      this.updateVehicle();
    }
  }

  updateVehicle(): void {
    const request = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber
    };
    const data = {
      numeroChasis: this.vehicleDetail.numeroChasis,
      numeroMotor: this.vehicleDetail.numeroMotor,
      nuevaPlaca: this.numberPlate.value
    };
    this.showLoading = true;
    this.mfModalMessage.mfModalAlert.sizeClass = 'g-modal--medium';
    this.vehicleService.updateVehiclePlate(request, data).subscribe(
      (response: IClientVehicleUpdateResponse) => {
        this.numberPlateUpdated.emit(response.nuevaPlaca);
        this.doneMessage = GeneralLang.Messages.Done;
        this.messageSuccessOrError = GeneralLang.Messages.SuccessAdd;
        this.doneUpdate = true;
        this.mfModal.close();
        this.mfModalMessage.open();
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
        this.doneUpdate = false;
      }
    );
  }
}
