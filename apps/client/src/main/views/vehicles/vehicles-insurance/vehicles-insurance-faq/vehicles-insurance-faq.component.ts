import { Component } from '@angular/core';
import { VEHICLES_INSURANCE_FAQS } from '@mx/settings/faqs/vehicles-insurance.faq';

@Component({
  selector: 'client-vehicles-insurance-faq-component',
  templateUrl: 'vehicles-insurance-faq.component.html'
})
export class VehiclesInsuranceFAQComponent {
  faqs: Array<{ title: string; content: string; collapse: boolean }>;

  constructor() {
    this.faqs = VEHICLES_INSURANCE_FAQS.map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
  }
}
