import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesInsuranceFAQComponent } from './vehicles-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesInsuranceFAQComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesInsuranceFAQRoutingModule {}
