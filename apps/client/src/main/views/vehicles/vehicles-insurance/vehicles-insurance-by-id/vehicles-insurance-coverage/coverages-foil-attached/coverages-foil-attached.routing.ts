import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoveragesFoilAttachedComponent } from './coverages-foil-attached.component';

const routes: Routes = [
  {
    path: '',
    component: CoveragesFoilAttachedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoveragesFoilAttachedRoutingModule {}
