import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FoilAttachedModule } from '@mx/components/shared/foil-attached/foil-attached.module';
import { CoveragesFoilAttachedComponent } from './coverages-foil-attached.component';
import { CoveragesFoilAttachedRoutingModule } from './coverages-foil-attached.routing';

@NgModule({
  imports: [CommonModule, RouterModule, CoveragesFoilAttachedRoutingModule, FoilAttachedModule],
  declarations: [CoveragesFoilAttachedComponent]
})
export class CoveragesFoilAttachedModule {}
