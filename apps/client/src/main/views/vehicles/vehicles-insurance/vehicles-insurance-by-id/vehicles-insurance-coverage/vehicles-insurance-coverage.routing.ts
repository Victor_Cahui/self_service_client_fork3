import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VehiclesInsuranceCoverageComponent } from './vehicles-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesInsuranceCoverageComponent,
    children: [
      {
        path: '',
        loadChildren: './coverages-coverage/coverages-coverage.module#CoveragesCoverageModule'
      },
      {
        path: 'deductibles',
        loadChildren: './coverages-deductibles/coverages-deductibles.module#CoveragesDeductiblesModule'
      },
      {
        path: 'exclusions',
        loadChildren: './coverages-exclusions/coverages-exclusions.module#CoveragesExclusionsModule'
      },
      {
        path: 'foil-attached',
        loadChildren: './coverages-foil-attached/coverages-foil-attached.module#CoveragesFoilAttachedModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesInsuranceCoverageRoutingModule {}
