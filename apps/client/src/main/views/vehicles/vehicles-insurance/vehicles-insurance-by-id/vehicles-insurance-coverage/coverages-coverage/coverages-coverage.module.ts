import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { CoveragesCoverageComponent } from './coverages-coverage.component';
import { CoveragesCoverageRoutingModule } from './coverages-coverage.routing';

@NgModule({
  imports: [
    CommonModule,
    CoveragesCoverageRoutingModule,
    CardIconsCoverageModule,
    FormsModule,
    ReactiveFormsModule,
    MfLoaderModule
  ],
  declarations: [CoveragesCoverageComponent]
})
export class CoveragesCoverageModule {}
