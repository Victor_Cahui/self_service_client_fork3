import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VehiclesInsuranceByIdComponent } from './vehicles-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './vehicles-insurance-detail/vehicles-insurance-detail.module#VehiclesInsuranceDetailModule'
      },
      {
        path: 'coverages',
        loadChildren: './vehicles-insurance-coverage/vehicles-insurance-coverage.module#VehiclesInsuranceCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesInsuranceByIdRoutingModule {}
