import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoveragesExclusionsComponent } from './coverages-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: CoveragesExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoveragesExclusionsRoutingModule {}
