import { Component, OnDestroy, OnInit } from '@angular/core';

import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { VEHICLE_STOLEN_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-stolen-component',
  templateUrl: './vehicles-stolen.component.html'
})
export class VehiclesStolenComponent implements OnDestroy, OnInit {
  tabItemsList: Array<ITabItem>;

  constructor(private readonly headerHelperService: HeaderHelperService) {
    this.tabItemsList = VEHICLE_STOLEN_TAB;
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
  }

  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
