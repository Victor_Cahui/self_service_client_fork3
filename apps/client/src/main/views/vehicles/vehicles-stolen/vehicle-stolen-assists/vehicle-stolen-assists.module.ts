import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationsnModule } from '@mx/components/notifications/notifications.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { ViewItemListModule } from '@mx/components/shared/timeline-item-list/view-item-list/view-item-list.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { VehicleStolenAssistsComponent } from './vehicle-stolen-assists.component';
import { VehicleStolenAssistsRouting } from './vehicle-stolen-assists.routing';

@NgModule({
  imports: [
    CommonModule,
    VehicleStolenAssistsRouting,
    ViewItemListModule,
    MfLoaderModule,
    NotificationsnModule,
    BannerCarouselModule
  ],
  declarations: [VehicleStolenAssistsComponent]
})
export class VehicleStolenAssistsModule {}
