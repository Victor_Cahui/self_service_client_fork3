import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TimelineVehiclesBase } from '@mx/components/vehicles/utils/timeline-vehicles-base';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { VEHICLES_STOLEN_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-vehicle-stolen-assists-detail',
  templateUrl: './vehicle-stolen-assists-detail.component.html',
  providers: [GeolocationService]
})
export class VehicleStolenAssistsDetailComponent extends TimelineVehiclesBase implements OnInit {
  constructor(
    protected headerHelperService: HeaderHelperService,
    protected activePath: ActivatedRoute,
    protected vehicleService: VehicleService,
    protected geolocationService: GeolocationService
  ) {
    super(headerHelperService, activePath, vehicleService, geolocationService);
  }

  ngOnInit(): void {
    this.titleHeader = VEHICLES_STOLEN_HEADER.title;
    this.subtitleHeader = this.lang.StolenNumber.toUpperCase();
    this.setView();
    this.assistanceType$ = this.vehicleService.getStolenDetail(this.getParams());
    this.titleCardDeclared = this.cards.AUTOS_ASIST_ESTADO_DECLARADO.title2;
    this.getData();
  }
}
