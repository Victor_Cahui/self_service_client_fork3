import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentMethodModule } from '@mx/components';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonModule, MfCheckboxModule, MfModalModule } from '@mx/core/ui';
import { VehiclesQuoteBuyStepFourComponent } from './vehicles-quote-buy-step-four.component';
import { VehiclesQuoteBuyStepFourRoutingModule } from './vehicles-quote-buy-step-four.routing';

@NgModule({
  imports: [
    CommonModule,
    MfModalModule,
    MfButtonModule,
    MfCheckboxModule,
    PaymentMethodModule,
    VehiclesQuoteBuyStepFourRoutingModule,
    GoogleModule
  ],
  declarations: [VehiclesQuoteBuyStepFourComponent]
})
export class VehiclesQuoteBuyStepFourModule {}
