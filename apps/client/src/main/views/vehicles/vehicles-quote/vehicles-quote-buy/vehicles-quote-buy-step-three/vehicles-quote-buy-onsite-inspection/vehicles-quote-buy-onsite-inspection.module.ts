import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { QuoteBuyAddressModule } from '@mx/components/vehicles/quote-buy-address/quote-buy-address.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import {
  DirectivesModule,
  MfButtonModule,
  MfCheckboxModule,
  MfInputDatepickerModule,
  MfInputModule,
  MfSelectModule,
  MfShowErrorsModule
} from '@mx/core/ui';
import { VehiclesQuoteBuyOnsiteInspectionComponent } from './vehicles-quote-buy-onsite-inspection.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    GoogleModule,
    MfButtonModule,
    MfCheckboxModule,
    MfInputDatepickerModule,
    MfInputModule,
    MfSelectModule,
    MfShowErrorsModule,
    QuoteBuyAddressModule,
    ReactiveFormsModule
  ],
  exports: [VehiclesQuoteBuyOnsiteInspectionComponent],
  declarations: [VehiclesQuoteBuyOnsiteInspectionComponent]
})
export class VehiclesQuoteBuyOnsiteInspectionModule {}
