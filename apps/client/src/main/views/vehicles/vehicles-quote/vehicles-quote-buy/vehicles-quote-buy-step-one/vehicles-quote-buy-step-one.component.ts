import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { ModalErrorBuyStepOneComponent } from '@mx/components/vehicles/modal-error-buy-step-one/modal-error-buy-step-one.component';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { AlphanumericValidator, AmountValidator, SelectListItem } from '@mx/core/ui';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { MFP_Compra_Tu_Seguro_Paso_1_14A } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { IMAGE_VEHICLE_QUOTE } from '@mx/settings/constants/images-values';
import { VEHICLES_QUOTE_BUY_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehicleQuoteBuyLang, VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import {
  IClientVehicleDetail,
  IGetColorsResponse,
  ISearchChassisMotorRequest,
  ISearchChassisMotorResponse,
  IVerifyInspectionRequest,
  IVerifyPolicyChassisMotorRequest
} from '@mx/statemanagement/models/vehicle.interface';
import { Subscription, zip } from 'rxjs';

@Component({
  selector: 'client-vehicles-quote-buy-step-one',
  templateUrl: './vehicles-quote-buy-step-one.component.html'
})
export class VehiclesQuoteBuyStepOneComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(ModalErrorBuyStepOneComponent) mfModalErrorCancel: ModalErrorBuyStepOneComponent;

  generalLAng = GeneralLang;
  vehicleLang = VehiclesLang;
  lang = VehicleQuoteBuyLang;
  images = IMAGE_VEHICLE_QUOTE;
  maxLength = GENERAL_MAX_LENGTH;
  minLength = GENERAL_MIN_LENGTH;

  form: FormGroup;

  vehicle: IClientVehicleDetail;
  quote: IPaymentQuoteCard;
  colors: Array<SelectListItem> = [];

  zipSub: Subscription;
  verifySub: Subscription;
  verifyInspectionSub: Subscription;

  currentMotor: string;
  currentChassis: string;

  constructor(
    private readonly fb: FormBuilder,
    private readonly policiesQuoteService: PoliciesQuoteService,
    private readonly vehicleService: VehicleService,
    private readonly paymentService: PaymentService,
    private readonly router: Router,
    protected gaService: GAService
  ) {
    super(gaService);
    this.ga = [MFP_Compra_Tu_Seguro_Paso_1_14A()];
    this.policiesQuoteService.setCurrentStep(1);
  }

  ngOnInit(): void {
    this.vehicle = this.policiesQuoteService.getVehicleData();
    this.quote = this.paymentService.getCurrentQuote();
    this.createForm();
    this.getInfo();
    this.mfModalErrorCancel.mfModal.titleMobile = VEHICLES_QUOTE_BUY_HEADER.title;
  }

  createForm(): void {
    this.form = this.fb.group({
      chassis: [
        this.vehicle && this.vehicle.numeroChasis ? this.vehicle.numeroChasis : '',
        this.getValidators('chassis')
      ],
      motor: [this.vehicle && this.vehicle.numeroMotor ? this.vehicle.numeroMotor : '', this.getValidators()],
      accessories: [
        this.vehicle && this.vehicle.cantidadAccesorios ? this.vehicle.cantidadAccesorios : null,
        [Validators.maxLength(this.maxLength.vehicle_accesories), AmountValidator.isRangeValid(0, 99)]
      ],
      color: [this.vehicle && this.vehicle.codigoColor ? this.vehicle.codigoColor : null, Validators.required]
    });
  }

  getValidators(type: string = 'motor'): Array<ValidatorFn> {
    const maxLength = type === 'motor' ? this.maxLength.motor : this.maxLength.chassis;
    const minLength = type === 'motor' ? this.minLength.motor : this.minLength.chassis;

    return [
      Validators.required,
      Validators.maxLength(maxLength),
      Validators.minLength(minLength),
      AlphanumericValidator.isValid
    ];
  }

  getInfo(): void {
    // Verifica si hay datos cargados
    const colors = this.policiesQuoteService.getColors();
    if (colors && colors.length) {
      this.setColors(colors);
    } else {
      const chassisParams: ISearchChassisMotorRequest = { codigoApp: APPLICATION_CODE, placa: this.vehicle.placa };
      const chassis$ = this.vehicleService.searchChassisMotor(chassisParams);
      const colors$ = this.vehicleService.getColors({ codigoApp: APPLICATION_CODE });
      this.zipSub = zip(chassis$, colors$).subscribe(
        data => {
          this.setChassisMotor(data && data[0]);
          this.setColors(data && data[1]);
          if (data && data[1]) {
            this.policiesQuoteService.setColors(data[1]);
          }
          this.zipSub.unsubscribe();
        },
        () => {
          this.zipSub.unsubscribe();
        }
      );
    }
  }

  setChassisMotor(data: ISearchChassisMotorResponse): void {
    if (!this.form.controls['chassis'].value && data && data.numChasis) {
      this.form.controls['chassis'].setValue(data.numChasis.toUpperCase());
    }
    if (!this.form.controls['motor'].value && data && data.numMotor) {
      this.form.controls['motor'].setValue(data.numMotor.toUpperCase());
    }
  }

  setColors(colors: Array<IGetColorsResponse>): void {
    if (colors && colors.length) {
      colors.forEach(color => {
        const tmp = {
          value: color.identificadorColorVehiculo,
          text: color.nombreTipoColor,
          selected: true,
          disabled: false
        };
        this.colors.push(tmp);
      });
    }
  }

  showErrors(controlName: string): boolean {
    return FormComponent.showBasicErrors(this.form, controlName);
  }

  back(): void {
    // Limpia datos
    this.policiesQuoteService.clearDataVehicleBuyStep1();
    // Vuelve al cuadro
    this.router.navigate(['/vehicles/quote/detail/step2']);
  }

  // Verificar datos de vehiculo y si puede comprar
  verifyInfo(): void {
    // Si hay cambios
    if (this.isChangedData()) {
      const params: IVerifyPolicyChassisMotorRequest = {
        codigoApp: APPLICATION_CODE,
        placa: this.vehicle.placa,
        chasis: this.form.controls['chassis'].value,
        motor: this.form.controls['motor'].value
      };
      this.verifySub = this.vehicleService.verifyPolicyChassisMotor(params).subscribe(
        result => {
          if (!result.puedeComprar) {
            this.showErrorsInput(result.esValidoMotor, result.esValidoChasis);
            this.mfModalErrorCancel.mfModal.open();
          } else {
            const numeroChasis = this.form.controls['chassis'].value.toUpperCase();
            const numeroMotor = this.form.controls['motor'].value.toUpperCase();
            const cantidadAccesorios = parseInt(this.form.controls['accessories'].value || 0, 10);

            this.verifySelfinspection(numeroChasis, numeroMotor, cantidadAccesorios, result.fechaVigencia);
          }
          this.verifySub.unsubscribe();
        },
        () => {
          this.verifySub.unsubscribe();
        }
      );
    } else {
      // Actualiza color
      this.vehicle = this.policiesQuoteService.getVehicleData();
      this.vehicle.color = ArrayUtil.getTextfromList(this.colors, this.form.controls['color'].value);
      this.vehicle.codigoColor = parseInt(this.form.controls['color'].value, 10);
      this.policiesQuoteService.setVehicleData(this.vehicle);
      // Paso 2
      this.router.navigate(['/vehicles/quote/buy/step2']);
    }
  }

  // Verifica si requiere inspeccion
  verifySelfinspection(numeroChasis: string, numeroMotor: string, accesorios: number, vigencia: string): void {
    const params: IVerifyInspectionRequest = {
      codigoApp: APPLICATION_CODE,
      codigoMarca: this.vehicle.codigoMarca,
      codigoModelo: this.vehicle.codigoModelo,
      anio: this.vehicle.anio,
      numeroChasis,
      numeroMotor,
      codigoTipoVehiculo: this.vehicle.codigoTipoVehiculo,
      codigoMonedaValorSugerido: this.vehicle.codigoMonedaValorSugerido,
      sumaAsegurada: this.vehicle.valorSugerido,
      accesorios,
      esCeroKm: this.vehicle.es0Km,
      codigoModalidad: this.quote.modalitySelected.modality,
      numeroPlaca: this.vehicle.placa
    };

    this.verifyInspectionSub = this.vehicleService.verifyInspection(params).subscribe(
      data => {
        // Actualiza vehiculo
        this.vehicle.numeroChasis = numeroChasis;
        this.vehicle.numeroMotor = numeroMotor;
        this.vehicle.cantidadAccesorios = accesorios;
        this.vehicle.color = ArrayUtil.getTextfromList(this.colors, this.form.controls['color'].value);
        this.vehicle.codigoColor = parseInt(this.form.controls['color'].value, 10);
        this.vehicle.fechaVigencia = vigencia;
        this.vehicle.puedeAutoInspeccionar = data.puedeAutoInspeccionar;
        this.vehicle.requiereInspeccion = data.necesitaInspeccion;
        this.policiesQuoteService.setVehicleData(this.vehicle);
        // Paso 2
        this.router.navigate(['/vehicles/quote/buy/step2']);
        this.verifyInspectionSub.unsubscribe();
      },
      () => {
        this.verifyInspectionSub.unsubscribe();
      }
    );
  }

  showErrorsInput(motorValid: boolean, chassisValid: boolean): void {
    this.currentMotor = motorValid ? null : this.form.controls['motor'].value;
    if (!motorValid) {
      const validators = this.getValidators().concat(
        this.changeDataValidator(this.currentMotor, this.lang.Step1.Errors.motor)
      );
      this.form.controls['motor'].setValidators(validators);
      this.form.controls['motor'].updateValueAndValidity();
    }

    this.currentChassis = chassisValid ? null : this.form.controls['chassis'].value;
    if (!chassisValid) {
      const validators = this.getValidators('chassis').concat(
        this.changeDataValidator(this.currentChassis, this.lang.Step1.Errors.chassis)
      );
      this.form.controls['chassis'].setValidators(validators);
      this.form.controls['chassis'].updateValueAndValidity();
    }
  }

  changeDataValidator(currentValue: string, error: string): any {
    return (AC: AbstractControl) => {
      if (currentValue && AC.value === currentValue) {
        return { custom: error };
      }

      return null;
    };
  }

  // Verifica si hay cambios en la data
  isChangedData(): boolean {
    const data = this.policiesQuoteService.getVehicleData();

    return (
      data.numeroChasis !== this.form.controls['chassis'].value ||
      data.numeroMotor !== this.form.controls['motor'].value ||
      data.cantidadAccesorios !== parseInt(this.form.controls['accessories'].value, 10)
    );
  }
}
