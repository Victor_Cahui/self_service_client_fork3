import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleBuyStepFourGuard } from '@mx/guards/vehicle/vehicle-buy-step-four.guard';
import { VehicleBuyStepThreeGuard } from '@mx/guards/vehicle/vehicle-buy-step-three.guard';
import { VehicleBuyStepTwoGuard } from '@mx/guards/vehicle/vehicle-buy-step-two.guard';
import { VehicleBuyGuard } from '@mx/guards/vehicle/vehicle-buy.guard';
import { VehiclesQuoteBuyComponent } from './vehicles-quote-buy.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteBuyComponent,
    canActivate: [VehicleBuyGuard],
    children: [
      {
        path: '',
        redirectTo: 'step1'
      },
      {
        path: 'step1',
        canActivate: [],
        loadChildren: './vehicles-quote-buy-step-one/vehicles-quote-buy-step-one.module#VehiclesQuoteBuyStepOneModule'
      },
      {
        path: 'step2',
        canActivate: [VehicleBuyStepTwoGuard],
        loadChildren: './vehicles-quote-buy-step-two/vehicles-quote-buy-step-two.module#VehiclesQuoteBuyStepTwoModule'
      },
      {
        path: 'step3',
        canActivate: [VehicleBuyStepThreeGuard],
        loadChildren:
          './vehicles-quote-buy-step-three/vehicles-quote-buy-step-three.module#VehiclesQuoteBuyStepThreeModule'
      },
      {
        path: 'step4',
        canActivate: [VehicleBuyStepFourGuard],
        loadChildren:
          './vehicles-quote-buy-step-four/vehicles-quote-buy-step-four.module#VehiclesQuoteBuyStepFourModule'
      }
    ]
  },
  {
    path: 'contact',
    canActivate: [VehicleBuyGuard],
    loadChildren: './../vehicles-quote-detail/vehicles-quote-other/vehicles-quote-other.module#VehiclesQuoteOtherModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteBuyRoutingModule {}
