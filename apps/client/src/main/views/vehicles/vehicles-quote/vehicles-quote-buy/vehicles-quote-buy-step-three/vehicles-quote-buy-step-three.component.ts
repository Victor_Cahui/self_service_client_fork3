import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import {
  MFP_Compra_Tu_Seguro_Paso_3_16A,
  MFP_Compra_Tu_Seguro_Paso_3_16B
} from '@mx/settings/constants/events.analytics';
import { VehicleQuoteBuyLang } from '@mx/settings/lang/vehicles.lang';

@Component({
  selector: 'client-vehicles-quote-buy-step-three',
  templateUrl: './vehicles-quote-buy-step-three.component.html'
})
export class VehiclesQuoteBuyStepThreeComponent implements OnInit {
  selectInspection: boolean;
  selfInspection: boolean;
  onSiteInspection: boolean;

  ga: Array<IGaPropertie>;

  constructor(private readonly quoteService: PoliciesQuoteService, private readonly router: Router) {
    this.quoteService.setCurrentStep(3);
    this.ga = [MFP_Compra_Tu_Seguro_Paso_3_16A(), MFP_Compra_Tu_Seguro_Paso_3_16B()];
  }

  ngOnInit(): void {
    const auto = this.quoteService.selfInspection();
    if (auto) {
      this.setView(true, false, false);
    } else {
      this.setView(false, true, false);
    }
  }

  onSelectType(type: number): void {
    if (type === VehicleQuoteBuyLang.StepSelectInspection.Options.onsiteInspection.Id) {
      this.setView(false, true, false);
    } else {
      this.setView(false, false, true);
    }
  }

  goBack(target?: number): void {
    if (this.quoteService.selfInspection()) {
      switch (target) {
        case VehicleQuoteBuyLang.StepSelectInspection.Options.onsiteInspection.Id:
          this.setView(false, true, false);
          break;
        case VehicleQuoteBuyLang.StepSelectInspection.Options.selfInspection.Id:
          this.setView(false, false, true);
          break;
        default:
          this.setView(true, false, false);
          break;
      }
    } else {
      this.router.navigate(['/vehicles/quote/buy/step2']);
    }
  }

  setView(select: boolean, onSite: boolean, self: boolean): void {
    this.selectInspection = select;
    this.selfInspection = self;
    this.onSiteInspection = onSite;
  }
}
