import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesPaymentConfirmComponent } from './vehicles-payment-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesPaymentConfirmComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesPaymentConfirmRoutingModule {}
