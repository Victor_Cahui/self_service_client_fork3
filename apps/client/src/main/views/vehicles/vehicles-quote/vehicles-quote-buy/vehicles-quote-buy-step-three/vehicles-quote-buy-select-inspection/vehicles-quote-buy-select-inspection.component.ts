import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MfModal } from '@mx/core/ui';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { INSPECTION_ICON } from '@mx/settings/constants/images-values';
import { VEHICLES_QUOTE_BUY_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehicleQuoteBuyLang } from '@mx/settings/lang/vehicles.lang';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import {
  IClientVehicleDetail,
  IInspection,
  IInspectionConditionsRequest
} from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

import { GAPushEvent } from '@mx/core/shared/common/services/google.analytics.utils';
import { MFP_Compra_Tu_Seguro_Paso_3_16B } from '@mx/settings/constants/events.analytics';

@Component({
  selector: 'client-vehicles-quote-buy-select-inspection',
  templateUrl: './vehicles-quote-buy-select-inspection.component.html'
})
export class VehiclesQuoteBuySelectInspectionComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(MfModal) modal: MfModal;

  @Output() confirmSelect: EventEmitter<number> = new EventEmitter<number>();

  generalLang = GeneralLang;
  lang = VehicleQuoteBuyLang.StepSelectInspection;
  selectedInspection = VehicleQuoteBuyLang.StepSelectInspection.Options.onsiteInspection.Id;
  agreement: boolean;
  vehicles = VEHICLES_QUOTE_BUY_HEADER;
  inspectionIcon = INSPECTION_ICON;
  conditions: string;
  extraLabel: string;
  // tslint:disable-next-line: no-unbound-method
  objectKeys = Object.keys;

  vehicleData: IClientVehicleDetail;

  conditionsSub: Subscription;

  quote: IPaymentQuoteCard;
  inspection: IInspection;

  private readonly _selectedInspection: string;

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly policiesQuoteService: PoliciesQuoteService,
    private readonly paymentService: PaymentService,
    private readonly router: Router,
    protected gaService: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.quote = this.paymentService.getCurrentQuote();
    this.inspection = this.quote && this.quote.inspection ? this.quote.inspection : null;
    if (this.inspection) {
      if (this.inspection.previousStep) {
        setTimeout(() => {
          this.confirmSelect.emit(this.inspection.previousStep);
        });
      }
      this.loadStorage();
    }
    this.vehicleData = this.policiesQuoteService.getVehicleData();
    this.extraLabel = `<span class="g-c--green1 g-font--medium"> ${this.lang.Agreement.Link}</span>`;
    this.getConditions();
  }

  loadStorage(): void {
    this.onSelectInspection(
      this.inspection.selfInspection ? this.lang.Options.selfInspection.Id : this.lang.Options.onsiteInspection.Id
    );
    this.agreement = this.inspection.conditionsAgreement;
  }

  onSelectInspection({ Id, Title }: any): void {
    // HACK: para q no se ejecute 2 veces al cambiar de check
    if (this.selectedInspection === Id) {
      return void 0;
    }

    this.selectedInspection = Id;
    const dataForGA = {
      ...MFP_Compra_Tu_Seguro_Paso_3_16B(),
      label: MFP_Compra_Tu_Seguro_Paso_3_16B().label.replace('{{label}}', Title)
    };
    GAPushEvent(dataForGA);
  }

  onAgree(event: any): void {
    this.agreement = event.target.checked;
  }

  getInspectionType(e, y): void {}

  getConditions(): void {
    const data = this.policiesQuoteService.getInspectionConditions();
    if (data) {
      this.conditions = data;
    } else {
      const params: IInspectionConditionsRequest = {
        codigoApp: APPLICATION_CODE,
        placa: this.vehicleData.placa,
        numeroChasis: this.vehicleData.numeroChasis,
        numeroMotor: this.vehicleData.numeroMotor
      };
      this.conditionsSub = this.vehicleService.getInspectionConditions(params).subscribe(
        res => {
          this.conditions = res.descripcionCondicion || '';
          this.policiesQuoteService.setInspectionConditions(this.conditions);
          this.conditionsSub.unsubscribe();
        },
        () => {
          this.conditionsSub.unsubscribe();
        }
      );
    }
  }

  conditionsOpen(): void {
    this.modal.open();
  }

  ngOnDestroy(): void {
    if (this.conditionsSub) {
      this.conditionsSub.unsubscribe();
    }
  }

  // Vuelve a paso 2
  back(): void {
    this.router.navigate(['/vehicles/quote/buy/step2']);
  }

  gotoInspection(): void {
    // Guardar seleccion en Storage
    this.quote.inspection = {
      selfInspection: this.selectedInspection === this.lang.Options.selfInspection.Id,
      conditionsAgreement: this.agreement,
      previousStep: this.selectedInspection,
      data: this.inspection && this.inspection.data
    };
    this.paymentService.setCurrentQuote(this.quote);

    // Va a paso 3a o 3b
    this.confirmSelect.emit(this.selectedInspection);
  }
}
