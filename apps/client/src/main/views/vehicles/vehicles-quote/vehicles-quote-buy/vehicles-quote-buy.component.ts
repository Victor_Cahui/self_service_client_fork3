import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CardSummaryQuoteComponent } from '@mx/components/shared/payments/card-summary-quote/card-summary-quote.component';
import { StepperComponent } from '@mx/core/ui/lib/components/stepper/stepper.component';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { CAR_ICON, COIN_ICON, PROFILE_ICON } from '@mx/settings/constants/images-values';
import { OPTIONAL_STEP, VEHICLES_QUOTE_BUY_STEP } from '@mx/settings/constants/router-step';
import { VEHICLES_QUOTE_BUY_HEADER } from '@mx/settings/constants/router-titles';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-vehicles-quote-buy',
  templateUrl: './vehicles-quote-buy.component.html'
})
export class VehiclesQuoteBuyComponent implements OnInit, OnDestroy {
  @ViewChild(StepperComponent) stepper: StepperComponent;

  stepList = [...VEHICLES_QUOTE_BUY_STEP];
  icon: string;
  currentStep: number;
  currentStepSub: Subscription;
  vigencyDateSub: Subscription;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly quoteService: PoliciesQuoteService,
    protected paymentService: PaymentService,
    private readonly cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.headerHelperService.set(VEHICLES_QUOTE_BUY_HEADER);
    this.stepList[OPTIONAL_STEP].visible = false;
    this.currentStepSub = this.quoteService.getCurrentStepSubject().subscribe(
      data => {
        this.currentStep = data;
        this.cdr.markForCheck();
        this.setIcon();
        // Muestra/oculta paso Inspeccion
        if (this.currentStep >= OPTIONAL_STEP) {
          const vehicle = this.quoteService.getVehicleData();
          if (vehicle) {
            this.stepList[OPTIONAL_STEP].visible = vehicle.requiereInspeccion;
            this.stepper.stepList = this.stepList;
          }
        }
        // Cierra card en responsive
        CardSummaryQuoteComponent.updateView.next(true);
      },
      () => {},
      () => {
        this.currentStepSub.unsubscribe();
      }
    );

    this.vigencyDateSub = this.paymentService.onVigencyDateChange().subscribe(dates => {
      if (dates.start) {
        const quote = this.paymentService.getCurrentQuote();
        if (quote.startDate !== dates.start && quote.endDate !== dates.end) {
          quote.startDate = dates.start;
          quote.endDate = dates.end;
          this.paymentService.setCurrentQuote(quote);
        }
      }
    });
  }

  setIcon(): void {
    this.icon =
      this.currentStep === 1 || this.currentStep === 3
        ? CAR_ICON
        : this.currentStep === 2
        ? PROFILE_ICON
        : this.currentStep === 4
        ? COIN_ICON
        : '';
  }

  ngOnDestroy(): void {
    if (this.currentStepSub) {
      this.currentStepSub.unsubscribe();
    }
    if (this.vigencyDateSub) {
      this.vigencyDateSub.unsubscribe();
    }
  }
}
