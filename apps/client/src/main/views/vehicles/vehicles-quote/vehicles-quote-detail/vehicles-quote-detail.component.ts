import { Component, HostBinding } from '@angular/core';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { VEHICLES_QUOTE_DETAIL_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-vehicles-quote-detail-component',
  templateUrl: './vehicles-quote-detail.component.html'
})
export class VehiclesQuoteDetailComponent {
  @HostBinding('class') class = 'w-100';

  constructor(private readonly headerHelperService: HeaderHelperService) {
    this.headerHelperService.set(VEHICLES_QUOTE_DETAIL_HEADER);
  }
}
