import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentMethodModule } from '@mx/components';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { VehiclesQuoteStepThreeComponent } from './vehicles-quote-step-three.component';
import { VehiclesQuoteStepThreeRoutingRoutes } from './vehicles-quote-step-three.routing';

@NgModule({
  imports: [CommonModule, VehiclesQuoteStepThreeRoutingRoutes, StepperModule, PaymentMethodModule],
  declarations: [VehiclesQuoteStepThreeComponent]
})
export class VehiclesQuoteStepThreeModule {}
