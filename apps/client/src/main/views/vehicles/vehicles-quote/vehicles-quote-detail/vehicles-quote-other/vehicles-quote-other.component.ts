import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormContactedByAgentComponent } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.component';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IContactData } from '@mx/statemanagement/models/client.models';

@Component({
  selector: 'client-vehicles-quote-other',
  templateUrl: './vehicles-quote-other.component.html'
})
export class VehiclesQuoteOtherComponent implements OnInit {
  @ViewChild(FormContactedByAgentComponent) formContactedByAgentComponent: FormContactedByAgentComponent;

  showTitle: boolean;
  showActions: boolean;
  disableButtonSend: boolean;

  contactData: IContactData;
  btnSend = GeneralLang.Buttons.Send;
  btnBack = GeneralLang.Buttons.Back;
  btnConfirm = GeneralLang.Buttons.Confirm;
  title = VehiclesLang.Titles.QuoteContact;
  message = VehiclesLang.Messages.QuoteContact;

  constructor(private readonly router: Router, private readonly location: Location) {}

  ngOnInit(): void {
    this.showTitle = true;
    this.showActions = true;
    this.disableButtonSend = true;
    this.contactData = {
      subject: POLICY_TYPES.MD_AUTOS.code.replace('MD_', ''),
      description: PolicyLang.Texts.ContactReason.QuotePolicy.toUpperCase()
    };
  }

  // Enviar Datos
  actionConfirm(): void {
    this.disableButtonSend = true;
    this.formContactedByAgentComponent.sendMessage();
  }

  // Volver a Form datos de Vehículo
  actionCancel(): void {
    this.formContactedByAgentComponent.formContact.reset();
    this.formContactedByAgentComponent.isSent = false;
    this.location.back();
  }

  enableButton(valid: boolean): void {
    this.disableButtonSend = !valid;
  }

  // Datos enviados
  doSuccess(): void {
    this.disableButtonSend = false;
    this.showTitle = false;
    this.showActions = false;
  }

  // Error en envio de datos
  showError(message: string): void {
    this.disableButtonSend = false;
  }

  // Aceptar en mensaje de enviado
  onConfirm(): void {
    this.disableButtonSend = true;
    this.showActions = true;
    this.router.navigate(['/vehicles/quote']);
  }
}
