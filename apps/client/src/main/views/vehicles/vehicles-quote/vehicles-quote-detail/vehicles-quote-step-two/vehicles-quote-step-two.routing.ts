import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteStepTwoComponent } from './vehicles-quote-step-two.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteStepTwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteStepTwoRoutingModule {}
