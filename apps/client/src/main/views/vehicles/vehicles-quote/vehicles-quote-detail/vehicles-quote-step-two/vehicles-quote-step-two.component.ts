import { DecimalPipe } from '@angular/common';
import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseVehicleComparePolicies } from '@mx/components/shared/compare-policy/base-vehicle-compare-policies';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import {
  MFP_Cotiza_Tu_Seguro_Paso_2_13A,
  MFP_Cotiza_Tu_Seguro_Paso_2_Comprar_13B
} from '@mx/settings/constants/events.analytics';
import { EPaymentProcess } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { VEHICLES_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { VEHICLES_QUOTE_DETAIL_HEADER } from '@mx/settings/constants/router-titles';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { IChoiseTime } from '@mx/statemanagement/models/policy.interface';
import { IClientVehicleDetail } from '@mx/statemanagement/models/vehicle.interface';

@Component({
  selector: 'client-vehicles-quote-step-two',
  templateUrl: './vehicles-quote-step-two.component.html',
  providers: [DecimalPipe]
})
export class VehiclesQuoteStepTwoComponent extends BaseVehicleComparePolicies implements OnInit {
  @HostBinding('class') class = 'w-100';

  lang = VehiclesLang;
  titleEdit = VehiclesLang.Titles.DataYourVehicle;
  vehicleData: IClientVehicleDetail;
  vehicleModel: string;
  vehicleValue: string;
  stepList = VEHICLES_QUOTE_STEP;
  ga: Array<IGaPropertie>;

  constructor(
    protected policyListService: PolicyListService,
    protected headerHelperService: HeaderHelperService,
    protected quoteService: PoliciesQuoteService,
    protected vehicleService: VehicleService,
    private readonly paymentService: PaymentService,
    protected decimalPipe: DecimalPipe,
    private readonly router: Router
  ) {
    super(policyListService, headerHelperService, quoteService, vehicleService, decimalPipe);
    this.headerHelperService.set(VEHICLES_QUOTE_DETAIL_HEADER);
    this.ga = [MFP_Cotiza_Tu_Seguro_Paso_2_13A(), MFP_Cotiza_Tu_Seguro_Paso_2_Comprar_13B()];
  }

  ngOnInit(): void {
    this.vehicleData = this.quoteService.getVehicleData();
    if (this.vehicleData) {
      this.vehicleModel = `${this.vehicleData.marca} ${this.vehicleData.modelo} ${this.vehicleData.anio}`;
      this.vehicleValue = `${StringUtil.getMoneyDescription(this.vehicleData.codigoMonedaValorSugerido)} ${
        this.vehicleData.valorSugerido
      }`;
    }
    this.viewInitial();
  }

  // Ir a comprar
  onPurchase(values): void {
    const item = values.data;
    this.setSelectedPlan(values.tab);
    this.vehicleData = this.quoteService.getVehicleData();
    // Guardar datos para pago
    const data: IPaymentQuoteCard = {
      typePayment: EPaymentProcess.BUY_VEHICLE,
      policy: item.title,
      policyType: POLICY_TYPES.MD_AUTOS.code,
      currencySymbol: item.moneySymbol,
      total: item.quote,
      totalAmount: `${item.moneySymbol}${this.decimalPipe.transform(item.quote, '1.2-2')}`,
      quota: `${item.moneySymbol}${this.decimalPipe.transform(item.monthlyCost, '1.2-2')}`,
      modalitySelected: {
        company: item.company,
        branch: item.branch,
        modality: item.modality,
        quoteNumber: item.quoteNumber,
        textPaymentFrecuency: item.textPaymentFrecuency,
        quotas: item.amountFee,
        fractionationCode: item.fractionationCode,
        years: item.years
      }
    };
    this.paymentService.setCurrentQuote(data);
    setTimeout(() => {
      this.router.navigate(['/vehicles/quote/buy']);
    }, 50);
  }

  // Volver a paso 1 para editar
  onEdit(event): void {
    this.setSelectedPlan();
    this.router.navigate([VEHICLES_QUOTE_STEP[0].route]);
  }

  // Guardar Plan seleccionado
  setSelectedPlan(tab?: number): void {
    const data: IChoiseTime = {
      years: this.timeSelected,
      month: this.monthSelected,
      selectedTab: tab
    };
    this.quoteService.setChoiseData(data);
  }
}
