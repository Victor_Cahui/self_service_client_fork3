import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteStepThreeComponent } from './vehicles-quote-step-three.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteStepThreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteStepThreeRoutingRoutes {}
