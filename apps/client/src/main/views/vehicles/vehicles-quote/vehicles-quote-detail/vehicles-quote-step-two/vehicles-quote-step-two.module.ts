import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PoliciesCompareModule } from '@mx/components/shared/compare-policy/policies-compare/policies-compare.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { VehiclesQuoteStepTwoComponent } from './vehicles-quote-step-two.component';
import { VehiclesQuoteStepTwoRoutingModule } from './vehicles-quote-step-two.routing';

@NgModule({
  imports: [CommonModule, PoliciesCompareModule, StepperModule, VehiclesQuoteStepTwoRoutingModule, MfLoaderModule],
  declarations: [VehiclesQuoteStepTwoComponent]
})
export class VehiclesQuoteStepTwoModule {}
