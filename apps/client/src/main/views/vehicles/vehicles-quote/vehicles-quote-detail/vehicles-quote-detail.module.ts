import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VehiclesQuoteDetailComponent } from './vehicles-quote-detail.component';
import { VehiclesQuoteDetailRoutingModule } from './vehicles-quote-detail.routing';

@NgModule({
  imports: [CommonModule, VehiclesQuoteDetailRoutingModule],
  declarations: [VehiclesQuoteDetailComponent]
})
export class VehiclesQuoteDetailModule {}
