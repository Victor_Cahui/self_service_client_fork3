import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleBuyConfirmGuard } from '@mx/guards/vehicle/vehicle-buy-confirm.guard';
import { VehiclesQuoteComponent } from './vehicles-quote.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteComponent
  },
  {
    path: 'detail',
    loadChildren: './vehicles-quote-detail/vehicles-quote-detail.module#VehiclesQuoteDetailModule'
  },
  {
    path: 'buy',
    loadChildren: './vehicles-quote-buy/vehicles-quote-buy.module#VehiclesQuoteBuyModule'
  },
  {
    path: 'payment-confirm',
    canActivate: [VehicleBuyConfirmGuard],
    loadChildren:
      './vehicles-quote-buy/vehicles-payment-confirm/vehicles-payment-confirm.module#VehiclesPaymentConfirmModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteRoutingModule {}
