import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, RouterState } from '@angular/router';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, filter } from 'rxjs/operators';

import { BaseCardAssistanceVehicle } from '@mx/components/vehicles/card-assistance-base/card-assistance-base.component';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { FrmProvider } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { ASSISTANCE, RECORDS } from '@mx/settings/lang/vehicles.lang';
import { getItemsByYear, getYearsToCboOptions, isAccidentsPath, isStolenPath } from './vehicles-records.utils';

@Component({
  selector: 'client-vehicles-records',
  templateUrl: './vehicles-records.component.html'
})
export class VehiclesRecordsComponent extends BaseCardAssistanceVehicle implements AfterViewInit, OnDestroy, OnInit {
  frm: FormGroup;
  getAllRecords;
  itemsPerPage = 10;
  labelPlural: string;
  labelSingular: string;
  msgNotFound: string;
  numberLabel: string;
  page = 1;
  title: string;
  total: number;
  txtFilter: string;
  yearsOptions;
  allrecords;
  cboYearRx: Subscription;

  private _recordService;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    protected _Router: Router,
    private readonly _FrmProvider: FrmProvider,
    private readonly headerHelperService: HeaderHelperService,
    private readonly vehicleService: VehicleService,
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService: GAService,
    protected generalService: GeneralService
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService, generalService, _Router);
  }

  ngOnInit(): void {
    this._initValues();
    this.getAllRecords(this.getParamsForRecords());
    this._mapState();
  }

  ngAfterViewInit(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.add('g-none');
  }

  ngOnDestroy(): void {
    this.cboYearRx && this.cboYearRx.unsubscribe();
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.remove('g-none');
  }

  pageChange(page: number): void {
    this.page = page;
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
  }

  getItems(): void {
    // HACK: solo definida x la herencia usada
  }

  viewDetail(item): void {
    this._Router.navigate([`/vehicles/stolen/assists/detail/${item.assistanceNumber}`]);
  }

  configView(config): void {
    this.headerHelperService.set({
      subTitle: config.subTitle,
      title: config.title.toUpperCase(),
      icon: '',
      back: true
    });
  }

  private _mapState(): void {
    this.cboYearRx = this.frm
      .get('year')
      .valueChanges.pipe(
        filter(v => !!v),
        distinctUntilChanged()
      )
      .subscribe(this._filter.bind(this));
  }

  private _filter(year: string): void {
    if (year === '0') {
      this.itemsHistory = [...this.allrecords];
      this.total = this.allrecords.length;

      return void 0;
    }

    this.itemsHistory = getItemsByYear(this.allrecords, year);
    this.total = this.itemsHistory.length;
  }

  private _accidentConfig(): void {
    this.title = RECORDS.TitleAccident;
    this.labelSingular = RECORDS.Label.Assistance;
    this.labelPlural = RECORDS.Label.Assists;
    this.numberLabel = ASSISTANCE.AssistanceNumber;
    this._recordService = this.vehicleService.getAccidents.bind(this.vehicleService);
    this.getAllRecords = this._getRecords.bind(this);
    this.configView({ title: RECORDS.Breadcrumb.Accident.Title, subTitle: RECORDS.Breadcrumb.Accident.SubTitle });
  }

  private _stolenConfig(): void {
    this.title = RECORDS.TitleStolen;
    this.labelSingular = RECORDS.Label.Assistance;
    this.labelPlural = RECORDS.Label.Assists;
    this.numberLabel = ASSISTANCE.StolenNumber;
    this._recordService = this.vehicleService.getStolens.bind(this.vehicleService);
    this.getAllRecords = this._getRecords.bind(this);
    this.configView({ title: RECORDS.Breadcrumb.Stolen.Title, subTitle: RECORDS.Breadcrumb.Stolen.SubTitle });
  }

  private _getRecords(params): void {
    this.showLoading = true;
    this._recordService(params).subscribe(
      response => {
        this.itemsHistory = this.sortHistory(response && (response.historial || []), 'registerDateObj', 'DESC');
        this.allrecords = [...this.itemsHistory];
        this.total = this.itemsHistory.length;
        this.yearsOptions = getYearsToCboOptions(this.itemsHistory);
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  private _setConfig(type: string): void {
    const types = {
      accident: this._accidentConfig.bind(this),
      stolen: this._stolenConfig.bind(this)
    };

    types[type] && types[type]();
  }

  private _getPageType(): string {
    const state: RouterState = this._Router.routerState;
    const url = state.snapshot.url;

    return isAccidentsPath(url) ? 'accident' : isStolenPath(url) ? 'stolen' : '';
  }

  private _initValues(): void {
    this.frm = this._FrmProvider.VehiclesRecordsComponent();
    this.frm.get('year').setValue(0);
    this.txtFilter = RECORDS.Filter;
    this.msgNotFound = ASSISTANCE.NoRegisterHistory;
    this._setConfig(this._getPageType());
  }
}
