import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfButtonModule, MfCardModule, MfPaginatorModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';

import { ItemRequestModule } from '@mx/components/shared/timeline-item-list/components/item-request/item-request.module';
import { VehiclesRecordsComponent } from './vehicles-records.component';
import { VehiclesRecordsRoutingModule } from './vehicles-records.routing';

@NgModule({
  imports: [
    CommonModule,
    InputSearchModule,
    ItemNotFoundModule,
    ItemRequestModule,
    MfButtonModule,
    MfCardModule,
    MfLoaderModule,
    MfPaginatorModule,
    MfSelectModule,
    MfShowErrorsModule,
    ReactiveFormsModule,
    VehiclesRecordsRoutingModule
  ],
  declarations: [VehiclesRecordsComponent]
})
export class VehiclesRecordsModule {}
