import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesSoatModule } from '@mx/components/card-my-policies/card-my-policies-soat/card-my-policies-soat.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { VehiclesSoatListComponent } from './vehicles-soat-list.component';
import { VehiclesSoatListRoutingModule } from './vehicles-soat-list.routing';

@NgModule({
  imports: [
    BannerModule,
    CommonModule,
    CardMyPoliciesSoatModule,
    VehiclesSoatListRoutingModule,
    LandingModule,
    BannerCarouselModule
  ],
  declarations: [VehiclesSoatListComponent]
})
export class VehiclesSoatListModule {}
