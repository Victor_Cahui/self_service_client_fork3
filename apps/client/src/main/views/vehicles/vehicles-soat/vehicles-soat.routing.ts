import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './vehicles-soat-list/vehicles-soat-list.module#VehiclesSoatListModule'
  },
  {
    path: 'renew',
    loadChildren: '../../payments/payment-quota/payment-quota.module#PaymentQuotaModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesSoatRoutingModule {}
