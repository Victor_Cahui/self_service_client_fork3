import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VehiclesAccidentsComponent } from './vehicles-accidents.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesAccidentsComponent,
    children: [
      {
        path: '',
        redirectTo: 'assists',
        pathMatch: 'prefix'
      },
      {
        path: 'assists',
        loadChildren: './vehicle-accidents-assists/vehicle-accidents-assists.module#VehicleAccidentsAssistsModule'
      },
      {
        path: 'reports',
        loadChildren: './vehicle-accidents-report/vehicle-accidents-report.module#VehicleAccidentsReportModule'
      },
      {
        path: 'records',
        loadChildren: '../vehicles-records/vehicles-records.module#VehiclesRecordsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesAccidentsRoutingModule {}
