import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseCardAssistanceVehicle } from '@mx/components/vehicles/card-assistance-base/card-assistance-base.component';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { VEHICLES_ACCIDENTS_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IRequestItemView } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-vehicle-accidents-assists',
  templateUrl: './vehicle-accidents-assists.component.html'
})
export class VehicleAccidentsAssistsComponent extends BaseCardAssistanceVehicle implements OnInit {
  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly vehicleService: VehicleService,
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService: GAService,
    protected _Router: Router,
    protected generalService: GeneralService,
    public router: Router
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService, generalService, _Router);
    this.headerHelperService.set(VEHICLES_ACCIDENTS_HEADER);
    this.configUI = this.getRouteRecords();
  }

  getItems(params: GeneralRequest): void {
    this.showLoading = true;
    this.showNotification = false;
    this.vehicleSub = this.vehicleService.getAccidents(params).subscribe(
      res => {
        this.itemsHistory = this.sortHistory(res && (res.historial || []), 'registerDateObj', 'DESC');
        this.itemsProcess = this.formatProcess(res && (res.enProceso || []));
        this.showLoading = false;
      },
      () => {
        this.itemsHistory = [];
        this.itemsProcess = [];
        this.showLoading = false;
      }
    );
  }

  viewDetail(item: IRequestItemView): void {
    this.router.navigate([`/vehicles/accidents/assists/detail/${item.assistanceNumber}`]);
  }
}
