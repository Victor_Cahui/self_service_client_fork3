import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ViewTimelineModule } from '@mx/components/shared/timeline/view-timeline/view-timeline.module';
import { ModalAttachLetterModule } from '@mx/components/vehicles/modal-attach-letter/modal-attach-letter.module';
import { VehicleAccidentsAssistsDetailComponent } from './vehicle-accidents-assists-detail.component';
import { VehicleAccidentsAssistsDetailRouting } from './vehicle-accidents-assists-detail.routing';

@NgModule({
  imports: [CommonModule, ViewTimelineModule, VehicleAccidentsAssistsDetailRouting, ModalAttachLetterModule],
  declarations: [VehicleAccidentsAssistsDetailComponent]
})
export class VehicleAccidentsAssistsDetailModule {}
