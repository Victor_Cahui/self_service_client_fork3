import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationsnModule } from '@mx/components/notifications/notifications.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { ViewItemListModule } from '@mx/components/shared/timeline-item-list/view-item-list/view-item-list.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { VehicleAccidentsAssistsComponent } from '../vehicle-accidents-assists/vehicle-accidents-assists.component';
import { VehicleAccidentsAssistsRouting } from '../vehicle-accidents-assists/vehicle-accidents-assists.routing';

@NgModule({
  imports: [
    CommonModule,
    VehicleAccidentsAssistsRouting,
    ViewItemListModule,
    NotificationsnModule,
    MfLoaderModule,
    BannerCarouselModule
  ],
  declarations: [VehicleAccidentsAssistsComponent]
})
export class VehicleAccidentsAssistsModule {}
