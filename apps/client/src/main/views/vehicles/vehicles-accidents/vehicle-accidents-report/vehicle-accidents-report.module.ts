import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationHelpModule } from '@mx/components/notifications/notification-help/notification-help.module';
import { NotificationsnModule } from '@mx/components/notifications/notifications.module';
import { BannerAppDownloadModule } from '@mx/components/shared/banner-app-download/banner-app-download.module';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { PageTitleModule } from '@mx/components/shared/page-title/page-title.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { VehicleAccidentsReportComponent } from './vehicle-accidents-report.component';
import { VehicleAccidentsReportRouting } from './vehicle-accidents-report.routing';

@NgModule({
  imports: [
    CommonModule,
    VehicleAccidentsReportRouting,
    PageTitleModule,
    BannerAppDownloadModule,
    LandingModule,
    NotificationHelpModule,
    NotificationsnModule,
    GoogleModule
  ],
  declarations: [VehicleAccidentsReportComponent]
})
export class VehicleAccidentsReportModule {}
