import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { MfModalErrorComponent } from '@mx/core/ui/lib/components/modals/modal-error/modal-error.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { TRAVEL_INSURANCE_QUOTE_DIR } from '@mx/settings/constants/general-values';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { LIFE_INSURANCE_HEADER } from '@mx/settings/constants/router-titles';
import { TRAVEL_INSURANCE_FAQS } from '@mx/settings/faqs/travel-insurance.faq';
import * as source from '@mx/settings/lang/travel.lang';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-quote-component',
  templateUrl: './travel-quote.component.html'
})
export class TravelQuoteComponent extends ConfigurationBase implements OnInit {
  @ViewChild(MfModalErrorComponent) modalError: MfModalErrorComponent;
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;

  staticBanners: string;
  titleQuote = source.TravelLang.Titles.Form;
  textQuote = source.TravelLang.Texts.Quote;
  btnQuote = source.TravelLang.Titles.QuoteNow;

  faqs = TRAVEL_INSURANCE_FAQS;
  insuranceTop = source.INSURANCE_TOP;
  howToWork = source.HOW_TO_WORK;
  paymentOptions = source.PAYMENTS_OPTIONS;
  subTitleFaq = LIFE_INSURANCE_HEADER.title;
  masonry = true;

  configurationSub: Subscription;
  sendDataSub: Subscription;
  scrollSub: Subscription;

  pathImgBanner: IPathImageSize;
  staticlImgs: IPathImageSize;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_VIAJES.code;
    this.staticBanners = POLICY_TYPES[this.policyType].staticBanners;
    this.pathImgBanner = {
      sm: this.staticBanners + STATIC_BANNERS.sm,
      lg: this.staticBanners + STATIC_BANNERS.lg,
      xl: this.staticBanners + STATIC_BANNERS.xl
    };
    this.staticlImgs = {
      xs: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-xs.png`,
      sm: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-sm.png`,
      md: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-md.png`,
      lg: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-lg.png`,
      xl: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-xl.png`
    };
    this.loadConfiguration();
  }

  quote(): void {
    window.open(TRAVEL_INSURANCE_QUOTE_DIR, '_blank');
  }

  openModalMoreFaqs(): void {
    this.modalFaqs.open();
  }

  // Scroll a form
  eventQuote(): void {
    this.scrollSub = ScrollUtil.goTo(0).subscribe(
      () => {},
      () => {},
      () => {
        this.scrollSub.unsubscribe();
      }
    );
  }
}
