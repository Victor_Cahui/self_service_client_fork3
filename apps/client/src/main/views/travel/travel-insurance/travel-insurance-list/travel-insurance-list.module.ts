import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { LandingTravelModule } from '@mx/components/travel/landing-travel/landing-travel.module';
import { TravelInsuranceListComponent } from './travel-insurance-list.component';
import { TravelInsuranceListRoutingModule } from './travel-insurance-list.routing';

@NgModule({
  imports: [
    BannerCarouselModule,
    BannerModule,
    CommonModule,
    TravelInsuranceListRoutingModule,
    CardMyPoliciesByTypeModule,
    LandingTravelModule,
    CardWhatYouWantToDoModule
  ],
  declarations: [TravelInsuranceListComponent]
})
export class TravelInsuranceListModule {}
