import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Mis_Polizas_8A, MFP_Mis_Polizas_8B } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-travel-insurance-list',
  templateUrl: './travel-insurance-list.component.html'
})
export class TravelInsuranceListComponent extends ConfigurationBase implements OnInit {
  ga: Array<IGaPropertie>;
  staticlImgs: IPathImageSize;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.policyType = POLICY_TYPES.MD_VIAJES.code;
    this.ga = [MFP_Mis_Polizas_8A(), MFP_Mis_Polizas_8B()];
  }

  ngOnInit(): void {
    this.staticlImgs = {
      xs: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-xs.png`,
      sm: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-sm.png`,
      md: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-md.png`,
      lg: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-lg.png`,
      xl: `${POLICY_TYPES.MD_VIAJES.staticBanners}/static/banner-xl.png`
    };
    this.policiesInfoService.resetClientPolicyResponse();
    this.loadInfoProfile();
    this.loadConfiguration();
  }
}
