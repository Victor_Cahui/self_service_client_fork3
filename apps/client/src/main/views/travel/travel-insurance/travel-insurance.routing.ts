import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './travel-insurance-list/travel-insurance-list.module#TravelInsuranceListModule'
  },
  {
    path: 'faqs',
    loadChildren: './travel-insurance-faq/travel-insurance-faq.module#TravelInsuranceFAQModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './travel-insurance-by-id/travel-insurance-by-id.module#TravelInsuranceByIdModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelInsuranceRoutingModule {}
