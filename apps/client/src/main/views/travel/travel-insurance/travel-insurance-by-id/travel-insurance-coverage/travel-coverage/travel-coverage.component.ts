import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { COVERAGES_TRAVEL, ICON_COVERAGE_DEFAULT, QUERY_SOURCE } from '@mx/settings/constants/coverage-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { COVERAGE } from '@mx/settings/lang/life.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { CoveragesGroup, TravelLang as TL } from '@mx/settings/lang/travel.lang';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import {
  ICoveragesPolicy,
  ICoveragesPolicyRequest,
  IPoliciesByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'client-travel-coverage-component',
  templateUrl: './travel-coverage.component.html'
})
export class TravelCoverageComponent implements OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';

  coverageTitle: string;
  policyType: string;
  policyNumber: string;
  description: string;
  showLoading: boolean;
  policiesServiceSub: Subscription;
  coverageList: Array<ICoveragesPolicy>;
  itemList: Array<ICoverageItem>;
  itemListBase: Array<ICoverageIcons>;
  clientPolicy: IPoliciesByClientResponse;
  titlesGroup: any;
  coveragesListGroup: any;
  coveragesGroup = CoveragesGroup;
  title: string;
  coverageDescription: string;
  vigencyDays: number;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService,
    protected headerHelperService: HeaderHelperService
  ) {
    this.title = PolicyLang.Titles.Coverages;
  }

  ngOnInit(): void {
    this.coverageTitle = COVERAGE.title.toUpperCase();
    this.description = COVERAGE.content;
    this.policyType = POLICY_TYPES.MD_VIAJES.code;
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    timer(0)
      .toPromise()
      .then(() => {
        this.clientPolicy = this.policiesInfoService.getClientPolicy(this.policyNumber);
        this.description = this.description.replace(
          '{{typeInsured}}',
          (this.clientPolicy.descripcionPoliza || '').toUpperCase()
        );
      });
    this.getCoverages();
  }

  getCoverages(): void {
    this.showLoading = true;
    this.policiesServiceSub = this.policiesService
      .getCoveragesByPolicy({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        tipoRamoPoliza: this.policyType,
        origenConsulta: QUERY_SOURCE.POLICY_DETAIL
      } as ICoveragesPolicyRequest)
      .subscribe(
        (res: any) => {
          // tslint:disable-next-line: no-parameter-reassignment
          res = res || [];
          this.coverageList = res.coberturas || [];
          this.setGroupList(this.coverageList);
          this.vigencyDays = res.vigencia;
          this.setCoverageDescription();
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
          this.policiesServiceSub.unsubscribe();
        }
      );
  }

  setCoverageDescription(): void {
    this.headerHelperService.get().subscribe(header => {
      this.coverageDescription = `${TL.Texts.CoverageDescription.replace('{{policyName}}', header.title)}
        ${this.vigencyDays ? TL.Texts.VigencyDescription.replace('{{vigency}}', this.vigencyDays.toString()) : ''}`;
    });
  }

  setDataItemCoverage(list: Array<ICoveragesPolicy>): Array<ICoverageItem> {
    let itemList = [];
    itemList = list.map(item => {
      const iconCoverage = COVERAGES_TRAVEL.find(c => {
        return c.key === item.llave;
      });
      const itemCoverage: ICoverageItem = {
        title: item.tituloInfo,
        description: item.descripcionInfo,
        question: item.accionTexto,
        questionBool: item.accion,
        routeKey: item.accionLlave,
        iconKey: item.llave,
        icon: (iconCoverage && iconCoverage.icon) || ICON_COVERAGE_DEFAULT
      };

      return itemCoverage;
    });

    return itemList;
  }

  setGroupList(list: Array<ICoveragesPolicy>): void {
    this.titlesGroup = list.map(item => item.nombreGrupo);
    const titles = [];
    const itemsGroupList = [];
    this.titlesGroup.forEach(element => (titles.indexOf(element) === -1 ? titles.push(element) : null));
    this.titlesGroup = titles;

    // orden: PRINCIPALES, ADICIONALES
    if (this.titlesGroup.includes(this.coveragesGroup.Main)) {
      const first = this.coveragesGroup.Main;
      this.titlesGroup.sort((a, b) => (a === first ? -1 : b === first ? 1 : 0));
    }

    this.titlesGroup.forEach(element => {
      this.coveragesListGroup = list.filter(item => item.nombreGrupo === element);
      itemsGroupList.push({
        title: `${PolicyLang.Titles.Coverages} ${element}`,
        items: this.setDataItemCoverage(this.coveragesListGroup)
      });
    });

    this.coveragesListGroup = itemsGroupList;
  }
}
