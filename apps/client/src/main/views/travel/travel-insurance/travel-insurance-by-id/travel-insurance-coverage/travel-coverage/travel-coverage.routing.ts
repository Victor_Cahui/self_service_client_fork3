import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TravelCoverageComponent } from './travel-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: TravelCoverageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelCoverageRoutingModule {}
