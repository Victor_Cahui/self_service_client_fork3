import { Component } from '@angular/core';
import { EXCLUSIONS } from '@mx/settings/lang/travel.lang';

@Component({
  selector: 'client-travel-exclusions-component',
  templateUrl: './travel-exclusions.component.html'
})
export class TravelExclusionsComponent {
  content = EXCLUSIONS;
}
