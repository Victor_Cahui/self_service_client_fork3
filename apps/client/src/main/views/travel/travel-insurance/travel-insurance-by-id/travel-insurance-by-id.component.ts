import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { TRAVEL_INSURANCE_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-travel-insurance-by-id',
  templateUrl: './travel-insurance-by-id.component.html'
})
export class TravelInsuranceByIdComponent implements OnDestroy, OnInit {
  tabItemsList: Array<ITabItem>;
  constructor(private readonly headerHelperService: HeaderHelperService) {
    this.tabItemsList = TRAVEL_INSURANCE_TAB;
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
  }
  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
