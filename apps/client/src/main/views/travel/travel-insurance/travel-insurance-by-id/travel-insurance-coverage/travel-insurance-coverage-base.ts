import { ActivatedRoute, Router } from '@angular/router';
import { CardDetailPolicyBase } from '@mx/components/shared/card-detail-policy/card-detail-policy-base';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { HOUSEHOLD_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';

export class TravelInsuranceCoverageBase extends CardDetailPolicyBase {
  options: Array<ITabItem>;
  hideService: boolean;
  coveragesSub: Subscription;
  itemsList: Array<ICoverageItem>;
  itemsListBase: Array<ICoverageIcons>;
  title: string;
  text: string;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      false,
      _Autoservicios
    );
    this.options = HOUSEHOLD_COVERAGE_TABS;
    this.policyType = POLICY_TYPES.MD_VIAJES.code;
    this.loadInfoProfile();
    this.loadConfiguration();
    this.loadInfoDetail();
  }
}
