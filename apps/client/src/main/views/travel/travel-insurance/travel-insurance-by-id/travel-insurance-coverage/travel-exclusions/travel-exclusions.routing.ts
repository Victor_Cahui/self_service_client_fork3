import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TravelExclusionsComponent } from './travel-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: TravelExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelExclusionsRoutingModule {}
