import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { TravelCoverageComponent } from './travel-coverage.component';
import { TravelCoverageRoutingModule } from './travel-coverage.routing';

@NgModule({
  imports: [CommonModule, TravelCoverageRoutingModule, CardIconsCoverageModule, MfLoaderModule],
  declarations: [TravelCoverageComponent]
})
export class TravelCoverageModule {}
