import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { DownLoadConditionedModule } from '@mx/components/shared/download-conditioned/download-conditioned.module';
import { TravelExclusionsComponent } from './travel-exclusions.component';
import { TravelExclusionsRoutingModule } from './travel-exclusions.routing';

@NgModule({
  imports: [CommonModule, TravelExclusionsRoutingModule, CardContentModule, DownLoadConditionedModule],
  declarations: [TravelExclusionsComponent]
})
export class TravelExclusionsModule {}
