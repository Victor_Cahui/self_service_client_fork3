import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FoilAttachedModule } from '@mx/components/shared/foil-attached/foil-attached.module';
import { TravelFoilAttachedComponent } from './travel-foil-attached.component';
import { TravelFoilAttachedRoutingModule } from './travel-foil-attached.routing';

@NgModule({
  imports: [CommonModule, TravelFoilAttachedRoutingModule, FoilAttachedModule],
  declarations: [TravelFoilAttachedComponent]
})
export class TravelFoilAttachedModule {}
