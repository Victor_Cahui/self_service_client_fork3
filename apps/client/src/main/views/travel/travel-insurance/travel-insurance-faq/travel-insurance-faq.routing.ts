import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TravelInsuranceFAQComponent } from './travel-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: TravelInsuranceFAQComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelInsuranceFAQRoutingModule {}
