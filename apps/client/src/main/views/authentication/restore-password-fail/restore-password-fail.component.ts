import { Component } from '@angular/core';

@Component({
  selector: 'client-restore-password-fail',
  template: '<client-confirm-expired-token></client-confirm-expired-token>'
})
export class RestorePasswordFailComponent {}
