import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginTypeComponent } from './login-type.component';

const routes: Routes = [
  {
    path: '',
    component: LoginTypeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginTypeRoutingModule {}
