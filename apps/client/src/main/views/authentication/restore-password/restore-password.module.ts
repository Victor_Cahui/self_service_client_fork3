import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { RestorePasswordComponent } from './restore-password.component';
import { RestorePasswordRoutingModule } from './restore-password.routing';

@NgModule({
  imports: [RestorePasswordRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [RestorePasswordComponent],
  providers: []
})
export class RestorePasswordModule {}
