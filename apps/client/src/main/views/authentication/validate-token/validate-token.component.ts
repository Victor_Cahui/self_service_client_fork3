import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseLogin } from '@mx/components/login/base-login';
import { Subscription } from 'rxjs';

import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { AuthEndpoint } from '@mx/endpoints/auth.endpoint';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';

@Component({
  selector: 'client-validate-token',
  template: '<mf-loader-full-screen></mf-loader-full-screen>'
})
export class ValidateTokenComponent extends BaseLogin implements OnInit, OnDestroy {
  token: string;
  paramsSubs: Subscription;
  tokenOIMSubs: Subscription;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected clientService: ClientService,
    protected router: Router,
    protected policiesService: PoliciesService,
    protected generalService: GeneralService,
    protected paymentService: PaymentService,
    protected fb: FormBuilder,
    protected localStorage: LocalStorageService,
    protected adminService: UserAdminService,
    protected _Comun: Comun,
    protected _Autoservicios: Autoservicios,
    protected notifyService: NotificationService
  ) {
    super(
      router,
      policiesService,
      clientService,
      generalService,
      authService,
      paymentService,
      fb,
      localStorage,
      adminService,
      _Comun,
      _Autoservicios,
      notifyService
    );
  }

  ngOnInit(): void {
    this.paramsSubs = this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['token'];
      this.getUserData(this.token);
    });
  }

  ngOnDestroy(): void {
    this.paramsSubs.unsubscribe();
    this.tokenOIMSubs.unsubscribe();
  }

  private getUserData(token: string): void {
    this.tokenOIMSubs = this.authService
      .getUserFromTokenOIM(token)
      .subscribe(this.setLoginData.bind(this), this.redirectLoginOIM.bind(this));
  }

  private setLoginData(userData): void {
    const data = {
      token: this.token,
      typeDoc: userData.data.documentType,
      numberDoc: userData.data.documentNumber
    };

    this.clientID = {
      tipoDocumento: userData.data.documentType,
      documento: userData.data.documentNumber
    };

    this.userValidateFromOIM({ ...data });
  }

  private redirectLoginOIM(): void {
    window.location.href = AuthEndpoint.loginOIM;
  }
}
