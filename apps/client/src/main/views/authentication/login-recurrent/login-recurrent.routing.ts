import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginRecurrentComponent } from './login-recurrent.component';

const routes: Routes = [
  {
    path: '',
    component: LoginRecurrentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRecurrentRoutingModule {}
