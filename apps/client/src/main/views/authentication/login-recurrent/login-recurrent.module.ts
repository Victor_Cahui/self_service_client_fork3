import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { LoginRecurrentComponent } from './login-recurrent.component';
import { LoginRecurrentRoutingModule } from './login-recurrent.routing';

@NgModule({
  imports: [LoginRecurrentRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [LoginRecurrentComponent],
  providers: []
})
export class LoginRecurrentModule {}
