import { Component } from '@angular/core';

@Component({
  selector: 'client-restore-password-success',
  template: '<client-confirm-restore-password></client-confirm-restore-password>'
})
export class RestorePasswordSuccessComponent {}
