import { Component } from '@angular/core';

@Component({
  selector: 'client-app-enable-user',
  template: '<client-form-enable-user></client-form-enable-user>'
})
export class EnableUserComponent {}
