import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { EnableUserComponent } from './enable-user.component';
import { EnableUserRoutingModule } from './enable-user.routing';

@NgModule({
  imports: [EnableUserRoutingModule, ComponentesLogin],
  exports: [EnableUserComponent],
  declarations: [EnableUserComponent]
})
export class EnableUserModule {}
