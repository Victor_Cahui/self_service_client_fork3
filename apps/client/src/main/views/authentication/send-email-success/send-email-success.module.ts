import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { SendEmailSuccessComponent } from './send-email-success.component';
import { SendEmailSuccessRoutingModule } from './send-email-success.routing';

@NgModule({
  imports: [SendEmailSuccessRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [SendEmailSuccessComponent],
  providers: []
})
export class SendEmailSuccessModule {}
