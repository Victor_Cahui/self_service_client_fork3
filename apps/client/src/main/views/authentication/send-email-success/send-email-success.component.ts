import { Component } from '@angular/core';

@Component({
  selector: 'client-send-email-success',
  template: '<client-confirm-send-email></client-confirm-send-email>'
})
export class SendEmailSuccessComponent {}
