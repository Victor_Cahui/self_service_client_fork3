import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyPoliciesComponent } from './my-policies/my-policies.component';

const routes: Routes = [
  {
    path: '',
    component: MyPoliciesComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPoliciesRoutingModule {}
