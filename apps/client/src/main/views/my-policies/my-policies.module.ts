import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesModule } from '@mx/components/card-my-policies/card-my-policies/card-my-policies.module';
import { MyPoliciesRoutingModule } from './my-policies-routing.module';
import { MyPoliciesComponent } from './my-policies/my-policies.component';

@NgModule({
  imports: [CommonModule, MyPoliciesRoutingModule, CardMyPoliciesModule],
  declarations: [MyPoliciesComponent]
})
export class MyPoliciesModule {}
