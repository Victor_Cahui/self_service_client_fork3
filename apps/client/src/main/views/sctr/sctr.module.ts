import { NgModule } from '@angular/core';
import { SctrRoutingModule } from './sctr-routing.module';

@NgModule({
  imports: [SctrRoutingModule],
  declarations: []
})
export class SctrModule {}
