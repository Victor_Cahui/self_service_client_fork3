import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Mis_Polizas_8B } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';

@Component({
  selector: 'client-sctr-insurance-list',
  templateUrl: './sctr-insurance-list.component.html',
  styles: []
})
export class SctrInsuranceListComponent extends ConfigurationBase implements OnInit {
  ga: Array<IGaPropertie>;
  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.policyType = POLICY_TYPES.MD_SCTR.code;
    this.ga = [MFP_Mis_Polizas_8B()];
  }

  ngOnInit(): void {
    this.loadConfiguration();
  }
}
