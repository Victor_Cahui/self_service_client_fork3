import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './sctr-insurance-list/sctr-insurance-list.module#SctrInsuranceListModule'
  },
  {
    path: 'faqs/:policyType',
    loadChildren: './sctr-insurance-faq/sctr-insurance-faq.module#SctrInsuranceFaqModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './sctr-insurance-by-id/sctr-insurance-by-id.module#SctrInsuranceByIdModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrInsuranceRoutingModule {}
