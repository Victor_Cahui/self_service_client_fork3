import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { SCTR_HEADER } from '@mx/settings/constants/router-titles';
import * as SCTR_FAQS from '@mx/settings/faqs/sctr-insurance.faq';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Faq } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-sctr-insurance-faq',
  templateUrl: './sctr-insurance-faq.component.html'
})
export class SctrInsuranceFaqComponent extends ConfigurationBase implements OnInit {
  policyType: string;
  faqList: Array<Faq>;
  faqGroup: Array<Array<Faq>>;
  faqGroupConfig: any;
  titulo = GeneralLang.Titles.Faq;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly activatedRoute: ActivatedRoute,
    protected policiesService: PoliciesService,
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
  }

  ngOnInit(): void {
    this.headerHelperService.set(SCTR_HEADER);
    this.policyType = this.activatedRoute.snapshot.params['policyType'];
    let faqsSctr = [];
    this.policyType === 'salud' && (faqsSctr = SCTR_FAQS.SCTR_SALUD_INSURANCE_FAQS);
    this.policyType === 'pension' && (faqsSctr = SCTR_FAQS.SCTR_PENSION_INSURANCE_FAQS);
    this.policyType === 'salud-pension' && (faqsSctr = SCTR_FAQS.SCTR_SALUD_PENSION_INSURANCE_FAQS);

    this.faqList = faqsSctr.map(this._mapFaq.bind(this));
    this.policyType.includes('salud-pension') && this._setfaqGroup(faqsSctr);

    this.policyType.includes('pension') && (this.policyType = this.policyType.replace('pension', 'pensión'));
    this.headerHelperService.set({
      subTitle: `SCTR ${this.policyType.replace('-', ' ').toUpperCase()}`,
      title: this.titulo.toUpperCase(),
      icon: '',
      back: true
    });
  }

  private _setfaqGroup(faqsSctr: any): void {
    this.faqGroup = faqsSctr.map(group => group.map(this._mapFaq.bind(this)));
    this.faqGroupConfig = {
      label: 'Seleccione Ramo',
      options: [{ text: 'SCTR Salud', value: 0 }, { text: 'SCTR Pensión', value: 1 }]
    };
  }

  private _mapFaq(faq: Faq): Faq {
    return { ...faq, collapse: true };
  }
}
