import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { SctrInsuranceFaqComponent } from '../sctr-insurance-faq/sctr-insurance-faq.component';
import { SctrInsuranceFaqRouting } from '../sctr-insurance-faq/sctr-insurance-faq.routing';
@NgModule({
  imports: [CommonModule, SctrInsuranceFaqRouting, CardMoreFaqModule],
  declarations: [SctrInsuranceFaqComponent]
})
export class SctrInsuranceFaqModule {}
