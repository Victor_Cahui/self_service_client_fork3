import { NgModule } from '@angular/core';
import { SctrInsuranceRoutingModule } from './sctr-insurance.routing';

@NgModule({
  imports: [SctrInsuranceRoutingModule],
  declarations: []
})
export class SctrInsuranceModule {}
