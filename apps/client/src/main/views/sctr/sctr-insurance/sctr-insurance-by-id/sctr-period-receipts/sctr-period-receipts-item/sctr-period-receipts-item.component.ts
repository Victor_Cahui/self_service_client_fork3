import { Component, Input, OnInit } from '@angular/core';
import { SITUATION_TYPE, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { GeneralService } from '@mx/services/general/general.service';
import { APPLICATION_CODE, COLOR_STATUS } from '@mx/settings/constants/general-values';
import { CALENDAR_ICON } from '@mx/settings/constants/images-values';
import { EARLY_POLICY_RENEWAL_DAYS } from '@mx/settings/constants/key-values';
import { IPolicyFeeResponse } from '@mx/statemanagement/models/policy.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-sctr-period-receipts-item',
  templateUrl: './sctr-period-receipts-item.component.html'
})
export class SctrPeriodReceiptsItemComponent extends GaUnsubscribeBase implements OnInit {
  @Input() receipt: any;

  showSpinner: boolean;
  renewDays: number;
  fileUtil: FileUtil;
  situationType = SITUATION_TYPE;
  trafficLightColor = TRAFFIC_LIGHT_COLOR;
  colorStatus = COLOR_STATUS;
  calendarIcon = CALENDAR_ICON;
  symbol: string;

  constructor(
    private readonly generalService: GeneralService,
    private readonly _Autoservicios: Autoservicios,
    protected gaService: GAService
  ) {
    super(gaService);
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.getParameters();
    this.symbol = this.receipt ? `${StringUtil.getMoneyDescription(this.receipt.codigoMoneda)} ` : '';
  }

  getParameters(): void {
    this.generalService
      .getParametersSubject()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => (this.renewDays = +this.generalService.getValueParams(EARLY_POLICY_RENEWAL_DAYS)));
  }

  downloadPDF(receipt: IPolicyFeeResponse): void {
    if (this.showSpinner) {
      return;
    }
    const numberReciboOrAviso = receipt.numRecibo;
    const pathReq = {
      codigoApp: APPLICATION_CODE,
      codCia: receipt.codCia,
      numeroRecibo: numberReciboOrAviso,
      tipoDocumentoPago: receipt.tipoDocumentoPago
    };
    this.showSpinner = true;
    this._Autoservicios.ObtenerComprobantePagoHpexstream(pathReq).subscribe(
      (res: any) => {
        try {
          const fileName = `Recibo ${receipt.numRecibo}`;
          this.fileUtil.download(res.base64, 'pdf', fileName);
        } catch (error) {}
      },
      () => (this.showSpinner = false),
      () => (this.showSpinner = false)
    );
  }
}
