import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { SctrCoverageBase } from '../sctr-insurance-coverage/sctr-coverage-base';

@Component({
  selector: 'client-sctr-insurance-detail',
  templateUrl: './sctr-insurance-detail.component.html',
  styles: []
})
export class SctrInsuranceDetailComponent extends SctrCoverageBase implements OnInit {
  hidePeriodCard: boolean;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
    this.policyType = POLICY_TYPES.MD_SCTR.code;
  }

  ngOnInit(): void {
    this.loadInfoProfile();
    this.loadConfiguration();
  }

  viewAll(): void {
    const policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    this.router.navigate([`/sctr/sctr-insurance/${policyNumber}/insureds`]);
  }
}
