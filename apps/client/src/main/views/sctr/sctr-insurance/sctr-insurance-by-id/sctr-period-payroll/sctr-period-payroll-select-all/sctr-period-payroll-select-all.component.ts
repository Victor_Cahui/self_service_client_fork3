import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { SctrService } from '@mx/services/sctr.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-sctr-period-payroll-select-all',
  templateUrl: './sctr-period-payroll-select-all.component.html'
})
export class SctrPeriodPayrollSelectAllComponent extends GaUnsubscribeBase implements OnChanges, OnInit {
  @Input() isSelectAll: boolean;
  @Input() total: number;
  @Output() doSelectAll: EventEmitter<boolean> = new EventEmitter();

  checked: boolean;

  constructor(private readonly sctrService: SctrService) {
    super();
  }

  ngOnInit(): void {
    this.sctrService
      .getInsuredConstancyListRx()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(list => {
        list.length > 0 && this.isSelectAll && (this.checked = false);
        !list.length && !this.checked && this._updateChecked(true);
        list.length === this.total && !this.isSelectAll && this._updateChecked(true);
        !list.length && !this.isSelectAll && (this.checked = false);
      });
  }

  ngOnChanges(): void {
    this.checked = this.isSelectAll;
  }

  onCheck(): void {
    this.isSelectAll = !this.isSelectAll;
    this.doSelectAll.emit(this.isSelectAll);
    this.sctrService.clearInsuredConstancyList();
  }

  private _updateChecked(value: boolean): void {
    this.checked = value;
    this.doSelectAll.emit(this.checked);
    this.sctrService.clearInsuredConstancyList();
  }
}
