import { Component, OnInit } from '@angular/core';
import { SCTR_INSURANCE_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-sctr-insurance-by-id',
  templateUrl: './sctr-insurance-by-id.component.html',
  styles: []
})
export class SctrInsuranceByIdComponent implements OnInit {
  tabItemsList: Array<ITabItem>;
  constructor() {
    this.tabItemsList = SCTR_INSURANCE_TAB();
  }

  ngOnInit(): void {}
}
