import { DatePipe, DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseSctrPeriods } from '@mx/components/sctr/card-sctr-periods/base-sctr-periods';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { ISctrByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { ICardPeriodPreview } from '@mx/statemanagement/models/sctr.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';

export abstract class SctrPeriodReceiptsBase extends BaseSctrPeriods {
  header: IHeader;

  sctr: ISctrByClientResponse;
  period: ICardPeriodPreview;
  auth: IdDocument;

  showLoading: boolean;
  showLoadingPeriod: boolean;
  showFilters: boolean;
  isAdditional: boolean;
  isLoadingFilterVisible = true;
  _isEventFromFilter: boolean;

  lang = PERIODS_LANAG;

  page = 1;
  itemsPerPage = 10;
  total: number;
  fechaInicioVigencia: string;
  fechaFinVigencia: string;
  policyType: string;
  periodType: string;
  policyNumber: string;

  frmPeriod: FormGroup;
  frmFilter: FormGroup;

  constructor(
    @Inject(DOCUMENT) protected document: Document,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected policiesInfoService: PoliciesInfoService,
    protected headerHelperService: HeaderHelperService,
    protected policiesService: PoliciesService,
    protected storageService: StorageService,
    protected gaService?: GAService
  ) {
    super(datePipe, activatedRoute, authService, _Autoservicios, gaService);
  }

  hideTabTop(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.add('g-none');
  }

  showTabTop(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.remove('g-none');
  }

  loadInfoDetailSctr(): void {
    const requestParams = {
      tipoDocumento: this.auth.type,
      documento: this.auth.number,
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber
    };
    this.policiesService.sctrSearchByClient(requestParams).subscribe(response => {
      if (response && response.length) {
        const clientPolicyResponse = response[0];
        this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
        this.configSctr(clientPolicyResponse);
      }
    });
  }

  configSctr(sctr: ISctrByClientResponse): void {
    this.headerHelperService.set({
      subTitle: sctr.descripcionPoliza.toUpperCase(),
      title: this.header.title,
      back: true
    });
    this.sctr = sctr;
  }

  verifySelected(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.periodId) {
        setTimeout(() => {
          this.frmPeriod.controls['periodMonth'].setValue(params.periodId);
        }, 100);
      }
    });
  }
}
