import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrPeriodOperationComponent } from './sctr-period-operation.component';

const routes: Routes = [
  {
    path: '',
    component: SctrPeriodOperationComponent
  },
  {
    path: 'confirm',
    loadChildren:
      './sctr-period-operation-confirm/sctr-period-operation-confirm.module#SctrPeriodOperationConfirmModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrPeriodOperationRoutingModule {}
