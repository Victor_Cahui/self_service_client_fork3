import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrCoverageExclusionsComponent } from '../sctr-coverage-exclusions/sctr-coverage-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: SctrCoverageExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrCoverageExclusionsRouting {}
