import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { EXCLUSIONS_HEALTH, EXCLUSIONS_PENSION } from '@mx/settings/lang/sctr.lang';
import { SctrCoverageBase } from '../sctr-coverage-base';

@Component({
  selector: 'client-sctr-coverage-exclusions',
  templateUrl: './sctr-coverage-exclusions.component.html'
})
export class SctrCoverageExclusionsComponent extends SctrCoverageBase implements OnInit {
  @HostBinding('class') class = 'w-100';

  public content;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
  }

  ngOnInit(): void {
    const isPension = this.activatedRoute.parent.snapshot.routeConfig.path.endsWith('exclusions-double-emission');

    setTimeout(() => {
      this.content = this.isSctrPension ? EXCLUSIONS_PENSION : EXCLUSIONS_HEALTH;
      this.isSctrDoubleEmission && isPension && (this.content = EXCLUSIONS_PENSION);
    });
  }
}
