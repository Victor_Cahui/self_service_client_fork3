import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentButtonModule } from '@mx/components/payment/payment-button/payment-button.module';
import { IconPaymentTypeModule } from '@mx/components/shared/icon-payment-type/icon-payment-type.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';

import { SctrPeriodReceiptsItemComponent } from './sctr-period-receipts-item.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, IconPaymentTypeModule, PaymentButtonModule],
  declarations: [SctrPeriodReceiptsItemComponent],
  exports: [SctrPeriodReceiptsItemComponent]
})
export class SctrPeriodReceiptsItemModule {}
