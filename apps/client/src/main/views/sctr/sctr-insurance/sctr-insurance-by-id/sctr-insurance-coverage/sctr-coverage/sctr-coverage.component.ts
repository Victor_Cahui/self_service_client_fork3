import { Component, HostBinding, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardDetailPolicyBase } from '@mx/components/shared/card-detail-policy/card-detail-policy-base';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { ICON_COVERAGE_DEFAULT } from '@mx/settings/constants/coverage-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import {
  SCTR_COVERAGE_DOUBLE_EMISSION_TABS,
  SCTR_COVERAGE_HEALTH_TABS,
  SCTR_COVERAGE_PENSION_TABS
} from '@mx/settings/constants/router-tabs';
import { ASSISTANCE, COVERAGES_ASSISTANCE, COVERAGES_DISABILITY, DISABILITY } from '@mx/settings/lang/sctr.lang';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import {
  ICoveragesPolicy,
  IPoliciesByClientRequest,
  ISctrByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-sctr-coverage',
  template: `
    <client-icons-coverage [itemsList]="itemsList" [title]="title" [text]="text"></client-icons-coverage>
  `
})
export class SctrCoverageComponent extends CardDetailPolicyBase implements OnDestroy {
  @HostBinding('attr.class') attr_class = 'w-100';

  public options: Array<ITabItem>;
  public isSctrPension: boolean;
  public isSctrDoubleEmission: boolean;
  public coveragesSub: Subscription;
  public itemsList: Array<ICoverageItem>;
  public itemsListBase: Array<ICoverageIcons>;
  public title: string;
  public text: string;
  public typeCoverage: string;
  public paramsSub: Subscription;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      true,
      _Autoservicios
    );

    this.policyType = POLICY_TYPES.MD_SCTR.code;
    this.loadInfoProfile();
    this.loadConfiguration();
  }

  ngOnDestroy(): void {
    if (this.paramsSub) {
      this.paramsSub.unsubscribe();
    }
  }

  // Detalle de la poliza
  loadInfoDetailSctr(): void {
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(this.policyNumber);
    if (!clientPolicy) {
      this.setClearInfoDetail();
      this.policiesService
        .sctrSearchByClient({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        } as IPoliciesByClientRequest)
        .subscribe((response: Array<ISctrByClientResponse>) => {
          if (response && response.length > 0) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
            this.setInfoDetailSctr(clientPolicyResponse);
            this.configView(clientPolicyResponse);
          }
        });
    } else {
      this.setInfoDetailSctr(clientPolicy);
      this.configView(clientPolicy);
    }
  }

  configView(sctr: ISctrByClientResponse): void {
    // Tipo de SCTR
    this.isSctrPension = PolicyUtil.isSctrPension(sctr.tipoPoliza, sctr.codCia, sctr.codRamo);
    this.isSctrDoubleEmission = PolicyUtil.isSctrDoubleEmission(sctr.numeroPoliza);
    // Tabs
    this.options = this.isSctrDoubleEmission
      ? SCTR_COVERAGE_DOUBLE_EMISSION_TABS
      : this.isSctrPension
      ? SCTR_COVERAGE_PENSION_TABS
      : SCTR_COVERAGE_HEALTH_TABS;
    // Titulos
    this.setSubTitle();
    this.headerHelperService.set({
      title: sctr.descripcionPoliza.toUpperCase(),
      subTitle: this.subTitle,
      icon: '',
      back: true
    });
    // Contenido
    this.paramsSub = this.activatedRoute.params.subscribe(params => {
      this.typeCoverage = params['typeCoverage'];
      this.setContent();
    });
  }

  // Setear contenido
  setContent(): void {
    !this.typeCoverage
      ? (this.typeCoverage = this.isSctrPension ? 'disability' : 'assistance')
      : (this.typeCoverage = this.typeCoverage.toLowerCase());
    switch (this.typeCoverage) {
      case 'disability':
        this.title = DISABILITY.title;
        this.text = DISABILITY.content;
        this.itemsList = COVERAGES_DISABILITY;
        break;
      case 'assistance':
        this.title = ASSISTANCE.title;
        this.text = ASSISTANCE.content;
        this.itemsList = COVERAGES_ASSISTANCE;
        break;
      default:
      // redirecciona a 404
    }
  }

  setDataItemCoverage(list: Array<ICoveragesPolicy>): void {
    this.itemsList = list.map(item => {
      const iconC = this.itemsListBase.find(c => {
        return c.key === item.llave;
      });
      const itemCoverage: ICoverageItem = {
        title: item.tituloInfo,
        description: item.descripcionInfo,
        question: item.accionTexto,
        questionBool: item.accion,
        routeKey: item.accionLlave,
        iconKey: item.llave,
        icon: (iconC && iconC.icon) || ICON_COVERAGE_DEFAULT
      };

      return itemCoverage;
    });
  }
}
