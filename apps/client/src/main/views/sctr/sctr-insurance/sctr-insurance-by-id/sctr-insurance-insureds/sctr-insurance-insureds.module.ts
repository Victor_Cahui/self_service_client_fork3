import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ItemSctrInsuredModule } from '@mx/components/sctr/item-sctr-insured/item-sctr-insured.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfButtonModule, MfCardModule, MfPaginatorModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { SctrInsuranceInsuredsComponent } from './sctr-insurance-insureds.component';
import { SctrInsuranceInsuredsRoutingModule } from './sctr-insurance-insureds.routing';

@NgModule({
  imports: [
    CommonModule,
    InputSearchModule,
    InputSearchModule,
    ItemNotFoundModule,
    ItemSctrInsuredModule,
    MfButtonModule,
    MfCardModule,
    MfLoaderModule,
    MfPaginatorModule,
    MfShowErrorsModule,
    ReactiveFormsModule,
    SctrInsuranceInsuredsRoutingModule
  ],
  declarations: [SctrInsuranceInsuredsComponent]
})
export class SctrInsuranceInsuredsModule {}
