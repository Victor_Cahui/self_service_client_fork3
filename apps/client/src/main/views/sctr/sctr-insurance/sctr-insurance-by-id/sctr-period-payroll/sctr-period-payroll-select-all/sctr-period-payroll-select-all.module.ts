import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCardModule, MfCheckboxModule } from '@mx/core/ui';

import { SctrPeriodPayrollSelectAllComponent } from './sctr-period-payroll-select-all.component';

@NgModule({
  imports: [CommonModule, MfCheckboxModule, MfCardModule],
  declarations: [SctrPeriodPayrollSelectAllComponent],
  exports: [SctrPeriodPayrollSelectAllComponent]
})
export class SctrPeriodPayrollSelectAllModule {}
