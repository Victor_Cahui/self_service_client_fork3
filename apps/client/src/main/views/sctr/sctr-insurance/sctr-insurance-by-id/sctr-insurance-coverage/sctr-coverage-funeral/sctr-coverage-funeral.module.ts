import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { SctrCoverageFuneralComponent } from '../sctr-coverage-funeral/sctr-coverage-funeral.component';
import { SctrCoverageFuneralRouting } from '../sctr-coverage-funeral/sctr-coverage-funeral.routing';

@NgModule({
  imports: [CommonModule, SctrCoverageFuneralRouting, CardContentModule],
  declarations: [SctrCoverageFuneralComponent]
})
export class SctrCoverageFuneralModule {}
