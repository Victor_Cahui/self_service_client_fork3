import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrFoilAttachedComponent } from './sctr-foil-attached.component';

const routes: Routes = [
  {
    path: '',
    component: SctrFoilAttachedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrFoilAttachedRoutingModule {}
