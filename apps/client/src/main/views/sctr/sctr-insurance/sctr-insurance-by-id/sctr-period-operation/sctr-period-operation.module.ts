import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardSctrPeriodInfoModule } from '@mx/components/shared/card-sctr-period-info/card-sctr-period-info.module';
import { CardSctrPeriodRisksModule } from '@mx/components/shared/card-sctr-period-risks/card-sctr-period-risks.module';
import { CardSctrPeriodSummaryModule } from '@mx/components/shared/card-sctr-period-summary/card-sctr-period-summary.module';
import { CardSctrPeriodUploadModule } from '@mx/components/shared/card-sctr-period-upload/card-sctr-period-upload.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';

import { SctrPeriodOperationComponent } from './sctr-period-operation.component';
import { SctrPeriodOperationRoutingModule } from './sctr-period-operation.routing';

@NgModule({
  imports: [
    CommonModule,
    MfLoaderModule,
    CardSctrPeriodInfoModule,
    CardSctrPeriodRisksModule,
    CardSctrPeriodUploadModule,
    CardSctrPeriodSummaryModule,
    SctrPeriodOperationRoutingModule
  ],
  declarations: [SctrPeriodOperationComponent]
})
export class SctrPeriodOperationModule {}
