import { OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardDetailPolicyBase } from '@mx/components/shared/card-detail-policy/card-detail-policy-base';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import {
  SCTR_COVERAGE_DOUBLE_EMISSION_TABS,
  SCTR_COVERAGE_HEALTH_TABS,
  SCTR_COVERAGE_PENSION_TABS
} from '@mx/settings/constants/router-tabs';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import { IPoliciesByClientRequest, ISctrByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';

export class SctrCoverageBase extends CardDetailPolicyBase implements OnInit {
  public options: Array<ITabItem>;
  public isSctrPension: boolean;
  public isSctrDoubleEmission: boolean;
  public hideService: boolean;
  public coveragesSub: Subscription;
  public itemsList: Array<ICoverageItem>;
  public itemsListBase: Array<ICoverageIcons>;
  public title: string;
  public text: string;
  public showPayments: boolean;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      true,
      _Autoservicios
    );
    this.policyType = POLICY_TYPES.MD_SCTR.code;
    this.loadInfoProfile();
    this.loadConfiguration();
  }

  ngOnInit(): void {}

  loadInfoDetailSctr(): void {
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(this.policyNumber);
    this.mpKeys = this.policiesInfoService.getMPKeys();
    if (!clientPolicy) {
      this.setClearInfoDetail();
      this.showLoader = true;
      this.policiesService
        .sctrSearchByClient({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        } as IPoliciesByClientRequest)
        .subscribe((response: Array<ISctrByClientResponse>) => {
          this.policyFound = response && response.length > 0;
          if (this.policyFound) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
            this.mpKeys = this.policiesInfoService.getMPKeys();
            this.setInfoDetailSctr(clientPolicyResponse);
            this.configView(clientPolicyResponse);
          }
          this.showLoader = false;
        });
    } else {
      this.policyFound = true;
      this.setInfoDetailSctr(clientPolicy);
      this.configView(clientPolicy);
    }
  }

  configView(sctr: ISctrByClientResponse): void {
    // para configuracion
    this.isSctrPension = PolicyUtil.isSctrPension(sctr.tipoPoliza, sctr.codCia, sctr.codRamo);
    this.isSctrDoubleEmission = PolicyUtil.isSctrDoubleEmission(sctr.numeroPoliza);
    this.setSctrFaqPath();
    this.configurationService.setHideServicesIcons(this.isSctrPension);
    // tabs
    this.options = this.isSctrDoubleEmission
      ? SCTR_COVERAGE_DOUBLE_EMISSION_TABS
      : this.isSctrPension
      ? SCTR_COVERAGE_PENSION_TABS
      : SCTR_COVERAGE_HEALTH_TABS;
    // Titulos
    this.setSubTitle();
    this.headerHelperService.set({
      title: sctr.descripcionPoliza.toUpperCase(),
      subTitle: this.subTitle,
      icon: '',
      back: true
    });
  }

  setSctrFaqPath(): void {}
}
