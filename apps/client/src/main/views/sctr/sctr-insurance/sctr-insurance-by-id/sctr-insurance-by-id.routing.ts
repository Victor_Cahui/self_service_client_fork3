import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrInsuranceByIdComponent } from './sctr-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: SctrInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './sctr-insurance-detail/sctr-insurance-detail.module#SctrInsuranceDetailModule'
      },
      {
        path: 'coverages',
        loadChildren: './sctr-insurance-coverage/sctr-insurance-coverage.module#SctrInsuranceCoverageModule'
      },
      {
        path: 'periods/receipts',
        loadChildren: './sctr-period-receipts/sctr-period-receipts.module#SctrPeriodReceiptsModule'
      },
      {
        path: 'periods/receipts/:periodId',
        loadChildren: './sctr-period-receipts/sctr-period-receipts.module#SctrPeriodReceiptsModule'
      },
      {
        path: 'periods/payroll',
        loadChildren: './sctr-period-payroll/sctr-period-payroll.module#SctrPeriodPayrollModule'
      },
      {
        path: 'periods/payroll/:periodId',
        loadChildren: './sctr-period-payroll/sctr-period-payroll.module#SctrPeriodPayrollModule'
      },
      {
        path: 'periods/declaration',
        loadChildren: './sctr-period-operation/sctr-period-operation.module#SctrPeriodOperationModule'
      },
      {
        path: 'periods/inclusion',
        loadChildren: './sctr-period-operation/sctr-period-operation.module#SctrPeriodOperationModule'
      },
      {
        path: 'records',
        loadChildren: './sctr-insurance-records/sctr-insurance-records.module#SctrInsuranceRecordsModule'
      },
      {
        path: 'records/:numberRecord',
        loadChildren: './sctr-insurance-insureds/sctr-insurance-insureds.module#SctrInsuranceInsuredsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrInsuranceByIdRoutingModule {}
