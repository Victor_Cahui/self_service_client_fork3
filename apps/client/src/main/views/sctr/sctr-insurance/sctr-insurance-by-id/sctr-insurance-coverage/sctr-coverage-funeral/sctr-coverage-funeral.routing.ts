import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrCoverageFuneralComponent } from '../sctr-coverage-funeral/sctr-coverage-funeral.component';

const routes: Routes = [
  {
    path: '',
    component: SctrCoverageFuneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrCoverageFuneralRouting {}
