import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardSctrPeriodItemModule } from '@mx/components/shared/card-sctr-period-item/card-sctr-period-item.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfButtonModule, MfCardModule, MfPaginatorModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { SctrPeriodReceiptsItemModule } from './sctr-period-receipts-item/sctr-period-receipts-item.module';

import { SctrPeriodReceiptsComponent } from './sctr-period-receipts.component';
import { SctrPeriodReceiptsRoutingModule } from './sctr-period-receipts.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputSearchModule,
    MfButtonModule,
    MfCardModule,
    MfPaginatorModule,
    MfSelectModule,
    MfShowErrorsModule,
    CardSctrPeriodItemModule,
    SctrPeriodReceiptsRoutingModule,
    SctrPeriodReceiptsItemModule,
    ItemNotFoundModule,
    MfLoaderModule
  ],
  exports: [],
  declarations: [SctrPeriodReceiptsComponent]
})
export class SctrPeriodReceiptsModule {}
