import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoevaluadorComponent } from './autoevaluador.component';

const routes: Routes = [
  {
    path: '',
    component: AutoevaluadorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutoevaluadorRoutingModule {}
