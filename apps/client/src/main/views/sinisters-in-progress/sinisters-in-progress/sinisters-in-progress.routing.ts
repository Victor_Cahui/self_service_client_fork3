import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SinistersInProgressComponent } from './sinisters-in-progress.component';

const routes: Routes = [
  {
    path: '',
    component: SinistersInProgressComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SinistersInProgressRoutingModule {}
