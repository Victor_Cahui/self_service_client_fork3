import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SinistersInProgressRoutingModule } from './sinisters-in-progress.routing';

@NgModule({
  imports: [CommonModule, SinistersInProgressRoutingModule],
  declarations: []
})
export class SinistersInProgressModule {}
