import { NgModule } from '@angular/core';
import { PaymentQuotaModule } from './payment-quota/payment-quota.module';
import { PaymentReceiptsModule } from './payment-receipts/payment-receipts.module';
import { PaymentsListModule } from './payments-list/payments-list.module';
import { PaymentMyCardsModule } from './payments-my-cards/payments-my-cards.module';
import { PaymentsRoutingModule } from './payments-routing.module';
import { PaymentsSettingModule } from './payments-setting/payments-setting.module';

@NgModule({
  imports: [
    PaymentsRoutingModule,
    PaymentsListModule,
    PaymentReceiptsModule,
    PaymentsSettingModule,
    PaymentQuotaModule,
    PaymentMyCardsModule
  ],
  exports: [],
  declarations: [],
  providers: []
})
export class PaymentsModule {}
