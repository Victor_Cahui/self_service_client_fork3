import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BankPaymentConfirmModule } from '@mx/components/shared/payments/bank-payment-confirm/bank-payment-confirm.module';
import { PaymentBankConfirmComponent } from './payment-bank-confirm.component';
import { PaymentBankConfirmRoutingModule } from './payment-bank-confirm.routing';

@NgModule({
  imports: [CommonModule, PaymentBankConfirmRoutingModule, BankPaymentConfirmModule],
  declarations: [PaymentBankConfirmComponent],
  exports: [PaymentBankConfirmComponent]
})
export class PaymentBankConfirmModule {}
