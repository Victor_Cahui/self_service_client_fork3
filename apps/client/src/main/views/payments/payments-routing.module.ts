import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentReceiptsComponent } from './payment-receipts/payment-receipts.component';
import { PaymentsListComponent } from './payments-list/payments-list.component';
import { PaymentsMyCardsComponent } from './payments-my-cards/payments-my-cards.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: PaymentsListComponent
  },
  {
    path: 'receipts',
    component: PaymentReceiptsComponent
  },
  {
    path: 'settings',
    loadChildren: './payments-setting/payments-setting.module#PaymentsSettingModule'
  },
  {
    path: 'settings/:policyNumber',
    loadChildren:
      './payments-setting/payments-setting-policy-edit/payments-setting-policy-edit.module#PaymentsSettingPolicyEditModule'
  },
  {
    path: 'list/:typeInsurance',
    component: PaymentsListComponent
  },
  {
    path: 'list/:typeInsurance/:policyNumber',
    component: PaymentsListComponent
  },
  {
    path: 'quota',
    loadChildren: './payment-quota/payment-quota.module#PaymentQuotaModule'
  },
  {
    path: 'mycards',
    component: PaymentsMyCardsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule {}
