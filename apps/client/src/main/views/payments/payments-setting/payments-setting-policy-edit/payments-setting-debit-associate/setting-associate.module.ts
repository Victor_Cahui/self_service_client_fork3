import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentCardListModule } from '@mx/components/shared/payments';
import { SettingAssociateComponent } from './setting-associate.component';
import { SettingAssociateRouting } from './setting-associate.routing';

@NgModule({
  imports: [CommonModule, SettingAssociateRouting, PaymentCardListModule],
  declarations: [SettingAssociateComponent]
})
export class SettingAssociateModule {}
