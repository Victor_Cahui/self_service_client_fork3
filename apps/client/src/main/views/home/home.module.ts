import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardSinistersInProgressModule, CardWhatYouWantToDoModule } from '@mx/components';
import { CardMyPoliciesModule } from '@mx/components/card-my-policies/card-my-policies/card-my-policies.module';
import { NotificationsnModule } from '@mx/components/notifications/notifications.module';
import { CardPendingPaymentModule } from '@mx/components/payment/card-pending-payment/card-pending-payment.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { CardUserProfileModule } from '@mx/components/shared/card-user-profile/card-user-profile.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { MfModalAlertModule } from '@mx/core/ui/public-api';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home.routing.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    LinksModule,
    CardMyPoliciesModule,
    CardPendingPaymentModule,
    CardSinistersInProgressModule,
    BannerCarouselModule,
    BannerModule,
    CardUserProfileModule,
    CardWhatYouWantToDoModule,
    NotificationsnModule,
    MfModalAlertModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule {}
