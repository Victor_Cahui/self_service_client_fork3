import { NgModule } from '@angular/core';

import { DeathInsuranceRoutingModule } from './death-insurance.routing';

@NgModule({
  imports: [DeathInsuranceRoutingModule],
  declarations: []
})
export class DeathInsuranceModule {}
