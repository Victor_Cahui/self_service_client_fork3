import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { DeathInsuranceListComponent } from './death-insurance-list.component';
import { DeathInsuranceListRoutingModule } from './death-insurance-list.routing';

@NgModule({
  imports: [
    BannerCarouselModule,
    BannerModule,
    CardWhatYouWantToDoModule,
    CommonModule,
    DeathInsuranceListRoutingModule,
    CardMyPoliciesByTypeModule
  ],
  declarations: [DeathInsuranceListComponent]
})
export class DeathInsuranceListModule {}
