import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathInsuranceCoverageComponent } from './death-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: DeathInsuranceCoverageComponent,
    children: [
      {
        path: '',
        loadChildren: './death-coverage/death-coverage.module#DeathCoverageModule'
      },
      {
        path: 'exclusions',
        loadChildren: './death-exclusions/death-exclusions.module#DeathExclusionsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathInsuranceCoverageRoutingModule {}
