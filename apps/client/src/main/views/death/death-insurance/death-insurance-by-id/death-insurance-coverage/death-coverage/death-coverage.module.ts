import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { DeathCoverageComponent } from './death-coverage.component';
import { DeathCoverageRoutingModule } from './death-coverage.routing';

@NgModule({
  imports: [CommonModule, DeathCoverageRoutingModule, CardIconsCoverageModule, MfLoaderModule],
  declarations: [DeathCoverageComponent]
})
export class DeathCoverageModule {}
