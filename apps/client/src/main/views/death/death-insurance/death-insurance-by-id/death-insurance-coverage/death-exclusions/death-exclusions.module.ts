import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { DeathExclusionsComponent } from './death-exclusions.component';
import { DeathExclusionsRoutingModule } from './death-exclusions.routing';

@NgModule({
  imports: [CommonModule, DeathExclusionsRoutingModule, CardContentModule],
  declarations: [DeathExclusionsComponent]
})
export class DeathExclusionsModule {}
