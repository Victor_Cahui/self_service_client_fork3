import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardMyFeesModule } from '@mx/components/card-my-fees/card-my-fees/card-my-fees.module';
import { CardPaymentPolicyModule } from '@mx/components/payment/card-payment-policy/card-payment-policy.module';
import { CardContractingInfoModule } from '@mx/components/policy/card-contracting-info/card-contracting-info.module';
import { CardPolicyBasicInfoModule } from '@mx/components/policy/card-policy-basic-info/card-policy-basic-info.module';
import { CardPolicyInsuredGroupedModule } from '@mx/components/policy/card-policy-insured-grouped/card-policy-insured-grouped.module';
import { CardDetailPolicyModule } from '@mx/components/shared/card-detail-policy/card-detail-policy.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { DeathInsuranceDetailComponent } from './death-insurance-detail.component';
import { DeathInsuranceDetailRoutingModule } from './death-insurance-detail.routing';

@NgModule({
  imports: [
    CommonModule,
    DeathInsuranceDetailRoutingModule,
    CardDetailPolicyModule,
    CardContractingInfoModule,
    CardPaymentPolicyModule,
    CardPolicyBasicInfoModule,
    CardWhatYouWantToDoModule,
    CardMyFeesModule,
    CardPolicyInsuredGroupedModule,
    ItemNotFoundModule,
    MfLoaderModule
  ],
  declarations: [DeathInsuranceDetailComponent]
})
export class DeathInsuranceDetailModule {}
