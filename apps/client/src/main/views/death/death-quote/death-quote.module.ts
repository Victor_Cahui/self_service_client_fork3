import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { CardFaqModule } from '@mx/components/shared/card-faq/card-faq.module';
import { CardHowToWorkModule } from '@mx/components/shared/card-how-to-work/card-how-to-work.module';
import { CardInsurancePlansModule } from '@mx/components/shared/card-insurance-plans/card-insurance-plans.module';
import { CardPaymentOptionsModule } from '@mx/components/shared/card-payment-options/card-payment-options.module';
import { FormContactedByAgentModule } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.module';
import { ModalMoreFaqsModule } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.module';
import { DirectivesModule } from '@mx/core/ui';
import { DeathQuoteComponent } from './death-quote.component';
import { DeathQuoteRoutingModule } from './death-quote.routing';

@NgModule({
  imports: [
    DeathQuoteRoutingModule,
    CommonModule,
    BannerCarouselModule,
    BannerModule,
    CardInsurancePlansModule,
    CardHowToWorkModule,
    CardPaymentOptionsModule,
    CardFaqModule,
    ModalMoreFaqsModule,
    DirectivesModule,
    FormContactedByAgentModule
  ],
  exports: [DeathQuoteComponent],
  declarations: [DeathQuoteComponent],
  providers: []
})
export class DeathQuoteModule {}
