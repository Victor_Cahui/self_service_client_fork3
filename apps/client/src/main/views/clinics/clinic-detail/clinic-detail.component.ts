import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HealthService } from '@mx/services/health/health.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { CLINIC_DETAIL_HEADER } from '@mx/settings/constants/router-titles';
import { IClinicFavoriteRequest, IClinicResponse } from '@mx/statemanagement/models/health.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-clinic-detail',
  templateUrl: './clinic-detail.component.html'
})
export class ClinicDetailComponent implements OnDestroy, OnInit {
  clinic: IClinicResponse;
  healthSub: Subscription;
  showLoading: boolean;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly healthService: HealthService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    const clinicStorage = this.healthService.getCurrentClinic();
    if (!clinicStorage) {
      this.router.navigate(['/clinics/search']);
    } else {
      this.clinic = clinicStorage;
      CLINIC_DETAIL_HEADER.title = this.clinic.nombre;
      this.headerHelperService.set(CLINIC_DETAIL_HEADER);
    }
  }

  ngOnDestroy(): void {
    if (this.healthSub) {
      this.healthSub.unsubscribe();
    }
  }

  favoriteFn(esFavorito: boolean): void {
    this.clinic.esFavorito = esFavorito;

    const params: IClinicFavoriteRequest = {
      codigoApp: APPLICATION_CODE,
      clinicaId: this.clinic.clinicaId
    };

    this.healthSub = this.healthService.clinicFavorite(params, { esFavorito }).subscribe(
      () => {
        this.healthService.setCurrentClinic(this.clinic);
        this.healthSub.unsubscribe();
      },
      () => {
        this.healthSub.unsubscribe();
      }
    );
  }
}
