import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { CardFaqModule } from '@mx/components/shared/card-faq/card-faq.module';
import { CardHowToWorkModule } from '@mx/components/shared/card-how-to-work/card-how-to-work.module';
import { CardInsurancePlansModule } from '@mx/components/shared/card-insurance-plans/card-insurance-plans.module';
import { CardPaymentOptionsModule } from '@mx/components/shared/card-payment-options/card-payment-options.module';
import { FormContactedByAgentModule } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.module';
import { ModalMoreFaqsModule } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { HouseholdQuoteComponent } from './household-quote.component';
import { HouseholdQuoteRoutingModule } from './household-quote.routing';

@NgModule({
  imports: [
    CommonModule,
    BannerCarouselModule,
    HouseholdQuoteRoutingModule,
    CardHowToWorkModule,
    CardPaymentOptionsModule,
    CardInsurancePlansModule,
    CardFaqModule,
    ModalMoreFaqsModule,
    DirectivesModule,
    FormContactedByAgentModule,
    GoogleModule
  ],
  exports: [],
  declarations: [HouseholdQuoteComponent],
  providers: []
})
export class HouseholdQuoteModule {}
