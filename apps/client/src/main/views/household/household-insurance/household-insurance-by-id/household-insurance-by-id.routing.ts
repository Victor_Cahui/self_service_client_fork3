import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HouseholdInsuranceByIdComponent } from './household-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './household-insurance-detail/household-insurance-detail.module#HouseholdInsuranceDetailModule'
      },
      {
        path: 'coverages',
        loadChildren:
          './household-insurance-coverage/household-insurance-coverage.module#HouseholdInsuranceCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdInsuranceByIdRoutingModule {}
