import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HouseholdInsuranceDetailComponent } from './household-insurance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdInsuranceDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdInsuranceDetailRoutingModule {}
