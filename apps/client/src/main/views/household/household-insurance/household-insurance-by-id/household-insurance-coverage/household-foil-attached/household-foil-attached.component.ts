import { Component, HostBinding, OnInit } from '@angular/core';
import { FOIL_ATTACHED } from '@mx/settings/lang/household.lang';

@Component({
  selector: 'client-household-foil-attached-component',
  templateUrl: './household-foil-attached.component.html'
})
export class HouseholdFoilAttachedComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  content = { title: FOIL_ATTACHED.title, content: '' };

  ngOnInit(): void {}
}
