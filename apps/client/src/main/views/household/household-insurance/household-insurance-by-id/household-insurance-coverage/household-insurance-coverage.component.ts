import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HomeService } from '@mx/services/home/home.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';
import { HouseHoldCoverageBase } from './household-coverage-base';

@Component({
  selector: 'client-household-insurance-coverage-component',
  templateUrl: './household-insurance-coverage.component.html'
})
export class HouseholdInsuranceCoverageComponent extends HouseHoldCoverageBase implements OnDestroy, OnInit {
  isLoadingList: boolean;
  faqPath: string;
  coveragesSub: Subscription;
  options: Array<ITabItem>;
  optionsFilter: Array<ITabItem> = [];
  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected homeService: HomeService,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      homeService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
  }

  ngOnInit(): void {
    this.faqPath = '/household/household-insurance/faqs';
  }

  ngOnDestroy(): void {
    if (!!this.coveragesSub) {
      this.coveragesSub.unsubscribe();
    }
  }
}
