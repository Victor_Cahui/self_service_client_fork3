import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HouseholdInsuranceCoverageComponent } from './household-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdInsuranceCoverageComponent,
    children: [
      {
        path: '',
        redirectTo: 'coverages'
      },
      {
        path: 'coverages',
        loadChildren: './household-coverages/household-coverages.module#HouseholdCoveragesModule'
      },
      {
        path: 'deductibles',
        loadChildren: './household-deductibles/household-deductibles.module#HouseholdDeductiblesModule'
      },
      {
        path: 'exclusions',
        loadChildren: './household-exclusions/household-exclusions.module#HouseholdExclusionsModule'
      },
      {
        path: 'foil-attached',
        loadChildren: './household-foil-attached/household-foil-attached.module#HouseholdFoilAttachedModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdInsuranceCoverageRoutingModule {}
