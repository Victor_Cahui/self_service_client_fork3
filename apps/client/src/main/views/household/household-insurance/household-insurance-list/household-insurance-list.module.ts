import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { HouseholdInsuranceListComponent } from './household-insurance-list.component';
import { HouseholdInsuranceListRoutingModule } from './household-insurance-list.routing';

@NgModule({
  imports: [
    CommonModule,
    BannerCarouselModule,
    HouseholdInsuranceListRoutingModule,
    CardMyPoliciesByTypeModule,
    CardWhatYouWantToDoModule
  ],
  declarations: [HouseholdInsuranceListComponent]
})
export class HouseholdInsuranceListModule {}
