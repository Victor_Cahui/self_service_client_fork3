import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdCompareComponent } from './household-compare.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdCompareComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdCompareRoutingModule {}
