import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { FileItem } from '@mx/core/ui/lib/directives/ng-drop/file-item.class';
import { IErrorEvent } from '@mx/core/ui/lib/directives/ng-drop/ng-drop-files.directive';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_3_36A } from '@mx/settings/constants/events.analytics';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { DECLARE_SINISTER_HOME_STEP_3 } from '@mx/settings/lang/household.lang';
import { IDeclareSinister } from '@mx/statemanagement/models/home.interface';

@Component({
  selector: 'client-declare-sinister-step-three-component',
  templateUrl: './declare-sinister-step-three.component.html',
  styleUrls: ['./declare-sinister-step-three.component.scss']
})
export class DeclareSinisterStepThreeComponent implements OnInit {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;
  maximumSizeFileMB: number;
  minImages: number;
  maxImages: number;
  enter: boolean;
  files: Array<FileItem>;
  sinister: IDeclareSinister;

  titleAlert: string;
  messageAlert: string;

  titleLang: string;
  descriptionLang: string;
  descriptionAddLang: string;
  btnReturnLang: string;
  btnSuccessLang: string;

  btnConfirm: string;
  OopsMessage: string;

  gaReturn: IGaPropertie;
  gaSuccess: IGaPropertie;

  constructor(
    private readonly router: Router,
    private readonly declareSinisterHomeService: DeclareSinisterHomeService
  ) {
    this.maximumSizeFileMB = 20;
    this.minImages = 1;
    this.maxImages = 3;
    this.files = [];
    this.titleLang = DECLARE_SINISTER_HOME_STEP_3.title;
    this.descriptionLang = DECLARE_SINISTER_HOME_STEP_3.description;
    this.descriptionAddLang = DECLARE_SINISTER_HOME_STEP_3.descriptionAdd;
    this.btnReturnLang = DECLARE_SINISTER_HOME_STEP_3.form.btnReturn;
    this.btnSuccessLang = DECLARE_SINISTER_HOME_STEP_3.form.btnSuccess;
    this.btnConfirm = GeneralLang.Buttons.Confirm;
    this.OopsMessage = GeneralLang.Messages.Fail;
    this.gaReturn = MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_3_36A(this.btnReturnLang);
    this.gaSuccess = MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_3_36A(this.btnSuccessLang);
  }

  ngOnInit(): void {
    this.maximumSizeFileMB = this.declareSinisterHomeService.getMaxSizeFileMB();
    this.sinister = this.declareSinisterHomeService.getSinester();
    this.files = [...this.sinister.files];
  }

  onSubmit(): void {
    this.declareSinisterHomeService.successSteep3(this.files);
    this.router.navigate(['/household/hurt-and-steal/declare-sinister-home/step/4']);
  }

  getImages($event: Array<FileItem>): void {
    if ($event.length) {
      if (!this.checkDuplicateImagen($event)) {
        let files = [...this.files];
        files = files.concat($event);
        if (files.length > 3) {
          // Message de error mas de 3 imágenes
          this.titleAlert = this.OopsMessage;
          this.messageAlert = GeneralLang.Alert.errorMaxImages.error.message.replace(
            '#number',
            this.maxImages.toString()
          );
          this.modalMessage.open();
        } else {
          this.files = [...files];
        }
      } else {
        this.titleAlert = this.OopsMessage;
        this.messageAlert = GeneralLang.Alert.errorDuplicateImage.error.message;
        this.modalMessage.open();
      }
    }
  }

  checkDuplicateImagen(files: Array<FileItem>): boolean {
    let bool = false;
    const currentFiles = [...this.files];
    for (const image of currentFiles) {
      const index = files.findIndex((obj: FileItem) => obj.name === image.name);
      if (index !== -1) {
        bool = true;
        break;
      }
    }

    return bool;
  }

  onErrorEvent($event: IErrorEvent): void {
    this.titleAlert = this.OopsMessage;
    this.messageAlert = $event.message;
    this.modalMessage.open();
  }

  eventEnter($event: boolean): void {
    this.enter = $event;
  }

  goToStep2(): void {
    this.router.navigate(['/household/hurt-and-steal/declare-sinister-home/step/2']);
  }

  dropFile(index: number): void {
    const files = [...this.files];
    this.files = [...files.slice(0, index), ...files.slice(index + 1)];
  }

  trackByFn(index, item): any {
    return index;
  }
}
