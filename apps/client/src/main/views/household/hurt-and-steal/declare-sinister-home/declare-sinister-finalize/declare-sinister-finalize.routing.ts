import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinisterFinalizeComponent } from './declare-sinister-finalize.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterFinalizeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterFinalizeRoutingModule {}
