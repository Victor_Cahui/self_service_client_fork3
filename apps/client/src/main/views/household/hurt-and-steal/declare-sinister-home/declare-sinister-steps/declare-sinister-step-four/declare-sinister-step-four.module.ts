import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DeclareSinisterStepFourComponent } from './declare-sinister-step-four.component';
import { DeclareSinisterStepFourRoutingModule } from './declare-sinister-step-four.routing';

@NgModule({
  imports: [CommonModule, DeclareSinisterStepFourRoutingModule, GoogleModule],
  declarations: [DeclareSinisterStepFourComponent]
})
export class DeclareSinisterStepFourModule {}
