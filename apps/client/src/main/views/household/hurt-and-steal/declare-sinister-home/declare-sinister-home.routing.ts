import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinesterDeactivateGuard } from '@mx/guards/declare-sinester/declare-sinester-deactivate.guard';
import { DeclareSinesterFinalizeGuard } from '@mx/guards/declare-sinester/declare-sinester-finalize.guard';
import { DeclareSinesterStepGuard } from '@mx/guards/declare-sinester/declare-sinister-step.guard';
import { DeclareSinisterHomeComponent } from './declare-sinister-home.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterHomeComponent,
    canDeactivate: [DeclareSinesterDeactivateGuard],
    children: [
      {
        path: '',
        redirectTo: 'step',
        pathMatch: 'prefix'
      },
      {
        path: 'step',
        loadChildren: './declare-sinister-steps/declare-sinister-steps.module#DeclareSinisterStepsModule',
        canActivate: [DeclareSinesterStepGuard]
      },
      {
        path: 'finalize',
        loadChildren: './declare-sinister-finalize/declare-sinister-finalize.module#DeclareSinisterFinalizeModule',
        canActivate: [DeclareSinesterFinalizeGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterHomeRoutingModule {}
