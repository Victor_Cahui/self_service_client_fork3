import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinisterStepOneComponent } from './declare-sinister-step-one.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterStepOneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterStepOneRoutingModule {}
