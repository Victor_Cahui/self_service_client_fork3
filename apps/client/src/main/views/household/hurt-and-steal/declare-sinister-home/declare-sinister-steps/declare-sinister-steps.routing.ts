import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinesterStepFourGuard } from '@mx/guards/declare-sinester/declare-sinester-step-four.guard';
import { DeclareSinesterStepThreeGuard } from '@mx/guards/declare-sinester/declare-sinester-step-three.guard';
import { DeclareSinesterStepTwoGuard } from '@mx/guards/declare-sinester/declare-sinester-step-two.guard';
import { DeclareSinisterStepsComponent } from './declare-sinister-steps.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterStepsComponent,
    children: [
      {
        path: '',
        redirectTo: '1',
        pathMatch: 'prefix'
      },
      {
        path: '1',
        loadChildren: './declare-sinister-step-one/declare-sinister-step-one.module#DeclareSinisterStepOneModule'
      },
      {
        path: '2',
        loadChildren: './declare-sinister-step-two/declare-sinister-step-two.module#DeclareSinisterStepTwoModule',
        canActivate: [DeclareSinesterStepTwoGuard]
      },
      {
        path: '3',
        loadChildren: './declare-sinister-step-three/declare-sinister-step-three.module#DeclareSinisterStepThreeModule',
        canActivate: [DeclareSinesterStepThreeGuard]
      },
      {
        path: '4',
        loadChildren: './declare-sinister-step-four/declare-sinister-step-four.module#DeclareSinisterStepFourModule',
        canActivate: [DeclareSinesterStepFourGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterStepsRoutingModule {}
