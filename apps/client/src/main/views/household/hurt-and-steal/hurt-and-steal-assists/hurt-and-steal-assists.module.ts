import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UnderConstructionModule } from '@mx/pages/under-construction/under-construction.module';
import { HurtAndStealAssistsComponent } from './hurt-and-steal-assists.component';
import { HurtAndStealAssistsRouting } from './hurt-and-steal-assists.routing';

@NgModule({
  imports: [CommonModule, HurtAndStealAssistsRouting, UnderConstructionModule],
  declarations: [HurtAndStealAssistsComponent]
})
export class HurtAndStealAssistsModule {}
