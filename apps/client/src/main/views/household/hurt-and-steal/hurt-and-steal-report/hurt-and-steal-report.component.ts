import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { GAService, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Danhos_y_Robos_33B, MFP_SECTION_24A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { HOUSE_HURTANDSTEAL_HEADER } from '@mx/settings/constants/router-titles';
import { HouseholdLang } from '@mx/settings/lang/household.lang';
import { LANDING_HURT_AND_STEAL } from '@mx/settings/lang/landing';

@Component({
  selector: 'client-hurt-and-steal-report',
  templateUrl: './hurt-and-steal-report.component.html'
})
export class HurtAndStealReportComponent extends ConfigurationBase implements OnInit {
  ga: Array<IGaPropertie>;
  titleHurtAndSteal = HouseholdLang.Titles.HurtAndSteal;
  messageHurtAndSteal = HouseholdLang.Messages.HurtAndSteal;
  textHurtAndStealHelp = HouseholdLang.Messages.HurtAndStealHelp;
  textHurtAndStealReport = HouseholdLang.Messages.HurtAndStealReport;
  labelBtnAssistReport = HouseholdLang.Links.AssistReport;
  landingContent = LANDING_HURT_AND_STEAL;
  envConstants = environment;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService: GAService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly router: Router
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService);
    this.ga = [MFP_SECTION_24A(), MFP_Danhos_y_Robos_33B()];
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_HOGAR.code;
    this.loadConfiguration();
    this.headerHelperService.set(HOUSE_HURTANDSTEAL_HEADER);
  }

  goToDeclareSinisterHome(): void {
    this.router.navigate(['/household/hurt-and-steal/declare-sinister-home']);
  }
}
