import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { CardFaqModule } from '@mx/components/shared/card-faq/card-faq.module';
import { CardHowToWorkModule } from '@mx/components/shared/card-how-to-work/card-how-to-work.module';
import { CardInsurancePlansModule } from '@mx/components/shared/card-insurance-plans/card-insurance-plans.module';
import { CardPaymentOptionsModule } from '@mx/components/shared/card-payment-options/card-payment-options.module';
import { ModalMoreFaqsModule } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfShowErrorsModule } from '@mx/core/ui/lib/components/global/show-errors/show-errors.module';
import { MfModalErrorModule } from '@mx/core/ui/lib/components/modals/modal-error/modal-error.module';
import { LifeQuoteComponent } from './life-quote.component';
import { LifeQuoteRoutingModule } from './life-quote.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MfSelectModule,
    MfModalErrorModule,
    MfShowErrorsModule,
    MfButtonModule,
    BannerCarouselModule,
    BannerModule,
    CardInsurancePlansModule,
    CardHowToWorkModule,
    CardPaymentOptionsModule,
    CardFaqModule,
    ModalMoreFaqsModule,
    LifeQuoteRoutingModule,
    DirectivesModule,
    GoogleModule
  ],
  exports: [LifeQuoteComponent],
  declarations: [LifeQuoteComponent]
})
export class LifeQuoteModule {}
