import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LifeQuoteComponent } from './life-quote.component';

const routes: Routes = [
  {
    path: '',
    component: LifeQuoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeQuoteRoutingModule {}
