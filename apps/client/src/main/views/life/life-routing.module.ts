import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'life-insurance',
    loadChildren: './life-insurance/life-insurance.module#LifeInsuranceModule'
  },
  {
    path: 'quote',
    loadChildren: './life-quote/life-quote.module#LifeQuoteModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeRoutingModule {}
