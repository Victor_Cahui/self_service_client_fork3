import { Component, OnInit } from '@angular/core';
import { LIFE_INSURANCE_FAQS } from '@mx/settings/faqs/life-insurance.faq';

@Component({
  selector: 'client-life-insurance-faq-component',
  templateUrl: './life-insurance-faq.component.html'
})
export class LifeInsuranceFAQComponent implements OnInit {
  faqs: Array<{ title: string; content: string; collapse: boolean }>;

  constructor() {
    this.faqs = LIFE_INSURANCE_FAQS.map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
  }

  ngOnInit(): void {}
}
