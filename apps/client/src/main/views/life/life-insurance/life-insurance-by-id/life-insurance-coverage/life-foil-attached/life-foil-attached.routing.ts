import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LifeFoilAttachedComponent } from './life-foil-attached.component';

const routes: Routes = [
  {
    path: '',
    component: LifeFoilAttachedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeFoilAttachedRoutingModule {}
