import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FoilAttachedModule } from '@mx/components/shared/foil-attached/foil-attached.module';
import { LifeFoilAttachedComponent } from './life-foil-attached.component';
import { LifeFoilAttachedRoutingModule } from './life-foil-attached.routing';

@NgModule({
  imports: [CommonModule, LifeFoilAttachedRoutingModule, FoilAttachedModule],
  declarations: [LifeFoilAttachedComponent]
})
export class LifeFoilAttachedModule {}
