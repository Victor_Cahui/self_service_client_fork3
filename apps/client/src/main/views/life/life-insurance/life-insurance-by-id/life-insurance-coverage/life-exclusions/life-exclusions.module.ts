import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { LifeExclusionsComponent } from './life-exclusions.component';
import { LifeExclusionsRoutingModule } from './life-exclusions.routing';

@NgModule({
  imports: [CommonModule, LifeExclusionsRoutingModule, CardContentModule],
  declarations: [LifeExclusionsComponent]
})
export class LifeExclusionsModule {}
