import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { CardContentCoverageModule } from '@mx/components/shared/card-content-coverage/card-content-coverage.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { LifeInsuranceCoverageComponent } from './life-insurance-coverage.component';
import { LifeInsuranceCoverageRoutingModule } from './life-insurance-coverage.routing';

@NgModule({
  imports: [
    CommonModule,
    LifeInsuranceCoverageRoutingModule,
    CardContentCoverageModule,
    CardWhatYouWantToDoModule,
    ItemNotFoundModule,
    MfLoaderModule
  ],
  declarations: [LifeInsuranceCoverageComponent]
})
export class LifeInsuranceCoverageModule {}
