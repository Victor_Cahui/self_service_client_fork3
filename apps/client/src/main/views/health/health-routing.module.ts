import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'health-insurance',
    loadChildren: './health-insurance/health-insurance.module#HealthInsuranceModule'
  },
  {
    path: 'quote',
    loadChildren: './health-quote/health-quote.module#HealthQuoteModule'
  },
  {
    path: 'refund',
    loadChildren: './health-refund/health-refund.module#HealthRefundModule'
  },
  {
    path: 'refund/request/detail/:number',
    loadChildren:
      './health-refund/health-refund-tray/health-refund-detail/health-refund-detail.module#HealthRefundDetailModule'
  },
  {
    path: 'guarantee-letters',
    loadChildren: './health-guarantee-letters/health-guarantee-letters.module#HealthGuaranteeLettersModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRoutingModule {}
