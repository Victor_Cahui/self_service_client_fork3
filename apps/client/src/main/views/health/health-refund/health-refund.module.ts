import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top';
import { HealthRefundComponent } from './health-refund.component';
import { HealthRefundRoutingModule } from './health-refund.routing';

@NgModule({
  imports: [CommonModule, HealthRefundRoutingModule, MfTabTopModule],
  exports: [],
  declarations: [HealthRefundComponent],
  providers: []
})
export class HealthRefundModule {}
