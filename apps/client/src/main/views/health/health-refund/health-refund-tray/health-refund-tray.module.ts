import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationRequestModule } from '@mx/components/notifications/notification-request/notification-request.module';
import { ViewItemListModule } from '@mx/components/shared/timeline-item-list/view-item-list/view-item-list.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { HealthRefundTrayComponent } from './health-refund-tray.component';
import { HealthRefundTrayRouting } from './health-refund-tray.routing';

@NgModule({
  imports: [CommonModule, MfLoaderModule, ViewItemListModule, NotificationRequestModule, HealthRefundTrayRouting],
  declarations: [HealthRefundTrayComponent]
})
export class HealthRefundTrayModule {}
