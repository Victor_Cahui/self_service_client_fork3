import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HealthModalUploadFileModule } from '@mx/components/health/health-modal-upload-file/health-modal-upload-file.module';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { UploadFilesModule } from '@mx/components/shared/upload-files/upload-files.module';
import { DirectivesModule, MfModalModule } from '@mx/core/ui';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HealthRefundRequestStepTwoComponent } from './health-refund-request-step-two.component';
import { HealthRefundRequestStepTwoRoutesModule } from './health-refund-request-step-two.routing';

@NgModule({
  imports: [
    CommonModule,
    FormBaseModule,
    StepperModule,
    MfModalModule,
    UploadFilesModule,
    HealthModalUploadFileModule,
    HealthRefundRequestStepTwoRoutesModule,
    DirectivesModule
  ],
  declarations: [HealthRefundRequestStepTwoComponent]
})
export class HealthRefundRequestStepTwoModule {}
