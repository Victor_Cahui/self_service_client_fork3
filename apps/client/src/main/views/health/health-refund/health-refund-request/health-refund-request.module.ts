import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormRequestRefundModule } from '@mx/components/shared/form-request-refund/form-request-refund.module';
import { HowWorksModule } from '@mx/components/shared/how-works/how-works.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { HealthRefundRequestComponent } from './health-refund-request.component';
import { HealthRefundRequestRoutingMoudule } from './health-refund-request.routing';

@NgModule({
  imports: [
    CommonModule,
    HealthRefundRequestRoutingMoudule,
    DirectivesModule,
    HowWorksModule,
    FormRequestRefundModule,
    GoogleModule
  ],
  declarations: [HealthRefundRequestComponent]
})
export class HealthRefundRequestModule {}
