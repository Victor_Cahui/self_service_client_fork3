import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HealthRefundRequestStepFinishComponent } from './health-refund-request-step-finish.component';
import { HealthRefundRequestStepFinishRoutesModule } from './health-refund-request-step-finish.routing';

@NgModule({
  imports: [CommonModule, HealthRefundRequestStepFinishRoutesModule],
  declarations: [HealthRefundRequestStepFinishComponent]
})
export class HealthRefundRequestStepFinishModule {}
