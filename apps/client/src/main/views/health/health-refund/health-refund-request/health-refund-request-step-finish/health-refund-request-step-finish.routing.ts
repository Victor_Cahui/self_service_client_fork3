import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthRefundRequestStepFinishComponent } from './health-refund-request-step-finish.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundRequestStepFinishComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundRequestStepFinishRoutesModule {}
