import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthRefundRequestStepTwoComponent } from './health-refund-request-step-two.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundRequestStepTwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundRequestStepTwoRoutesModule {}
