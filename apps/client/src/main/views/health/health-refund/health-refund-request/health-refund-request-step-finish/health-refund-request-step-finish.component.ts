import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { COMPLETE_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import { IRefundHealth } from '@mx/statemanagement/models/health.interface';

@Component({
  selector: 'client-health-refund-request-step-finish',
  templateUrl: './health-refund-request-step-finish.component.html'
})
export class HealthRefundRequestStepFinishComponent implements OnInit {
  refund: IRefundHealth;
  generalLang = GeneralLang;
  lang = REFUND_HEALT_LANG.FnishRequest;
  completeIcon = COMPLETE_ICON;

  constructor(private readonly healthRefundService: HealthRefundService, private readonly router: Router) {}

  ngOnInit(): void {
    this.refund = this.healthRefundService.getRefund();
  }

  finish(): void {
    this.healthRefundService.setRefund(null);
    this.router.navigate(['/health/refund/request/']);
  }
}
