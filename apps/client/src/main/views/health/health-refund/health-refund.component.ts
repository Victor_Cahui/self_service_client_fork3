import { Component, OnDestroy, OnInit } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { MFP_Reembolsos_32A } from '@mx/settings/constants/events.analytics';
import { HEALTH_REFUND_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-health-refund-component',
  templateUrl: './health-refund.component.html'
})
export class HealthRefundComponent implements OnInit, OnDestroy {
  tabItemsList: Array<ITabItem>;
  ga: Array<IGaPropertie>;

  constructor(private readonly headerHelperService: HeaderHelperService) {
    this.tabItemsList = HEALTH_REFUND_TAB;
    this.ga = [MFP_Reembolsos_32A()];
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
  }

  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
