import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthInsuranceFAQComponent } from './health-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: HealthInsuranceFAQComponent
  },
  {
    path: ':policyType',
    component: HealthInsuranceFAQComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceFAQRoutingModule {}
