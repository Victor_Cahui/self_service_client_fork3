import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthInsuranceAssistsComponent } from './health-insurance-assists.component';

const routes: Routes = [
  {
    path: '',
    component: HealthInsuranceAssistsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceAssistsRoutingModule {}
