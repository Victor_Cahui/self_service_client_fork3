import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { EXCLUSSIONS, HealthLang } from '@mx/settings/lang/health.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IExclusionsResponse, IPolicyInsuredResponse } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-health-exclusions-component',
  templateUrl: './health-exclusions.component.html'
})
export class HealthExclusionsComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  policyNumber: string;
  exclusionsSub: Subscription;
  content = { ...EXCLUSSIONS };
  labelInsured = PolicyLang.Labels.InsuredSelect;
  messageExclusions: string;
  messageInsured: string;
  insuredList: any;
  exclusionsText: Array<IExclusionsResponse>;
  auth: IdDocument;
  showLoading: boolean;
  flowFinished: boolean;

  constructor(
    private readonly _Autoservicios: Autoservicios,
    private readonly policiesService: PoliciesService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    this.getInsured();
  }

  // Lista de asegurados
  getInsured(): void {
    this.showLoading = true;
    const params = {
      codigoApp: APPLICATION_CODE
    };

    this._Autoservicios.ObtenerAseguradosTipoUsuario({ numeroPoliza: this.policyNumber }, params).subscribe(
      (response: Array<IPolicyInsuredResponse>) => {
        if (response && response.length) {
          this.insuredList = [];
          // Si hay un solo asegurado muestra las exclusiones
          const oneInsured = response.length === 1;
          if (oneInsured) {
            const insured = response[0];
            const name = `${insured.nombre} ${insured.apellidoPaterno}`;
            const titular = this.auth.type === insured.tipoDocumento && this.auth.number === insured.documento;
            this.content.content = titular
              ? HealthLang.Messages.exclusionsOneInsuredTitular.replace('{{nameInsured}}', name)
              : HealthLang.Messages.exclusionsOneInsured.replace('{{nameInsured}}', name);
            this.messageInsured = titular
              ? HealthLang.Messages.withOutExclusionsTitular.replace('{{nameInsured}}', name)
              : HealthLang.Messages.withOutExclusionsOneInsured.replace('{{nameInsured}}', name);
            this.getExclusions(`${insured.tipoDocumento}-${insured.documento}`);
          } else {
            this.messageInsured = HealthLang.Messages.withOutExclusions;
            // Llena Select
            response.forEach(item => {
              this.insuredList.push({
                text: `${item.nombre} ${item.apellidoPaterno} - ${item.relacionDescripcion}`,
                value: `${item.tipoDocumento}-${item.documento}`,
                selected: false,
                disabled: false
              });
            });
          }
        } else {
          this.showLoading = false;
          delete this.content.content;
          this.messageExclusions = `${GeneralLang.Alert.titleFail} ${GeneralLang.Messages.ItemNotFound}`;
        }
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  // Exclusiones por asegurado
  getExclusions(insured: any): void {
    // tslint:disable-next-line: no-parameter-reassignment
    insured = insured.split('-');
    delete this.exclusionsText;
    delete this.messageExclusions;
    this.showLoading = true;
    this.exclusionsSub = this.policiesService
      .getExclusionsByInsured({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        tipoDocumentoAsegurado: insured[0],
        documentoAsegurado: insured[1]
      })
      .subscribe(
        (response: Array<IExclusionsResponse>) => {
          this.flowFinished = true;
          if (response && response.length) {
            this.exclusionsText = response;
            delete this.messageExclusions;
          } else {
            delete this.content.content;
            this.messageExclusions = this.messageInsured;
          }
        },
        () => {
          this.flowFinished = true;
          this.showLoading = false;
          this.messageExclusions = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
        },
        () => {
          this.showLoading = false;
          this.exclusionsSub.unsubscribe();
        }
      );
  }

  changeInsuranced(insured: any): void {
    this.getExclusions(insured);
  }
}
