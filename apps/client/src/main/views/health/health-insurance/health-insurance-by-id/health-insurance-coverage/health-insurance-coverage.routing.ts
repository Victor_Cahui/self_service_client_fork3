import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthInsuranceCoverageComponent } from './health-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: HealthInsuranceCoverageComponent,
    children: [
      {
        path: '',
        redirectTo: 'coverages',
        pathMatch: 'full'
      },
      {
        path: 'hospital-care',
        loadChildren: './health-hospital-care/hospital-care.module#HospitalCareModule'
      },
      {
        path: 'ambulatory-care',
        loadChildren: './health-ambulatory-care/ambulatory-care.module#AmbulatoryCareModule'
      },
      {
        path: 'maternity',
        loadChildren: './health-maternity/maternity.module#MaternityModule'
      },
      {
        path: 'exclusions',
        loadChildren: './health-exclusions/health-exclusions.module#HealthExclusionsModule'
      },
      {
        path: 'observations',
        loadChildren: './health-observations/health-observations.module#HealthObservationsModule'
      },
      {
        path: 'conditioned',
        loadChildren: './health-conditioned/health-conditioned.module#HealthConditionedModule'
      },
      {
        path: 'waiting-period',
        loadChildren: './health-waiting-period/health-waiting-period.module#HealthWaitingPeriodModule'
      },
      {
        path: 'foil-attached',
        loadChildren: './health-foil-attached/health-foil-attached.module#HealthFoilAttachedModule'
      },
      {
        path: ':typeCoverage',
        loadChildren: './health-coverage/health-coverage.module#HealthCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceCoverageRoutingModule {}
