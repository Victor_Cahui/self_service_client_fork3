import { Component } from '@angular/core';
import { WAITING_PERIOD } from '@mx/settings/lang/health.lang';

@Component({
  selector: 'client-health-waiting-period-component',
  templateUrl: './health-waiting-period.component.html'
})
export class HealthWaitingPeriodComponent {
  content = WAITING_PERIOD;
}
