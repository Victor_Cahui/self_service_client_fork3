import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthInsuranceByIdComponent } from './health-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: HealthInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './health-insurance-detail/health-insurance-detail.module#HealthInsuranceDetailModule'
      },
      {
        path: 'coverages',
        loadChildren: './health-insurance-coverage/health-insurance-coverage.module#HealthInsuranceCoverageModule'
      },
      {
        path: 'assists',
        loadChildren: './health-insurance-assists/health-insurance-assists.module#HealthInsuranceAssistsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceByIdRoutingModule {}
