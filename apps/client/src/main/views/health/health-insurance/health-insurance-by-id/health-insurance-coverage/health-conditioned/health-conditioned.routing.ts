import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthConditionedComponent } from './health-conditioned.component';

const routes: Routes = [
  {
    path: '',
    component: HealthConditionedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthConditionedRoutingModule {}
