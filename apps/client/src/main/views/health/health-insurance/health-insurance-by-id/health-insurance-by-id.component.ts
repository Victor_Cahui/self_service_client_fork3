import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { environment } from '@mx/environments/environment';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Poliza_de_Seguro_de_Salud_27A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GET_HEALTH_INSURANCE_TAB } from '@mx/settings/constants/router-tabs';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-health-insurance-by-id-component',
  templateUrl: './health-insurance-by-id.component.html'
})
export class HealthInsuranceByIdComponent extends UnsubscribeOnDestroy implements OnDestroy, OnInit {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;

  ga: Array<IGaPropertie>;
  tabItemsList: Array<ITabItem> = [];
  openModal: boolean;
  titleAlert: string;
  messageAlert: string;
  btnConfirm = GeneralLang.Buttons.Confirm;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly insuredDependentEditService: InsuredDependentEditService,
    private readonly policiesInfoService: PoliciesInfoService
  ) {
    super();
    this.ga = [MFP_Poliza_de_Seguro_de_Salud_27A()];
    this.titleAlert = '';
    this.messageAlert = '';
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
    this.openModal = this.insuredDependentEditService.getOpenModal();
    if (this.openModal) {
      this.titleAlert = GeneralLang.Alert.titleSuccess;
      this.messageAlert = GeneralLang.Alert.updateWork.success.message;
      this.modalMessage.open();
      this.insuredDependentEditService.setOpenModal(false);
      this.openModal = false;
    }

    this.policiesInfoService
      .getPolicyBeh()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(policy => {
        if (!policy) {
          return void 0;
        }

        const policyType = policy.tipoPoliza;
        const isPPFM = policy.esBeneficioAdicional;
        const isVisbleTabAssistance =
          policyType === POLICY_TYPES.MD_EPS.code
            ? environment.ACTIVATE_ASSISTS_EPS
            : environment.ACTIVATE_ASSISTS_HEALTH;
        this.tabItemsList = GET_HEALTH_INSURANCE_TAB(isVisbleTabAssistance);
        isPPFM && (this.tabItemsList = this.policiesInfoService.hideCoveragesTab(this.tabItemsList));
      });
  }

  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
