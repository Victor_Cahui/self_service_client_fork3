import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.module';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { HealthInsuranceByIdComponent } from './health-insurance-by-id.component';
import { HealthInsuranceByIdRoutingModule } from './health-insurance-by-id.routing';

@NgModule({
  imports: [
    CommonModule,
    HealthInsuranceByIdRoutingModule,
    MfTabTopModule,
    MfModalModule,
    MfModalAlertModule,
    MfModalMessageModule
  ],
  declarations: [HealthInsuranceByIdComponent]
})
export class HealthInsuranceByIdModule {}
