import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  MFP_Poliza_de_Seguro_de_Salud_30A,
  MFP_Poliza_de_Seguro_de_Salud_30B
} from '@mx/settings/constants/events.analytics';
import { HealthCoverageBase } from './health-coverage-base';

@Component({
  selector: 'client-health-insurance-coverage',
  templateUrl: './health-insurance-coverage.component.html'
})
export class HealthInsuranceCoverageComponent extends HealthCoverageBase implements OnInit {
  faqPath: string;
  ga: Array<IGaPropertie>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
    this.ga = [MFP_Poliza_de_Seguro_de_Salud_30A(), MFP_Poliza_de_Seguro_de_Salud_30B()];
  }

  ngOnInit(): void {
    this.faqPath = `/health/health-insurance/faqs/${this.policyType}`;
  }
}
