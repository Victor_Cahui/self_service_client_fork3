import { Component, HostBinding } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { COB_SAL_CONS_MED } from '@mx/settings/constants/coverage-values';
import { TYPE_AMBULATORY_CARE } from '@mx/settings/constants/policy-values';
import { AMBULATORY_CARE } from '@mx/settings/lang/health.lang';
import { HealthCoverageBase } from '../health-coverage-base';

@Component({
  selector: 'client-ambulatory-care',
  templateUrl: './ambulatory-care.component.html'
})
export class AmbulatoryCareComponent extends HealthCoverageBase {
  @HostBinding('class') class = 'w-100';

  content = AMBULATORY_CARE;
  coverageType = TYPE_AMBULATORY_CARE;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
    this.setMapParams(COB_SAL_CONS_MED);
  }
}
