import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { HealthObservationsComponent } from './health-observations.component';
import { HealthObservationsRoutingModule } from './health-observations.routing';

@NgModule({
  imports: [CommonModule, HealthObservationsRoutingModule, MfSelectModule, MfLoaderModule],
  declarations: [HealthObservationsComponent]
})
export class HealthObservationsModule {}
