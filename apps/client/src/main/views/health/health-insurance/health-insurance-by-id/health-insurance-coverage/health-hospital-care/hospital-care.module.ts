import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { ListCareNetworkModule } from '@mx/components/shared/list-care-network/list-care-network.module';
import { HospitalCareComponent } from './hospital-care.component';
import { HospitalCareRoutingModule } from './hospital-care.routing';

@NgModule({
  imports: [CommonModule, HospitalCareRoutingModule, CardContentModule, ListCareNetworkModule],
  declarations: [HospitalCareComponent]
})
export class HospitalCareModule {}
