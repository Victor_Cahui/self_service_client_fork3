import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthExclusionsComponent } from './health-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: HealthExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthExclusionsRoutingModule {}
