import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmbulatoryCareComponent } from './ambulatory-care.component';

const routes: Routes = [
  {
    path: '',
    component: AmbulatoryCareComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmbulatoryCareRouting {}
