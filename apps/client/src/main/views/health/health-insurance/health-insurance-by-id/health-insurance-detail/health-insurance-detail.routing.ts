import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthInsuranceDetailComponent } from './health-insurance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HealthInsuranceDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceDetailRoutingModule {}
