import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { filter } from 'rxjs/internal/operators/filter';
import { skip } from 'rxjs/internal/operators/skip';

import { everyPropsAreTruthy } from '@mx/core/shared/helpers';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { FrmProvider } from '@mx/core/shared/providers/services';
import { AffiliatesFacade, State } from '@mx/core/shared/state/affiliates';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { DOCUMENT_ICON, SEARCH_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-affiliates',
  templateUrl: './affiliates.component.html'
})
export class AffiliatesComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('inputSearch') public inputSearch: any;

  arrPeriods: any[];
  arrPlan: any[];
  configCbo: ConfigCbo;
  frm: FormGroup;
  graLng = GeneralLang;
  iconNotFound = DOCUMENT_ICON;
  isEditing: boolean;
  isLoadingFiltersVisible = true;
  isLoadingListVisible = true;
  itemsPerPage = 10;
  list: any[];
  msgNotFound: string;
  nroPolicy: string;
  page = 1;
  searchText = '';
  total: number;
  totalAffiliates: number;
  private _isEventFromFilter: boolean;
  private _totalByFilter: number;

  constructor(private readonly _AffiliatesFacade: AffiliatesFacade, private readonly _FrmProvider: FrmProvider) {
    super();
  }

  ngOnInit(): void {
    this._initValues();
    this._mapState();
  }

  getCboLoadingState(e): void {
    this.isLoadingFiltersVisible = e.isLoadingVisible;
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this._getAllAffiliates({ ...this.frm.value, datoPersonal: this.searchText });
  }

  pageChange(page: number): void {
    this.page = page;
    this._getAllAffiliates({ ...this.frm.value, datoPersonal: this.searchText });
  }

  getIsEditing(e: boolean): void {
    this.isEditing = e;
  }

  onSearch(text: string): void {
    this.searchText = text;
  }

  clearSearch(): void {
    this._totalByFilter = 0;
    this.page = 1;
    this.searchText = '';
    this._isEventFromFilter || this._getAllAffiliates(this.frm.value);
  }

  btnClear(): void {
    this.inputSearch.clearText();
  }

  search(): void {
    this._totalByFilter = this.totalAffiliates;
    this.page = 1;
    this._getAllAffiliates({ ...this.frm.value, datoPersonal: this.searchText });
  }

  save(frm): void {
    this._AffiliatesFacade.UpdateAffiliate(frm);
  }

  private _mapState(): void {
    const getAffiliates$ = this._AffiliatesFacade.getState$.subscribe((st: State) => {
      this.isEditing = false;
      this._isEventFromFilter = false;
      this.isLoadingListVisible = st.isLoading;
      this.list = st.listaResultados;
      this.total = st.totalRegistros;
      this.totalAffiliates = this._totalByFilter || st.totalAffiliates;
    });
    const frmRx = this.frm.valueChanges
      .pipe(
        filter(everyPropsAreTruthy),
        // HACK: el select al inicio dispara 2 veces el seteo del frm
        skip(1)
      )
      .subscribe(this._mapChangedFrm.bind(this));

    this.arrToDestroy.push(getAffiliates$, frmRx);
  }

  private _mapChangedFrm(f): void {
    this._isEventFromFilter = true;
    this.page = 1;
    this.btnClear();
    this._getAllAffiliates(f);
  }

  private _getAllAffiliates(f): void {
    this._AffiliatesFacade.GetAllAffiliates({
      ...f,
      codPlan: f.codPlan.replace('default', ''),
      pagina: this.page,
      registros: this.itemsPerPage
    });
  }

  private _configNotFound(): void {
    this.iconNotFound = SEARCH_ICON;
    this.msgNotFound = this.graLng.Messages.ItemNotFound;
  }

  private _initValues(): void {
    this.configCbo = { canAutoSelectFirstOpt: true };
    this.nroPolicy = this._AffiliatesFacade.getState().nroPolicy;
    this.frm = this._FrmProvider.AffiliatesComponent();
    this._configNotFound();
  }
}
