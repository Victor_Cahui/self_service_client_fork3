import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthFoilAttachedComponent } from './health-foil-attached.component';

const routes: Routes = [
  {
    path: '',
    component: HealthFoilAttachedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthFoilAttachedRoutingModule {}
