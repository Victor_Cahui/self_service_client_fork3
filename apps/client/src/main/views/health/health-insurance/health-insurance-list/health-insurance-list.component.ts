import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Mis_Polizas_8A, MFP_Mis_Polizas_8B } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';

@Component({
  selector: 'client-health-insurance-list-component',
  templateUrl: './health-insurance-list.component.html'
})
export class HealthInsuranceListComponent extends ConfigurationBase implements OnInit {
  ga: Array<IGaPropertie>;
  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.policyType = `${POLICY_TYPES.MD_SALUD.code}|${POLICY_TYPES.MD_EPS.code}`;
    this.ga = [MFP_Mis_Polizas_8A(), MFP_Mis_Polizas_8B()];
  }

  ngOnInit(): void {
    this.policiesInfoService.resetClientPolicyResponse();
    this.loadInfoProfile();
    this.loadConfiguration();
  }
}
