import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { HealthInsuranceListComponent } from './health-insurance-list.component';
import { HealthInsuranceListRoutingModule } from './health-insurance-list.routing';

@NgModule({
  imports: [
    BannerCarouselModule,
    BannerModule,
    CardMyPoliciesByTypeModule,
    CardWhatYouWantToDoModule,
    CommonModule,
    HealthInsuranceListRoutingModule
  ],
  declarations: [HealthInsuranceListComponent]
})
export class HealthInsuranceListModule {}
