import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './health-insurance-list/health-insurance-list.module#HealthInsuranceListModule'
  },
  {
    path: 'insured-dependent',
    loadChildren: './insured-dependent/insured-dependent.module#InsuredDependentModule'
  },
  {
    path: 'faqs',
    loadChildren: './health-insurance-faq/health-insurance-faq.module#HealthInsuranceFAQModule'
  },
  {
    path: 'affiliates',
    loadChildren: './health-insurance-by-id/affiliates/affiliates.module#AffiliatesModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './health-insurance-by-id/health-insurance-by-id.module#HealthInsuranceByIdModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceRoutingModule {}
