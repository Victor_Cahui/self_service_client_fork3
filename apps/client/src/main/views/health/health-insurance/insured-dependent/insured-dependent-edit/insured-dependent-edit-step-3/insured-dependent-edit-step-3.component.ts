import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { HEALTH_INSURANCE_EDIT } from '@mx/settings/constants/router-titles';
import { EditProfileLang } from '@mx/settings/lang/insured-dependent';

@Component({
  selector: 'client-insured-dependent-edit-step-3-component',
  templateUrl: './insured-dependent-edit-step-3.component.html'
})
export class InsuredDependentEditStep3Component implements OnInit {
  maximumConfimationDaysPerRequest: number;
  policeNumber: string;
  langtitle: string;
  langmessage1: string;
  langmessage2: string;
  langBSubmit: string;
  constructor(
    private readonly router: Router,
    private readonly insuredDependentEditService: InsuredDependentEditService,
    private readonly headerHelperService: HeaderHelperService
  ) {}

  ngOnInit(): void {
    this.headerHelperService.set({
      title: HEALTH_INSURANCE_EDIT.title,
      subTitle: this.insuredDependentEditService.namePolice,
      icon: '',
      back: false
    });
    this.policeNumber = this.insuredDependentEditService.numberPolice;
    this.maximumConfimationDaysPerRequest = this.insuredDependentEditService.getMaxDay();
    this.langtitle = EditProfileLang.step3.title;
    this.langmessage1 = EditProfileLang.step3.message1;
    this.langmessage2 = EditProfileLang.step3.message2;
    this.langBSubmit = EditProfileLang.step3.BSubmit;
  }

  finalize(): void {
    this.router.navigate([`/health/health-insurance/${this.policeNumber}/detail`]);
  }
}
