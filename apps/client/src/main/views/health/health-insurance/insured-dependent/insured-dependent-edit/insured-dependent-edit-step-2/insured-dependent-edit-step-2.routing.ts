import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InsuredDependentEditStep2Component } from './insured-dependent-edit-step-2.component';

const routes: Routes = [
  {
    path: '',
    component: InsuredDependentEditStep2Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuredDependentEditStep2RoutingModule {}
