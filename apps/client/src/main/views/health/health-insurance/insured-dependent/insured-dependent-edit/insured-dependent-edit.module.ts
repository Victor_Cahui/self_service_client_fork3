import { NgModule } from '@angular/core';
import { InsuredDependentEditComponent } from './insured-dependent-edit.component';
import { InsuredDependentEditRoutingModule } from './insured-dependent-edit.routing';

@NgModule({
  imports: [InsuredDependentEditRoutingModule],
  exports: [],
  declarations: [InsuredDependentEditComponent],
  providers: []
})
export class InsuredDependentEditModule {}
