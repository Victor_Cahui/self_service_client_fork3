import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthGuaranteeLettersComponent } from './health-guarantee-letters.component';

const routes: Routes = [
  {
    path: '',
    component: HealthGuaranteeLettersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthGuaranteeLettersRoutingModule {}
