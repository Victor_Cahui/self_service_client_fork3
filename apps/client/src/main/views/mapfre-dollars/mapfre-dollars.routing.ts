import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MapfeDollarsComponent } from './mapfre-dollars.component';

const routes: Routes = [
  {
    component: MapfeDollarsComponent,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapfreDollarsRoutingModule {}
