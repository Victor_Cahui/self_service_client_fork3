import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfCardModule, MfPaginatorModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { MapfeDollarsComponent } from './mapfre-dollars.component';
import { MapfreDollarsRoutingModule } from './mapfre-dollars.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ItemNotFoundModule,
    MapfreDollarsRoutingModule,
    MfCardModule,
    MfLoaderModule,
    MfPaginatorModule,
    ReactiveFormsModule
  ],
  exports: [],
  declarations: [MapfeDollarsComponent],
  providers: []
})
export class MapfreDollarsModule {}
