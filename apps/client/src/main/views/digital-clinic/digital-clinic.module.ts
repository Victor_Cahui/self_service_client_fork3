import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { DigitalClinicNoServiceModule, DigitalClinicServiceModule } from '@mx/components/shared/digital-clinic';
import { TermsConditionsModule } from '@mx/components/shared/terms_conditions/terms-conditions.module';
import { DirectivesModule } from '@mx/core/shared/common/directives/directives.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.module';
import { DigitalClinicComponent } from './digital-clinic.component';
import { DigitalClinicRoutingModule } from './digital-clinic.routing';

@NgModule({
  imports: [
    DigitalClinicRoutingModule,
    BannerCarouselModule,
    BannerModule,
    MfModalAlertModule,
    DirectivesModule,
    CommonModule,
    GoogleModule,
    DigitalClinicServiceModule,
    DigitalClinicNoServiceModule,
    MfLoaderModule,
    TermsConditionsModule
  ],
  declarations: [DigitalClinicComponent]
})
export class DigitalClinicModule {}
