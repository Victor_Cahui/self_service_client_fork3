import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ResponsiveCarouselModule } from '@mx/components/shared/responsive-carousel/responsive-carousel.module';
import { MfModalAlertModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { DigitalClinicLandingComponent } from './digital-clinic-landing.component';
import { DigitalClinicLandingRoutingModule } from './digital-clinic-landing.routing';

@NgModule({
  imports: [
    CommonModule,
    DigitalClinicLandingRoutingModule,
    MfLoaderModule,
    MfButtonModule,
    ResponsiveCarouselModule,
    MfModalAlertModule
  ],
  exports: [],
  declarations: [DigitalClinicLandingComponent],
  providers: []
})
export class DigitalClinicLandingModule {}
