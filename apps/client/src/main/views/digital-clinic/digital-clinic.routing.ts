import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DigitalClinicComponent } from './digital-clinic.component';

const routes: Routes = [
  {
    path: 'services',
    component: DigitalClinicComponent
  },
  {
    path: 'quote',
    loadChildren: './digital-clinic-landing/digital-clinic-landing.module#DigitalClinicLandingModule'
  },
  {
    path: 'assists',
    loadChildren:
      './../mapfre-services/mapfre-services-assists/mapfre-services-assists.module#MapfreServicesAssistsModule'
  },
  {
    path: 'schedule',
    loadChildren: './../mapfre-services/schedule-appointment/schedule-appointment.module#ScheduleAppointmentModule'
  },
  {
    path: 'schedule-detail',
    loadChildren: './../mapfre-services/schedule-appointment/detail/schedule-detail.module#ScheduleDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigitalClinicRoutingModule {}
