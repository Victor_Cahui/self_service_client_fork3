import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClinicMapComponent } from './clinic-map.component';

const routes: Routes = [
  {
    path: '',
    component: ClinicMapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicMapRouting {}
