import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleStep3Component } from './schedule-step-3.component';
import { ScheduleStep3Guard } from './schedule-step-3.guard';

const routes: Routes = [
  {
    canActivate: [ScheduleStep3Guard],
    component: ScheduleStep3Component,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleStep3RoutingModule {}
