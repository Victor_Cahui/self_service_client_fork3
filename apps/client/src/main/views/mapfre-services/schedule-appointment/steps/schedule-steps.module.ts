import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaptureRouteSteps } from '../guards/schedule-appointment.guard';
import { ScheduleStepsComponent } from './schedule-steps.component';
import { ScheduleStepsRoutingModule } from './schedule-steps.routing';
import { AppointmentService } from './service/appointment.service';

@NgModule({
  imports: [CommonModule, ScheduleStepsRoutingModule],
  declarations: [ScheduleStepsComponent],
  providers: [AppointmentService, CaptureRouteSteps]
})
export class ScheduleStepsModule {}
