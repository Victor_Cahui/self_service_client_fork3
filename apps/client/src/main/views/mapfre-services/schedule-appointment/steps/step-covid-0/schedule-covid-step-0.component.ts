import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { WARNING_ICON } from '@mx/settings/constants/images-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { SCHEDULE_COVID_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { SCHEDULE_APPOINTMENT_COVID_STEP_0_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MapLang } from '@mx/settings/lang/map.lang';
import { take } from 'rxjs/internal/operators/take';
import { AppointmentService } from '../service/appointment.service';

@Component({
  selector: 'client-schedule-covid-step-0',
  templateUrl: './schedule-covid-step-0.component.html'
})
export class ScheduleCovidStep0Component extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  stepList = SCHEDULE_COVID_QUOTE_STEP;
  frm: FormGroup;
  lang = GeneralLang;

  testTypeList: any[] = [];
  amount: any;
  testDescription: any;
  appointment: State;
  timeResult: string;
  iconError: string = WARNING_ICON;

  showLoading = true;
  showLoadingSpecialty = true;
  dataClinics;
  dataClinicSave;
  value;
  configView;
  hasPermission;
  params;

  especialityCovidCodMoneda = 1;
  especialityCovidObject: any;
  codigoMonedaPruebaCovid;

  private _specialty: any;
  private readonly _policy: any;
  arrPolicies: any[];

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Router: Router,
    private readonly _GeneralService: GeneralService,
    private readonly _appointmentService: AppointmentService,
    private readonly geolocationService: GeolocationService,
    private readonly _Autoservicios: Autoservicios
  ) {
    super();
  }

  ngOnInit(): void {
    this._initValues();
    this.geolocationService.hasPermission().then(hasPermission => {
      this.configView = { ...this.configView, canViewDistance: hasPermission };
      this.hasPermission = hasPermission;
      this.params = {
        ...this.params,
        latitud: MapLang.CLINIC.defaultCoords.latitude,
        longitud: MapLang.CLINIC.defaultCoords.longitude
      };
      this.getClinics();
    });
  }

  private _mapStateClinica(): void {
    const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st => {
      this.appointment = st;
      if (this.appointment.clinicaId) {
        this.value = this.appointment.clinicaId;
        this.dataClinicSave = this.dataClinics.find(element => element.clinicaId === this.value);
      }
    });

    const getAppointmentOneTime$ = this._AppointmentFacade.getAppointment$
      .pipe(take(1))
      .subscribe(this._mapAppointmentOneTime.bind(this));
    this.arrToDestroy.push(getAppointment$, getAppointmentOneTime$);
  }

  private _mapAppointmentOneTime(st): void {
    if (!st.especialidad) {
      st.poliza && (this.arrPolicies = [{ ...st.poliza }]);

      return void 0;
    }
    this.frm.patchValue({
      especialidadId: st.especialidad.especialidadId.toString()
    });
    this.onTestTypeSelected(this.frm.controls.especialidadId.value);
    // this.arrPolicies = [...st.paciente.polizas];
    // this.frm.patchValue(st.frmStep1);
  }

  getClinics(): void {
    this.showLoading = true;
    this._Autoservicios
      .ObtenerClinicasAgendables(
        { codigoApp: APPLICATION_CODE },
        { latitud: `${this.params.latitud}`, longitud: `${this.params.longitud}` }
      )
      .subscribe(r => {
        this.dataClinics = r;
        this.value = this.dataClinics[0].clinicaId;
        this.dataClinicSave = this.dataClinics.find(element => element.clinicaId === this.value);
        this._AppointmentFacade.AddEstablishment(this.dataClinicSave);
        this._mapStateClinica();
        this.showLoading = false;
      });
  }

  actionConfirm(): void {
    this._specialty = this.especialityCovidObject;
    this._specialty.codigoTipoDeducible = 'DAGR';
    this._specialty.montoCopago = parseFloat(this.amount);
    this._specialty.codigoMonedaCopago = this.especialityCovidCodMoneda;

    this._AppointmentFacade.AddSpeciality(this._specialty);
    this._AppointmentFacade.AddFrmStep1(true);

    this._Router.navigate(['/digital-clinic/schedule/steps-covid/1']);
  }

  actionCancel(): void {
    this.modalMessage.open();
  }

  onConfirmModal(r: boolean): void {
    const url = this._appointmentService.getUrlReturn;
    if (r) {
      this._Router.navigateByUrl(url);
    }
  }

  private _initValues(): void {
    this._AppointmentFacade.CleanStep2();
    this._HeaderHelperService.set(SCHEDULE_APPOINTMENT_COVID_STEP_0_HEADER);
    this.frm = new FormGroup({
      especialidadId: new FormControl('104', Validators.required)
    });
    this.getTestTypeValues();
    this.onTestTypeSelected(this.frm.controls.especialidadId.value);
  }

  getTestTypeValues(): void {
    const molecularObj = {
      codigo: this._GeneralService.getValueParams(KEYS_VALUES.COD_PRUEBA_MOLECULAR_CDM),
      descripcion: this._GeneralService.getValueParams(KEYS_VALUES.DES_PRUEBA_MOLECULAR_CDM),
      valor: this._GeneralService.getValueParams(KEYS_VALUES.COSTO_PRUEBA_MOLECULAR_CDM),
      texto: this._GeneralService.getValueParams(KEYS_VALUES.TEXTO_PRUEBA_MOLECULAR_CDM)
    };

    const antigenoObj = {
      codigo: this._GeneralService.getValueParams(KEYS_VALUES.COD_PRUEBA_ANTIGENO_CDM),
      descripcion: this._GeneralService.getValueParams(KEYS_VALUES.DES_PRUEBA_ANTIGENO_CDM),
      valor: this._GeneralService.getValueParams(KEYS_VALUES.COSTO_PRUEBA_ANTIGENO_CDM),
      texto: this._GeneralService.getValueParams(KEYS_VALUES.TEXTO_PRUEBA_ANTIGENO_CDM)
    };

    this.testTypeList.push(antigenoObj, molecularObj);
  }

  onTestTypeSelected(id: any): any {
    const obj = this.testTypeList.find(e => e.codigo === id);
    this.amount = parseFloat(obj.valor);
    this.testDescription = obj.texto;
    this.timeResult =
      id === '104'
        ? GeneralLang.Texts.TestResultCDM.replace('{{time}}', '1 hora')
        : GeneralLang.Texts.TestResultCDM.replace('{{time}}', '24 horas');
    this.especialityCovidObject = {
      especialidadId: parseInt(obj.codigo, 10),
      especialidadDescripcion: obj.descripcion,
      isFirstTime: true,
      selected: false
    };
  }
}
