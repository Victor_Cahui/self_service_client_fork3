// tslint:disable: no-for-in
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  public returnUrl: string;

  get getUrlReturn(): any {
    return this.returnUrl ? this.returnUrl : '/';
  }
  set setUrlReturn(url) {
    this.returnUrl = url;
  }
}
