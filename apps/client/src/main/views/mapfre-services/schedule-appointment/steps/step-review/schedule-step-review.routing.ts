import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleStepReviewComponent } from './schedule-step-review.component';
import { ScheduleStepReviewGuard } from './schedule-step-review.guard';

const routes: Routes = [
  {
    canActivate: [ScheduleStepReviewGuard],
    component: ScheduleStepReviewComponent,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleStepReviewRoutingModule {}
