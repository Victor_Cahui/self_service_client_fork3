import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from '@mx/services/general/general.service';
import { TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { take } from 'rxjs/operators';

import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { ClientService } from '@mx/services/client.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { APPOINTMENT_RESCHEDULE_STEP_SUCCESS_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-schedule-step-success',
  templateUrl: './schedule-step-success.component.html'
})
export class ScheduleStepSuccessComponent extends UnsubscribeOnDestroy implements OnInit {
  currentUser: any;
  graLng = GeneralLang;
  appointment: State;
  title = GeneralLang.Texts.AppointmentSuccessTitle;
  recommendationText: string;
  typeAppoiment = TYPE_APPOINTMENT;
  tipoCita = this.typeAppoiment.PRESENCIAL;
  especialityCovid = [104, 503];
  covidIsActive = false;

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _ClientService: ClientService,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Router: Router,
    private readonly generalService: GeneralService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }

  ngOnInit(): void {
    this.currentUser = this._ClientService.getUserData();
    this.recommendationText = this.generalService.getValueParams(KEYS_VALUES.MSJ_CITA_MEDICA_PRESENCIAL);
    this._setTitleHeader();
    this._mapState();
  }

  goToAppointments(): void {
    this._Router.navigate(['/digital-clinic/assists']);
  }

  private _mapState(): void {
    const getAppointmentOneTime$ = this._AppointmentFacade.getAppointment$.pipe(take(1)).subscribe(st => {
      this.appointment = st;
      if (this.especialityCovid.includes(this.appointment.especialidad.especialidadId)) {
        this.covidIsActive = true;
        this.title = GeneralLang.Texts.AppointmentCovidSuccessTitle;
        this.recommendationText = '';
      }
      this._HeaderHelperService.set(APPOINTMENT_RESCHEDULE_STEP_SUCCESS_HEADER);
      if (st.canReschedule) {
        this.title = GeneralLang.Texts.AppointmentRescheduleSuccessTitle;
      }
      if (st.puedeModificar && !st.canReschedule) {
        this.title = GeneralLang.Texts.AppointmentEditSuccessTitle;
      }
      if (st.tipoCita === this.typeAppoiment.VIRTUAL) {
        this.recommendationText = GeneralLang.Texts.AppointmentSuccessSubTitle4;
        this.tipoCita = this.typeAppoiment.VIRTUAL;
      }
      this._AppointmentFacade.CleanAppointment();
    });
    this.arrToDestroy.push(getAppointmentOneTime$);
  }

  private _setTitleHeader(): void {
    const st = this._AppointmentFacade.getState() || {};
    if (st.canReschedule) {
      this._HeaderHelperService.set(APPOINTMENT_RESCHEDULE_STEP_SUCCESS_HEADER);
    }
  }
}
