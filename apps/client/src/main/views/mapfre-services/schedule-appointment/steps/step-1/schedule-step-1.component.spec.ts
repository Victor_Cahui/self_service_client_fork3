import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { DirectivesModule, MfCardModule, MfInputModule, MfModalMessageModule, MfSelectModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { ScheduleStep1Component } from './schedule-step-1.component';
import { ScheduleStep1RoutingModule } from './schedule-step-1.routing';

describe('ScheduleStep1Component', () => {
  let component: ScheduleStep1Component;
  let fixture: ComponentFixture<ScheduleStep1Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormBaseModule,
        FormsModule,
        MfModalMessageModule,
        MfSelectBoxModule,
        MfSelectModule,
        ReactiveFormsModule,
        ScheduleStep1RoutingModule,
        StepperModule,
        DirectivesModule,
        MfCardModule,
        MfInputModule
      ],
      declarations: [ScheduleStep1Component]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleStep1Component);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('to validate componetn properties step-1', async(() => {
    const text = VehiclesLang;
    expect(component.lang.Titles.DataMedicalAppointmentStepOne).toEqual(text.Titles.DataMedicalAppointmentStepOne);
    expect(component.lang.Texts.VirtualMeeting).toEqual(text.Texts.VirtualMeeting);
    expect(component.lang.Texts.VirtualMeetingDescription).toEqual(text.Texts.VirtualMeetingDescription);
    expect(component.lang.Texts.Appointment).toEqual(text.Texts.Appointment);
    expect(component.lang.Texts.AppointmentDescription).toEqual(text.Texts.AppointmentDescription);
    expect(component.lang.Labels.attention).toEqual(text.Labels.attention);
    expect(component.lang.Labels.MedicalCenter).toEqual(text.Labels.MedicalCenter);
  }));
});
