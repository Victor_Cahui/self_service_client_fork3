import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { MfButtonModule, MfModalMessageModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { ScheduleStepReviewComponent } from './schedule-step-review.component';
import { ScheduleStepReviewGuard } from './schedule-step-review.guard';
import { ScheduleStepReviewRoutingModule } from './schedule-step-review.routing';

@NgModule({
  declarations: [ScheduleStepReviewComponent],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfButtonModule,
    MfModalMessageModule,
    ReactiveFormsModule,
    ScheduleStepReviewRoutingModule,
    StepperModule,
    TooltipsModule,
    MfCustomAlertModule
  ],
  providers: [ScheduleStepReviewGuard]
})
export class ScheduleStepReviewModule {}
