import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppointmentService } from '../steps/service/appointment.service';

@Injectable({
  providedIn: 'root'
})
export class CaptureRouteSteps implements CanActivate {
  constructor(private readonly router: Router, private readonly appointmentService: AppointmentService) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    this.appointmentService.setUrlReturn = this.router.url;

    return true;
  }
}
