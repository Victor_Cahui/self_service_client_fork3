import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleAppointmentComponent } from './schedule-appointment.component';

const routes: Routes = [
  {
    path: '',
    component: ScheduleAppointmentComponent,
    children: [
      {
        path: '',
        redirectTo: 'map',
        pathMatch: 'prefix'
      },
      {
        path: 'map',
        loadChildren: './clinic-map/clinic-map.module#ClinicMapModule'
      },
      {
        path: 'steps',
        loadChildren: './steps/schedule-steps.module#ScheduleStepsModule'
      },
      {
        path: 'steps-covid',
        loadChildren: './steps/schedule-covid-steps.module#ScheduleCovidStepsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleAppointmentRouting {}
