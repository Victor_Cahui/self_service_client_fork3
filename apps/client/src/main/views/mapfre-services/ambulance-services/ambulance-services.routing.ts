import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmbulanceServicesComponent } from './ambulance-services.component';

const routes: Routes = [
  {
    path: '',
    component: AmbulanceServicesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmbulanceServicesRouting {}
