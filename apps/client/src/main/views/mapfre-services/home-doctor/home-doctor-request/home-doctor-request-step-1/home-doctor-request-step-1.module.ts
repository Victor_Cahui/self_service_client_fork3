import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GmMsHomeDoctorFormModule, GmMsSearchModule, GoogleMapMyServiceModule } from '@mx/components';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HomeDoctorRequestStep1Component } from './home-doctor-request-step-1.component';

@NgModule({
  imports: [CommonModule, StepperModule, GoogleMapMyServiceModule, GmMsHomeDoctorFormModule, GmMsSearchModule],
  declarations: [HomeDoctorRequestStep1Component]
})
export class HomeDoctorRequestStep1Module {}
