import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeDoctorRequestStep1Module } from './home-doctor-request-step-1/home-doctor-request-step-1.module';
import { HomeDoctorRequestStep2Module } from './home-doctor-request-step-2/home-doctor-request-step-2.module';
import { HomeDoctorRequestStep3Module } from './home-doctor-request-step-3/home-doctor-request-step-3.module';
import { HomeDoctorRequestComponent } from './home-doctor-request.component';
import { HomeDoctorRequestRouting } from './home-doctor-request.routing';

@NgModule({
  imports: [
    CommonModule,
    HomeDoctorRequestRouting,
    HomeDoctorRequestStep1Module,
    HomeDoctorRequestStep2Module,
    HomeDoctorRequestStep3Module
  ],
  declarations: [HomeDoctorRequestComponent]
})
export class HomeDoctorRequestModule {}
