import { Location } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalHowWorksComponent } from '@mx/components/shared/modals/modal-how-works/modal-how-works.component';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { FrmProvider } from '@mx/core/shared/providers/services';
import { SelectListItem } from '@mx/core/ui';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { APPLICATION_CODE, DEFAULT_HOME_DOCTOR_HOURS, STORAGE } from '@mx/settings/constants/general-values';
import { HOME_DOCTOR_HOURS } from '@mx/settings/constants/key-values';
import { HOME_DOCTOR_STEP } from '@mx/settings/constants/router-step';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HOME_DOCTOR_LANG } from '@mx/settings/lang/health.lang';
import { HowHomeDoctorWorks } from '@mx/settings/lang/services.lang';
import { GeneralRequest, IModalHowWorks } from '@mx/statemanagement/models/general.models';
import { IHomeDoctorResponse } from '@mx/statemanagement/models/service.interface';
import { isEmpty } from 'lodash-es';

@Component({
  selector: 'client-home-doctor-request-step-2',
  templateUrl: './home-doctor-request-step-2.html'
})
export class HomeDoctorRequestStep2Component extends UnsubscribeOnDestroy {
  @ViewChild(ModalHowWorksComponent) mfModalHowWorks: ModalHowWorksComponent;
  frm: FormGroup;
  policyListResponse: Array<IHomeDoctorResponse> = [];
  graLng = GeneralLang;
  lang = HOME_DOCTOR_LANG;
  stepList = HOME_DOCTOR_STEP;
  currentServiceSelected: IHomeDoctorResponse;
  serviceType: Array<SelectListItem> = [];
  beneficiaries: Array<any> = [];
  specialties: Array<any> = [];
  showLoading: boolean;
  showLoadingPolicy: boolean;
  openModalHowWorks: boolean;
  howItWorks: IModalHowWorks;
  infoFrm: any;
  infoFrmOne: any;
  header = HOME_DOCTOR_HEADER;

  constructor(
    protected readonly _FrmProvider: FrmProvider,
    protected readonly servicesMFService: ServicesMFService,
    protected readonly generalService: GeneralService,
    protected readonly _StorageService: StorageService,
    protected readonly _clientService: ClientService,
    protected location: Location,
    private readonly router: Router
  ) {
    super();
    this._initValues();
  }

  private _initValues(): void {
    this.infoFrmOne = this._StorageService.getStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_ONE);
    if (this.infoFrmOne == null) {
      this.router.navigate(['/mapfre-services/home-doctor/request/steps/1']);
    }
    this.frm = this._FrmProvider.HomeDoctorRequestFormComponent();
    this.setValuesModal(HowHomeDoctorWorks, HOME_DOCTOR_HOURS, DEFAULT_HOME_DOCTOR_HOURS);
    this.getPolicyList();
    this.onPolicyChange();
  }

  private getInformationSaved(): void {
    const userData = this._clientService.getUserData();
    this.frm.controls.telephone.setValue(userData.telefonoMovil || userData.telefonoFijo || '');
    this.infoFrm = this._StorageService.getStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO);
    if (this.infoFrm == null) {
      return void 0;
    }
    this.frm.controls.policy.patchValue(this.infoFrm.policy);
    this.frm.patchValue(this.infoFrm);
  }

  private getPolicyList(): void {
    this.showLoading = true;
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.servicesMFService.getPoliciesHomeDoctor(params).subscribe(response => {
      this.policyListResponse = response || [];
      this.policyListResponse.forEach((resp, index) => {
        const text = resp.producto.numeroPoliza
          ? `${resp.producto.nombreProducto} - ${resp.producto.numeroPoliza}`
          : resp.producto.nombreProducto;
        const type = {
          text,
          value: resp.producto.identificadorProducto,
          selected: true,
          disabled: false
        };

        this.serviceType.push(type);
      });
      this.getInformationSaved();
      this.showLoading = false;
    });
  }

  viewInfo(): void {
    this.openModalHowWorks = true;
    setTimeout(() => {
      this.mfModalHowWorks.open();
    }, 100);
  }

  setValuesModal(data: IModalHowWorks, key: string, defaultHours: number): void {
    this.howItWorks = data;
    this.generalService.getParameters().subscribe(() => {
      let hours = this.generalService.getValueParams(key);
      hours = hours ? hours : defaultHours.toString();
      if (this.howItWorks.warning) {
        this.howItWorks.warning = this.howItWorks.warning.replace('{{hours}}', hours);
      }
    });
  }

  onPolicyChange(): void {
    this.frm.controls['policy'].valueChanges.subscribe(value => {
      this.beneficiaries = [];
      this.specialties = [];

      if (this.policyListResponse && this.policyListResponse.length) {
        this.currentServiceSelected = this.policyListResponse.find(
          vehicle => vehicle.producto.identificadorProducto === Number(value)
        );
        if (this.currentServiceSelected) {
          this.currentServiceSelected.precio.monedaSimbolo = StringUtil.getMoneyDescription(
            this.currentServiceSelected.precio.codigoMoneda
          ).concat(' ');
          const beneficiaries = this.currentServiceSelected.beneficiarios || [];
          const specialties = this.currentServiceSelected.especialidades || [];

          beneficiaries.forEach((beneficiary, index) => {
            const type = {
              text: beneficiary.nombre,
              value: beneficiary.identificador,
              selected: true,
              disabled: false
            };

            this.beneficiaries.push(type);
            if (!index) {
              this.frm.controls['patient'].setValue(type.value);
            }
          });

          specialties.forEach((specialty, index) => {
            const type = {
              text: specialty.nombre,
              value: specialty.identificador,
              selected: true,
              disabled: false
            };

            this.specialties.push(type);
            if (!index) {
              this.frm.controls['specialty'].setValue(type.value);
            }
          });
        }
      }
    });
  }

  showErrors(control: string): boolean {
    return (
      (this.frm.controls[control].dirty || this.frm.controls[control].touched) &&
      !isEmpty(this.frm.controls[control].errors)
    );
  }

  public actionCancel(): void {
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO);
    this.location.back();
  }

  public actionConfirm(): void {
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO);
    this._StorageService.saveStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO, this.frm.value);
    this._StorageService.saveStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO_DATA, this.currentServiceSelected);
    this.router.navigate(['/mapfre-services/home-doctor/request/steps/3']);
  }
}
