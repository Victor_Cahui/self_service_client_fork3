import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  GmMsHomeDoctorFormModule,
  GmMsHomeDoctorSummaryModule,
  GmMsSearchModule,
  GoogleMapMyServiceModule
} from '@mx/components';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HomeDoctorRequestModule } from './home-doctor-request/home-doctor-request.module';
import { HomeDoctorComponent } from './home-doctor.component';
import { HomeDoctorRouting } from './home-doctor.routing';

@NgModule({
  imports: [
    CommonModule,
    HomeDoctorRouting,
    GoogleMapMyServiceModule,
    GmMsHomeDoctorFormModule,
    GmMsSearchModule,
    StepperModule,
    GmMsHomeDoctorSummaryModule,
    HomeDoctorRequestModule
  ],
  declarations: [HomeDoctorComponent]
})
export class HomeDoctorModule {}
