import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDoctorRequestComponent } from './home-doctor-request/home-doctor-request.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'request/steps'
  },
  {
    path: 'request/steps',
    component: HomeDoctorRequestComponent,
    loadChildren: './home-doctor-request/home-doctor-request.module#HomeDoctorRequestModule'
  },
  {
    path: 'detail/:number',
    loadChildren: './home-doctor-detail/home-doctor-detail.module#HomeDoctorDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeDoctorRouting {}
