import { DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesUtils } from '@mx/components/shared/mapfre-services/services.utils';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { HOME_DOCTOR_ICON, PIN_END_MARK_ICON } from '@mx/settings/constants/images-values';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';
import { ICON_ITEMS, SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HomeDoctorLang, ServiceFormLang } from '@mx/settings/lang/services.lang';
import { IUserMarker } from '@mx/statemanagement/models/google-map.interface';
import { IServicesItemResponse, ISummaryView } from '@mx/statemanagement/models/service.interface';

@Component({
  selector: 'client-home-doctor-detail',
  templateUrl: './home-doctor-detail.component.html',
  providers: [DecimalPipe]
})
export class HomeDoctorDetailComponent implements OnInit {
  data: ISummaryView;
  fullMap: boolean;
  doctorLang = HomeDoctorLang;
  generalLang = GeneralLang;
  requestLang = ServiceFormLang;
  header: IHeader;
  endMarker: IUserMarker;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly activePath: ActivatedRoute,
    private readonly servicesMfService: ServicesMFService,
    private readonly decimalPipe: DecimalPipe
  ) {}

  ngOnInit(): void {
    const serviceNumber = this.activePath.snapshot.params.number;
    if (serviceNumber) {
      const data: IServicesItemResponse = this.servicesMfService.getServiceData(
        SERVICES_TYPE.HOME_DOCTOR,
        serviceNumber
      );
      if (data) {
        this.setViewData(data);
      }
    }
  }

  setViewData(values: IServicesItemResponse): void {
    // Titulo
    this.header = ServicesUtils.getHeader(HOME_DOCTOR_HEADER, values);
    this.headerHelperService.set(this.header);

    // Costo
    values.costoServicioFormat =
      values.costoServicio > 0 ? this.decimalPipe.transform(values.costoServicio, '1.2-2') : '';

    this.endMarker = {
      pinUrl: PIN_END_MARK_ICON,
      latitude: values.latitudDestino,
      longitude: values.longitudDestino
    };

    this.data = {
      icon: HOME_DOCTOR_ICON,
      title: ServicesUtils.getStatusTitle(values),
      text: ServicesUtils.getStatusText(values),
      warning: ServicesUtils.getWarning(values),
      itemsDetail: [
        {
          icon: ICON_ITEMS.DOCTOR,
          text: `${this.requestLang.Doctor.toUpperCase()}: ${values.empresaProveedora ||
            this.requestLang.ToBeAssigned}`,
          other: values.medico.tipoMedicina
        },
        {
          icon: ICON_ITEMS.POLICY,
          label: values.medico.nombreProducto,
          text: values.medico.numeroPoliza
        },
        {
          icon: ICON_ITEMS.PERSON,
          label: `${GeneralLang.Labels.Patient.toUpperCase()}:`,
          text: values.medico.paciente,
          other: `${this.requestLang.Symptoms.toUpperCase()}: ${values.medico.sintomas || ''}`
        }
      ],
      itemsSchedule: [
        {
          label: this.generalLang.Labels.DateTime,
          text: ServicesUtils.getScheduledText(values.fechaAgendada, values.codigoEstado, values.medico.esUrgente)
        },
        {
          label: this.requestLang.Address,
          text: values.destinoServicio,
          other: `${this.requestLang.Reference}: ${values.referenciaDestino}` || ''
        },
        {
          label: this.requestLang.ContactPhone,
          text: values.telefono
        }
      ]
    };
  }
}
