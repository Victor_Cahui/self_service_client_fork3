import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDoctorDetailComponent } from './home-doctor-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HomeDoctorDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeDoctorDetailRouting {}
