import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleMapMyServiceModule } from '@mx/components';
import { ServicesDetailModule } from '@mx/components/shared/mapfre-services/services-detail/services-detail.module';
import { HomeDoctorDetailComponent } from './home-doctor-detail.component';
import { HomeDoctorDetailRouting } from './home-doctor-detail.routing';

@NgModule({
  imports: [CommonModule, HomeDoctorDetailRouting, GoogleMapMyServiceModule, ServicesDetailModule],
  declarations: [HomeDoctorDetailComponent]
})
export class HomeDoctorDetailModule {}
