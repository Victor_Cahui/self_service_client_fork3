import { DOCUMENT } from '@angular/common';
import { Component, Inject, Renderer2 } from '@angular/core';
import { GmMsLayoutBase } from '@mx/components/gm-ms-views/gm-ms-base-layout';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { SUBSTITUTE_DRIVER_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-substitute-driver',
  templateUrl: './substitute-driver.component.html'
})
export class SubstituteDriverComponent extends GmMsLayoutBase {
  constructor(
    protected servicesMfService: ServicesMFService,
    protected render2: Renderer2,
    protected headerHelperService: HeaderHelperService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(servicesMfService, 'getAllowedAreasDriver', render2, headerHelperService, document);
    this.header = SUBSTITUTE_DRIVER_HEADER;
  }
}
