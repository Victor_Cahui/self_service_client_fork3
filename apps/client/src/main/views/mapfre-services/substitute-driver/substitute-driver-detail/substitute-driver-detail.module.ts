import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleMapMyServiceModule } from '@mx/components';
import { ServicesDetailModule } from '@mx/components/shared/mapfre-services/services-detail/services-detail.module';
import { SubstituteDriverDetailComponent } from './substitute-driver-detail.component';
import { SubstituteDriverDetailRouting } from './substitute-driver-detail.routing';

@NgModule({
  imports: [CommonModule, SubstituteDriverDetailRouting, GoogleMapMyServiceModule, ServicesDetailModule],
  declarations: [SubstituteDriverDetailComponent]
})
export class SubstituteDriverDetailModule {}
