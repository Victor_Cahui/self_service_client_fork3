import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardBasicEnterpriseModule } from '@mx/components/profile/card-basic-enterprise/card-basic-enterprise.module';
import { CardBasicInfoModule } from '@mx/components/profile/card-basic-info/card-basic-info.module';
import { CardBasicJobInfoModule } from '@mx/components/profile/card-basic-job-info/card-basic-job-info.module';
import { CardCorrespondenceAddressModule } from '@mx/components/profile/card-correspondence-address/card-correspondence-address.module';
import { EditDataWorkModule } from '@mx/components/profile/edit-data-work/edit-data-work.module';
import { CardUserProfileModule } from '@mx/components/shared/card-user-profile/card-user-profile.module';
import { MfCheckboxModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.module';
import { MfModalAlertModule } from '@mx/core/ui/public-api';
import { HomeProfileComponent } from './home-profile.component';
import { HomeProfileRoutingModule } from './home-profile.routing';

@NgModule({
  imports: [
    CardBasicEnterpriseModule,
    CardBasicInfoModule,
    CardBasicJobInfoModule,
    CardCorrespondenceAddressModule,
    CardUserProfileModule,
    MfButtonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EditDataWorkModule,
    HomeProfileRoutingModule,
    MfCheckboxModule,
    MfModalMessageModule,
    MfModalAlertModule
  ],
  exports: [],
  declarations: [HomeProfileComponent],
  providers: []
})
export class HomeProfileModule {}
