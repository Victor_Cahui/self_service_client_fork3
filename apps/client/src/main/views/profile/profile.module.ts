import { NgModule } from '@angular/core';
import { EditProfileGuard } from './edit-profile.guard';
import { EditProfileService } from './edit.service';
import { ProfileRoutingModule } from './profile.routing';

@NgModule({
  imports: [ProfileRoutingModule],
  exports: [],
  declarations: [],
  providers: [EditProfileService, EditProfileGuard]
})
export class ProfileModule {}
