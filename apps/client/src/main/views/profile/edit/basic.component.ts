import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';
import { isEmpty } from 'lodash-es';

export abstract class BasicComponent {
  static showBasicErrors(form: FormGroup, control: string): boolean {
    return (
      form &&
      (form.controls[control].dirty || form.controls[control].touched) &&
      !isEmpty(form.controls[control].errors)
    );
  }

  static getFormValidationErrors(controls: FormGroupControls): Array<AllValidationErrors> {
    let errors: Array<AllValidationErrors> = [];
    Object.keys(controls).forEach(key => {
      const control = controls[key];
      if (control instanceof FormGroup) {
        errors = errors.concat(BasicComponent.getFormValidationErrors(control.controls));
      }
      const controlErrors: ValidationErrors = controls[key].errors;
      if (controlErrors !== null) {
        Object.keys(controlErrors).forEach(keyError => {
          errors.push({
            control_name: key,
            error_name: keyError,
            error_value: controlErrors[keyError]
          });
        });
      }
    });

    return errors;
  }
}

export interface AllValidationErrors {
  control_name: string;
  error_name: string;
  error_value: any;
}

export interface FormGroupControls {
  [key: string]: AbstractControl;
}
