import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SafeHtmlPipe } from '@mx/core/shared/common/pipes/safe-html.pipe';
import { MfModalAlertModule, MfModalMessageModule, MfModalModule } from '@mx/core/ui';
import { NgDropModule } from '@mx/core/ui/lib/directives/ng-drop/ng-drop.module';
import { NgxUploaderModule } from 'ngx-uploader';
import { StepTwoComponent } from './step-two.component';
import { StepTwoRoutingModule } from './step-two.routing';

@NgModule({
  imports: [
    CommonModule,
    StepTwoRoutingModule,
    MfModalModule,
    MfModalAlertModule,
    MfModalMessageModule,
    NgxUploaderModule,
    NgDropModule
  ],
  exports: [],
  declarations: [StepTwoComponent, SafeHtmlPipe],
  providers: []
})
export class StepTwoModule {}
