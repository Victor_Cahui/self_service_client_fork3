import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { WebSocketAPI } from '@mx/services/websockets/websocket-api';

import { MainHeaderModule, MainMenuModule, WebChatModule } from '@mx/components';
import { ModalContactModule } from '@mx/components/shared/modals/modal-contact/modal-contact.module';
import { ModalExpiredSessionModule } from '@mx/components/shared/modals/modal-expired-session/modal-expired-session.module';
import { MfButtonTopModule } from '@mx/core/ui/lib/components/button-top/button-top.module';
import { MainLayoutComponent } from './main-layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MainMenuModule,
    MainHeaderModule,
    ModalExpiredSessionModule,
    ModalContactModule,
    MfButtonTopModule,
    WebChatModule
  ],
  declarations: [MainLayoutComponent],
  providers: [WebSocketAPI]
})
export class MainLayoutModule {}
