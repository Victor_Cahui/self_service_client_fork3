import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginModule } from '@mx/components/login/login.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { LoginTopComponent } from './login-top.component';
import { LoginWelcomeComponent } from './login-welcome/login-welcome.component';

@NgModule({
  imports: [RouterModule, CommonModule, LoginModule],
  exports: [LoginTopComponent],
  declarations: [LoginWelcomeComponent, LoginFormComponent, LoginTopComponent],
  providers: []
})
export class LoginTopModule {}
