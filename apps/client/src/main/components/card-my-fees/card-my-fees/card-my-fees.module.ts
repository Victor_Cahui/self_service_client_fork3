import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaymentButtonModule } from '@mx/components/payment/payment-button/payment-button.module';
import { SendReceiptModule } from '@mx/components/payment/send-receipt/send-receipt.module';
import { IconPaymentTypeModule } from '@mx/components/shared/icon-payment-type/icon-payment-type.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardMyFeesComponent } from './card-my-fees.component';

@NgModule({
  imports: [
    CommonModule,
    IconPaymentTypeModule,
    DirectivesModule,
    MfCardModule,
    LinksModule,
    RouterModule,
    PaymentButtonModule,
    MfLoaderModule,
    SendReceiptModule,
    GoogleModule
  ],
  declarations: [CardMyFeesComponent],
  exports: [CardMyFeesComponent]
})
export class CardMyFeesModule {}
