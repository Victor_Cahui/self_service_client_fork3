import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalHowWorksModule } from '@mx/components/shared/modals/modal-how-works/modal-how-works.module';
import { PageTitleModule } from '@mx/components/shared/page-title/page-title.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { BannerPaymentConfigurationModule } from '../banner-payment-configuration/banner-payment-configuration.module';
import { CardPaymentsSettingComponent } from './card-payments-setting.component';
import { PaymentsSettingsPoliciesModule } from './card-payments-settings-policies/card-payments-settings-policies.module';
@NgModule({
  imports: [
    CommonModule,
    PageTitleModule,
    BannerPaymentConfigurationModule,
    ModalHowWorksModule,
    PaymentsSettingsPoliciesModule,
    GoogleModule
  ],
  exports: [CardPaymentsSettingComponent],
  declarations: [CardPaymentsSettingComponent]
})
export class CardPaymentsSettingModule {}
