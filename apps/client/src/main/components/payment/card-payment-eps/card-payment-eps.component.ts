import { CurrencyPipe } from '@angular/common';
import { Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { gaEventDecorator, GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModal, SelectListItem } from '@mx/core/ui';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import {
  MEDICAL_EXPENSES_WITHOUT_CROSS_ICON,
  PORCENTAGE_ICON,
  WALLET_ICON
} from '@mx/settings/constants/images-values';
import { CARD_EPS_PAYMENT_SUBTITLE } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { MethodPaymentLang, PaymentLang } from '@mx/settings/lang/payment.lang';
import { IEpsPaymentConcept, IEpsPaymentConceptItem } from '@mx/statemanagement/models/payment.models';
import { IPaymentPolicy, IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-payment-eps',
  templateUrl: './card-payment-eps.component.html',
  providers: [CurrencyPipe]
})
export class CardPaymentEpsComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @HostBinding('attr.class') attr_class = 'w-10';

  @ViewChild(MfModal) mfModal: MfModal;

  @Input() enableEdit: boolean;
  @Output() edit?: EventEmitter<IPaymentPolicy>;
  @Output() paymentView: EventEmitter<IPaymentPolicy>;

  iconCard = WALLET_ICON;
  medicalExpensesIcon = MEDICAL_EXPENSES_WITHOUT_CROSS_ICON;
  porcentageIcon = PORCENTAGE_ICON;
  policyTypes = POLICY_TYPES;
  methodPaymentLang = MethodPaymentLang;
  paymentLang = PaymentLang;
  collapse = false;
  epsPaymentCardSubtitle: string;
  paymentConceptTatbleTitle: string;
  showLoading: boolean;
  policy: IPoliciesByClientResponse;
  openModalPaymentConcept: boolean;
  enablePaymentsLink: boolean;

  formPaymentConcept: FormGroup;
  combo: AbstractControl;

  paymentConceptList: Array<IEpsPaymentConcept>;
  paymentConceptListItems: Array<SelectListItem> = [];
  currentPaymentConceptItem: Array<IEpsPaymentConceptItem>;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly generalService: GeneralService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _AuthService: AuthService,
    private readonly currencyPipe: CurrencyPipe,
    public formBuilder: FormBuilder,
    protected activatedRoute: ActivatedRoute,
    protected gaService: GAService
  ) {
    super(gaService);
    this.edit = new EventEmitter<IPaymentPolicy>();
    this.paymentView = new EventEmitter<IPaymentPolicy>();
    this.formPaymentConcept = this.formBuilder.group({
      combo: new FormControl('')
    });
    this.combo = this.formPaymentConcept.controls['combo'];
  }

  ngOnInit(): void {
    this.enableEdit = environment.ACTIVATE_CONFIGURATION_PAYMENT;
    this.enablePaymentsLink = environment.ACTIVATE_EPS_VIEW_PAYMENTS;
    this.policy = this.policiesInfoService.getPolicy();
    this.getParameters();
    this.getPaymentConcepts();
  }

  getParameters(): void {
    this.generalService
      .getParametersSubject()
      .subscribe(() => (this.epsPaymentCardSubtitle = this.generalService.getValueParams(CARD_EPS_PAYMENT_SUBTITLE)));
  }

  getPaymentConcepts(): void {
    this.showLoading = true;
    const pathParams = { nroContrato: this.policy.numeroPoliza };
    const queryParams = {
      codigoApp: APPLICATION_CODE,
      codCliente: this.policy.codCliente
    };
    this._Autoservicios.ObtenerEpsContratanteConceptoPago(pathParams, queryParams).subscribe(
      response => {
        this.paymentConceptList = response || [];
        this.paymentConceptListItems = this.paymentConceptList.map((item, index) => {
          return new SelectListItem({ text: item.descPlan.toUpperCase(), value: item.codPlan, selected: index === 0 });
        });
      },
      error => {
        console.error(error);
      },
      () => (this.showLoading = false)
    );
  }

  editClick(): void {
    this.edit && this.edit.next();
  }

  @gaEventDecorator()
  paymentViewClick(): void {
    this.paymentView && this.paymentView.next(this.activatedRoute.snapshot.params['policyNumber']);
  }

  @gaEventDecorator()
  tableViewClick(): void {
    if (this.paymentConceptList && this.paymentConceptList.length) {
      this.openModalPaymentConcept = true;
      setTimeout(() => {
        this.mfModal.open();
        const firstPlan = this.paymentConceptList[0];
        this.combo.setValue(firstPlan.codPlan);
        this.getSelectedOpt(new SelectListItem({ text: firstPlan.descPlan, value: firstPlan.codPlan }));
      });
    }
  }

  getSelectedOpt(selected: SelectListItem): void {
    this.paymentConceptTatbleTitle = selected.text;
    const data = this.paymentConceptList.find(item => item.codPlan === selected.value).conceptos;
    this.currentPaymentConceptItem = data.map(this._mapCurrentPaymentConceptItem.bind(this));
  }

  private _mapCurrentPaymentConceptItem(item: IEpsPaymentConceptItem): IEpsPaymentConceptItem {
    const symbol = `${StringUtil.getMoneyDescription(item.codigoMoneda)} `;
    const price = this.currencyPipe.transform(item.monto, symbol);

    return { ...item, price };
  }

  ngOnDestroy(): void {
    this.paymentView && this.paymentView.unsubscribe();
  }
}
