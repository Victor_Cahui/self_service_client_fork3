import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule, MfSelectModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { CardPaymentEpsComponent } from './card-payment-eps.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DirectivesModule,
    GoogleModule,
    MfCardModule,
    MfSelectModule,
    MfModalModule,
    TooltipsModule,
    MfLoaderModule
  ],
  declarations: [CardPaymentEpsComponent],
  exports: [CardPaymentEpsComponent]
})
export class CardPaymentEpsModule {}
