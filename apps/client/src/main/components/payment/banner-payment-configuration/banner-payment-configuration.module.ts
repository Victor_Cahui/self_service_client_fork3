import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { BannerPaymentConfigurationComponent } from './banner-payment-configuration.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, GoogleModule],
  declarations: [BannerPaymentConfigurationComponent],
  exports: [BannerPaymentConfigurationComponent]
})
export class BannerPaymentConfigurationModule {}
