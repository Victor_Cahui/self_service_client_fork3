import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { COVERAGE_STATUS, POLICY_QUOTA_TYPE, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { FormatMMMYYYY, isExpired } from '@mx/core/shared/helpers/util/date';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { MfModal, SelectListItem } from '@mx/core/ui';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import {
  APPLICATION_CODE,
  COLOR_STATUS,
  FORMAT_DATE_DDMMYYYY,
  GENERAL_MAX_LENGTH,
  REG_EX,
  SEARCH_ORDEN_ASC
} from '@mx/settings/constants/general-values';
import {
  FILE_ICON as invoiceIcon,
  NOTICE_ICON as noticeIcon,
  WARNING_ICON
} from '@mx/settings/constants/images-values';
import { SOAT_ANTIC_RENOVAR_DIAS, TIPO_SEGURO } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import {
  IAssociatedReceipt,
  ICardPaymentDetailView,
  ICardPaymentGroupByMonthView,
  IPaymentResponse
} from '@mx/statemanagement/models/payment.models';
import { IPoliciesAndProductsByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { Observable } from 'rxjs';
import { BasePayments } from './base-payments-detail';

@Component({
  selector: 'client-payments-detail',
  templateUrl: './card-payments-detail.component.html',
  providers: [DatePipe, CurrencyPipe]
})
export class CardPaymentsDetailComponent extends BasePayments implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;

  titleNextPayments = PaymentLang.Titles.NextPayments;
  noticeNumber = PolicyLang.Labels.NoticeNumber;
  billNumber = PolicyLang.Labels.BillNumber;
  healthLang = HealthLang;
  invoiceNumber = HealthLang.Labels.InvoiceNumber;
  preInvoiceNumber = HealthLang.Labels.PreInvoiceNumber;
  contractorNumber = PolicyLang.Texts.NumberContract;
  emissionDateLabel = HealthLang.Labels.DateEmission;
  labelPolicy = PolicyLang.Labels.Policy;
  labelPolicies = PolicyLang.Labels.Policies;
  labelFee = PolicyLang.Texts.Fee;
  labelWithOutFees = PolicyLang.Texts.WithOutFees;
  labelPayment = GeneralLang.Buttons.Payment;
  labelNumberPolicy = PolicyLang.Texts.NumberPolicy;
  labelDeadline = PolicyLang.Texts.DeadLine;
  labelWithOutCoverage = PolicyLang.Texts.WithOutCoverage;
  txts = PolicyLang;
  labelInsuranceType = GeneralLang.Labels.InsuranceType;
  labelFilterBy = GeneralLang.Labels.FilterBy;
  btnFilter = GeneralLang.Buttons.Filter;
  btnAccountStatus = GeneralLang.Buttons.AccountStatus;
  btnUnFiltered = GeneralLang.Buttons.UnFiltered;
  btnCleanFilter = GeneralLang.Buttons.CleanFilter;
  coverageStatus = COVERAGE_STATUS;
  showLoading: boolean;
  showFilter: boolean;
  showModalPaymentPlaces: boolean;
  renewDays: number;
  iconError = WARNING_ICON;

  listPendingPaymentsGroupByMonth: Array<ICardPaymentGroupByMonthView>;
  listPaymentsOrigin: Array<ICardPaymentDetailView>;
  insuranceTypeList: Array<SelectListItem>;

  formFilter: FormGroup;
  activatePayment = environment.ACTIVATE_PAYMENTS;
  parametersSubject: Observable<Array<IParameterApp>>;
  policyTypeData: Array<IPoliciesAndProductsByClientResponse>;
  numericRegExp = REG_EX.numeric;
  maxLengthPolice = GENERAL_MAX_LENGTH.numberPolice;
  isFilteredFrm: boolean;

  constructor(
    private readonly paymentService: PaymentService,
    protected authService: AuthService,
    protected datePipe: DatePipe,
    protected currencyPipe: CurrencyPipe,
    protected _Autoservicios: Autoservicios,
    private readonly _StorageService: StorageService,
    private readonly _FrmProvider: FrmProvider,
    private readonly activePath: ActivatedRoute,
    public router: Router,
    private readonly generalService: GeneralService,
    protected gaService?: GAService
  ) {
    super(_Autoservicios, authService, currencyPipe, gaService);
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.getParameters();
    this.paymentService.clearCurrentQuote();
    this.paymentService.clearPaymentListConfiguration();
    this.formFilter = this._FrmProvider.CardPaymentsDetailComponent();
    // Verificar si trae parámetros para filtrar por tipo de seguro
    this._setPolicyFromNavParams();
    this.getPayments();
  }

  // Parametros Generales
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(() => {
      this.renewDays = Number(this.generalService.getValueParams(SOAT_ANTIC_RENOVAR_DIAS));
    });
  }

  // Obtener Pagos Pendientes
  getPayments(): void {
    this.showLoading = true;
    this.showFilter = false;
    this.listPendingPaymentsGroupByMonth = [];
    this.isFilteredFrm = this._isFilteredFrm();
    this._Autoservicios
      .ListadoPagos(
        { codigoApp: APPLICATION_CODE },
        {
          criterio: 'fechaExpiracion',
          orden: SEARCH_ORDEN_ASC,
          ...this.formFilter.value
        }
      )
      .subscribe(this._mapResponsePayments.bind(this), () => {
        this.showLoading = false;
      });
  }

  // Agrupa pagos por mes
  groupByMonth(payments: Array<ICardPaymentDetailView>): void {
    this.listPendingPaymentsGroupByMonth = [];

    const paymentsGroup = payments.reduce(
      (g, i) => {
        g[i.month] =
          g[i.month] ||
          ({
            titleMonth: FormatMMMYYYY(i.month).toUpperCase(),
            paymentResume: '',
            payments: [],
            collapse: false
          } as ICardPaymentGroupByMonthView);
        g[i.month].payments.push(i);

        return g;
      },
      {} as ICardPaymentGroupByMonthView
    );

    this.listPendingPaymentsGroupByMonth = Object.keys(paymentsGroup).map(group => paymentsGroup[group]);

    this.listPendingPaymentsGroupByMonth.forEach(g => {
      let totalPEN = 0;
      let totalUSD = 0;
      g.payments.forEach(payment => {
        if (payment.response.codigoMoneda === 1) {
          totalPEN += payment.response.montoTotal;
        } else {
          totalUSD += payment.response.montoTotal;
        }
      });
      g.paymentResume = StringUtil.getMonthPaymentResume(this.currencyPipe, totalPEN, totalUSD);
    });
  }

  setSubtitle(total): string {
    const text = total > 1 ? PaymentLang.Texts.Payments : PaymentLang.Texts.Payment;

    return `-  ${total} ${text}`;
  }

  filter(): void {
    setTimeout(() => {
      this.getPayments();
    }, 10);
  }

  // Mostrar Todos
  resetFilter(): void {
    setTimeout(() => {
      this.formFilter.reset();
      this.getPayments();
    }, 10);
    this.isFilteredFrm = false;
  }

  private _isFilteredFrm(): boolean {
    const fk = Object.keys(this.formFilter.value);

    return !fk.every(k => !this.formFilter.value[k]);
  }

  private _mapResponsePayments(response: IPaymentResponse[] = []): void {
    this.showLoading = false;
    if (!response || !response.length) {
      this.listPendingPaymentsGroupByMonth = [];

      return void 0;
    }

    this.listPaymentsOrigin = response.map(this._mapPayments.bind(this));
    this.groupByMonth(this.listPaymentsOrigin);
  }

  private _mapPayments(payment: IPaymentResponse, index: number): ICardPaymentDetailView {
    const expired = isExpired(payment.fechaExpiracion);
    const symbol = `${StringUtil.getMoneyDescription(payment.codigoMoneda)} `;
    const semaforo = TRAFFIC_LIGHT_COLOR[payment.semaforo];
    const coverageStatus = `${payment.estadoCobertura}`;
    const isWithoutCoverage = coverageStatus === this.coverageStatus.SIN_COBERTURA;
    const showQuota = coverageStatus === COVERAGE_STATUS.CON_COBERTURA;
    const isNotice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
    const isInvoice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.FACTURA;
    const billNumber = payment.numRecibo;
    const noticeNumber = payment.numeroAviso;
    const preInvoiceNumber = payment.nroPreFactura;
    const isUniqueQuota = payment.cuota === '1/1';
    const customIcon = isNotice ? noticeIcon : isInvoice ? invoiceIcon : null;
    const beneficiaries = payment.beneficiarios
      ? payment.beneficiarios.map(item => ({ fullName: item.nombreBeneficiario }))
      : [];

    const paymentlabel = isInvoice ? this.invoiceNumber : this.billNumber;
    const label1 = isNotice ? `${this.noticeNumber}: ${noticeNumber}` : `${paymentlabel}: ${billNumber}`;
    const label2 = isInvoice ? `${this.preInvoiceNumber}: ${preInvoiceNumber}` : null;
    const paymentNumberLabels = [label1, label2];

    const associatedReceipts = (payment.listaRecibosAsociados || []).length;
    const noticeLabel = `${associatedReceipts} ${associatedReceipts === 1 ? this.labelPolicy : this.labelPolicies}`;
    const invoiceLabel = `${this.contractorNumber}: ${payment.numeroPoliza}`;
    const receiptLabel = `1 ${this.labelPolicy}`;
    const paymentDetailLabel = `${isNotice ? noticeLabel : isInvoice ? invoiceLabel : receiptLabel}`;

    return {
      customIcon,
      paymentNumberLabels,
      paymentDetailLabel,
      policy: payment.descripcionPoliza,
      policyType: PolicyUtil.verifyPolicyType(payment),
      policyNumber: payment.numeroPoliza,
      billNumber,
      noticeNumber,
      policyStatusLabel: expired ? PaymentLang.Texts.ExpiredPayment : PaymentLang.Texts.CurrentPayment,
      currencySymbol: symbol,
      expired,
      isNotice,
      isInvoice,
      invoiceGloss: payment.glosa,
      policyStatus: payment.estado,
      startDate: this.datePipe.transform(payment.fechaEmision, FORMAT_DATE_DDMMYYYY),
      expiredDate: this.datePipe.transform(payment.fechaExpiracion, FORMAT_DATE_DDMMYYYY),
      month: payment.fechaExpiracion.substr(0, 7),
      quota: isUniqueQuota ? PolicyLang.Texts.Unique.toLowerCase() : payment.cuota,
      showQuota,
      deadline: this.datePipe.transform(payment.fechaExpiracion, FORMAT_DATE_DDMMYYYY),
      beneficiaries,
      totalAmount: this.currencyPipe.transform(payment.montoTotal, symbol),
      total: payment.montoTotal,
      coverage: payment.tieneCobertura,
      coverageStatus,
      isWithoutCoverage,
      debitRenovation: this.datePipe.transform(payment.fechaDebitoRenovacion, FORMAT_DATE_DDMMYYYY),
      semaforo,
      bulletColor: COLOR_STATUS[semaforo],
      associatedReceipts: payment.listaRecibosAsociados.map(this._mapAssociatedReceipts.bind(this)),
      policyData: [payment.datosPoliza],
      paymentReceiptList: this.setPaymentReceiptList(payment),
      collapse: index > 0,
      hasPendingPayments: payment.tienePagoPendiente,
      canMakeAutomaticPayment: payment.puedeHacerDebitoAutomatico,
      response: payment
    };
  }

  private _setPolicyFromNavParams(): void {
    const typeInsurance = this.activePath.snapshot.params.typeInsurance;
    const policyNumber = this.activePath.snapshot.params.policyNumber;
    if (typeInsurance) {
      const arrPolicies = this._StorageService.getStorage(TIPO_SEGURO) || [];
      const id = (arrPolicies.find(p => p.codigo.includes(typeInsurance)) || {}).codigo;
      this.formFilter.get('tipoPoliza').setValue(id);
    }
    if (policyNumber) {
      this.formFilter.get('numeroPoliza').setValue(policyNumber);
    }
  }

  _mapAssociatedReceipts(associatedReceipts: IAssociatedReceipt): IAssociatedReceipt {
    const symbol = `${StringUtil.getMoneyDescription(associatedReceipts.codigoMoneda)} `;
    const totalAmount = this.currencyPipe.transform(associatedReceipts.montoTotal, symbol);

    return { ...associatedReceipts, totalAmount };
  }

  openModalPaymentPlaces(): void {
    this.showModalPaymentPlaces = true;
    setTimeout(() => {
      this.mfModal.open();
    });
  }
}
