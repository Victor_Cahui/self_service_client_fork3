import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonModule, MfModalMessageModule } from '@mx/core/ui';
import { PaymentButtonComponent } from './payment-button.component';

@NgModule({
  imports: [CommonModule, MfButtonModule, MfModalMessageModule, GoogleModule],
  declarations: [PaymentButtonComponent],
  exports: [PaymentButtonComponent]
})
export class PaymentButtonModule {}
