import { DatePipe } from '@angular/common';
import { Component, HostBinding, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentDocumentType } from '@mx/components/shared/utils/payment';
import { POLICY_STATUS } from '@mx/components/shared/utils/policy';
import * as DateUtils from '@mx/core/shared/helpers/util/date';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PaymentLang, PaymentNotAvailableReason } from '@mx/settings/lang/payment.lang';
import { IPaymentQuoteCard, ISoatQuoteRequest } from '@mx/statemanagement/models/payment.models';
import { ICardPoliciesView } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-payment-button',
  templateUrl: './payment-button.component.html',
  providers: [DatePipe]
})
export class PaymentButtonComponent extends GaUnsubscribeBase implements OnInit, OnChanges {
  @HostBinding('class') class = 'w-10';

  @Input() item: ICardPoliciesView;
  @Input() routerLink: string;
  @Input() comesSoat: boolean;
  @Input() renewDays: number;
  @Input() disabled: boolean;

  @ViewChild(MfModalMessage) mfModalMessage: MfModalMessage;

  activateRenew = environment.ACTIVATE_RENEW_SOAT;
  activatePayment = environment.ACTIVATE_PAYMENTS;

  lang = GeneralLang;
  labelFrom = PaymentLang.Texts.SoatStatus.RenewFrom;
  paymentwithNotice = PaymentLang.Texts.WithNotice;
  isSoat: boolean;
  modalTitle: string;
  modallMessage: string;
  auth: IdDocument;
  paymentServiceSub: Subscription;
  showModal: boolean;

  expiredPolicy: boolean;
  renewal: any;
  renewButton: boolean;

  isNotice: boolean;
  showBtnPaymentSoat: boolean;
  showBtnRenewSoat: boolean;
  showLabelSoat: boolean;

  constructor(
    private readonly datePipe: DatePipe,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly paymentService: PaymentService,
    protected gaService: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    const endDate = this.item.endDate || this.item.expiredDate;
    this.expiredPolicy = this.fnIsExpiredPolicy(endDate);
    this.renewal = this.calculateStartRenew(endDate);
    this.renewButton = !this.fnHideButtonByStatus(this.renewal.isBetween);

    this.isNotice = this.item.policyStatus === POLICY_STATUS.PAYMENT_WITH_NOTICE;
    this.showBtnPaymentSoat = this.item.response.estadoPagoRenovacion === 'BTN_PAGO';
    this.showBtnRenewSoat = this.item.response.estadoPagoRenovacion === 'BTN_RENOVACION';
    this.showLabelSoat = !this.showBtnPaymentSoat && !this.showBtnRenewSoat;

    this.auth = this.authService.retrieveEntity();
    if (this.item.policyType) {
      this.isSoat = this.item.policyType.startsWith(POLICY_TYPES.MD_SOAT.code);
    }
    if (this.isSoat && this.renewDays) {
      this.formatSoat();
    }
    if (this.item.debitRenovation) {
      this.setAutomaticDebitStatus();
    }
  }

  ngOnChanges(): void {
    if (this.isSoat && this.renewDays) {
      this.formatSoat();
    }
    if (this.item.debitRenovation) {
      this.setAutomaticDebitStatus();
    }
  }

  goToPayment(): void {
    if (this.ga && this.ga.find(c => c.control === 'buttonPayPolicy')) {
      const ga = this.ga.find(c => c.control === 'buttonPayPolicy');
      this.addGAEvent(ga, this.item);
    }
    if (!this.isSoat) {
      this.paymentService.setCurrentQuote(this.item);
      this.router.navigate([this.routerLink]);
    } else {
      this.showBtnPaymentSoat ? this.paymentSoat() : this.renewSoat();
    }
  }

  paymentSoat(): void {
    const montoTotal = this.item.response.montoCuota ? this.item.response.montoCuota : this.item.response.montoTotal;
    const tipoDocumentoPago = PaymentDocumentType.RECIBO;
    const numRecibo = this.item.response.numRecibo
      ? this.item.response.numRecibo
      : this.item.response.datosPago.numRecibo;

    this.paymentService.setCurrentQuote({
      ...this.item,
      response: {
        ...this.item.response,
        montoTotal,
        tipoDocumentoPago,
        numRecibo
      }
    });

    this.router.navigate([`payments/quota`]);
  }

  renewSoat(): void {
    const params: ISoatQuoteRequest = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.item.policyNumber
    };
    this.paymentServiceSub = this.paymentService.SoatQuote(params).subscribe(
      response => {
        const quote: IPaymentQuoteCard = {
          policy: this.item.policy,
          policyNumber: this.item.policyNumber,
          policyType: this.item.policyType,
          comesSoat: this.comesSoat,
          documentType: this.auth.type,
          document: this.auth.number,
          withMPD: false, // swicth
          expiredDate: this.item.expired ? '' : this.item.endDate || this.item.expiredDate,
          dataQuote: response,
          hasPendingPayments: false,
          canMakeAutomaticPayment: false
        };
        this.paymentService.setCurrentQuote(quote);
        const availablePayment = response.datosVehiculo.soat.diponibleCompraSoat;
        if (availablePayment) {
          this.router.navigate([`vehicles/soat/renew`]);
        } else {
          this.setModalMessage(response);
          setTimeout(() => {
            this.mfModalMessage.open();
          });
        }
      },
      () => {
        // verificar error, de validacion o error 900
      },
      () => {
        this.paymentServiceSub.unsubscribe();
      }
    );
  }

  formatSoat(): void {
    setTimeout(() => {
      this.item.expired = this.expiredPolicy;
      this.item.renewButton = this.renewButton;
      this.item.renewDate = this.renewal.dateFormat;
      this.setStatus();
    });
  }

  fnIsExpiredPolicy(date: string): boolean {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    const endDate = DateUtils.stringDDMMYYYYToDate(date);

    return today > endDate;
  }

  calculateStartRenew(date: string): { isBetween: boolean; dateFormat: string } {
    const endDate = DateUtils.stringDDMMYYYYToDate(date);
    endDate.setHours(0, 0, 0, 0);
    const beforeToExpired = DateUtils.diffDays(endDate, Number(this.renewDays));
    const today = new Date();
    const isBetween = DateUtils.betweenDate(beforeToExpired, endDate, today);
    const dateFormat = this.datePipe.transform(beforeToExpired, FORMAT_DATE_DDMMYYYY);

    return { isBetween, dateFormat };
  }

  fnHideButtonByStatus(isBetween: boolean): boolean {
    const hideButton =
      this.item.policyStatus === POLICY_STATUS.PENDING_EMIT ||
      this.item.policyStatus === POLICY_STATUS.EARLY_RENEWAL ||
      this.item.policyStatus === POLICY_STATUS.NEW_POLICY ||
      (this.item.policyStatus === POLICY_STATUS.ACTIVE && !isBetween);

    return hideButton;
  }

  setStatus(): void {
    const soatStatus = this.item.policyStatus;
    if (soatStatus === POLICY_STATUS.PENDING_EMIT) {
      this.labelFrom = PaymentLang.Texts.SoatStatus.ProcessingRenovation.text1;
      this.item.renewDate = PaymentLang.Texts.SoatStatus.ProcessingRenovation.text2;
      this.item.renewButton = false;
    }

    if (soatStatus === POLICY_STATUS.EARLY_RENEWAL) {
      this.labelFrom = PaymentLang.Texts.SoatStatus.SoatRenewed.text1;
      this.item.renewDate = PaymentLang.Texts.SoatStatus.SoatRenewed.text2;
    }

    if (soatStatus === POLICY_STATUS.NEW_POLICY) {
      this.labelFrom = PaymentLang.Texts.SoatStatus.VigencyStarts;
      this.item.renewDate = this.item.startDate;
    }
  }

  setAutomaticDebitStatus(): void {
    const soatStatus = this.item.policyStatus;
    if (soatStatus === POLICY_STATUS.AUTOMATIC_DEBIT) {
      this.labelFrom = PaymentLang.Texts.SoatStatus.AutomaticDebit;
      this.item.renewDate = this.item.debitRenovation;
    }
  }

  setModalMessage(response: any): void {
    this.showModal = true;
    const keyPaymentNotAvaibale = response.datosVehiculo.soat.motivoNoDisponibleCompra;
    this.modalTitle = this.lang.Messages.EstimatedUser;
    if (keyPaymentNotAvaibale === PaymentNotAvailableReason.apeseg.key) {
      this.modallMessage = `${PaymentNotAvailableReason.apeseg.reason}<br>${this.datePipe.transform(
        response.datosVehiculo.soat.fechaFinVigencia,
        FORMAT_DATE_DDMMYYYY
      )}`;
    } else {
      this.modallMessage = PaymentNotAvailableReason.default.reason;
    }
  }
}
