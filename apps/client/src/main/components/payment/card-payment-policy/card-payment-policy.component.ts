import { CurrencyPipe } from '@angular/common';
import { Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { gaEventDecorator, GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { environment } from '@mx/environments/environment';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PAYMENT_TYPE, PORCENTAGE_ICON, WALLET_ICON } from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PAYMENT_SETTINGS_RENEWAL, PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IPaymentTypeResponse } from '@mx/statemanagement/models/payment.models';
import { IPaymentPolicy, IPolicyList } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-card-payment-policy',
  templateUrl: './card-payment-policy.component.html',
  providers: [CurrencyPipe]
})
export class CardPaymentPolicyComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @HostBinding('attr.class') attr_class = 'w-10';
  @Input() info?: IPaymentPolicy;
  @Input() enableEdit: boolean;
  @Output() edit?: EventEmitter<IPaymentPolicy>;
  @Output() paymentView?: EventEmitter<IPaymentPolicy>;

  iconCard = WALLET_ICON;
  porcentageIcon = PORCENTAGE_ICON;
  isSctr = false;
  collapse = false;
  titleCard = PaymentLang.Titles.CardPayment;
  btnViewPayment = PaymentLang.Buttons.ViewPayment;
  hasAmountMax: boolean;
  viewPayment: boolean;
  showRenewal: boolean;
  hideInsuranceCost: boolean;
  frequencyOrQuota: string;
  iconPaymentType: string;
  renewalType: string;
  renewalIcon: string;
  primaLabel: string;
  paymentsTypes: Array<IPaymentTypeResponse>;

  paymentPolicySub: Subscription;
  paymentTypeSub: Subscription;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly paymentService: PaymentService,
    private readonly currencyPipe: CurrencyPipe,
    protected activatedRoute: ActivatedRoute,
    private readonly policyListService: PolicyListService,
    protected gaService: GAService
  ) {
    super(gaService);
    this.edit = new EventEmitter<IPaymentPolicy>();
    this.paymentView = new EventEmitter<IPaymentPolicy>();
  }

  ngOnInit(): void {
    this.viewPayment = environment.ACTIVATE_PAYMENTS;
    this.enableEdit = environment.ACTIVATE_CONFIGURATION_PAYMENT;
    this.paymentPolicySub = this.policiesInfoService.getPaymentPolicy().subscribe((result: IPaymentPolicy) => {
      this.info = result;
      this.hasAmountMax = this.info && this.info.amountMax > -1;
      this.isSctr = this.info && this.info.policyType && PolicyUtil.isSctr(this.info.policyType);
      this.frequencyOrQuota = this.isSctr ? this.getFrequency() : this.getQuotaNumber();
      this.showRenewal = !this.isSctr && (this.info && this.info.renewalTypeId !== 0);
      this.getPaymentTypeIcon();
    });

    // Actualizar 'Tipo de renovación' cuando se redirecciona de 'Configuración de pagos'
    const policyList: IPolicyList = this.policyListService.getPolicyListLocal();
    if (policyList && policyList.list && policyList.list.length) {
      const policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
      const policy = policyList.list.find(p => p.policyNumber === policyNumber);
      if (policy) {
        this.info.renewalTypeId = policy.response.tipoRenovacionId || 0;
        this.info.renewalType = policy.response.tipoRenovacionDescripcion || '';
        this.primaLabel = policy.fractionationDesc;
      }
    }
    if (this.isSctr) {
      this.primaLabel = this.info.fractionationDesc;
    }

    this.setRenewalType();
  }

  // Monto Prima
  insuranceCost(): string {
    const code = `${(this.info && this.info.moneyCode) || ''} `;
    const cost = (this.info && this.currencyPipe.transform(this.info.insuranceCost, code)) || '';
    const insuranceCost = `${this.primaLabel || PolicyLang.Texts.Quote}: ${cost}`;
    const policyPaid = !this.info.hasPendingPayment;

    this.hideInsuranceCost =
      policyPaid && [POLICY_TYPES.MD_VIDA.code, POLICY_TYPES.MD_DECESOS.code].includes(this.info.policyType);

    return insuranceCost;
  }

  // Monto Topado
  amountMax(): string {
    const symbol = `${(this.info && this.info.moneyCode) || ''} `;
    const amount = (this.info && this.info.amountMax && this.currencyPipe.transform(this.info.amountMax, symbol)) || '';

    return `${PolicyLang.Messages.AmountMax} ${amount}`;
  }

  // Cantidad de Cuotas / Monto Planilla
  quota(): string {
    const code = `${(this.info && this.info.moneyCode) || ''} `;
    const cost = (this.info && this.currencyPipe.transform(this.info.amountDue, code)) || '';

    if (this.isSctr) {
      return `${PolicyLang.Texts.Amount}: ${cost}`;
    }
    const quotas = (this.info && this.info.quotas) || 0;
    const quotaNumber = (this.info && this.info.quotaNumber) || 0;
    if (this.info && !this.info.hasPendingPayment) {
      return PolicyLang.Texts.PolicyPaid;
    }

    return `${PolicyLang.Texts.Fee} ${quotaNumber}/${quotas}: ${cost}`;
  }

  getFrequency(): string {
    return this.info && this.info.frequencyStatement
      ? ` ${PolicyLang.Texts.Frequency}: ${this.info.frequencyStatement}`
      : '';
  }

  // Icono según tipo de pago
  getPaymentTypeIcon(): void {
    // Tipo de Pago
    const params = { codigoApp: APPLICATION_CODE };
    this.iconPaymentType = PAYMENT_TYPE.DEFAULT.icon;
    if (this.info && this.info.paymentTypeId) {
      this.paymentTypeSub = this.paymentService.paymentType(params).subscribe(
        (response: Array<IPaymentTypeResponse>) => {
          this.paymentsTypes = response;
          const key = response.find(type => type.tiposPagosId === this.info.paymentTypeId);
          if (key) {
            this.iconPaymentType = PolicyUtil.getPaymentTypeIcon(key.tiposPagosLlave || '');
          }
        },
        () => {},
        () => {
          this.paymentService.setPaymentTypes(this.paymentsTypes);
          this.paymentTypeSub.unsubscribe();
        }
      );
    }
  }

  // Tipo de pago
  paymentType(): string {
    return `${GeneralLang.Labels.PaymentType}: ${(this.info && this.info.paymentType) ||
      PaymentLang.Texts.PaymentsDefault}`;
  }

  // Tipo de renovación
  setRenewalType(): void {
    this.renewalType = !this.showRenewal
      ? ''
      : `${PolicyLang.Texts.Renew} ${this.info.renewalType.toLowerCase() || ''}`;
    this.renewalIcon = !this.showRenewal
      ? ''
      : this.info.renewalTypeId === 1
      ? PAYMENT_SETTINGS_RENEWAL.options.automatic.icon
      : PAYMENT_SETTINGS_RENEWAL.options.manual.icon;
  }

  getQuotaNumber(): string {
    return this.info && this.info.quotas
      ? this.info.quotas === 1
        ? PolicyLang.Texts.WithOutFees
        : `${this.info.quotas} ${PolicyLang.Texts.Fees}`
      : '';
  }

  editClick(): void {
    this.edit && this.edit.next();
  }

  @gaEventDecorator()
  paymentViewClick(): void {
    this.paymentView && this.paymentView.next(this.activatedRoute.snapshot.params['policyNumber']);
  }

  ngOnDestroy(): void {
    this.paymentPolicySub && this.paymentPolicySub.unsubscribe();
    this.paymentView && this.paymentView.unsubscribe();
  }
}
