import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { TabsPaymentsComponent } from './tabs-payments.component';

@NgModule({
  imports: [CommonModule, MfTabTopModule],
  declarations: [TabsPaymentsComponent],
  exports: [TabsPaymentsComponent]
})
export class TabsPaymentsModule {}
