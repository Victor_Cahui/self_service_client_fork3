import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ComparePolicyComponent } from '@mx/components/shared/compare-policy/compare-policy.component';
import { COVERAGE_STATUS, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { hasWarning } from '@mx/core/shared/helpers/util/date';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import {
  APPLICATION_CODE,
  COLOR_STATUS,
  FORMAT_DATE_DDMMYYYY,
  PPFM_ICON,
  REG_EX,
  SEARCH_ORDEN_DESC
} from '@mx/settings/constants/general-values';
import { ESTADO_POLIZA } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import {
  ICardPoliciesView,
  IPoliciesByClientRequest,
  IPoliciesByClientResponse,
  IPolicyList
} from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { takeUntil } from 'rxjs/operators';
import { BaseCardMyPolicies } from './../base-card-my-policies';

@Component({
  selector: 'client-card-my-policies-by-type',
  templateUrl: './card-my-policies-by-type.component.html',
  providers: [DatePipe, CurrencyPipe]
})
export class CardMyPoliciesByTypeComponent extends BaseCardMyPolicies implements OnInit {
  @ViewChild(ComparePolicyComponent) mfModalSelect: ComparePolicyComponent;
  @Input() policyType: string;

  showComparate: boolean;
  listPolicies: Array<ICardPoliciesView> = [];
  showLoading: boolean;
  messageWithoutPolicies = PolicyLang.Messages.WithoutPolicies;
  btnComparePolicy = PolicyLang.Buttons.ComparePolicy;
  btnQuotePolicy = PolicyLang.Buttons.QuotePolicyNew;
  colorStatus = COLOR_STATUS;
  coverageStatus = COVERAGE_STATUS;
  lang = GeneralLang;
  auth: IdDocument;
  shouldOpenModalSelect: boolean;
  validPolicies: boolean;
  codRamoClinicaDigital = 275;
  detailPolicyCldKey = environment.ACTIVATE_DETAIL_POLICY_CLD;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService,
    private readonly datePipe: DatePipe,
    private readonly currencyPipe: CurrencyPipe,
    public router: Router,
    private readonly authService: AuthService,
    private readonly policyListService: PolicyListService,
    protected gaService: GAService
  ) {
    super(generalService, policiesService, gaService);
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    // Lista de Pólizas
    this.listPolicies = this.policyListService.getDataPolicies();
    this.getParams();
    this.getPolicyList();
  }

  goToQuote(): void {
    const routerLinkQuote = this.getLink(this.policyType, 'quote');
    if (REG_EX.url.test(routerLinkQuote)) {
      window.open(routerLinkQuote, '_blank');

      return void 0;
    }

    this.router.navigate([routerLinkQuote]);
  }

  fnParamsSearchByClient(): IPoliciesByClientRequest {
    const params: IPoliciesByClientRequest = {
      codigoApp: APPLICATION_CODE,
      criterio: 'fechaInicio',
      estadoPoliza: ESTADO_POLIZA.vigente.code,
      orden: SEARCH_ORDEN_DESC,
      tipoPoliza: this.policyType
    };

    return params;
  }

  fnShowComparate(): boolean {
    return this.policyType === POLICY_TYPES.MD_AUTOS.code && environment.ACTIVATE_COMPARE_POLICY && this.validPolicies;
  }

  // Lista de Pòlizas
  getPolicyList(): void {
    const params = this.fnParamsSearchByClient();

    this.listPolicies = [];
    this.showLoading = true;
    this.policiesService
      .searchByClientService(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        (response: Array<IPoliciesByClientResponse>) => {
          this.listPolicies = response.map((policy: IPoliciesByClientResponse) => {
            const expiredPolicy = this.fnIsExpiredPolicy(policy.fechaFin);
            const warningPolicy = hasWarning(policy.fechaFin, this.renewalDays);
            const symbol = `${StringUtil.getMoneyDescription(policy.codigoMoneda)} `;
            const semaforo = TRAFFIC_LIGHT_COLOR[policy.semaforo];
            const change = !policy.comparacionPoliza
              ? true
              : !(policy.comparacionPoliza && !policy.comparacionPoliza.puedeCambiarModalidad);
            const isEPSContractor = policy.tipoPoliza === POLICY_TYPES.MD_EPS.code && policy.esContratante;
            const affiliates = isEPSContractor && this.getAffiliatesDescription(policy);
            const isPPFM = policy.esBeneficioAdicional;
            const customIcon = isPPFM && PPFM_ICON[policy.tipoPoliza];

            return {
              amountFee: this.currencyPipe.transform(policy.montoCuota, symbol),
              beneficiaries: policy.beneficiarios,
              coverageStatus: policy.estadoCobertura || '',
              endDate: this.datePipe.transform(policy.fechaFin, FORMAT_DATE_DDMMYYYY),
              expired: expiredPolicy,
              hiddenViewDetail: this.fnHideViewDetail(policy.tipoPoliza),
              insuranceCost: this.currencyPipe.transform(policy.prima, symbol),
              planEps: policy.planEps,
              policy: policy.descripcionPoliza,
              policyNumber: policy.numeroPoliza,
              policyType: PolicyUtil.verifyPolicyType(policy),
              quotaNumber: policy.numeroCuota,
              quotas: policy.cuotas,
              isVisibleCoverage: this.isVisibleCoverage(policy),
              response: policy,
              semaforo,
              showIconPdf: this.fnShowIconPdf(policy.tipoPoliza),
              isVitalicia: this.isVitalicia(policy),
              warning: warningPolicy,
              isContractor: policy.esContratante,
              contractorName: policy.nombreContratante,
              isEPSContractor,
              affiliates,
              isVIP: policy.esPolizaVip,
              fractionationDesc: policy.descripcionFraccionamiento,
              bulletColor: COLOR_STATUS[semaforo],
              isPPFM,
              ...(customIcon && { customIcon }),
              compare: {
                modalityInProgress: policy.comparacionPoliza
                  ? policy.comparacionPoliza.cambioModalidadEnProceso
                  : false,
                changeModality: change,
                nextChangeFrom: change
                  ? ''
                  : this.datePipe.transform(
                      policy.comparacionPoliza.fechaSiguienteCambioModalidad,
                      FORMAT_DATE_DDMMYYYY
                    ) || ''
              }
            } as ICardPoliciesView;
          });

          if (this.listPolicies && this.listPolicies.length) {
            this.setShowComparate();
            this.setDataLocal();
          }
          this.showLoading = false;
        },
        () => (this.showLoading = false)
      );
  }

  // Verifica si hay polizas vigentes
  setShowComparate(): void {
    const validPolicyList = this.listPolicies.filter(policy => !policy.expired);
    this.validPolicies = validPolicyList && validPolicyList.length > 0;
    this.showComparate = this.fnShowComparate();
    this.policyListService.setValidPolicies(this.validPolicies);
  }

  // Ir a detalle
  goToDetail(policy: ICardPoliciesView): void {
    const type = this.policyType.split('|')[0];
    const path = `${POLICY_TYPES[type].path}/${policy.policyNumber}`;
    this.policiesInfoService.setClientPolicyResponse(policy.response);
    this.router.navigate([path]);
  }

  // Comparar pólizas
  goToCompare(): void {
    this.shouldOpenModalSelect = true;
    setTimeout(() => {
      this.mfModalSelect.open();
    }, 100);
  }

  // Guardar en Local
  setDataLocal(): void {
    const data: IPolicyList = this.policyListService.newPolicyList();
    data.policyType = this.policyType;
    data.typeDocument = this.auth.type;
    data.document = this.auth.number;
    data.list = this.listPolicies;
    data.policySelected = this.policySelected;
    this.policyListService.setPolicyListData(data);
  }
}
