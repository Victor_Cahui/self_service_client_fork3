import { COVERAGE_STATUS } from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { BUTTONS_LINK, DEFAULT_EARLY_POLICY_RENEWAL_DAYS, RAMOS } from '@mx/settings/constants/general-values';
import { EARLY_POLICY_RENEWAL_DAYS, ESTADO_POLIZA } from '@mx/settings/constants/key-values';
import {
  ACCIDENTES_PERSONALES,
  NECESIDAD_FUTURA,
  NECESIDAD_INMEDIATA,
  POLICY_TYPES as PT
} from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ICardPoliciesView } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export abstract class BaseCardMyPolicies extends GaUnsubscribeBase {
  data = PolicyUtil.setDataCardMyPolicy();
  fileUtil: FileUtil;
  renewalDays: number;
  titleAlertError: string;
  messageAlertError: string;
  showSpinnerPolicy: boolean;
  policySelected: string;
  policeSub: Subscription;
  parametersSub: Subscription;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.fileUtil = new FileUtil();
    this.titleAlertError = GeneralLang.Alert.errorDownloadPDF.error.title;
    this.messageAlertError = GeneralLang.Alert.errorDownloadPDF.error.message;
  }

  getParams(): void {
    this.parametersSub = this.generalService
      .getParametersSubject()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        () =>
          (this.renewalDays =
            +this.generalService.getValueParams(EARLY_POLICY_RENEWAL_DAYS) || DEFAULT_EARLY_POLICY_RENEWAL_DAYS)
      );
  }

  isEPS(item: ICardPoliciesView): boolean {
    return item.policyType === PT.MD_EPS.code;
  }

  fnIsExpiredPolicy(endDate): boolean {
    const vToday = new Date();
    const vEndDate = new Date();
    const vSplitEndDate = endDate.split('-');

    vEndDate.setFullYear(vSplitEndDate[0], vSplitEndDate[1] - 1, vSplitEndDate[2]);

    return vToday > vEndDate;
  }

  fnShowIconPdf(policyType): boolean {
    return policyType === PT.MD_SOAT_ELECTRO.code;
  }

  fnHideCol2EPS(policyType): boolean {
    return policyType === PT.MD_EPS.code;
  }

  fnHideViewDetail(policy): boolean {
    return (
      [PT.MD_SOAT.code, PT.MD_SOAT_ELECTRO.code, PT.MD_OTROS.code].includes(policy.tipoPoliza) ||
      policy.codRamo === ACCIDENTES_PERSONALES.COD_RAMO ||
      [NECESIDAD_FUTURA.COD_RAMO, NECESIDAD_INMEDIATA.COD_RAMO].includes(policy.codRamo) ||
      (policy.tipoPoliza === PT.MD_EPS.code && policy.estadoPoliza === ESTADO_POLIZA.expirada.txt)
    );
  }

  seePolicePdf(policyNumber: string): void {
    if (this.showSpinnerPolicy) {
      return;
    }
    this.showSpinnerPolicy = true;
    this.policySelected = policyNumber;
    this.policeSub = this.policiesService.seePolicePdfById(policyNumber).subscribe(
      (res: any) => {
        this.fileUtil.download(res.base64, 'pdf', `Poliza N°: ${policyNumber}`);
      },
      () => {
        this.showSpinnerPolicy = false;
      },
      () => {
        this.showSpinnerPolicy = false;
        this.policeSub.unsubscribe();
      }
    );
  }

  // Obtener link segun tipo de poliza y opcion
  getLink(policyType, option): string {
    if (policyType) {
      // tslint:disable-next-line: no-parameter-reassignment
      policyType = policyType.split('|')[0];
      const links = BUTTONS_LINK.find(link => link.key === option);
      if (links) {
        const index = links.policyTypes.findIndex(type => type === policyType);

        return links.routeByType[index];
      }
    }

    return '';
  }

  trackByFn(index, item): any {
    return index;
  }

  isVitalicia(policy): boolean {
    return [PT.MD_VIDA.code].includes(policy.tipoPoliza) && policy.codRamo === RAMOS.VIDA_ENTERA && !policy.fechaFin;
  }

  isSoat(tipoPoliza: string): boolean {
    return [PT.MD_SOAT.code, PT.MD_SOAT_ELECTRO.code].includes(tipoPoliza);
  }

  isCurrentPolicy(policy): boolean {
    return this.isEPS(policy) ? true : policy.estadoPoliza === ESTADO_POLIZA.vigente.txt;
  }

  isExpiredEps(policy): boolean {
    return this.isEPS(policy) && policy.estadoPoliza === ESTADO_POLIZA.expirada.txt;
  }

  isVisibleCoverage(policy): boolean {
    return !(policy.estadoCobertura === COVERAGE_STATUS.CON_COBERTURA);
  }

  getAffiliatesDescription(policy): string {
    return `${policy.totalAfiliados} ${
      policy.totalAfiliados === 1
        ? GeneralLang.Labels.Affiliate.toLowerCase()
        : GeneralLang.Labels.Affiliates.toLowerCase()
    }`;
  }
}
