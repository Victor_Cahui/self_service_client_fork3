import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentButtonModule } from '@mx/components/payment/payment-button/payment-button.module';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardMyPoliciesSoatComponent } from './card-my-policies-soat.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    DropdownItemsPolicyModule,
    GoogleModule,
    IconPoliciesModule,
    LinksModule,
    MfCardModule,
    MfLoaderModule,
    PaymentButtonModule
  ],
  declarations: [CardMyPoliciesSoatComponent],
  exports: [CardMyPoliciesSoatComponent]
})
export class CardMyPoliciesSoatModule {}
