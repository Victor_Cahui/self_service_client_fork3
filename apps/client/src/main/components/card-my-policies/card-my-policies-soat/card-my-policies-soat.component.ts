import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { POLICY_STATUS, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE, COLOR_STATUS, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { ESTADO_POLIZA, SOAT_ANTIC_RENOVAR_DIAS } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import {
  ICardPoliciesView,
  IPoliciesByClientRequest,
  IPoliciesByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Observable, Subscription } from 'rxjs';
import { BaseCardMyPolicies } from './../base-card-my-policies';

@Component({
  selector: 'client-card-my-policies-soat',
  templateUrl: './card-my-policies-soat.component.html',
  providers: [DatePipe, CurrencyPipe]
})
export class CardMyPoliciesSoatComponent extends BaseCardMyPolicies implements OnInit {
  listPolicies: Array<ICardPoliciesView>;
  slicePolicies: number;
  linkPaymentsSetting = PaymentLang.Buttons.PaymentsSetting;
  messageWithoutPolicies = PolicyLang.Messages.WithoutPolicies;
  showLoading: boolean;
  policyType = POLICY_TYPES.MD_SOAT.code;
  renewDays: number;
  auth: IdDocument;
  subZip: Subscription;
  paymentServiceSub: Subscription;
  showPaymentSetting = environment.ACTIVATE_PAYMENTS_SOAT;
  parametersSubject: Observable<Array<IParameterApp>>;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    private readonly authService: AuthService,
    private readonly datePipe: DatePipe,
    private readonly paymentService: PaymentService,
    public currencyPipe: CurrencyPipe,
    protected gaService: GAService
  ) {
    super(generalService, policiesService, gaService);
    this.data.titleMyPolicies = PolicyLang.Titles.MySoat;
  }

  fnParamsSearchByClient(): IPoliciesByClientRequest {
    this.auth = this.authService.retrieveEntity();
    const params: IPoliciesByClientRequest = {
      activarSoatVencidos: true,
      codigoApp: APPLICATION_CODE,
      estadoPoliza: ESTADO_POLIZA.vigente.code,
      tipoPoliza: `${POLICY_TYPES.MD_SOAT.code}|${POLICY_TYPES.MD_SOAT_ELECTRO.code}`
    };

    return params;
  }

  ngOnInit(): void {
    this.paymentService.clearCurrentQuote();
    const params = this.fnParamsSearchByClient();
    this.showLoading = true;
    this.getParameters();
    this.subZip = this.policiesService.searchByClientService(params).subscribe(
      results => {
        this.formatCards(results || []);
      },
      () => {
        this.listPolicies = [];
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
        this.subZip.unsubscribe();
      }
    );
  }

  // Parametros Generales
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(() => {
      this.renewDays = Number(this.generalService.getValueParams(SOAT_ANTIC_RENOVAR_DIAS)) || 0;
    });
  }

  formatCards(policies: Array<IPoliciesByClientResponse>): void {
    this.listPolicies = policies.map((policy: IPoliciesByClientResponse) => {
      const semaforo = TRAFFIC_LIGHT_COLOR[policy.semaforo];
      const symbol = `${StringUtil.getMoneyDescription(policy.codigoMoneda)} `;
      const billNumber = policy.datosPago ? policy.datosPago.numRecibo : '';
      const quota = policy.datosPago ? policy.datosPago.cuota : policy.cuotas;

      return {
        policyType: policy.tipoPoliza,
        policyNumber: policy.numeroPoliza,
        beneficiaries: policy.beneficiarios,
        billNumber,
        policy: policy.descripcionPoliza,
        isNew: policy.estado === POLICY_STATUS.NEW_POLICY,
        policyStatus: policy.estado,
        endDate: this.datePipe.transform(policy.fechaFin, FORMAT_DATE_DDMMYYYY),
        quota,
        startDate: this.datePipe.transform(policy.fechaInicio, FORMAT_DATE_DDMMYYYY),
        showIconPdf: this.fnShowIconPdf(policy.tipoPoliza),
        hiddenViewDetail: this.fnHideViewDetail(policy.tipoPoliza),
        semaforo,
        bulletColor: COLOR_STATUS[semaforo],
        totalAmount: this.currencyPipe.transform(policy.montoCuota, symbol),
        response: policy
      } as ICardPoliciesView;
    });

    this.showLoading = false;
  }
}
