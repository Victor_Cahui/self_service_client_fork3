import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentButtonModule } from '@mx/components/payment/payment-button/payment-button.module';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import {
  DirectivesModule,
  MfCardModule,
  MfCheckboxModule,
  MfInputModule,
  MfModalAlertModule,
  MfPaginatorModule
} from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { InputSearchModule } from '../shared/input-search/input-search.module';
import { CardMyGuaranteeLettersComponent } from './card-my-guarantee-letters.component';
import { GuaranteeDownloadOptionModule } from './donwload-option/donwload-option.module';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    DropdownItemsPolicyModule,
    FormsModule,
    GoogleModule,
    IconPoliciesModule,
    ItemNotFoundModule,
    LinksModule,
    MfButtonModule,
    MfCardModule,
    MfCheckboxModule,
    MfInputModule,
    MfLoaderModule,
    MfPaginatorModule,
    MfSelectModule,
    PaymentButtonModule,
    ReactiveFormsModule,
    InputSearchModule,
    TooltipsModule,
    MfModalAlertModule,
    GuaranteeDownloadOptionModule
  ],
  declarations: [CardMyGuaranteeLettersComponent],
  exports: [CardMyGuaranteeLettersComponent]
})
export class CardMyGuaranteeLettersModule {}
