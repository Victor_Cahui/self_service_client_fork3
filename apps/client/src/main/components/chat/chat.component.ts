import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Comun } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { Subscription, zip } from 'rxjs';
import { BaseComponent } from '../shared/base-component';

declare let _genesys;
declare let CXBus;

@Component({
  selector: 'client-web-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class WebChatComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  zipSub: Subscription;
  customPlugin = CXBus.registerPlugin('Custom');
  oMyPlugin = _genesys.widgets.bus.registerPlugin('MiPlugin');
  dataUser;
  userDataSub: Subscription;
  showChat;
  showChatPermission: boolean;
  showChatObservable = true;
  hideBox = false;

  constructor(
    protected authService: AuthService,
    protected _Comun: Comun,
    private readonly clientService: ClientService,
    private readonly generalService: GeneralService
  ) {
    super();
  }

  ngOnInit(): void {
    this.showChat = false;

    const permissions$ = this._Comun.ObtenerFunciones({
      codigoApp: APPLICATION_CODE
    });

    this.zipSub = zip(permissions$).subscribe(
      res => {
        const data = res[0].filter(element => element.codigo === 'ACTIVATE_CHAT');
        if (data[0].valor) {
          this.showChatPermission = data[0].valor;
          this.showChat = this.showChatPermission && this.showChatObservable;
          this.getInfoUSer();
        }
      },
      () => {},
      () => {
        this.zipSub.unsubscribe();
      }
    );

    const showChatGenesys$ = this.generalService.showChatGenesys.subscribe(
      val => {
        this.showChatObservable = val;
        this.showChat = this.showChatPermission && this.showChatObservable;
      },
      error => {
        console.error(error);
      }
    );

    this.customPlugin.subscribe('WebChatService.started', () => {
      {
        setTimeout(() => {
          this.resetChatBox();
        }, 200);
      }
    });

    this.customPlugin.subscribe('WebChatService.restored', () => {
      setTimeout(() => {
        this.resetChatBox();
      }, 1000);
    });

    this.customPlugin.subscribe('WebChatService.agentTypingStarted', () => {
      document.getElementsByClassName('cx-textarea-cell')[0].className = `cx-textarea-cell-active`;
      document.getElementsByClassName(
        'cx-input-container'
      )[0].className = `cx-input-container cx-input-container-active`;
    });

    this.customPlugin.subscribe('WebChatService.agentTypingStopped', () => {
      const inputContainer = document.getElementsByClassName('cx-input-container-active')[0];
      const textAreaCellActive = document.getElementsByClassName('cx-textarea-cell-active')[0];

      if (inputContainer) {
        inputContainer.className = `cx-input-container`;
      }
      if (textAreaCellActive) {
        textAreaCellActive.className = `cx-textarea-cell`;
      }
    });

    this.customPlugin.subscribe('WebChat.cancelled', () => {
      this.hideBox = false;
    });

    this.customPlugin.subscribe('WebChat.messageAdded', (e: any) => {
      this.addCircleUser(e);
      this.removeItemTitle(e.data.id);
    });
    this.arrToDestroy.push(showChatGenesys$);
  }

  ngOnDestroyd(): void {}

  ngAfterViewInit(): void {
    _genesys = {
      widgets: {
        webchat: {
          transport: {
            type: 'purecloud-v2-sockets',
            dataURL: 'https://api.mypurecloud.com',
            deploymentKey: '4e985cbb-8b0a-4aab-b925-ed7952e853e3',
            orgGuid: '913b00fc-3815-434c-aff6-c0adebbe94cc',
            interactionData: {
              routing: {
                targetType: 'QUEUE',
                targetAddress: 'Inbound Prueba',
                priority: 2
              }
            }
          },
          userData: {
            phoneNumber: '',
            firstname: '',
            customField1Label: 'DNI o RUC',
            customField1: '',
            customField2Label: 'Razón Social',
            customField2: ''
          }
        }
      }
    };
  }

  callChat(): void {
    this.hideBox = !this.hideBox;
    this.customPlugin.command('WebChat.open', this.getAdvancedConfig());
  }

  removeItemTitle(item): void {
    if (item) {
      document.querySelectorAll(`#${item} .cx-name`)[0].remove();
      document.querySelectorAll(`#${item} .cx-bubble-arrow`)[0].remove();
    }
  }

  addCircleUser(data): void {
    if (data.data.data.type === 'Client') {
      const avatar = `${this.dataUser.nombreCliente[0] || ''}${this.dataUser.apellidoPaternoCliente[0] || ''}`;

      if (document.querySelectorAll(`.cx-participant#${data.data.id} .cx-bubble`).length) {
        document
          .querySelectorAll(`.cx-participant#${data.data.id} .cx-bubble`)[0]
          .insertAdjacentHTML(
            'afterend',
            `<div class="circle-inbox g-circle--white g-box--shadow2 d-flex align-items-center justify-content-center mx-auto mx-lg-0"><span class="g-font--17 g-font--bold">${avatar}</span></div>`
          );
      }
    } else {
      document.getElementById(data.data.id).className = `cx-message cx-system i18n`;
    }
  }
  getInfoUSer(): void {
    this.userDataSub = this.clientService.getUserDataSubject().subscribe(data => {
      this.dataUser = data;
    });
  }

  resetChatBox(): void {
    const contentDiv = document.createElement(`div`);
    const contentDiv2 = document.createElement(`div`);
    const nameClient =
      this.dataUser.nombreCliente
        .split(' ')[0]
        .charAt(0)
        .toUpperCase() +
      this.dataUser.nombreCliente
        .split(' ')[0]
        .slice(1)
        .toLowerCase();
    const avatar = `${this.dataUser.nombreCliente[0] || ''}${this.dataUser.apellidoPaternoCliente[0] || ''}`;
    document.querySelectorAll(`.cx-date`)[0].innerHTML = '';
    document.querySelectorAll(`
    .cx-date`)[0].innerHTML = `<div class="circle g-circle--white g-box--shadow2 d-flex align-items-center justify-content-center"><span class="g-font--20 g-font--bold">${avatar}</span></div><div class="text"><span>Hola,</span><h2>${this.dataUser.nombreCliente}</h2><p>${this.dataUser.emailCliente}</p></div>`;
    document.querySelectorAll(`.cx-message-group`)[0].appendChild(contentDiv2);
    document.querySelectorAll(`.cx-message-text`)[0].innerHTML = '';
    document.querySelectorAll(`
    .cx-message-text`)[0].innerHTML = `<p>Buenas tardes, en que podemos ayudarlo </br> señor ${nameClient}</p>`;
    document.querySelectorAll(`.cx-date`)[0].appendChild(contentDiv);

    const frameZones = Array.from(document.querySelectorAll('.cx-participant'));
    frameZones.forEach((e: any) => {
      e.appendChild(document.createElement(`div`));
    });
  }

  getAdvancedConfig(): any {
    return {
      form: {
        autoSubmit: true,
        firstname: this.dataUser.nombreCliente,
        lastname: '',
        email: ''
      },
      formJSON: {
        wrapper: '<table></table>',
        inputs: [
          {
            id: 'cx_webchat_form_firstname',
            name: 'firstname',
            maxlength: '100',
            placeholder: 'Obligatorio',
            label: 'Nombre',
            value: this.dataUser.nombreCliente,
            disabled: true
          },
          {
            id: 'cx_webchat_form_customField1',
            name: 'customField1',
            maxlength: '100',
            placeholder: 'Obligatorio',
            label: 'DNI o RUC',
            value: this.dataUser.documento,
            disabled: true
          },
          {
            id: 'cx_webchat_form_customField2',
            name: 'customField2',
            maxlength: '100',
            placeholder: 'Opcional',
            label: 'Email',
            value: this.dataUser.emailCliente,
            disabled: true
          },
          {
            id: 'cx_webchat_form_phonenumber',
            name: 'phoneNumber',
            maxlength: '100',
            placeholder: 'Obligatorio',
            label: 'Teléfono',
            value: this.dataUser.telefonoMovil,
            disabled: true
          }
        ]
      }
    };
  }
  ngOnDestroy(): void {
    this.customPlugin
      .command('WebChatService.endChat')
      .done(() => {})
      .fail(() => {});
  }
}
