import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WebChatComponent } from './chat.component';

@NgModule({
  imports: [CommonModule],
  declarations: [WebChatComponent],
  exports: [WebChatComponent]
})
export class WebChatModule {}
