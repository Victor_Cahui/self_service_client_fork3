import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import {
  IFileTypeRefund,
  IRefundHealth,
  IRefundImageStorage,
  IUploadFileRefundFormData
} from '@mx/statemanagement/models/health.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';
import { IUploadFileList } from '../shared/upload-files/upload-files.component';

export class HealthRefunUploadBase {
  typeFile = IFileTypeRefund;

  // INFO: collección de cargas, para poder cancelar la subida
  uploadsFileSub: Array<{ tmpKey: number; subs: Subscription }> = [];
  fileUtil: FileUtil = new FileUtil();

  auth: IdDocument;
  refund: IRefundHealth;

  removeFileSub: Subscription;

  constructor(protected healthRefundService: HealthRefundService, protected authService: AuthService) {
    this.auth = this.authService.retrieveEntity();
    this.refund = this.healthRefundService.getRefund();
  }

  addFile(data: IUploadFileList, typeFile: IFileTypeRefund, provider?: number): void {
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    const body: IUploadFileRefundFormData = {
      numeroPreSolicitud: this.refund.numberPreRequest,
      tipoArchivo: typeFile,
      numeroArchivo: 0
    };

    if (provider) {
      body.numeroRuc = provider;
    }

    const uploadsFileSub = this.healthRefundService.addFile(params, body, data.file).subscribe(
      res => {
        this.afterAddFile(data, res.numeroArchivo, typeFile);
        this.completeLoad(data.tmpKey, typeFile);
        uploadsFileSub.unsubscribe();
      },
      () => {
        uploadsFileSub.unsubscribe();
        this.completeLoad(data.tmpKey, typeFile);
      }
    );

    this.uploadsFileSub.push({ tmpKey: data.tmpKey, subs: uploadsFileSub });
  }

  removeFile(tmpKey: number, type: IFileTypeRefund, tmpListFile?: Array<IRefundImageStorage>): void {
    const file = this.getFileByTmpKeyAndType(tmpKey, type, tmpListFile);

    if (file && file.numberFile) {
      this.removeFileService(tmpKey, type, [file.numberFile]);
    } else {
      this.removeFileOfSubscribe(tmpKey);
    }
  }

  private removeFileOfSubscribe(tmpKey: number): void {
    const index = this.uploadsFileSub.findIndex(upload => upload.tmpKey === tmpKey);
    if (index > -1) {
      this.uploadsFileSub[index].subs.unsubscribe();
      this.uploadsFileSub.splice(index, 1);
    }
  }

  removeFileService(tmpKey: number, type: IFileTypeRefund, numbersFiles: Array<number>): void {
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };

    this.removeFileSub = this.healthRefundService.removeFile(params, numbersFiles).subscribe(
      () => {
        if (tmpKey) {
          this.removeFileOfSubscribe(tmpKey);
          this.afterRemoveFile(tmpKey, type);
        }
        this.removeFileSub.unsubscribe();
      },
      () => {
        this.removeFileSub.unsubscribe();
      }
    );
  }

  getFilesByType(type: IFileTypeRefund): Array<IRefundImageStorage> {
    const documents = this.refund.documents;

    return type === IFileTypeRefund.REQUEST_FORMAT
      ? documents.formatRequest
      : type === IFileTypeRefund.ATTACHMENTS
      ? documents.attachments
      : documents.payments;
  }

  getFileByTmpKeyAndType(
    tmpKey: number,
    type: IFileTypeRefund,
    list?: Array<IRefundImageStorage>
  ): IRefundImageStorage {
    // tslint:disable-next-line: no-parameter-reassignment
    list = list || this.getFilesByType(type);

    return (list || []).find(item => item.tmpKey === tmpKey);
  }

  processCompleteLoad(tmpKey: number, list: Array<IUploadFileList>): void {
    const file = list.find(itemn => itemn.tmpKey === tmpKey);
    if (file) {
      file.load = false;
    }
  }

  pendingLoadAllFile(): boolean {
    return !!this.uploadsFileSub.find(upload => !upload.subs.closed);
  }

  formatDefaultFileUpload(fileList: Array<IRefundImageStorage>): Array<IUploadFileList> {
    return fileList.map(
      data =>
        ({
          file: this.fileUtil.getNewMockFile(data.name, data.size, data.type),
          tmpKey: data.tmpKey,
          text1: (data.ruc || 0).toString(),
          text2: data.social
        } as IUploadFileList)
    );
  }

  afterRemoveFile(...args): void {}
  afterAddFile(...args): void {}

  completeLoad(...args): void {}
}
