import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { MfInput, NumberValidator, RUCValidator } from '@mx/core/ui';
import { GeneralService } from '@mx/services/general/general.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import {
  APPLICATION_CODE,
  DEFAULT_RUC_LENGTH,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH
} from '@mx/settings/constants/general-values';
import { CIRCLE_CHECK_ICON, CIRCLE_CLOSE_ICON, WARNING_2_ICON } from '@mx/settings/constants/images-values';
import { TYPE_DOCUMENT_RUC } from '@mx/settings/constants/types-documents';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-health-search-provider',
  templateUrl: './health-search-provider.component.html'
})
export class HealthSearchProviderComponent implements OnInit {
  @ViewChild(MfInput) input: MfInput;

  @Input() form: FormGroup;
  @Input() description: string;

  closeIcon = CIRCLE_CLOSE_ICON;
  warningIcon = WARNING_2_ICON;
  circleCheckIcon = CIRCLE_CHECK_ICON;

  lang = REFUND_HEALT_LANG.SearchProvider;

  ruc: AbstractControl;
  social: AbstractControl;
  verify: AbstractControl;

  providerSub: Subscription;

  showLoading: boolean;
  maxLength: number;
  socialMinLength: number = GENERAL_MIN_LENGTH.reason_social;
  socialMaxLength: number = GENERAL_MAX_LENGTH.reason_social;
  isDefault: boolean;

  constructor(
    private readonly fb: FormBuilder,
    private readonly generalService: GeneralService,
    private readonly healthRefundService: HealthRefundService
  ) {}

  ngOnInit(): void {
    this.createFormRUC();
    this.setRUCValidator();
    this.onChangeRuc();
  }

  private createFormRUC(): void {
    const group = this.fb.group({
      ruc: [null, [RUCValidator.isValid(false), Validators.required]],
      social: [{ value: null, disabled: true }, [Validators.required, Validators.maxLength(this.socialMaxLength)]],
      verify: [null, Validators.required]
    });

    this.ruc = group.get('ruc');
    this.social = group.get('social');
    this.verify = group.get('verify');

    this.form.addControl('provider', group);
  }

  private setRUCValidator(): void {
    const documents = this.generalService.getDocumentTypesData();
    const rucData = documents.find(document => document.codigo === TYPE_DOCUMENT_RUC);
    this.ruc.setValidators([
      Validators.maxLength((rucData && rucData.longitudMaxima) || DEFAULT_RUC_LENGTH),
      Validators.minLength((rucData && rucData.longitudMinima) || DEFAULT_RUC_LENGTH),
      RUCValidator.isValid(false),
      NumberValidator.isValid,
      Validators.required
    ]);
    this.maxLength = rucData.longitudMaxima || DEFAULT_RUC_LENGTH;
  }

  clearRuc(): void {
    this.ruc.setValue(null);
    this.social.setValue(null);
    this.verify.setValue(null);
  }

  private getProvider(): void {
    this.showLoading = true;
    this.input.input_native.nativeElement.blur();
    this.social.disable();
    this.social.setValue(null);

    this.providerSub = this.healthRefundService
      .getProvider({ codigoApp: APPLICATION_CODE, numeroRuc: this.ruc.value })
      .subscribe(
        res => {
          if (res && res.nombres) {
            let social = res.nombres;
            social +=
              res.apellidoPaterno && res.apellidoMaterno ? ` ${res.apellidoPaterno} ${res.apellidoMaterno}` : '';
            this.social.setValue(social);
          } else {
            this.social.enable();
            this.social.updateValueAndValidity();
          }

          this.verify.setValue(!!(res && res.nombres));
          this.showLoading = false;
          this.providerSub.unsubscribe();
        },
        () => {
          this.showLoading = false;
          this.providerSub.unsubscribe();
        }
      );
  }

  private onChangeRuc(): void {
    this.ruc.valueChanges.subscribe(res => {
      this.verify.setValue(null);
      if (this.ruc.invalid) {
        this.social.setValue(null);
        this.social.disable();
      } else if (!this.showLoading && res && !this.isDefault) {
        this.getProvider();
      }
    });
  }

  showErrors(controlName: string): boolean {
    return FormComponent.showBasicErrors(this.form.get('provider') as FormGroup, controlName);
  }

  setDefault(ruc: number, social: string, verify: boolean): void {
    this.isDefault = true;
    this.ruc.setValue(ruc);
    this.social.setValue(social);
    this.verify.setValue(verify);
    if (verify) {
      this.social.disable();
    } else {
      this.social.enable();
    }
    this.isDefault = false;
  }
}
