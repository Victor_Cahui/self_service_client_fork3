import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SwitchModule } from '@mx/core/ui';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfShowErrorsModule } from '@mx/core/ui/lib/components/global/show-errors';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message';
import { AddEditAddressComponent } from './add-edit-address.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MfModalModule,
    MfModalAlertModule,
    MfInputModule,
    MfSelectModule,
    MfModalMessageModule,
    MfShowErrorsModule,
    SwitchModule
  ],
  exports: [AddEditAddressComponent],
  declarations: [AddEditAddressComponent]
})
export class AddEditAddressModule {}
