import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { InputPasswordModule } from '@mx/components/shared/input-password/input-password.module';
import { MfButtonModule, MfInputModule, MfModalMessageModule, MfModalModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { ModalChangePasswordComponent } from './modal-change-password.component';

@NgModule({
  imports: [
    CommonModule,
    MfModalModule,
    MfButtonModule,
    ReactiveFormsModule,
    InputPasswordModule,
    MfInputModule,
    MfShowErrorsModule,
    MfCustomAlertModule,
    MfModalMessageModule
  ],
  declarations: [ModalChangePasswordComponent],
  exports: [ModalChangePasswordComponent]
})
export class ModalChangePasswordModule {}
