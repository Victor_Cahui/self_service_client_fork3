import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { CardBasicInfoComponent } from './card-basic-info.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, MfLoaderModule],
  declarations: [CardBasicInfoComponent],
  exports: [CardBasicInfoComponent],
  providers: [DatePipe]
})
export class CardBasicInfoModule {}
