import { Component, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  BUILDING_ICON,
  CALENDAR_ICON,
  MAPFRE_CIRCLE_ICON,
  PHONE_ICON,
  PROFILE_ICON,
  RUC_ICON
} from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IPoliciesByClientResponse, ISctrByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-card-basic-info',
  templateUrl: './card-basic-info.component.html'
})
export class CardBasicInfoComponent extends UnsubscribeOnDestroy implements OnInit, OnDestroy {
  @HostBinding('attr.class') attr_class = 'w-10';
  @Input() info?: IProfileBasicInfo;
  @Input() policyType: string;
  // TODO: shouldEdit debe calcularse no asignarse, será true si inicio sesion el contratante
  @Input() isEnterprise: boolean;
  @Input() shouldEdit: boolean;
  @Output() edit?: EventEmitter<IProfileBasicInfo>;

  iconCard: string;
  phoneIcon = PHONE_ICON;
  calendarIcon = CALENDAR_ICON;
  rucIcon = RUC_ICON;
  mapfreIcon = MAPFRE_CIRCLE_ICON;
  collapse = false;
  @Input() titleCard: string;
  businessName: string;
  loggedUser: ILogguedUser;
  withOutInformation = GeneralLang.Texts.WithOutInformation;
  withOutName = ProfileLang.Texts.WithoutName;
  auth: IdDocument;
  userData: IClientInformationResponse;
  contractorName: string;
  policyNumber: string;
  clientPolicy: IPoliciesByClientResponse | ISctrByClientResponse;
  @Input() useSkeleton: boolean;
  @Input() showLoading = true;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly clientService: ClientService
  ) {
    super();
    this.edit = new EventEmitter<IProfileBasicInfo>();
  }

  ngOnInit(): void {
    this.iconCard = this.isEnterprise ? BUILDING_ICON : PROFILE_ICON;
    if (!this.useSkeleton) {
      this.titleCard = this.shouldEdit ? ProfileLang.Titles.MyData : ProfileLang.Titles.Contractor;
      this.auth = this.authService.retrieveEntity();
      this.userData = this.clientService.getUserData();
      this.clientPolicy =
        this.policyType === POLICY_TYPES.MD_SCTR.code
          ? this.policiesInfoService.getClientSctrPolicy(this.policyNumber)
          : this.policiesInfoService.getClientPolicy(this.policyNumber);
      // Datos
      this.policiesInfoService
        .getProfileBasicInfo()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          (result: IProfileBasicInfo) => {
            this.info = result;
            this.showLoading = false;
          },
          error => {
            console.error(error);
          }
        );
    }
  }

  ruc(): string {
    return `${this.auth.type}: ${this.auth.number}`;
  }

  phone(): string {
    const phone = this.isEnterprise
      ? this.userData && this.userData.trabajo.telefonoTrabajo
      : this.info && this.info.phone;

    return `${ProfileLang.Labels.Phone}: ${phone || this.withOutInformation}`;
  }

  birthday(): string {
    return `${ProfileLang.Labels.Birthday}
    ${(this.info && this.info.birthDay) || this.withOutInformation}`;
  }

  registrationDate(): string {
    return `${ProfileLang.Labels.RegistrationDate}
      ${(this.info && this.info.registrationDate) || this.withOutInformation}`;
  }

  editClick(): void {
    this.router.navigate(['/profile/edit/step/1']);
    if (this.edit) {
      this.edit.next(this.info);
    }
  }

  ngOnDestroy(): void {
    if (this.edit) {
      this.edit.unsubscribe();
    }
  }
}
