import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalAlertModule, MfModalMessageModule, MfShowErrorsModule } from '@mx/core/ui/public-api';
import { EditEmailComponent } from './edit-email.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MfModalModule,
    MfInputModule,
    MfModalAlertModule,
    MfShowErrorsModule,
    MfModalMessageModule,
    MfCustomAlertModule
  ],
  exports: [EditEmailComponent],
  declarations: [EditEmailComponent]
})
export class EditEmailModule {}
