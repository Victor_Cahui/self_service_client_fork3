import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Que_quieres_hacer_Iconos_6E } from '@mx/settings/constants/events.analytics';
import { CLIENT_ACTIONS } from '@mx/settings/constants/general-values';
import { IObtenerConfiguracionActionResponse } from '@mx/statemanagement/models/client.models';
import { IClientAction } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-action-item',
  templateUrl: './action-item.component.html'
})
export class ActionItemComponent extends GaUnsubscribeBase implements OnInit {
  @Input() actionItem: IObtenerConfiguracionActionResponse;

  @Output() extraAction: EventEmitter<string> = new EventEmitter<string>();

  item: IClientAction;

  ga: Array<IGaPropertie>;

  constructor(private readonly router: Router, protected gaService: GAService) {
    super(gaService);
    this.ga = [MFP_Que_quieres_hacer_Iconos_6E()];
  }

  ngOnInit(): void {
    this.item = CLIENT_ACTIONS.find(item => item.code === this.actionItem.llave);
  }

  onAction(): void {
    if (this.item.route && this.item.route.startsWith('/')) {
      this.router.navigate([this.item.route]);
    } else if (this.item.route && this.item.route.startsWith('http')) {
      window.open(this.item.route);
    } else {
      this.extraAction.next(this.item.route);
    }
  }
}
