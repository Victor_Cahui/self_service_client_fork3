import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { MfInputComponent } from '@mx/core/ui/lib/components/forms/input/input.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH,
  REG_EX
} from '@mx/settings/constants/general-values';
import { CONTACTED_BY_AGENT } from '@mx/settings/lang/contact';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IBodySendDataContact, IContactData, IReferredContactRequest } from '@mx/statemanagement/models/client.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-form-contacted-by-agent',
  templateUrl: './form-contacted-by-agent.component.html'
})
export class FormContactedByAgentComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(MfInputComponent) inputPhoneNumber: MfInputComponent;

  @Input() isModal: boolean;
  @Input() classTop = 'pt-xs--3 pt-lg--4';
  @Input() titleSent: string;
  @Input() messageSent: string;
  @Input() referred = false;
  @Input() contactData: IContactData;

  @Output() showError = new EventEmitter<string>();
  @Output() doSuccess = new EventEmitter<string>();
  @Output() enableButton = new EventEmitter<boolean>();

  formContact: FormGroup;
  hoursRange: AbstractControl;
  phoneNumber: AbstractControl;
  email: AbstractControl;

  titleContact = CONTACTED_BY_AGENT.title;
  labelPhoneNumber = CONTACTED_BY_AGENT.phoneNumber;
  labelEmail = CONTACTED_BY_AGENT.email;
  labelHoursRange = CONTACTED_BY_AGENT.hoursRange;
  labelHoursRangeWithColons = CONTACTED_BY_AGENT.labelHoursRangeWithColons;
  labelPhoneNumberWithColons = CONTACTED_BY_AGENT.labelPhoneNumberWithColons;
  btnSend = CONTACTED_BY_AGENT.sendButton;
  hoursRangeList = CONTACTED_BY_AGENT.hoursRangeList;
  phoneMaxLength = GENERAL_MAX_LENGTH.phone;
  phoneMinLength = GENERAL_MIN_LENGTH.phone;
  emailMaxLength = GENERAL_MAX_LENGTH.card_email;
  emailMinLength = GENERAL_MIN_LENGTH.card_email;
  numericRegEx = REG_EX.numeric;

  isSent: boolean;
  auth: IdDocument;
  showLoading: boolean;
  userPhone: string;
  userEmail: string;
  sendData$: any;

  sendDataSub: Subscription;
  formChangesSub: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    public authService: AuthService,
    private readonly clientService: ClientService,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.messageSent = this.isModal ? CONTACTED_BY_AGENT.messageSent : '';
    this.titleSent = this.isModal ? CONTACTED_BY_AGENT.titleSent : CONTACTED_BY_AGENT.titleShortSent;
    this.isSent = false;
    this.auth = this.authService.retrieveEntity();
    this.userPhone = this.clientService.getUserPhone();
    this.userEmail = this.clientService.getUserEmail();
    this.initForm();
  }

  initForm(): void {
    if (this.referred) {
      this.formContact = this.formBuilder.group({
        phoneNumber: [
          this.userPhone ? this.userPhone : '',
          FormComponent.phoneValidators(this.phoneMaxLength, this.phoneMinLength)
        ],
        email: [
          this.userEmail ? this.userEmail : '',
          FormComponent.emailValidators(this.emailMaxLength, this.emailMinLength)
        ]
      });
      this.email = this.formContact.get('email');
    } else {
      this.formContact = this.formBuilder.group({
        phoneNumber: [
          this.userPhone ? this.userPhone : '',
          FormComponent.phoneValidators(this.phoneMaxLength, this.phoneMinLength)
        ],
        hoursRange: ['', Validators.required]
      });
      this.hoursRange = this.formContact.get('hoursRange');
    }

    this.phoneNumber = this.formContact.get('phoneNumber');

    this.formChangesSub = this.formContact.valueChanges.subscribe(() => {
      setTimeout(() => {
        this.enableButton.emit(this.formContact.valid);
      });
    });
  }

  ngOnDestroy(): void {
    this.formChangesSub.unsubscribe();
  }

  sendMessage(): void {
    if (this.formContact.valid) {
      // Contacto a Referidos
      if (this.referred) {
        const codeString = {
          correo: this.email.value,
          telefono: this.phoneNumber.value,
          codigoCompania: this.contactData.company,
          codigoRamo: this.contactData.branch,
          codigoModalidad: this.contactData.modality,
          numeroPoliza: this.contactData.policyNumber
        };
        const body: IReferredContactRequest = {
          landingID: this.contactData.landingId,
          code: JSON.stringify(codeString)
        };
        this.sendData$ = this.clientService.sendReferredContact(body, { codigoApp: APPLICATION_CODE });
        // Contacto por telefono
      } else {
        const body: IBodySendDataContact = {
          telefono: this.phoneNumber.value,
          rangoHorario: this.hoursRange.value,
          motivoContacto: this.contactData.subject
        };
        if (this.contactData.description) {
          body.submotivoContacto = this.contactData.description;
        }
        const params = {
          codigoApp: APPLICATION_CODE
        };
        this.sendData$ = this.clientService.sendContactData(body, params);
      }
      this.showLoading = true;
      this.sendDataSub = this.sendData$.subscribe(
        () => {
          this.isSent = true;
          if (this.isModal) {
            this.doSuccess.next();
          }
        },
        error => {
          this.showLoading = false;
          this.isSent = false;
          if (this.isModal) {
            this.showError.next((error && error.message) || GeneralLang.Alert.sendData.error.message);
          }
        },
        () => {
          this.showLoading = false;
          this.sendDataSub.unsubscribe();
        }
      );
    }
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  setFocusPhoneInput(): void {
    this.inputPhoneNumber.input_native.nativeElement.focus();
  }

  formatValue(control): void {
    FormComponent.removeSpaces(control);
  }
}
