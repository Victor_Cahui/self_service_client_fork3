import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfButtonModule, MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { FormContactedByAgentComponent } from './form-contacted-by-agent.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    MfInputModule,
    MfSelectModule,
    MfShowErrorsModule,
    MfButtonModule,
    MfLoaderModule,
    GoogleModule
  ],
  exports: [FormContactedByAgentComponent],
  declarations: [FormContactedByAgentComponent]
})
export class FormContactedByAgentModule {}
