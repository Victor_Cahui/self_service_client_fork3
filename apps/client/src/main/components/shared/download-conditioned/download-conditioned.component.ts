import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { Content } from '@mx/statemanagement/models/general.models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-download-conditioned',
  templateUrl: './download-conditioned.component.html'
})
export class DownLoadConditionedComponent implements OnInit {
  @Input() content: Content;

  policyNumber: string;
  fileUtil: FileUtil;
  policeSub: Subscription;
  btnDownLoadConditioned = PolicyLang.Buttons.DownLoadConditioned;
  showSpinner: boolean;

  constructor(private readonly policiesService: PoliciesService, private readonly activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.fileUtil = new FileUtil();
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
  }

  seeExclusionsPdf(): void {
    if (this.showSpinner) {
      return;
    }
    this.showSpinner = true;
    this.policeSub = this.policiesService
      .seeExclusionsPdfByPolicy({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      })
      .subscribe(
        (res: any) => {
          try {
            const blobFile = this.fileUtil.dataURItoBlob(`data:application/pdf;base64,${res.base64}`);
            this.fileUtil.downloadBlob(blobFile, StringUtil.slugify(`Exclusiones - Poliza N°: ${this.policyNumber}`));
          } catch (error) {}
        },
        () => {
          this.showSpinner = false;
        },
        () => {
          this.showSpinner = false;
          this.policeSub.unsubscribe();
        }
      );
  }
}
