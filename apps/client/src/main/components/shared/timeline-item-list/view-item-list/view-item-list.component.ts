import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NO_PENDING_PROCEDURES_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IConfigUICardViewItemList } from '@mx/statemanagement/models/general.models';
import { IRequestItemView } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-view-item-list',
  templateUrl: './view-item-list.component.html'
})
export class ViewItemListComponent implements OnInit {
  @Input() itemsProcess: Array<IRequestItemView>;
  @Input() itemsHistory: Array<IRequestItemView>;
  @Input() yearsRange: number;
  @Input() numberLabel: string;
  @Input() textNotFoundHistory: string;
  @Input() textNotFound = GeneralLang.Messages.ItemNotFound;
  @Input() textNotFoundProcess: string;
  @Input() configUI: IConfigUICardViewItemList;
  @Output() viewDetail: EventEmitter<any> = new EventEmitter<any>();

  showProcess: boolean;
  showHistory: boolean;
  icon = NO_PENDING_PROCEDURES_ICON;

  ngOnInit(): void {
    this.showProcess = !!(this.itemsProcess && this.itemsProcess.length);
    this.showHistory = !!(this.itemsHistory && this.itemsHistory.length);
  }

  onViewDetail(item: IRequestItemView): void {
    this.viewDetail.emit(item);
  }
}
