import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { CardTimelineModule } from '../card-timeline/card-timeline.module';
import { ItemRequestModule } from '../item-request/item-request.module';
import { CardRequestProcessComponent } from './card-request-process.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    ItemRequestModule,
    CardTimelineModule,
    ItemNotFoundModule,
    GoogleModule
  ],
  declarations: [CardRequestProcessComponent],
  exports: [CardRequestProcessComponent]
})
export class CardRequestProcessModule {}
