import { Component, EventEmitter, Input, Output } from '@angular/core';
import { getDay, getMonth, getYear } from '@mx/core/shared/helpers/util/date';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { environment } from '@mx/environments/environment';
import { ASSISTANCE } from '@mx/settings/lang/vehicles.lang';
import { IItemView } from '@mx/statemanagement/models/general.models';
import { IRequestItemView } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-item-request',
  templateUrl: './item-request.component.html'
})
export class ItemRequestComponent extends GaUnsubscribeBase {
  @Input() numberLabel: string;
  @Input() item: IRequestItemView;
  @Output() viewDetail: EventEmitter<IRequestItemView>;
  @Output() collapse: EventEmitter<any>;

  lang = ASSISTANCE;
  numberText: string;
  viewDetailsLink = environment.ACTIVATE_VIEW_DETAIL_LINK_ASSISTS;

  constructor(protected gaService: GAService) {
    super(gaService);
    this.viewDetail = new EventEmitter<IRequestItemView>();
    this.collapse = new EventEmitter<IRequestItemView>();
  }

  onViewDetail(item: IRequestItemView): void {
    this.viewDetail.next(item);
  }

  calculateDay(myDate: string): string {
    return getDay(myDate);
  }

  calculateMonth(myDate: string): string {
    return getMonth(myDate);
  }

  calculateYear(myDate: string): string {
    return getYear(myDate);
  }

  collapseFn(): void {
    this.item.collapse = !this.item.collapse;
    if (!this.item.collapse) {
      this.collapse.emit();
    }
  }

  getTitle(text: IItemView): string {
    return `${text.label ? text.label : ''} ${text.text ? text.text : ''}`;
  }
}
