import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { DiffYears } from '@mx/core/shared/helpers/util/date';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Accidentes_25B, MFP_Accidentes_25C } from '@mx/settings/constants/events.analytics';
import { NO_PENDING_PROCEDURES_ICON } from '@mx/settings/constants/images-values';
import { YEARS_ANTIQUITY_ASSIST_DEFAULT } from '@mx/settings/constants/key-values';
import { ASSISTANCE } from '@mx/settings/lang/vehicles.lang';
import { ICardFilterOption, IConfigUICardViewItemList } from '@mx/statemanagement/models/general.models';
import { IRequestItemView } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-card-request-history',
  templateUrl: './card-request-history.component.html'
})
export class CardRequestHistoryComponent implements OnInit, OnChanges {
  @Input() items: Array<IRequestItemView>;
  @Input() yearsRange: number;
  @Input() numberLabel: string;
  @Input() textNotFoundHistory: string;
  @Output() viewDetail: EventEmitter<IRequestItemView>;
  @Input() configUI: IConfigUICardViewItemList;

  collapse = false;
  lang = ASSISTANCE;
  icon = NO_PENDING_PROCEDURES_ICON;
  tmpItems: Array<IRequestItemView> = [];
  options: Array<ICardFilterOption>;
  ga: Array<IGaPropertie>;

  constructor() {
    this.viewDetail = new EventEmitter<IRequestItemView>();
    this.ga = [MFP_Accidentes_25B(), MFP_Accidentes_25C()];
  }

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.getValue();
    this.getFilters();
  }

  viewDetailFn(item: IRequestItemView): void {
    this.viewDetail.next(item);
  }

  getValue(year?: string): void {
    const items = this.items || [];
    this.tmpItems = year ? items.filter(item => item.year === year) : items.slice(0, 5);
  }

  getFilters(): void {
    this.options = [{ text: this.lang.SelectYear, value: '' }];
    for (let i = 0; i < (this.yearsRange || YEARS_ANTIQUITY_ASSIST_DEFAULT); i++) {
      const year = DiffYears(new Date(), i)
        .getFullYear()
        .toString();
      this.options.push({ text: year, value: year });
    }
  }
}
