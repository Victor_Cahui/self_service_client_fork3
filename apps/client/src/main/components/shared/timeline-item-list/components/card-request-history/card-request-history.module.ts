import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardFilterModule } from '@mx/components/shared/card-filter/card-filter.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { ItemRequestModule } from '../item-request/item-request.module';
import { CardRequestHistoryComponent } from './card-request-history.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    ItemRequestModule,
    CardFilterModule,
    ItemNotFoundModule,
    GoogleModule
  ],
  declarations: [CardRequestHistoryComponent],
  exports: [CardRequestHistoryComponent]
})
export class CardRequestHistoryModule {}
