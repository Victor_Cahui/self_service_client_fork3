import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IconListModule } from '@mx/components/shared/icon-list/icon-list.module';
import { ModalDownLoadModule } from '@mx/components/shared/modals/modal-download/modal-download.module';
import { MfButtonModule, StarRatingModule } from '@mx/core/ui';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { CardIconListComponent } from './cards/card-icon-list/card-icon-list.component';
import { CardIconComponent } from './cards/card-icon/card-icon.component';
import { CardTableComponent } from './cards/card-table/card-table.component';
import { CardTextComponent } from './cards/card-text/card-text.component';
import { DynamicCardDirective } from './dynamic-card.directive';
import { FooterLinkDownloadComponent } from './footer/footer-link-download/footer-link-download.component';
import { FooterLinkComponent } from './footer/footer-link/footer-link.component';
import { FooterStarRatingComponent } from './footer/footer-star-rating/footer-star-rating.component';
import { TimelineComponent } from './timeline.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TooltipsModule,
    IconListModule,
    StarRatingModule,
    MfButtonModule,
    ModalDownLoadModule
  ],
  declarations: [
    TimelineComponent,
    DynamicCardDirective,
    CardIconComponent,
    CardIconListComponent,
    CardTableComponent,
    CardTextComponent,
    FooterLinkComponent,
    FooterLinkDownloadComponent,
    FooterStarRatingComponent
  ],
  exports: [TimelineComponent, DynamicCardDirective, MfButtonModule],
  entryComponents: [
    CardIconComponent,
    CardIconListComponent,
    CardTableComponent,
    CardTextComponent,
    FooterLinkComponent,
    FooterLinkDownloadComponent,
    FooterStarRatingComponent
  ]
})
export class TimelineModule {}
