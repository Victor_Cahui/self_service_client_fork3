import { Component, Input } from '@angular/core';

@Component({
  selector: 'client-card-icon-list',
  templateUrl: './card-icon-list.component.html'
})
export class CardIconListComponent {
  @Input() content: any;
}
