import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'client-card-icon',
  templateUrl: './card-icon.component.html'
})
export class CardIconComponent implements OnInit {
  @Input() content: any;

  ngOnInit(): void {}
}
