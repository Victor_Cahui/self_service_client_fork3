import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'client-card-table',
  templateUrl: './card-table.component.html'
})
export class CardTableComponent implements OnInit {
  @Input() content: any;

  table: any;

  ngOnInit(): void {
    this.table = this.content.table2;
    // TODO: verificar con varios cards
    this.table = this.table.filter(item => item.row);
  }

  trackByFn(index, item): any {
    return index;
  }
}
