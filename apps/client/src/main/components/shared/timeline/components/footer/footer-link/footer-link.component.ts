import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'client-footer-link',
  templateUrl: './footer-link.component.html'
})
export class FooterLinkComponent implements OnInit {
  @Input() content: any;

  ngOnInit(): void {}

  openLink(url: string): void {
    if (url) {
      window.open(url, '_blank');
    }
  }
}
