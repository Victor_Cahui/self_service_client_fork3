import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { TimelineModule } from '@mx/components/shared/timeline/components';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { ViewTimelineComponent } from './view-timeline.component';

@NgModule({
  imports: [CommonModule, TimelineModule, ItemNotFoundModule, MfLoaderModule],
  declarations: [ViewTimelineComponent],
  exports: [ViewTimelineComponent]
})
export class ViewTimelineModule {}
