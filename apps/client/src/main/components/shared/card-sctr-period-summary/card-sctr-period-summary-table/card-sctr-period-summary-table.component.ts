import { Component, Input, OnInit } from '@angular/core';
import { IOperationSummaryRiskDetail } from '@mx/statemanagement/models/sctr.interface';

@Component({
  selector: 'client-card-sctr-period-summary-table',
  templateUrl: './card-sctr-period-summary-table.component.html'
})
export class CardSctrPeriodSummaryTableComponent implements OnInit {
  @Input() riskDetail: IOperationSummaryRiskDetail;

  ngOnInit(): void {}
}
