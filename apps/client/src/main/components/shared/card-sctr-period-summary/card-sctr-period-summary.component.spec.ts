import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CardSctrPeriodSummaryBillingModule } from '@mx/components/shared/card-sctr-period-summary/card-sctr-period-summary-billing/card-sctr-period-summary-billing.module';
import { NotificationModule } from '@mx/core/shared/helpers';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AutoserviciosMock } from '@mx/core/shared/providers/services/proxy/Autoservicios.mock';
import { MfInputModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';

import { CardSctrPeriodSummaryComponent } from './card-sctr-period-summary.component';

describe('CardSctrPeriodSummaryComponent', () => {
  let component: CardSctrPeriodSummaryComponent;
  let fixture: ComponentFixture<CardSctrPeriodSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MfLoaderModule,
        NotificationModule,
        FormsModule,
        ReactiveFormsModule,
        MfInputModule,
        CardSctrPeriodSummaryBillingModule
      ],
      declarations: [CardSctrPeriodSummaryComponent],
      providers: [{ provide: Autoservicios, useClass: AutoserviciosMock }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardSctrPeriodSummaryComponent);
        component = fixture.debugElement.componentInstance;
      })
      .then(() => {
        component.isDeclaration = true;
        component.movement = 1234;
      })
      .then(() => {
        fixture.detectChanges();
      });
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  it('to validate component properties', async(() => {
    component.isDeclaration = true;
    component.movement = 1234;
    expect(component.isDeclaration).toBeDefined();
    expect(component.movement).toBeDefined();
  }));

  it('should load info summary', async(() => {
    component.isDeclaration = false;
    component.movement = 1234;
    component.getSummary();
  }));
});
