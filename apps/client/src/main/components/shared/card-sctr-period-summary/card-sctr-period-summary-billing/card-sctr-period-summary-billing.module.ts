import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';

import { CardSctrPeriodSummaryBillingComponent } from './card-sctr-period-summary-billing.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfLoaderModule, FormsModule, ReactiveFormsModule],
  declarations: [CardSctrPeriodSummaryBillingComponent],
  exports: [CardSctrPeriodSummaryBillingComponent]
})
export class CardSctrPeriodSummaryBillingModule {}
