import { Component, Input, OnInit } from '@angular/core';
import { validURL } from '@mx/core/shared/helpers/functions/general-functions';
import { GeneralService } from '@mx/services/general/general.service';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientInformationRequest, IClientInformationResponse } from '@mx/statemanagement/models/client.models';

@Component({
  selector: 'client-user-type',
  templateUrl: './user-type.component.html'
})
export class UserTypeComponent implements OnInit {
  @Input() clientInformation: IClientInformationResponse;
  @Input() clientID: IClientInformationRequest;

  linkMyBenefits = ProfileLang.Buttons.MyBenefits;
  textClient = ProfileLang.Labels.Client;

  public urlBenefits;

  constructor(private readonly generalService: GeneralService) {}

  ngOnInit(): void {
    // Enlace a Mis Beneficios
    if (this.clientInformation.enlaceTipoUsuario) {
      const url = `${this.clientInformation.enlaceTipoUsuario}${this.clientID.tipoDocumento}/${this.clientID.documento}`;
      if (validURL(url)) {
        this.urlBenefits = url;
      }
    }
  }
}
