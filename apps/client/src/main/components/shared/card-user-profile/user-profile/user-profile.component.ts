import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalChangePasswordComponent } from '@mx/components/profile/modal-change-password/modal-change-password.component';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Mi_Perfil_5A } from '@mx/settings/constants/events.analytics';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { EditEmailComponent } from '../../../profile/edit-email/edit-email.component';

@Component({
  selector: 'client-user-profile',
  templateUrl: './user-profile.component.html'
})
export class UserProfileComponent implements OnInit {
  @ViewChild(EditEmailComponent) editEmail: EditEmailComponent;
  @ViewChild(ModalChangePasswordComponent) editPassword: ModalChangePasswordComponent;

  @Input() clientInformation: IClientInformationResponse;
  @Input() isHome: boolean;
  @Input() isEnterprise: boolean;

  public avatar: string;
  labelPassword = AuthLang.Labels.Password;
  linkMyProfile = ProfileLang.Buttons.MyProfile;
  WithOutEmail = GeneralLang.Texts.WithOutEmail;

  gaMyProfile: IGaPropertie;

  constructor(public router: Router) {
    this.gaMyProfile = MFP_Mi_Perfil_5A(this.linkMyProfile);
  }

  ngOnInit(): void {
    if (this.isEnterprise && this.clientInformation.razonSocial) {
      this.avatar = this.clientInformation.razonSocial[0];
    } else {
      this.avatar = `${this.clientInformation.nombreCliente[0] || ''}${this.clientInformation
        .apellidoPaternoCliente[0] || ''}`;
    }
  }

  public myPerfil(): void {
    this.router.navigate(['/profile']);
  }

  openEditEmailModal(): void {
    this.editEmail.open();
  }

  openEditPasswordModal(): void {
    this.editPassword.open();
  }

  // Actualiza vista
  emitChangeClientInformation(email): void {
    this.clientInformation.emailCliente = email;
  }
}
