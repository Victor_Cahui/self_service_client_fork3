import { CurrencyPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TruncateDecimalsPipe } from '@mx/core/shared/common/pipes/truncatedecimals.pipe';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { APPLICATION_CODE, DEFAULT_MPD_DECIMALS } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MAPFRE_DOLARS, ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientInformationRequest } from '@mx/statemanagement/models/client.models';

@Component({
  selector: 'client-user-mapfre-dolars',
  templateUrl: './user-mapfre-dolars.component.html',
  providers: [CurrencyPipe, TruncateDecimalsPipe]
})
export class UserMapfreDolarsComponent implements OnInit {
  @Input() clientID: IClientInformationRequest;

  mapfreDolars: number;
  mpd: { int: string; dec: string };
  isVisibleLoader: boolean;
  title = ProfileLang.Labels.MapfreDolars;
  titleToolTip = MAPFRE_DOLARS.title;
  contentToolTip = MAPFRE_DOLARS.content;
  lang = GeneralLang;

  constructor(
    private readonly _Autoservicios: Autoservicios,
    private readonly truncateDecimalsPipe: TruncateDecimalsPipe,
    public currencyPipe: CurrencyPipe,
    private readonly _Router: Router
  ) {}

  ngOnInit(): void {
    this.isVisibleLoader = true;
    const pathReq = { codigoApp: APPLICATION_CODE };
    this._Autoservicios.ObtenerResumenMD(pathReq).subscribe(response => {
      this.isVisibleLoader = false;
      this.mapfreDolars = response.montoDisponible;
      const mpd = this.currencyPipe.transform(
        this.truncateDecimalsPipe.transform(this.mapfreDolars, DEFAULT_MPD_DECIMALS),
        ' '
      );
      this.mpd = { int: mpd.split('.')[0], dec: mpd.split('.')[1] };
    });
  }

  goToMD(): void {
    this._Router.navigate(['/mapfre-dollars']);
  }
}
