import { Location } from '@angular/common';
import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GROUP_TYPE_ID_PERSON } from '@mx/settings/auth/auth-values';
import { MFP_Conocer_mis_Beneficios_5C } from '@mx/settings/constants/events.analytics';
import { MY_BENEFITS_DIR } from '@mx/settings/constants/general-values';
import { LOCAL_STORAGE_KEYS, USER_NOT_REGISTERED } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { IClientInformationRequest, IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';

@Component({
  selector: 'client-card-user-profile',
  templateUrl: './card-user-profile.component.html'
})
export class CardUserProfileComponent implements OnInit {
  @HostBinding('class') clases = 'row mb-xs--2';
  @Input() isEnterprise: boolean;

  clientInformation: IClientInformationResponse;
  clientID: IClientInformationRequest;
  isHome = true;
  userNotRegistered = USER_NOT_REGISTERED;
  showLoading: boolean;
  isEnterpriseWithMPD: boolean;
  showMPD: boolean;
  isVisibleBenefitsLink: boolean;
  myBenefits = GeneralLang.Labels.MyBenefits;
  info: IProfileBasicInfo;

  gaMyBenefits: IGaPropertie;

  constructor(
    private readonly clientService: ClientService,
    private readonly localStorageService: LocalStorageService,
    private readonly authService: AuthService,
    public location: Location
  ) {
    this.gaMyBenefits = MFP_Conocer_mis_Beneficios_5C(this.myBenefits);
  }

  ngOnInit(): void {
    // Verifica URL
    this.isHome = this.checkUrl();

    // Recupera datos del usuario logeado
    const auth = this.authService.retrieveEntity();
    const logguedUser: ILogguedUser = this.localStorageService.getData(LOCAL_STORAGE_KEYS.USER);

    this.clientID = { tipoDocumento: auth.type, documento: auth.number };

    // Verifica si es cliente empresa y si se le debe mostrar mapfredolares
    this.isEnterpriseWithMPD = this.isEnterprise && (auth.number || '').startsWith('10');

    // verifica si se le muestra el link de: Mis beneficios
    this.isVisibleBenefitsLink =
      +logguedUser.groupTypeId === GROUP_TYPE_ID_PERSON || (auth.number || '').startsWith('10');

    this.showMPD = this.isEnterpriseWithMPD || !this.isEnterprise;
    this.getClientProfile();
  }

  getClientProfile(): void {
    // Busca Datos de Usuario
    this.showLoading = true;
    this.clientService.getUserDataSubject().subscribe(data => {
      if (data) {
        this.clientInformation = data;
      } else {
        const dataUser = this.clientService.getUserData();
        if (dataUser) {
          this.clientInformation = dataUser;
        }
      }
      this.showLoading = false;
    });
  }

  goToBenefits(): void {
    window.open(MY_BENEFITS_DIR, '_blank');
  }

  // Verifica donde se encuentra el usuario
  private checkUrl(): boolean {
    const route = this.location.prepareExternalUrl(this.location.path());

    return route.startsWith('#/home');
  }
}
