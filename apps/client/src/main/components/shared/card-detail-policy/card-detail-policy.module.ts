import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalPolicyTransferModule } from '@mx/components/vehicles/modal-policy-transfer/modal-policy-transfer.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { CardDetailPolicyComponent } from './card-detail-policy.component';

@NgModule({
  imports: [CommonModule, GoogleModule, MfCardModule, ModalPolicyTransferModule, DirectivesModule],
  declarations: [CardDetailPolicyComponent],
  exports: [CardDetailPolicyComponent]
})
export class CardDetailPolicyModule {}
