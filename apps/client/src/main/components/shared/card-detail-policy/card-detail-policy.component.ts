import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalPolicyTransferComponent } from '@mx/components/vehicles/modal-policy-transfer/modal-policy-transfer.component';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IPaymentTypeResponse } from '@mx/statemanagement/models/payment.models';
import { IPolicyBasicInfo } from '@mx/statemanagement/models/policy.interface';
import { zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-card-detail-policy',
  templateUrl: './card-detail-policy.component.html'
})
export class CardDetailPolicyComponent extends GaUnsubscribeBase implements OnInit {
  @Input() policyNumber?: string;
  @Input() policyType?: string;
  @Input() viewTrace: false;
  @Input() viewConstancy: false;
  // viewLabelPolicy es true solo para pólizas SCTR
  @Input() viewLabelPolicy: false;
  @Input() isEps: boolean;
  @Input() isEpsContractor: boolean;
  @Input() isSctrDoubleEmission: boolean;
  @Input() mpKeys: any;
  @ViewChild(ModalPolicyTransferComponent) modalPolicyTransfer: ModalPolicyTransferComponent;

  policy: any;
  fileUtil: FileUtil;
  linkViewPoliza: string;
  titleCard = PolicyLang.Titles.DetailMyPolicy;
  linkDownloadConstancy = PolicyLang.Buttons.DownloadConstancy;
  linkTracePoliza = PolicyLang.Buttons.TracePolicy;
  poLang = PolicyLang;
  showSpinnerPolicy: boolean;
  showSpinnerConstancy: boolean;
  showSpinnerAffiliateManual: boolean;
  showModal: boolean;
  showDownloadOption: boolean;
  paymentsTypes: Array<IPaymentTypeResponse>;
  policyInfo: IPolicyBasicInfo;

  constructor(
    private readonly _Autoservicios: Autoservicios,
    private readonly _AuthService: AuthService,
    protected policiesService: PoliciesService,
    protected _PoliciesInfoService: PoliciesInfoService,
    protected gaService: GAService
  ) {
    super(gaService);
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    if (this.isEps) {
      this.titleCard = PolicyLang.Titles.DetailMyContract;
    }
    this.policy = this._PoliciesInfoService.getClientPolicy(this.policyNumber);

    this.showDownloadOption = !this.isEps && this._PoliciesInfoService.canDownloadPolicy();

    this._PoliciesInfoService.getPolicyBasicInfo().subscribe(policyInfo => (this.policyInfo = policyInfo));

    // if: solo pólizas SCTR
    this.linkViewPoliza = this.viewLabelPolicy
      ? this.isSctrDoubleEmission
        ? `${PolicyLang.Buttons.DownloadPolicy}s`
        : PolicyLang.Buttons.DownloadPolicy
      : PolicyLang.Buttons.ViewPolicyCondition;
    this.mpKeys && (this.showDownloadOption = this.mpKeys.MP_VER_POLIZA_Y_CONDICIONADO);
  }

  openPolicyTransferModal(): void {
    this.showModal = true;
    setTimeout(() => {
      this.modalPolicyTransfer.open(true);
    });
  }

  downloadPolicyPdf(): void {
    if (this.showSpinnerPolicy) {
      return;
    }
    if (this.policyNumber) {
      this.showSpinnerPolicy = true;
      this.policiesService
        .seePolicePdfById(this.policyNumber)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            try {
              this.fileUtil.download(res.base64, 'pdf', `Poliza Nro: ${this.policyNumber}`);
            } catch (error) {}
          },
          () => (this.showSpinnerPolicy = false),
          () => (this.showSpinnerPolicy = false)
        );
    }
  }

  downloadPoliciesPdf(): void {
    if (this.showSpinnerPolicy) {
      return;
    }
    const policyHealth = this.policyInfo.policyNumber.split(',')[0].trim();
    const policyPension = this.policyInfo.policyNumber.split(',')[1].trim();
    if (policyHealth && policyPension) {
      this.showSpinnerPolicy = true;
      zip(this.policiesService.seePolicePdfById(policyHealth), this.policiesService.seePolicePdfById(policyPension))
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            try {
              this.fileUtil.download(res[0].base64, 'pdf', `Poliza Salud Nro: ${policyHealth}`);
              this.fileUtil.download(res[1].base64, 'pdf', `Poliza Pension Nro: ${policyPension}`);
            } catch (error) {}
          },
          () => (this.showSpinnerPolicy = false),
          () => (this.showSpinnerPolicy = false)
        );
    }
  }

  downloadConstancyPdf(): void {
    if (this.showSpinnerConstancy) {
      return;
    }
    if (this.policyNumber) {
      this.showSpinnerConstancy = true;
      this.policiesService
        .constancyPdf(this.policyNumber)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            try {
              this.fileUtil.download(res.base64, 'pdf', `Constancia - Poliza Nro: ${this.policyNumber}`);
            } catch (error) {}
          },
          () => (this.showSpinnerConstancy = false),
          () => (this.showSpinnerConstancy = false)
        );
    }
  }

  downloadAffiliateManual(): void {
    if (this.showSpinnerAffiliateManual) {
      return;
    }
    if (this.policyNumber) {
      this.showSpinnerAffiliateManual = true;
      this._Autoservicios
        .DescargarEpsContratanteManualAfiliado(
          { nroContrato: this.policyNumber },
          {
            codigoApp: APPLICATION_CODE
          }
        )
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            try {
              const blobFile = this.fileUtil.dataURItoBlob(`data:application/pdf;base64,${res.base64}`);
              this.fileUtil.downloadBlob(blobFile, StringUtil.slugify(`Manual Afiliado: ${this.policyNumber}`));
            } catch (error) {}
          },
          () => (this.showSpinnerAffiliateManual = false),
          () => (this.showSpinnerAffiliateManual = false)
        );
    }
  }
}
