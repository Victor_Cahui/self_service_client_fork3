import { Component, Input } from '@angular/core';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IPaymentItemDetailView } from '@mx/statemanagement/models/payment.models';

@Component({
  selector: 'client-card-item-detail',
  templateUrl: './card-item-detail.component.html'
})
export class CardItemDetailComponent {
  @Input() items: Array<IPaymentItemDetailView>;
  @Input() isRecipes: boolean;

  policyNumberLabel = GeneralLang.Texts.PolicyNumber;
}
