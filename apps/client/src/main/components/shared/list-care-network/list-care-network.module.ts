import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { DirectivesModule } from '@mx/core/ui/lib/directives/directives.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { ListCareNetworkComponent } from './list-care-network.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfLoaderModule, DirectivesModule, TooltipsModule, LinksModule],
  exports: [ListCareNetworkComponent],
  declarations: [ListCareNetworkComponent]
})
export class ListCareNetworkModule {}
