import { Component, Input } from '@angular/core';
import { IItemView } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-icon-list',
  templateUrl: './icon-list.component.html'
})
export class IconListComponent {
  @Input() itemList: Array<IItemView>;

  // Title de items
  getTitle(text: IItemView): string {
    return `${text.label ? text.label : ''} ${text.text ? text.text : ''}`;
  }
}
