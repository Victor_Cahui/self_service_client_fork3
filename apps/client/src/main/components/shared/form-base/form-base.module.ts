import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { FormBaseComponent } from './form-base.component';

@NgModule({
  imports: [CommonModule, GoogleModule, MfButtonModule],
  exports: [FormBaseComponent],
  declarations: [FormBaseComponent]
})
export class FormBaseModule {}
