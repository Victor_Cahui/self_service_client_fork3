import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { CardMoreFaqComponent } from './card-more-faq.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, DirectivesModule, MfCardModule, MfSelectModule, GoogleModule],
  exports: [CardMoreFaqComponent],
  declarations: [CardMoreFaqComponent]
})
export class CardMoreFaqModule {}
