import { DatePipe } from '@angular/common';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { SctrEndpoint } from '@mx/endpoints/sctr.endpoint';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { IHeader } from '@mx/services/general/header-helper.service';
import { KEY_USER_TOKEN } from '@mx/settings/auth/auth-values';
import {
  APPLICATION_CODE,
  DEFAULT_MAX_SIZE_MB_TEMPLATE_DECLARE,
  FORMAT_DATE_DDMMYYYYHHMM
} from '@mx/settings/constants/general-values';
import { XLS_ICON } from '@mx/settings/constants/images-values';
import { MAX_SIZE_MB_TEMPLATE_DECLARE } from '@mx/settings/constants/key-values';
import { SCTR_PERIOD_OPERATION_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HowToDeclarePeriodLang, HowToIncludePeriodLang, PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { ISctrPeriodResponse } from '@mx/statemanagement/models/policy.interface';
import { humanizeBytes, UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';
import { takeUntil } from 'rxjs/operators';
import { ModalHowWorksComponent } from '../modals/modal-how-works/modal-how-works.component';

@Component({
  selector: 'client-card-sctr-period-upload',
  templateUrl: './card-sctr-period-upload.component.html',
  providers: [DatePipe]
})
export class CardSctrPeriodUploadComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(ModalHowWorksComponent) modalHowWorks: ModalHowWorksComponent;
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;
  @ViewChild('inputFile') inputFile: ElementRef;

  @Input() isDeclaration: boolean;
  @Input() period: ISctrPeriodResponse;

  @Output() movementNumber: EventEmitter<number> = new EventEmitter<number>();
  @Output() canContinue: EventEmitter<boolean> = new EventEmitter<boolean>();

  HowItWorks: any;
  showModalHowWorks: boolean;
  showSpinnerTemplate: boolean;
  dragOver: boolean;

  lang = PERIODS_LANAG;
  xlsIcon = XLS_ICON;

  header: IHeader;

  fileUtil: FileUtil;
  files: Array<UploadFile> = [];
  options: UploaderOptions;
  uploadInput: EventEmitter<UploadInput>;
  maxSizeFileMB: number;
  titleAlert: string;
  messageAlert: string;
  uploadingText: string;
  fileSize: string;
  lastEditedText: string;
  canRemove = true;

  movement: number;
  responseCode: string;
  workerDeclaredList: Array<any> = [];
  errorList: Array<any> = [];
  commentList: Array<any> = [];

  showSpinnerErrors: boolean;
  showSpinnerComments: boolean;
  showLoaginfFile: boolean;

  constructor(
    protected datePipe: DatePipe,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    private readonly generalService: GeneralService,
    private readonly storageService: StorageService
  ) {
    super();
    this.options = {
      concurrency: 1,
      allowedContentTypes: [
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      ],
      maxUploads: 1
    };
    this.fileUtil = new FileUtil();
    this.uploadInput = new EventEmitter<UploadInput>();
  }

  ngOnInit(): void {
    this.getParameters();
    this.header = SCTR_PERIOD_OPERATION_HEADER;
    this.HowItWorks = this.isDeclaration ? HowToDeclarePeriodLang : HowToIncludePeriodLang;
  }

  getParameters(): void {
    this.generalService
      .getParametersSubject()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.maxSizeFileMB =
          +this.generalService.getValueParams(MAX_SIZE_MB_TEMPLATE_DECLARE) || DEFAULT_MAX_SIZE_MB_TEMPLATE_DECLARE;
      });
  }

  onHowItWorks(): void {
    this.showModalHowWorks = true;
    setTimeout(() => {
      this.modalHowWorks.open();
    });
  }

  downloadTemplate(): void {
    if (this.showSpinnerTemplate) {
      return;
    }

    this.showSpinnerTemplate = true;
    this._Autoservicios
      .DescargarPlanillaSCTR()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        res => {
          try {
            this.fileUtil.download(res.base64, 'application/vnd.ms-excel', 'Formato trama');
          } catch (error) {}
        },
        () => (this.showSpinnerTemplate = false),
        () => (this.showSpinnerTemplate = false)
      );
  }

  onUploadOutput(output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        this.files.length && this.uploadPayroll();
        break;
      case 'addedToQueue':
        if (output.file && output.file.size > this.maxSizeFileMB * 1024 * 1024) {
          this.titleAlert = GeneralLang.Messages.Fail;
          this.messageAlert = `${GeneralLang.Messages.ErrorMaxMB.start}${this.maxSizeFileMB}${GeneralLang.Messages.ErrorMaxMB.end}`;
          this.inputFile.nativeElement.value = '';
          this.modalMessage.open();

          return void 0;
        }

        this.files.push(output.file);
        break;
      case 'uploading':
        this.uploadingText = `Cargando: ${output.file.progress.data.percentage}% (${output.file.progress.data.speedHuman})`;
        if (output.file.progress.data.percentage === 100) {
          this.canRemove = false;
          this.uploadingText = 'Revisando archivo...';
        }
        break;
      case 'removed':
        this.files = this.files.filter((file: UploadFile) => file !== output.file);
        break;
      case 'start':
        this.fileSize = `${this.lang.FileSize}: ${humanizeBytes(output.file.size)}`;
        const lastModified = this.datePipe.transform(output.file.nativeFile.lastModified, FORMAT_DATE_DDMMYYYYHHMM);
        this.lastEditedText = `${this.lang.LastEdited}: ${lastModified}Hrs.`;
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        const dataFile = this.files[0];
        if (!dataFile || !dataFile.response) {
          return void 0;
        }

        switch (dataFile.responseStatus) {
          case 900:
          case 901:
            this.titleAlert = dataFile.response.excepcion;
            this.messageAlert = dataFile.response.mensaje;
            this.modalMessage.open();
            break;
          case 200:
            this._mapResponseTemplate(dataFile.response);
            break;
          default:
            break;
        }
        this.uploadingText = '';
        this.showLoaginfFile = false;
        this.canRemove = true;
        break;
      case 'rejected':
        if (output.file) {
          this.files = [];
          this.titleAlert = GeneralLang.Messages.Fail;
          this.messageAlert = GeneralLang.Messages.ErrorXLSFormat;
          this.modalMessage.open();
        }
        break;
      default:
        break;
    }
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id });
  }

  removeFile(id: string): void {
    this.inputFile.nativeElement.value = '';
    this.workerDeclaredList = this.errorList = this.commentList = [];
    this.canContinue.emit(false);
    this.uploadInput.emit({ type: 'remove', id });
    this.files = [];
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  uploadPayroll(): void {
    this.showLoaginfFile = true;
    const token = this.storageService.getItem(KEY_USER_TOKEN);
    const auth = this.authService.retrieveEntity();
    const declaracion = {
      fechaInicioVigencia: this.period.fechaInicioVigencia,
      fechaFinVigencia: this.period.fechaFinVigencia,
      polizas: this.period.polizas
    };
    const data = {
      declaracionJson: JSON.stringify(declaracion)
    };
    const tipoMovimiento = this.isDeclaration ? 'DE' : 'IN';
    const url = `${SctrEndpoint.UploadPayroll}?tipoDocumento=${auth.type}&documento=${auth.number}&codigoApp=${APPLICATION_CODE}&tipoMovimiento=${tipoMovimiento}`;
    const event: UploadInput = {
      type: 'uploadAll',
      url,
      method: 'POST',
      fieldName: 'planilla',
      data,
      headers: { Authorization: `Bearer ${token}` }
    };

    this.uploadInput.emit(event);
  }

  downloadErrors(): void {
    if (this.showSpinnerErrors) {
      return;
    }

    this.showSpinnerErrors = true;
    const queryReq = {
      numMovimiento: this.movement,
      codigoRespuesta: this.responseCode
    };
    this._Autoservicios
      .DescargarValidacionPlanillaSCTR(queryReq)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        res => {
          try {
            this.fileUtil.download(
              res.base64,
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              `${this.files[0].name.replace(/\.[^/.]+$/, '')} errores`
            );
          } catch (error) {}
        },
        () => (this.showSpinnerErrors = false),
        () => (this.showSpinnerErrors = false)
      );
  }

  downloadComments(): void {
    if (this.showSpinnerComments) {
      return;
    }

    this.showSpinnerComments = true;
    const queryReq = {
      numMovimiento: this.movement,
      codigoRespuesta: this.responseCode
    };
    this._Autoservicios
      .DescargarValidacionPlanillaSCTR(queryReq)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        res => {
          try {
            this.fileUtil.download(
              res.base64,
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              `${this.files[0].name.replace(/\.[^/.]+$/, '')} observaciones`
            );
          } catch (error) {}
        },
        () => (this.showSpinnerComments = false),
        () => (this.showSpinnerComments = false)
      );
  }

  private _mapResponseTemplate(response: any): void {
    if (!response) {
      return void 0;
    }

    this.movement = response.numMovimiento;
    this.movementNumber.emit(this.movement);
    this.responseCode = response.codigoRespuesta;
    this.workerDeclaredList = response.trabajadoresDeclarados || [];
    switch (response.codigoRespuesta) {
      case 'ERROR':
        this.errorList = response.respuestas;
        break;
      case 'OBSERVADO':
        this.commentList = response.respuestas;
        this.canContinue.emit(true);
        break;
      case 'EXITO':
        this.canContinue.emit(true);
        break;
      default:
        break;
    }
  }
}
