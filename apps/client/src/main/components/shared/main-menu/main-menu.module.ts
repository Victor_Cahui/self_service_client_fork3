import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MainMenuComponent } from './main-menu.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfLoaderModule, DirectivesModule, GoogleModule],
  declarations: [MainMenuComponent],
  exports: [MainMenuComponent]
})
export class MainMenuModule {}
