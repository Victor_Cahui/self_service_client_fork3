import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostBinding, Inject, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuComponent } from '@mx/components/shared/utils/menu';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { ReqPathGetObtenerConfiguracionMenuPorCliente } from '@mx/core/shared/providers/models';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MenuService } from '@mx/services/menu/menu.service';
import { PoliciesService } from '@mx/services/policies.service';
import { MFP_Menu_Principal_7A, MFP_Menu_Principal_Submenu_7B } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES as PT } from '@mx/settings/constants/policy-values';
import { DIGITAL_CLINIC_HEADER, HEALTH_QUOTE_HEADER } from '@mx/settings/constants/router-titles';
import { IMenuItem } from '@mx/statemanagement/models/general.models';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-main-menu',
  templateUrl: './main-menu.component.html'
})
export class MainMenuComponent extends GaUnsubscribeBase implements OnInit {
  static updateView: Subject<boolean> = new Subject();

  @HostBinding('class') class = 'col g-box-nav g-bg-c--gray8';

  showLoading: boolean;
  menuList: Array<IMenuItem> = [];
  isHistorical: boolean;

  ga: Array<IGaPropertie>;

  constructor(
    private readonly menuService: MenuService,
    private readonly renderer2: Renderer2,
    private readonly resizeService: ResizeService,
    private readonly router: Router,
    private readonly _Autoservicios: Autoservicios,
    public activatedRoute: ActivatedRoute,
    private readonly elementRef: ElementRef,
    @Inject(DOCUMENT) private readonly document: Document,
    protected gaService: GAService,
    private readonly policiesService: PoliciesService
  ) {
    super(gaService);
    this.ga = [MFP_Menu_Principal_7A(), MFP_Menu_Principal_Submenu_7B()];
  }

  ngOnInit(): void {
    this.checkHistorical();

    const params: ReqPathGetObtenerConfiguracionMenuPorCliente = {
      codigoApp: APPLICATION_CODE
    };

    this.showLoading = true;
    this._Autoservicios
      .ObtenerConfiguracionMenuPorCliente(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .pipe(map(this._mapMenuResponse.bind(this)))
      .subscribe(
        (response: Array<IMenuItem>) => {
          if (response && response.length) {
            this.menuList = response;
            this.menuService.setRoutesByMenu(this.menuList);
            setTimeout(() => {
              this.handlerSizeMenu();
              this.checkHistorical();
            }, 300);

            this.resizeService.resizeEvent.subscribe(() => {
              this.handlerSizeMenu();
            });

            this.subscribeToPendingPayments();

            // calcula si tiene o no polizas MD_SALUD, MD_EPS para ocultar/mostrar opciones el mapa de clinicas (AUT-)
            const hasHealthPolicies = !this.menuList
              .filter(menuItem => menuItem.policies.some(p => [PT.MD_SALUD.code, PT.MD_EPS.code].includes(p)))
              .find(menuItem => menuItem.route === HEALTH_QUOTE_HEADER.url && !menuItem.children.length);

            // calcula si tiene o no Clinica digital
            const hasDigitalClinic =
              this.menuList.filter(menuItem => menuItem.route === DIGITAL_CLINIC_HEADER.url).length > 0 ? 'S' : 'N';

            this.policiesService.setHasHealthPolicies(hasHealthPolicies);
            this.policiesService.setHasDigitalClinic(hasDigitalClinic);
          }
          this.showLoading = false;
        },
        () => {
          this.menuList = [];
          this.showLoading = false;
        }
      );
  }

  // Fix para la opcion del menu MIS POLIZAS, cuando se ve el detalle de una póliza no vigente (isHistorical = true)
  checkHistorical(): void {
    this.activatedRoute.queryParams.subscribe(data => {
      if (data && data.historical && data.historical === 'true') {
        this.isHistorical = true;
        const myPoliciesMenuOption = this.elementRef.nativeElement.querySelector('[href="#/myPolicies"]');
        myPoliciesMenuOption &&
          setTimeout(() => {
            this.renderer2.addClass(myPoliciesMenuOption, 'active');
          });
      } else {
        this.isHistorical = false;
      }
    });
  }

  subscribeToPendingPayments(): void {
    this.menuService.pendingPayments.subscribe(number => {
      const paymentMenuOption = this.menuList.findIndex(item => item.route === '/payments');
      number && paymentMenuOption !== -1 && (this.menuList[paymentMenuOption].badge = +number === 0 ? '' : number);
    });
  }

  // Modal de Contacto
  openModalContact(): void {
    this.menuService.setModalContact(true);
  }

  // Gestion de menu
  handlerSizeMenu(): void {
    const menu = this.document.getElementById('menu');
    if (menu) {
      this.renderer2.setStyle(menu, 'height', `${window.innerHeight - 97}px`);
    }
  }

  closeNavBox(): void {
    MenuComponent.closeNav();
  }

  closeAndRemoveActive(): void {
    this.collapseAll();
    this.inactiveAll();
  }

  // Click en menu sin opciones
  onClick(): void {
    this.closeAndRemoveActive();
    this.closeNavBox();
  }

  onClickChild(i: number): void {
    this.closeAndRemoveActive();
    this.menuList[i].active = true;
    this.menuList[i].collapse = false;
    this.closeNavBox();
  }

  // Gestion de opciones
  itemCollapse(item: IMenuItem): void {
    const collapse = !item.collapse;
    if (!collapse) {
      this.collapseAll();
    }
    item.collapse = collapse;
  }

  itemTitle(item: IMenuItem): void {
    this.collapseAll();
    this.removeHistoricalActived();
    this.router.navigate([item.children[0].route]);
  }

  collapseAll(): void {
    this.menuList.forEach((menuItem: IMenuItem) => {
      menuItem.collapse = true;
    });
  }

  inactiveAll(): void {
    this.menuList.forEach((menuItem: IMenuItem) => {
      menuItem.active = false;
    });
    this.removeHistoricalActived();
  }

  getCollapse(rla: boolean, collapse: boolean): boolean {
    return this.isHistorical ? this.isHistorical : rla ? false : collapse && !rla;
  }

  removeHistoricalActived(): void {
    const myPoliciesMenuOption = this.elementRef.nativeElement.querySelector('[href="#/myPolicies"]');
    myPoliciesMenuOption &&
      !!myPoliciesMenuOption.classList.contains('active') &&
      myPoliciesMenuOption.classList.remove('active');
  }

  private _mapMenuResponse(response): IMenuItem {
    return response.map(menuOption => {
      const children: IMenuItem = menuOption.opciones.map(item => {
        return {
          policies: item.condicionado,
          name: item.nombre,
          icon: item.icono,
          collapse: item.colapso,
          route: item.ruta,
          hideIcon: !item.verIcono,
          hideNewIcon: !item.verIconoNuevo
        };
      });

      return {
        policies: menuOption.condicionado,
        name: menuOption.nombre,
        icon: menuOption.icono,
        collapse: menuOption.colapso,
        children,
        route: menuOption.ruta,
        hideIcon: !menuOption.verIcono,
        hideNewIcon: !menuOption.verIconoNuevo
      };
    });
  }
}
