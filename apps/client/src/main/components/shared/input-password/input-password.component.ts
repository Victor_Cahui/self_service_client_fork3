import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { DiffControlValidator, MatchValidator, PasswordValidator } from '@mx/core/ui';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-input-password',
  templateUrl: './input-password.component.html'
})
export class InputPasswordComponent implements OnInit {
  @Input() form: FormGroup;
  @Output() changeData: EventEmitter<boolean> = new EventEmitter<boolean>();

  lang = AuthLang;
  newPassword: AbstractControl;
  confirmPassword: AbstractControl;
  generalMaxLength = GENERAL_MAX_LENGTH.password;
  generalMinLength = GENERAL_MIN_LENGTH.updatePassword;
  messagePassword = AuthLang.Messages.PasswordMatch;
  allowInputChecks: boolean;
  validFormSub: Subscription;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  // Formulario
  initForm(): void {
    const group = this.fb.group(
      {
        newPassword: ['', Validators.compose(this.getValidatorsForNewPassword(this.form.get('currentPassword')))],
        confirmPassword: [
          '',
          Validators.compose([
            PasswordValidator.isValid(this.lang.Messages.PasswordIncorrectCondition),
            Validators.required
          ])
        ]
      },
      {
        validator: [MatchValidator.validate('newPassword', 'confirmPassword')]
      }
    );

    this.form.addControl('changePassword', group);
    this.newPassword = this.form.controls['changePassword'].get('newPassword');
    this.confirmPassword = this.form.controls['changePassword'].get('confirmPassword');
  }

  showErrors(controlKey: string): boolean {
    return FormComponent.showBasicErrors(this.form.get('changePassword') as FormGroup, controlKey);
  }

  getValidatorsForNewPassword(hasCurrentPasswordInput): any[] {
    const defaultValidators = [
      PasswordValidator.isValid(this.lang.Messages.PasswordIncorrectCondition),
      Validators.required
    ];

    return hasCurrentPasswordInput
      ? defaultValidators.concat(DiffControlValidator.validate(this.form.get('currentPassword')))
      : defaultValidators;
  }

  resetForm(): void {
    FormComponent.setControl(this.newPassword, '');
    FormComponent.setControl(this.confirmPassword, '');
  }
}
