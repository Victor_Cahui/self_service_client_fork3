import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { Content } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-card-sctr-period-confirm',
  templateUrl: './card-sctr-period-confirm.component.html'
})
export class CardSctrPeriodConfirmComponent implements OnInit {
  @Input() content: Content;
  @Input() paymentNumber: string;
  @Input() documentData: string;

  labelBankPaymentCode = PaymentLang.Labels.BankPaymentCode;
  labelDocument = PaymentLang.Labels.PaymentDocument;
  digitalPlatform = PaymentLang.Labels.DigitalPlatform;

  constructor(private readonly router: Router) {}

  ngOnInit(): void {}

  goToAction(): void {
    if (this.content.action.routeLink) {
      this.router.navigate([this.content.action.routeLink]);
    }
  }
}
