import { Component, Input } from '@angular/core';
import { HELMET_OUT_ICON } from '@mx/settings/constants/images-values';
import { ISctrPeriodInfoCard } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-sctr-period-info',
  templateUrl: './card-sctr-period-info.component.html'
})
export class CardSctrPeriodInfoComponent {
  @Input() periodInfo: ISctrPeriodInfoCard;

  fileIcon = HELMET_OUT_ICON;
}
