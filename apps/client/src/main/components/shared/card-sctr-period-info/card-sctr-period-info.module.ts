import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardSctrPeriodInfoComponent } from './card-sctr-period-info.component';

@NgModule({
  imports: [CommonModule],
  exports: [CardSctrPeriodInfoComponent],
  declarations: [CardSctrPeriodInfoComponent]
})
export class CardSctrPeriodInfoModule {}
