import { Component, Input, OnInit } from '@angular/core';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { IModalHowWorks } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-how-works',
  templateUrl: './how-works.component.html'
})
export class HowWorksComponent implements OnInit {
  @Input() data: IModalHowWorks;
  @Input() stepsClass: string;
  status = NotificationStatus.INFO;

  ngOnInit(): void {}
}
