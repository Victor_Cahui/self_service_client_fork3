import { Component, Input } from '@angular/core';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';

@Component({
  selector: 'client-icon-payment-type',
  template: `
    <span class="pr-1" [ngClass]="getClassIcon(paymentType)"></span>
  `
})
export class IconPaymentTypeComponent {
  @Input() paymentType: string;

  getClassIcon(paymentType: string): string {
    return PolicyUtil.getPaymentTypeIcon(paymentType);
  }
}
