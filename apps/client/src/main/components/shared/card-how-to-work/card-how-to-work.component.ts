import { Component, Input } from '@angular/core';
import { ContentHowtoWork } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-how-to-work',
  templateUrl: './card-how-to-work.component.html'
})
export class CardHowToWorkComponent {
  @Input() content: ContentHowtoWork;
}
