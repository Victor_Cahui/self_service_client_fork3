import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { CardSctrPeriodRisksComponent } from './card-sctr-period-risks.component';

@NgModule({
  imports: [CommonModule, MfLoaderModule],
  declarations: [CardSctrPeriodRisksComponent],
  exports: [CardSctrPeriodRisksComponent]
})
export class CardSctrPeriodRisksModule {}
