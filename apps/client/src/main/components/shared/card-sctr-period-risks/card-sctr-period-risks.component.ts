import { Component, Input, OnInit } from '@angular/core';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { ICardPeriodRisks } from '@mx/statemanagement/models/sctr.interface';

@Component({
  selector: 'client-card-sctr-period-risks',
  templateUrl: './card-sctr-period-risks.component.html'
})
export class CardSctrPeriodRisksComponent implements OnInit {
  @Input() policyNumber: string;
  @Input() isDeclaration: boolean;

  showLoading: boolean;
  risksHealth: Array<any> = [];
  risksPension: Array<any> = [];
  lang = PERIODS_LANAG;

  constructor(protected authService: AuthService, protected _Autoservicios: Autoservicios) {}

  ngOnInit(): void {
    this.loadInfoRiskSctr();
  }

  loadInfoRiskSctr(): void {
    const auth = this.authService.retrieveEntity();
    const pathReq = { numeroPoliza: this.policyNumber };
    const queryReq = {
      codigoApp: APPLICATION_CODE,
      tipoDocumento: auth.type,
      documento: auth.number
    };
    this.showLoading = true;
    this._Autoservicios.ListarRiesgoSCTR(pathReq, queryReq).subscribe(
      (risks: ICardPeriodRisks) => {
        risks && risks.riesgoSalud && (this.risksHealth = risks.riesgoSalud);
        risks && risks.riesgoPension && (this.risksPension = risks.riesgoPension);
      },
      () => (this.showLoading = false),
      () => (this.showLoading = false)
    );
  }
}
