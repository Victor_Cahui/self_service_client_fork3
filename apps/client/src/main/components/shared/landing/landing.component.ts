import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ContentLanding } from '@mx/statemanagement/models/general.models';

declare var SimpleBar;

@Component({
  selector: 'client-landing',
  templateUrl: './landing.component.html'
})
export class LandingComponent extends GaUnsubscribeBase implements OnInit, AfterViewInit {
  @Input() landingContent: ContentLanding;
  @Input() hasCountriesPhones: boolean;
  @ViewChild('simplebar') simpleBar: ElementRef;

  labelSelect = GeneralLang.Labels.Select;
  sections: Array<any> = [];
  currentSection: string;

  // Mobile
  showSection;
  numberSection;

  constructor(protected gaService: GAService) {
    super(gaService);
  }

  ngAfterViewInit(): void {
    // Controlar movimiento del scroll con el simplebar
    this.currentSection = 'section1';
    this.simpleBar && this._scroll();
  }

  ngOnInit(): void {
    this.currentSection = 'section1';
    // Toma título de seccion para tab y select
    this.landingContent.sections.forEach((section, index) => {
      const i = index + 1;
      this.sections.push({ text: `${i}. ${section.nameSection}`, value: i });
    });
    // Mobile
    this.initMobile(1);
  }

  // Tabs Web
  scrollTo(section: string): void {
    const sectionId = `section${section}`;
    document.querySelector(`#${sectionId}`).scrollIntoView({ behavior: 'smooth', block: 'start' });
  }

  onSectionChange(section: string): void {
    this.currentSection = section;
  }

  // Mobile
  selectChanged(event): void {
    const section = event.target.value;
    this.initMobile(section);
    this.onSectionChange(section);
  }

  initMobile(i): void {
    // tslint:disable-next-line: no-parameter-reassignment
    i = parseInt(i, 10);
    this.showSection = this.landingContent.sections[i - 1];
    this.numberSection = i;
  }

  // Sección Activa en scroll
  activeSection(event): string {
    const elements = event.children;
    const currentPosition = event.scrollTop;
    const maxScroll = event.scrollHeight;
    const elementLength = elements.length;

    for (let i = elementLength; i--; ) {
      const element = elements[i];
      if (element.tagName === 'SECTION') {
        const prevElement = elements[i - 1];
        if (
          (typeof prevElement === 'undefined' || currentPosition > prevElement.offsetTop) &&
          currentPosition <= element.offsetTop
        ) {
          return element.id;
        }
      }
    }

    // tslint:disable-next-line: restrict-plus-operands
    if (currentPosition + event.clientHeight >= maxScroll) {
      return elements[elementLength - 1].id;
    }

    return this.currentSection;
  }

  private _scroll(): void {
    const scrollBar = new SimpleBar(this.simpleBar.nativeElement);
    scrollBar.getScrollElement().addEventListener('scroll', event => {
      const currentSection = this.activeSection(event.target);
      if (currentSection !== this.currentSection) {
        this.onSectionChange(currentSection);
      }
    });
  }
}
