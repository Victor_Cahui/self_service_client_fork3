import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardFaqComponent } from './card-faq.component';

@NgModule({
  imports: [CommonModule, LinksModule, GoogleModule],
  exports: [CardFaqComponent],
  declarations: [CardFaqComponent]
})
export class CardFaqModule {}
