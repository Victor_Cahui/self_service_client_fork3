import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { DropdownComponent } from './dropdown.component';

@NgModule({
  imports: [CommonModule, LinksModule, DirectivesModule],
  declarations: [DropdownComponent],
  exports: [DropdownComponent]
})
export class DropdownModule {}
