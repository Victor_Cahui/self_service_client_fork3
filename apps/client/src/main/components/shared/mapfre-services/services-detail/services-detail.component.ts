import { DOCUMENT, Location } from '@angular/common';
import { Component, ElementRef, HostBinding, Inject, Input, OnInit, Renderer2 } from '@angular/core';
import { GmMsBase } from '@mx/components/gm-ms-views/gm-ms-base';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { GeneralService } from '@mx/services/general/general.service';
import { IHeader } from '@mx/services/general/header-helper.service';
import { PHONE_SIS24, SERVICE_FORM_STYLE } from '@mx/settings/constants/key-values';
import { ISummaryView } from '@mx/statemanagement/models/service.interface';

@Component({
  selector: 'client-services-detail',
  templateUrl: './services-detail.component.html'
})
export class ServicesDetailComponent extends GmMsBase implements OnInit {
  @HostBinding('attr.class') attr_class = 'g-bg-c--white1 h-100 g-box--shadow1 g-map-filter--h100';

  @Input() data: ISummaryView;
  @Input() header: IHeader;
  status = NotificationStatus.INFO;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    private readonly location: Location,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(
      render2,
      elementRef,
      platformService,
      resizeService,
      generalService,
      document,
      SERVICE_FORM_STYLE.WORK_DEFAULT
    );
  }

  ngOnInit(): void {
    this.attr_class.concat(' d-none');
    this.setClassSizeMap();
    if (this.data.warning) {
      this.parametersSubject = this.generalService.getParametersSubject();
      this.parametersSubject.subscribe(params => {
        const phone = this.generalService.getValueParams(PHONE_SIS24);
        this.data.warning = this.data.warning.replace('{{phone}}', phone);
      });
    }
  }

  close(): void {
    this.location.back();
  }
}
