import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui/lib/directives/directives.module';
import { ServiceListItemComponent } from './service-list-item.component';

@NgModule({
  imports: [CommonModule, DirectivesModule],
  exports: [ServiceListItemComponent],
  declarations: [ServiceListItemComponent]
})
export class ServiceListItemModule {}
