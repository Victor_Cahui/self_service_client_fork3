import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { CarouselModule } from '@mx/core/ui/lib/components/carousel/carousel.module';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { CardInsurancePlansComponent } from './card-insurance-plans.component';

@NgModule({
  imports: [CommonModule, MfButtonModule, CarouselModule, GoogleModule],
  exports: [CardInsurancePlansComponent],
  declarations: [CardInsurancePlansComponent]
})
export class CardInsurancePlansModule {}
