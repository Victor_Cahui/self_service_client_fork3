import { Component, Input } from '@angular/core';
import { PaymentsOptions } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-payment-options',
  templateUrl: './card-payment-options.component.html'
})
export class CardPaymentOptionsComponent {
  @Input() content: PaymentsOptions;
}
