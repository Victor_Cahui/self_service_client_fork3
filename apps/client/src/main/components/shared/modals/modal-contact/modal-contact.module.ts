import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { ModalContactComponent } from './modal-contact.component';
@NgModule({
  imports: [CommonModule, RouterModule, MfModalModule],
  exports: [ModalContactComponent],
  declarations: [ModalContactComponent]
})
export class ModalContactModule {}
