import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { MFP_FAQS_11B } from '@mx/settings/constants/events.analytics';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Faq } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-modal-more-faqs',
  templateUrl: 'modal-more-faqs.component.html'
})
export class ModalMoreFaqsComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;
  @Input() faqs: Array<Faq>;
  @Input() subTitle: string;
  faqList: Array<Faq>;
  title = GeneralLang.Titles.Faq.toUpperCase();

  constructor(private readonly elementRef: ElementRef, protected gaService?: GAService) {
    super(gaService);
  }

  ngOnInit(): void {}

  open(): void {
    this.mfModal.open();
    setTimeout(() => {
      this.handlerClass();
      this.faqList = (this.faqs || []).map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
    }, 2);
  }

  handlerClass(): void {
    try {
      const ref = this.elementRef.nativeElement.getElementsByClassName('g-full-modal--wrapper')[0];
      ref.classList.add('g-full-modal--faqs');
    } catch (error) {}
  }

  closeEvent(): void {
    this.addGAEvent(MFP_FAQS_11B(), { buttonText: 'Cerrar' });
  }
}
