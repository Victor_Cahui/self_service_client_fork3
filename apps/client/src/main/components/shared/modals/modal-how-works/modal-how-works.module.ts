import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HowWorksModule } from '@mx/components/shared/how-works/how-works.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { ModalHowWorksComponent } from './modal-how-works.component';

@NgModule({
  imports: [CommonModule, MfModalModule, HowWorksModule],
  exports: [ModalHowWorksComponent],
  declarations: [ModalHowWorksComponent]
})
export class ModalHowWorksModule {}
