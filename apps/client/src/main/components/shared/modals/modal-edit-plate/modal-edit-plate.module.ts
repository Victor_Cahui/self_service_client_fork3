import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { FormEditPlateQuoteModule } from '../../form-edit-plate-quote/form-edit-plate-quote.module';
import { ModalEditPlateComponent } from './modal-edit-plate.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfModalModule, MfButtonModule, FormEditPlateQuoteModule, MfButtonModule],
  exports: [ModalEditPlateComponent],
  declarations: [ModalEditPlateComponent]
})
export class ModalEditPlateModule {}
