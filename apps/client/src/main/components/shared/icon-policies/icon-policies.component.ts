import { Component, Input, OnChanges } from '@angular/core';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';

@Component({
  selector: 'client-icon-policies',
  template: `
    <span [class]="customClass" [ngClass]="icon"></span>
  `
})
export class IconPoliciesComponent implements OnChanges {
  @Input() customIcon: string;
  @Input() policyType: string;
  @Input() customClass = 'g-c--gray18 g-items-icon g-font--30 g-font-lg--40 mt-1';
  icon: string;

  ngOnChanges(): void {
    const policyType = POLICY_TYPES[this.policyType];
    this.icon = this.customIcon || (policyType ? policyType.icon : '');
  }
}
