import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconPoliciesComponent } from './icon-policies.component';

@NgModule({
  imports: [CommonModule],
  declarations: [IconPoliciesComponent],
  exports: [IconPoliciesComponent]
})
export class IconPoliciesModule {}
