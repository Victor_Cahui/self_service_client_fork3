import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';

import { CardSctrPeriodItemDetailModule } from './card-sctr-period-item-detail/card-sctr-period-item-detail.module';
import { CardSctrPeriodItemComponent } from './card-sctr-period-item.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    LinksModule,
    RouterModule,
    MfLoaderModule,
    MfButtonModule,
    GoogleModule,
    CardSctrPeriodItemDetailModule,
    IconPoliciesModule
  ],
  declarations: [CardSctrPeriodItemComponent],
  exports: [CardSctrPeriodItemComponent]
})
export class CardSctrPeriodItemModule {}
