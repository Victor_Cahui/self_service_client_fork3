import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { CardContentModule } from '../card-content/card-content.module';
import { FoilAttachedComponent } from './foil-attached.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfLoaderModule, CardContentModule],
  exports: [FoilAttachedComponent],
  declarations: [FoilAttachedComponent]
})
export class FoilAttachedModule {}
