import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { DigitalClinicNoServiceComponent } from './digital-clinic-no-service.component';

@NgModule({
  imports: [CommonModule, MfLoaderModule],
  declarations: [DigitalClinicNoServiceComponent],
  exports: [DigitalClinicNoServiceComponent]
})
export class DigitalClinicNoServiceModule {}
