import { Component, OnInit, ViewChild } from '@angular/core';
import { FormContactedByAgentComponent } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.component';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { PoliciesService } from '@mx/services/policies.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { LANDING_ID, POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { CONTACTED_BY_AGENT } from '@mx/settings/lang/contact';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IContactData } from '@mx/statemanagement/models/client.models';
import { IContactedByAgent } from '@mx/statemanagement/models/general.models';
import { IAgent, IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-modal-contacted-by-agent-compare',
  templateUrl: 'modal-contacted-by-agent-compare.component.html',
  styles: [
    `
      /deep/ .row {
        margin-right: 0;
        margin-left: 0;
      }
    `
  ]
})
export class ModalContactedByAgentCompareComponent implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(FormContactedByAgentComponent) formContactedByAgentComponent: FormContactedByAgentComponent;

  title: string;
  message: string;
  titleMobile: string;
  subTitleMobile: string;

  policySelected: string;
  policyType: string;
  dataPolicy: IPoliciesByClientResponse;
  agent: IAgent;
  hasAgent: boolean;
  showMessage: boolean;
  showFail: boolean;
  editFooter: boolean;
  disableButtonSend: boolean;
  labelAgent = PolicyLang.Labels.Agent;
  btnConfirm = GeneralLang.Buttons.Confirm;
  titleAlertError = GeneralLang.Alert.titleFail;
  form: IContactedByAgent = CONTACTED_BY_AGENT;
  messageAlertError: string;
  policiesServiceSub: Subscription;
  showLoading: boolean;
  contactData: IContactData;
  referred = false;

  constructor(private readonly policyListService: PolicyListService, protected policiesService: PoliciesService) {}

  ngOnInit(): void {
    this.initButton();
  }

  initButton(): void {
    this.disableButtonSend = true;
    this.editFooter = false;
  }

  open(): void {
    this.showMessage = true;
    this.showFail = false;
    this.mfModal.open();
    this.subTitleMobile = PolicyLang.Titles.ComparePolicy.toUpperCase();
    this.title = GeneralLang.Messages.EstimatedUser;
    this.policySelected = this.policyListService.getPolicySelected();
    this.dataPolicy = this.policyListService.getDataPolicy(this.policySelected);
    if (!this.dataPolicy || !this.policySelected) {
      this.hasAgent = false;
      this.policyType = POLICY_TYPES.MD_AUTOS.code;
      this.message = PolicyLang.Messages.NoCompareForm;
      this.openContactAgentForm();
    } else {
      this.policyType = this.dataPolicy.tipoPoliza;
      // TODO: Evaluar para futuros: por ahora solo HOGAR va a referidos.
      this.referred = this.policyType === POLICY_TYPES.MD_HOGAR.code;
      this.getDataAgent();
    }
    this.titleMobile = POLICY_TYPES[this.policyType].title || '';
  }

  doSuccess(): void {
    this.showMessage = false;
    this.disableButtonSend = false;
    this.editFooter = true;
  }

  showError(message: string): void {
    this.showFail = true;
    this.showMessage = true;
    this.editFooter = true;
    this.disableButtonSend = false;
    this.title = GeneralLang.Messages.Fail;
    this.message = message;
    this.closeEvent();
  }

  closeEvent(): void {
    if (!this.hasAgent && this.formContactedByAgentComponent) {
      this.formContactedByAgentComponent.formContact.reset();
      this.formContactedByAgentComponent.isSent = false;
    }
  }

  onSend(event: boolean): void {
    this.disableButtonSend = true;
    this.showMessage = false;
    this.editFooter = true;
    if (event) {
      this.formContactedByAgentComponent.sendMessage();
    }
  }

  onConfirm(): void {
    this.initButton();
    this.mfModal.close();
  }

  enableButton(valid: boolean): void {
    if (!this.showFail) {
      this.disableButtonSend = !valid;
    }
  }

  // Buscar datos del agente
  getDataAgent(): void {
    const params = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policySelected
    };
    this.showLoading = true;
    this.policiesService.getAgentByPolicy(params).subscribe(
      response => {
        if (response) {
          this.policyListService.setAgent(response.agente);
          this.agent = response.agente;
          this.hasAgent = !!(this.agent && this.agent.nombre);
        }
        this.message = this.hasAgent ? PolicyLang.Messages.NoCompareAgent : PolicyLang.Messages.NoCompareForm;
        if (!this.hasAgent) {
          if (this.referred) {
            this.contactData = {
              policyNumber: this.policySelected,
              landingId: LANDING_ID.HOME_MULTIRISK
            };
          }
          this.openContactAgentForm();
        } else {
          this.editFooter = true;
          this.disableButtonSend = false;
        }
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  openContactAgentForm(): void {
    this.contactData = {
      subject: POLICY_TYPES[this.policyType].code.replace('MD_', ''),
      description: PolicyLang.Texts.ContactReason.ComparePolicy
    };
    setTimeout(() => {
      this.formContactedByAgentComponent.initForm();
    }, 100);
  }
}
