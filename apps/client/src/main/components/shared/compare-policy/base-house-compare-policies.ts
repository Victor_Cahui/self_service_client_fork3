import { DecimalPipe } from '@angular/common';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HomeService } from '@mx/services/home/home.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import {
  ICompareDetailView,
  ICompareHeaderHomeResponse,
  ICompareHeaderView,
  ICoverageGroupHomeResponse,
  ICoverageGroupView,
  ICoveragesHomeResponse,
  ITextCoveragesCompare
} from '@mx/statemanagement/models/policy.interface';
import { zip } from 'rxjs';
import { BaseComparePolicies } from './base-compare-policies';

export abstract class BaseHouseComparePolicies extends BaseComparePolicies {
  constructor(
    protected policyListService: PolicyListService,
    protected headerHelperService: HeaderHelperService,
    protected quoteService: PoliciesQuoteService,
    protected homeService: HomeService,
    protected decimalPipe: DecimalPipe
  ) {
    super(policyListService, headerHelperService, quoteService, decimalPipe);
  }

  // Vista Inicial
  viewInitial(): void {
    this.policySelected = this.policyListService.getPolicySelected();
    this.params = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policySelected
    };
    if (this.compare) {
      this.quoteService.setHeaderData(null);
      this.quoteService.setDetailsData(null);
    }

    this.compareHeader$ = this.homeService.getCompareHeader(this.params);
    this.compareDetails$ = this.homeService.getCompareDetails(this.params);

    // Datos de cuadro
    this.getDataCompare();
  }

  // Buscar datos para cuadro si hay cambios en la data
  getDataCompare(): void {
    const header = this.quoteService.getHeaderData();
    const details = this.quoteService.getDetailsData();
    if (header && header.length && details && details.coberturas.length) {
      this.setSelectMonth(header);
      this.setHeader(header);
      this.setDetails(details);
    } else {
      // Va a servicio
      this.showLoading = true;
      this.quoteService.setHeaderData(null);
      this.quoteService.setDetailsData(null);
      // tslint:disable-next-line: deprecation
      this.zipSub = zip(this.compareHeader$, this.compareDetails$).subscribe(
        res => {
          if (res[0] && res[0].length) {
            // Select
            this.setSelectMonth(res[0]);
            // Header
            this.quoteService.setHeaderData(res[0]);
            this.setHeader(res[0]);
          }
          // Detalle
          if (res[1]) {
            this.quoteService.setDetailsData(res[1]);
            this.setDetails(res[1]);
          }
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.zipSub.unsubscribe();
          this.showLoading = false;
        }
      );
    }
  }

  // Datos para Select de numero de cuotas y radio button de cobertura
  setSelectMonth(data: Array<ICompareHeaderHomeResponse>): void {
    this.monthlyFees = data[0].cuotas.map(fee => {
      return {
        text: fee.descripcion,
        value: fee.numero.toString()
      };
    });
    this.timeList = this.coverageTimeList;
    this.setDataSelect(data[0].cuotas[0].numero, this.yearDefault);
  }

  // Header
  setHeader(data: Array<ICompareHeaderHomeResponse>): void {
    this.coveragesHeader = data.map((modality: ICompareHeaderHomeResponse, index: number) => {
      return this.setHeaderModality(modality, index);
    });
    this.headerLength = this.coveragesHeader ? this.coveragesHeader.length : 0;
  }

  setHeaderModality(modality: ICompareHeaderHomeResponse, index: number): ICompareHeaderView {
    const symbol = `${StringUtil.getMoneyDescription(modality.cuotas[0].codigoMoneda)} `;
    const year = parseInt(this.timeSelected[index], 10);
    const cost = this.getMonthlyCost(modality, this.monthSelected, year);
    const quoteValue = this.getQuoteCost(modality, this.monthSelected, year);
    const policyCurrent = modality.flagIndicadorPoliza === this.policyIndicator.MY_POLICY;

    return {
      company: modality.codigoCompania,
      branch: modality.codigoRamo,
      modality: modality.codigoModalidad,
      fractionationCode: this.getFractionationCode(modality, this.monthSelected, year),
      title: modality.nombreProducto,
      moneySymbol: symbol,
      monthlyCost: cost,
      quote: quoteValue,
      requestYears: false,
      titleTooltips: modality.observaciones ? modality.observaciones.titulo : '',
      contentTooltips: modality.observaciones ? modality.observaciones.detalles : [],
      years: year,
      amountFee: this.monthSelected,
      textValue: this.getTextValue(),
      textSumaryValue: this.getTextSumaryValue(symbol, cost),
      current: policyCurrent,
      textChangeModality: PolicyLang.Buttons.ContactToImprove,
      changeModality: !policyCurrent && modality.flagPuedeCambiarPoliza,
      recommended: modality.flagIndicadorPoliza === this.policyIndicator.RECOMMENDED,
      textPaymentFrecuency: policyCurrent ? '' : this.getTextPaymentFrecuency(),
      quoteNumber: modality.numeroCotizacion
    };
  }

  // Detalle
  setDetails(data: Array<ICoveragesHomeResponse>): void {
    this.coverageList = [];
    this.deductibleList = [];
    this.coverageGroupList = [];
    data.forEach((coverages: ICoveragesHomeResponse) => {
      this.coverageGroupList.push(this.setDetailsModality(coverages));
    });
  }

  setDetailsModality(group: ICoveragesHomeResponse): ICoverageGroupView {
    return {
      name: group.nombre,
      coverages: this.setDetailsGroupCoverages(group.coberturas),
      collapse: true
    };
  }

  setDetailsGroupCoverages(coverages: Array<ICoverageGroupHomeResponse>): Array<ICompareDetailView> {
    const coverageList: Array<ICompareDetailView> = [];
    let coveragesOptions: Array<ITextCoveragesCompare> = [];
    let deduciblesOptions: Array<ITextCoveragesCompare> = [];
    let optionsArray = [];
    let deductibles: boolean;

    coverages.forEach(coverage => {
      const products = coverage.productos.slice(0, this.headerLength);
      products.forEach(producto => {
        coveragesOptions = [];
        deduciblesOptions = [];

        producto.deducibles.forEach((valor, i) => {
          // Primera a la vista
          if (i === 0) {
            coveragesOptions.push({
              descripcion: valor.descripcion.titulo,
              tipo: valor.tipo,
              detalles: valor.descripcion.detalles
            });
            // Resto collapsed
          } else {
            deductibles = true;
            deduciblesOptions.push({
              descripcion: valor.descripcion.titulo,
              tipo: valor.tipo,
              detalles: valor.descripcion.detalles
            });
          }
        });
        optionsArray.push({ coverages: coveragesOptions, deductibles: deduciblesOptions });
      });

      coverageList.push({
        codCoverages: coverage.codigo,
        title: coverage.nombre,
        titleTooltips: coverage.observaciones ? coverage.observaciones.titulo : '',
        contentTooltips: coverage.observaciones ? coverage.observaciones.detalles : [],
        options: optionsArray,
        hasDeductibles: deductibles
      });
      optionsArray = [];
      deductibles = false;
    });

    return coverageList;
  }

  // Cambiar número de cuotas
  changeMonthlyFee(month): void {
    this.monthSelected = parseInt(month, 10);
    const data = this.quoteService.getHeaderData();
    if (data) {
      this.setHeader(data);
    }
  }
}
