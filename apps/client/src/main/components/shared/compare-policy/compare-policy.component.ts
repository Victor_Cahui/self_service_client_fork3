import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MfModalMessage } from '@mx/core/ui';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { WARNING_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { ICardPoliciesView } from '@mx/statemanagement/models/policy.interface';
import { BaseCardMyPolicies } from '../../card-my-policies/base-card-my-policies';
import { ModalContactedByAgentCompareComponent } from './modal-contacted-by-agent-compare/modal-contacted-by-agent-compare.component';
import { ModalSelectCompareComponent } from './modal-select-compare/modal-select-compare.component';

@Component({
  selector: 'client-compare-policy',
  templateUrl: 'compare-policy.component.html'
})
export class ComparePolicyComponent extends BaseCardMyPolicies implements OnInit {
  @ViewChild(ModalSelectCompareComponent) mfModalSelect: ModalSelectCompareComponent;
  @ViewChild(ModalContactedByAgentCompareComponent) mfModalAgent: ModalContactedByAgentCompareComponent;
  @ViewChild(MfModalMessage) mfModalMessage: MfModalMessage;
  @Input() policyType: string;

  openModalSelect: boolean;
  openModalAgent: boolean;
  listPolicies: Array<ICardPoliciesView> = [];
  showModalError: boolean;
  modalTitle: string;
  modalMessage: string;
  iconError = WARNING_ICON;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    public router: Router,
    private readonly policyListService: PolicyListService
  ) {
    super(generalService, policiesService);
  }

  ngOnInit(): void {}

  open(): void {
    this.listPolicies = this.policyListService.getDataPolicies();

    // Filtra vencidas
    this.listPolicies = this.listPolicies.filter(policy => !policy.expired);

    // No hay polizas vigentes
    if (!this.listPolicies.length) {
      this.openModalMessage(PolicyLang.Messages.FailCompareExpiredPolicy);

      return;
    }

    // Varias pólizas
    if (this.listPolicies.length > 1) {
      this.policySelected = '';
      this.openModalSelect = true;
      setTimeout(() => {
        this.mfModalSelect.open();
      }, 100);
    } else {
      const policy = this.listPolicies[0];
      this.policyListService.setPolicySelected(policy.policyNumber);

      // Verifica cantidad de vehiculos
      if (policy.beneficiaries.length > 1) {
        this.openModalAgent = true;
        setTimeout(() => {
          this.mfModalAgent.open();
        }, 100);

        return;
      }
      // Verifica si está vigente
      if (policy.expired) {
        this.openModalMessage(PolicyLang.Messages.FailCompareExpiredPolicy);

        return;
      }
      // Verifica si puede comparar
      if (policy.compare && policy.compare.modalityInProgress) {
        this.openModalMessage(PolicyLang.Messages.FailComparePolicy);

        return;
      }

      // Va a comparar
      const path = `${this.getLink(policy.policyType, 'compare')}/${policy.policyNumber}`;
      if (path) {
        this.router.navigate([path]);
      }
    }
  }

  openModalMessage(message: string): void {
    this.showModalError = true;
    this.modalMessage = message;
    this.modalTitle = GeneralLang.Messages.EstimatedUser;
    setTimeout(() => {
      this.mfModalMessage.open();
    }, 50);
  }
}
