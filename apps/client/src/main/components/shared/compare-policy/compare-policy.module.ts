import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// tslint:disable-next-line: max-line-length
import { ModalContactedByAgentCompareModule } from '@mx/components/shared/compare-policy/modal-contacted-by-agent-compare/modal-contacted-by-agent-compare.module';
import { ModalSelectCompareModule } from '@mx/components/shared/compare-policy/modal-select-compare/modal-select-compare.module';
import { MfModalMessageModule } from '@mx/core/ui';
import { ComparePolicyComponent } from './compare-policy.component';

@NgModule({
  imports: [CommonModule, ModalSelectCompareModule, MfModalMessageModule, ModalContactedByAgentCompareModule],
  exports: [ComparePolicyComponent],
  declarations: [ComparePolicyComponent]
})
export class ComparePolicyModule {}
