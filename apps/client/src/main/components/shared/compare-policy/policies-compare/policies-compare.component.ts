import { DecimalPipe } from '@angular/common';
import { Component, ElementRef, EventEmitter, HostBinding, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { MfTabTop } from '@mx/core/ui';
import { CarouselComponent } from '@mx/core/ui/lib/components/carousel/carousel.component';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { CHECK_ICON, CLOSE_ICON, EDIT_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { ISelectList } from '@mx/statemanagement/models/general.models';
import {
  ICompareDetailView,
  ICompareHeaderView,
  ICoverageGroupView
} from '@mx/statemanagement/models/policy.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-policies-compare',
  templateUrl: './policies-compare.component.html',
  providers: [DecimalPipe]
})
export class PoliciesCompareComponent extends GaUnsubscribeBase implements OnInit {
  @HostBinding('class') class = 'w-100';
  @ViewChild(CarouselComponent) carousel: CarouselComponent;
  @ViewChild(MfTabTop) tabTop: MfTabTop;
  @ViewChild('headerContent') headerContent: ElementRef;

  @Input() coverageList: Array<ICoverageGroupView>;
  @Input() deductibleList: Array<ICompareDetailView>;
  @Input() coveragesHeader: Array<ICompareHeaderView>;
  @Input() monthlyFees: Array<ISelectList>;
  @Input() timeList: Array<ISelectList>;
  @Input() titleEdit: string;
  @Input() showSpinner: boolean;
  @Input() compare = true;
  @Input() showPaymentPlan = true;
  @Input() monthSelected: number;
  @Input() timeSelected: Array<string>;
  @Output() purchase: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeModality: EventEmitter<any> = new EventEmitter<any>();
  @Output() editRisk: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeMonthlyFee: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeTime: EventEmitter<any> = new EventEmitter<any>();

  lang = PolicyLang;
  btnPurchase = GeneralLang.Buttons.Purchase;
  btnEdit = GeneralLang.Buttons.Edit;
  textAmount: string;
  hideCoverages: boolean;
  hideDeductibles: boolean;
  list: Array<ITabItem>;
  selectedTab = 0;
  currentSlide = 0;
  isDesktop: boolean;
  offsetTop: number;
  titleTable: string;
  columns: number;
  iconEdit = EDIT_ICON;
  iconCheck = CHECK_ICON;
  iconClose = CLOSE_ICON;

  constructor(
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected quoteService: PoliciesQuoteService,
    private readonly decimalPipe: DecimalPipe,
    protected gaService: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.list = this.coveragesHeader.map((item, index) => {
      return { name: item.title, value: index, visible: true };
    });
    // tslint:disable-next-line: restrict-plus-operands
    setTimeout(() => (this.offsetTop = this.headerContent.nativeElement.offsetHeight + (this.compare ? 228 : 285)));
    this.titleTable = this.compare ? this.lang.Titles.CompareYourPolicy : this.lang.Titles.ChoiseYourInsurance;
    this.resizeService.resizeEvent.subscribe(() => (this.isDesktop = this.platformService.isDesktop));
    this.columns = this.coveragesHeader ? this.coveragesHeader.length : 3;
    // Slider por defecto
    this.getSelectedTab();
  }

  showDeductibles(item): void {
    item.collapse = !item.collapse;
  }

  trackByFn(index, item): any {
    return index;
  }

  // Tab seleccionado - responsive
  onSelectTab(option: number): void {
    this.selectedTab = option;
    this.textAmount = this.getTextAmount();
    this.carousel.goTo(this.selectedTab);
  }

  // Cambiar de modalidad carrousel
  onChangeSlide(option: number): void {
    this.selectedTab = option;
    this.textAmount = this.getTextAmount();
  }

  // Cambiar de Modalidad
  onChange(item: ICompareHeaderView): void {
    this.changeModality.emit(item);
  }

  // Comprar Póliza
  onPurchase(item: ICompareHeaderView, i: number): void {
    if (this.ga && this.ga.find(c => c.control === 'buttonComparePay')) {
      const ga = this.ga.find(c => c.control === 'buttonComparePay');
      const month = this.monthlyFees.filter(m => m.value === this.monthSelected.toString());
      this.addGAEvent(ga, {
        currency: item.moneySymbol,
        valorCobertura: item.monthlyCost,
        cobertura: item.title,
        planDePago: month[0].text
      });
    }

    this.purchase.emit({ data: item, tab: i });
  }

  // Editar Bien
  onEditRisk(item): void {
    this.editRisk.emit(item);
  }

  // Cambiar Cuota Mensual
  onChangeMonthlyFees(event): void {
    const month = event.target.value;
    this.changeMonthlyFee.emit(month);
  }

  // Cambiar Duracion
  onChangeTime(time, i): void {
    this.changeTime.emit({ years: time, index: i });
    setTimeout(() => {
      this.textAmount = this.getTextAmount();
    }, 100);
  }

  // Para tooltips
  isArrayContent(content): boolean {
    return Array.isArray(content);
  }

  // Costo de modalidad activa
  getTextAmount(): string {
    return (
      `${this.coveragesHeader[this.selectedTab].moneySymbol}
      ${this.decimalPipe.transform(this.coveragesHeader[this.selectedTab].monthlyCost, '1.2-2')}` || null
    );
  }

  // Tab Seleccionado por defecto responsive
  getSelectedTab(): void {
    const quote = this.quoteService.getChoiseData();
    if (quote && quote.selectedTab >= 0) {
      this.currentSlide = quote.selectedTab;
      this.selectedTab = quote.selectedTab;
    } else {
      const recommended = this.coveragesHeader.findIndex(m => m.recommended);
      this.selectedTab = recommended > 0 ? recommended : 0;
      this.currentSlide = this.selectedTab;
    }
    this.textAmount = this.getTextAmount();
  }
}
