import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BaseCardMyPolicies } from '@mx/components/card-my-policies/base-card-my-policies';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { MFP_Comparar_Poliza_20A, MFP_Comparar_Poliza_20B } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE, SEARCH_ORDEN_DESC } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import {
  ICardPoliciesView,
  IPoliciesByClientRequest,
  IPoliciesByClientResponse,
  IPolicyList
} from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { takeUntil } from 'rxjs/operators';
import { ModalContactedByAgentCompareComponent } from '../modal-contacted-by-agent-compare/modal-contacted-by-agent-compare.component';

@Component({
  selector: 'client-modal-select-compare',
  templateUrl: './modal-select-compare.component.html'
})
export class ModalSelectCompareComponent extends BaseCardMyPolicies implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(ModalContactedByAgentCompareComponent) mfModalAgent: ModalContactedByAgentCompareComponent;
  @Input() policyType: string;

  title: string;
  subTitle: string;
  labelType: string;
  btnConfirm: string;
  showLoading: boolean;
  selectedPolicy: string;
  listPolicies: Array<ICardPoliciesView>;
  listPoliciesFilter: Array<ICardPoliciesView>;
  openModalAgent: boolean;
  auth: IdDocument;
  textInProgress = PolicyLang.Texts.changeModalityInProgress;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService,
    public router: Router,
    public authService: AuthService,
    private readonly policyListService: PolicyListService,
    protected gaService?: GAService
  ) {
    super(generalService, policiesService, gaService);
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.title = PolicyLang.Titles.ComparePolicy.toUpperCase();
    this.subTitle = POLICY_TYPES[this.policyType].title || '';
    this.labelType = POLICY_TYPES[this.policyType].dropDownTitle;
    this.btnConfirm = GeneralLang.Buttons.Compare;
    // Lista de pÃ³lizas
    this.listPolicies = this.policyListService.getDataPolicies();
    if (!this.listPolicies) {
      // Busca las polizas
      this.getPoliciesList();
    }
    // Filtra vencidas
    this.listPoliciesFilter = this.listPolicies.filter(policy => !policy.expired);
  }

  open(): void {
    this.selectedPolicy = '';
    this.mfModal.open();
  }

  // Al seleccionar una pÃ³liza
  onSubmit(confirm): void {
    if (confirm && this.selectedPolicy) {
      const policyFilter = this.listPolicies.filter(item => item.policyNumber === this.selectedPolicy);
      const policy = policyFilter[0];
      // Guarda poliza seleccionada
      this.policySelected = policy.policyNumber;
      this.policyListService.setPolicySelected(this.selectedPolicy);
      // GAEvent
      const gaData = {
        buttonText: this.btnConfirm,
        policy: policy.policy,
        policyNumber: policy.policyNumber,
        policyStatus: policy.coverageStatus
      };
      this.addGAEvent(MFP_Comparar_Poliza_20A(), gaData);
      // Verifica cantidad de vehiculos
      if (policy.beneficiaries.length > 1) {
        this.mfModal.close(true);
        this.showLoading = true;
        this.openModalAgent = true;
        setTimeout(() => {
          this.showLoading = false;
          this.mfModalAgent.open();
        }, 100);
      } else {
        // Va directo a comparar
        const path = `${this.getLink(this.policyType, 'compare')}/${policy.policyNumber}`;
        if (path) {
          this.router.navigate([path]);
        }
        this.mfModal.close(true);
      }
    }
  }

  // Lista de PÃ³lizas
  getPoliciesList(): void {
    const params = this.fnParamsSearchByClient();
    this.showLoading = true;
    this.policiesService
      .searchByClientService(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        (response: Array<IPoliciesByClientResponse>) => {
          this.listPolicies = response.map((policy: IPoliciesByClientResponse) => {
            const expiredPolicy = this.fnIsExpiredPolicy(policy.fechaFin);

            return {
              policyType: policy.tipoPoliza,
              policyNumber: policy.numeroPoliza,
              beneficiaries: policy.beneficiarios,
              policy: policy.descripcionPoliza,
              expired: expiredPolicy,
              response: policy
            } as ICardPoliciesView;
          });
          // Filtra vencidas
          this.listPoliciesFilter = this.listPolicies.filter(policy => !policy.expired);
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  fnParamsSearchByClient(): IPoliciesByClientRequest {
    const params: IPoliciesByClientRequest = {
      codigoApp: APPLICATION_CODE,
      criterio: 'fechaInicio',
      orden: SEARCH_ORDEN_DESC,
      tipoPoliza: this.policyType
    };

    return params;
  }

  // Guardar en Local
  setDataLocal(): void {
    const data: IPolicyList = this.policyListService.newPolicyList();
    data.policyType = this.policyType;
    data.typeDocument = this.auth.type;
    data.document = this.auth.number;
    data.list = this.listPolicies;
    data.policySelected = this.policySelected;
    this.policyListService.setPolicyListData(data);
  }

  closeEvent(b: boolean): void {
    if (!b) {
      this.addGAEvent(MFP_Comparar_Poliza_20B(), { buttonText: 'Cancelar' });
    }
  }
}
