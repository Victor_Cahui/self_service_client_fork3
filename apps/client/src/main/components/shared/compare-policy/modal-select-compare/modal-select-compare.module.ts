import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { ModalContactedByAgentCompareModule } from '../modal-contacted-by-agent-compare/modal-contacted-by-agent-compare.module';
import { ModalSelectCompareComponent } from './modal-select-compare.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MfModalModule,
    IconPoliciesModule,
    ModalContactedByAgentCompareModule,
    GoogleModule
  ],
  exports: [ModalSelectCompareComponent],
  declarations: [ModalSelectCompareComponent]
})
export class ModalSelectCompareModule {}
