import { Component } from '@angular/core';
import { SOAT_DAYS_DATE_START_VALIDITY } from '@mx/settings/constants/key-values';

@Component({
  selector: 'client-card-vigency-soat',
  templateUrl: './card-vigency-soat.component.html'
})
export class CardVigencySoatComponent {
  key = SOAT_DAYS_DATE_START_VALIDITY;
}
