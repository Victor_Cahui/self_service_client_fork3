import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardVigencyModule } from '@mx/components/shared/card-vigency/card-vigency.module';
import { DirectivesModule, MfCardModule, MfInputDatepickerModule, MfShowErrorsModule, SwitchModule } from '@mx/core/ui';
import { CardVigencySoatComponent } from './card-vigency-soat.component';

@NgModule({
  imports: [
    CommonModule,
    MfCardModule,
    DirectivesModule,
    SwitchModule,
    MfInputDatepickerModule,
    MfShowErrorsModule,
    CardVigencyModule
  ],
  declarations: [CardVigencySoatComponent],
  exports: [CardVigencySoatComponent]
})
export class CardVigencySoatModule {}
