import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { TruncateDecimalsPipe } from '@mx/core/shared/common/pipes/truncatedecimals.pipe';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { DEFAULT_MPD_DECIMALS } from '@mx/settings/constants/general-values';
import { FILE_ICON } from '@mx/settings/constants/images-values';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { ICardTitle } from '@mx/statemanagement/models/general.models';
import { IPaymentQuoteCard, IQuoteWithQuota } from '@mx/statemanagement/models/payment.models';
import { Subject, Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'client-card-summary-quote',
  templateUrl: './card-summary-quote.component.html',
  providers: [DecimalPipe, TruncateDecimalsPipe, CurrencyPipe]
})
export class CardSummaryQuoteComponent implements OnInit {
  static updateView: Subject<boolean> = new Subject();
  static updateMPD: Subject<boolean> = new Subject();

  @HostBinding('class') class = 'w-px-lg--36 w-10 order-1 order-lg-2';
  @Input() riskCard: ICardTitle;

  lang = MethodPaymentLang;

  buyCard: ICardTitle;
  quote: IPaymentQuoteCard;

  stepLast: number;
  discount: number;
  currentStep: number;
  labelMapfreDolars: string;
  discountFormat: string;
  mapfreDolars: string;
  hasMPD: boolean;
  useMPD: boolean;
  showMPD: boolean;
  isDesktop: boolean;
  doingPayment: boolean;

  currentStepSub: Subscription;
  doingPaymentSub: Subscription;

  constructor(
    private readonly paymentService: PaymentService,
    private readonly quoteService: PoliciesQuoteService,
    private readonly decimalPipe: DecimalPipe,
    private readonly truncateDecimalsPipe: TruncateDecimalsPipe,
    public currencyPipe: CurrencyPipe,
    private readonly platformService: PlatformService
  ) {
    CardSummaryQuoteComponent.updateView.subscribe(() => {
      this.collapseAllCard();
    });
    CardSummaryQuoteComponent.updateMPD.subscribe(() => {
      this.setDataMPD();
    });

    this.currentStepSub = this.quoteService.getCurrentStepSubject().subscribe(
      data => {
        this.currentStep = data;
        if (this.currentStep === this.quoteService.getLastStep()) {
          this.setDataMPD();
        }
      },
      () => {},
      () => {
        this.currentStepSub.unsubscribe();
      }
    );

    this.doingPaymentSub = this.quoteService.doingPaymentSubject.subscribe(
      (value: boolean) => (this.doingPayment = value),
      () => {},
      () => {
        this.doingPaymentSub.unsubscribe();
      }
    );
  }

  ngOnInit(): void {
    this.buyCard = {
      title: MethodPaymentLang.BuyData,
      icon: FILE_ICON,
      collapse: !this.platformService.isDesktop
    };
    this.quote = this.paymentService.getCurrentQuote();
  }

  setDataMPD(): void {
    this.quote = this.paymentService.getCurrentQuote();
    if (this.quote.dataQuoteVehicle) {
      this.hasMPD =
        !isNullOrUndefined(this.quote.dataQuoteVehicle.cotizacionConMPD) &&
        !!Object.keys(this.quote.dataQuoteVehicle.cotizacionConMPD).length &&
        (this.quote.dataQuoteVehicle.cotizacionConMPD &&
          this.quote.dataQuoteVehicle.cotizacionConMPD.montoMapfreDolaresUsado > 0);
      this.showMPD = this.hasMPD;
      this.useMPD = this.hasMPD;
      this.switchChange(this.hasMPD);
    }
  }

  switchChange(value: boolean): void {
    this.quote.withMPD = value;
    this.useMPD = value;
    if (this.quote.withMPD) {
      this.setValues(this.quote.dataQuoteVehicle.cotizacionConMPD);
    } else {
      this.setValues(this.quote.dataQuoteVehicle.cotizacionSinMPD);
    }
    this.paymentService.setCurrentQuote(this.quote);
    this.labelMapfreDolars = this.discount > 0 ? this.lang.Remaining : this.lang.Available;
    this.discountFormat = `${this.discount > 0 ? '- ' : ''}
      ${this.quote.currencySymbol} ${this.formatDecimal(this.discount)}`;
    // Actualiza cuota en boton
    this.paymentService.updateQuota().next(this.quote.quota);
  }

  setValues(quote: IQuoteWithQuota): void {
    this.quote.total = quote.montoFinal || 0;
    this.discount = quote.montoAhorro || 0;
    this.quote.quota = `${this.quote.currencySymbol}${this.formatDecimal(quote.montoCuota || 0)}`;
    this.quote.totalAmount = `${this.quote.currencySymbol}${this.formatDecimal(this.quote.total)}`;
    this.mapfreDolars = this.currencyPipe.transform(
      this.truncateDecimalsPipe.transform(quote.montoMapfreDolaresRestantes || 0, DEFAULT_MPD_DECIMALS),
      ' '
    );
  }

  formatDecimal(value: number): string {
    return this.decimalPipe.transform(value, '1.2-2');
  }

  // Una sola card desplegada
  expandRiskCard(event): void {
    this.riskCard.collapse = event;
    if (!event) {
      this.buyCard.collapse = true;
    }
  }
  expandBuyCard(event): void {
    this.buyCard.collapse = event;
    if (!event) {
      this.riskCard.collapse = true;
    }
  }
  collapseAllCard(): void {
    if (this.buyCard) {
      this.buyCard.collapse = !this.platformService.isDesktop;
    }
    if (this.riskCard) {
      this.riskCard.collapse = true;
    }
  }
}
