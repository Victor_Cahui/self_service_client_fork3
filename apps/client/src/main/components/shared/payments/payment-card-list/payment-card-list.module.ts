import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule, MfButtonModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { CardModule } from '@mx/core/ui/lib/components/card/card.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { MfModalAlertModule, MfModalTermsModule, MfToolTipModule } from '@mx/core/ui/public-api';
import { InputWithIconModule } from '../../inputs';
import { BankPaymentModule } from '../bank-payment';
import { BankPaymentSoatModule } from '../bank-payment-soat/bank-payment-soat.module';
import { PaymentCardListComponent } from './payment-card-list.component';

@NgModule({
  imports: [
    CommonModule,
    MfButtonModule,
    MfInputModule,
    BankPaymentModule,
    InputWithIconModule,
    TooltipsModule,
    ReactiveFormsModule,
    MfShowErrorsModule,
    MfLoaderModule,
    MfModalAlertModule,
    DirectivesModule,
    BankPaymentSoatModule,
    FormsModule,
    CardModule,
    MfToolTipModule,
    MfModalTermsModule,
    MfCustomAlertModule
  ],
  declarations: [PaymentCardListComponent],
  exports: [PaymentCardListComponent]
})
export class PaymentCardListModule {}
