import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCardModule, MfModalAlertModule, SwitchModule } from '@mx/core/ui';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { IconPoliciesModule } from '../../icon-policies/icon-polices.module';
import { PaymentSettingsPolicyComponent } from './payment-settings-policy.component';

@NgModule({
  imports: [
    CommonModule,
    GoogleModule,
    MfCardModule,
    IconPoliciesModule,
    DropdownItemsPolicyModule,
    TooltipsModule,
    SwitchModule,
    MfModalAlertModule
  ],
  declarations: [PaymentSettingsPolicyComponent],
  exports: [PaymentSettingsPolicyComponent]
})
export class PaymentSettingsPolicyModule {}
