import { DOCUMENT, Location } from '@angular/common';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { MfButton } from '@mx/core/ui';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { COMPLETE_ICON, IMAGE_CARDS } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { IPaymentMethod } from '@mx/statemanagement/models/payment.models';
import { isEmpty } from 'lodash-es';
import { FormComponent } from '../../utils/form';
import { PaymentFormMode, PaymentMode, PaymentSoatType } from '../../utils/payment';
import { ITarjeta } from './../../../../../statemanagement/models/payment.models';

import { HttpHeaders } from '@angular/common/http';
import { CardPayment } from '@mx/core/shared/helpers/util/card-payment';
import { FormLyraObjects, lyraScripts } from '@mx/core/shared/providers/constants/lyra';
import { SCRIPT_TYPE, ScriptModel } from '@mx/core/shared/providers/models/script.interface';
import { ScriptService } from '@mx/core/shared/providers/services/script.service';
import { environment } from '@mx/environments/environment';
import { fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
declare const KR: any;
@Component({
  selector: 'client-payment-method',
  templateUrl: './payment-method.component.html'
})
export class PaymentMethodComponent extends GaUnsubscribeBase implements AfterViewInit, OnInit, OnDestroy {
  @HostBinding('class') class = 'd-flex w-10 g-mr-lg--30 g-ie-flex--0-1-auto order-2 order-lg-1';
  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;
  @ViewChild('buttonPay') emitPayButton: MfButton;
  @Output() payment: EventEmitter<IPaymentMethod> = new EventEmitter<IPaymentMethod>();
  @Output() readonly processing: EventEmitter<boolean>;

  @Input() totalAmount: string;
  @Input() showLoading: boolean;
  @Input() quoataEmited: boolean;
  @Input() onlyForm: boolean;
  @Input() paymentButtonText = 'Pagar';
  @Input() placeholderCardHolderName = 'Nombre del titular';
  @Input() alloToUseOtherCards = true;

  public frm: FormGroup;
  public apiPublicKey: string = environment.lyraPublicKeySoles;
  hiddenBtnPayment = false;
  ownerName: AbstractControl;
  cardNumber: AbstractControl;
  cardExpired: AbstractControl;
  cvv: AbstractControl;
  email: AbstractControl;
  currentPaymentMode: PaymentMode = PaymentMode.CARD;
  thereAreSavedCards: boolean;
  formMode = PaymentFormMode.TOKEN;
  currentPaymentSoatType: PaymentSoatType = PaymentSoatType.RE;
  paymentMode = PaymentMode;
  lang = MethodPaymentLang;
  generalLang = GeneralLang;
  imagesCards = IMAGE_CARDS;
  form: FormGroup;
  modalText: string;
  modalImg: string = COMPLETE_ICON;
  urlLyraFrame;
  apiFormToken = '';
  quote: any;
  eventResponseTransactionLyra: any;
  chbSaveCard: HTMLInputElement;
  chbAutoDebit: HTMLInputElement;
  chbSaveCardFromLyra: HTMLInputElement;
  listSavedCards: ITarjeta[];
  titleSavedCards = GeneralLang.Titles.SelectedSavedCards;
  selectedCard: ITarjeta;
  useForm: boolean;
  showChbAutoDebit: boolean;
  private _scripts: ScriptModel[];
  private _divHolderName: HTMLDivElement;
  private readonly _idDivHolderName: string;

  constructor(
    @Inject(DOCUMENT) readonly document: Document,
    private readonly _FrmProvider: FrmProvider,
    private readonly _Autoservicios: Autoservicios,
    private readonly _Location: Location,
    private readonly _PaymentService: PaymentService,
    protected gaService: GAService,
    private readonly _AuthService: AuthService,
    private readonly _NgZone: NgZone,
    private readonly _ScriptService: ScriptService,
    private readonly clientService: ClientService,
    private readonly _Renderer2: Renderer2,
    private readonly location: Location
  ) {
    super(gaService);
    this.showLoading = true;
    this._idDivHolderName = 'acme-holder-name-data';
  }

  ngOnInit(): void {
    (window as any).KR = {};
    this.frm = this._FrmProvider.DirectPayFormComponent();
    this.setVariables();
    this.thereAreSavedCards = false;
    this.useForm = true;
    this.loadThereSavedCards();
  }

  ngAfterViewInit(): void {
    if (!this.thereAreSavedCards) {
      this.loadLyraForm();
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (KR && !Object.keys(KR).length) {
      return void 0;
    }
    this._NgZone.run(() => {
      KR.removeForms()
        .then(this._removeKR.bind(this))
        .catch(this._removeKR.bind(this));
    });
  }

  private setVariables(): void {
    this.quote = this._PaymentService.getCurrentQuote();
    this.showChbAutoDebit = this.quoataEmited && this.quote.canMakeAutomaticPayment;
  }

  private _mapSavedCards(response: any): ITarjeta {
    return {
      numero: response.numero,
      tipo: response.tipo,
      tarjetaToken: response.tarjetaToken
    };
  }

  private _removeKR(): void {
    try {
      FormLyraObjects.removeForms(document);
      this._ScriptService.remove(this._scripts);
    } catch (error) {}
  }

  async loadThereSavedCards(): Promise<void> {
    if (!this.quoataEmited) {
      return void 0;
    }

    this.showLoading = true;
    this._Autoservicios.ObtenerTarjetas({ codigoApp: APPLICATION_CODE }).subscribe(
      response => {
        this.showLoading = false;
        if (response && response.length > 0) {
          this.thereAreSavedCards = true;
          this.useForm = false;
          this.listSavedCards = response.map(this._mapSavedCards.bind(this));
        } else {
          this.thereAreSavedCards = false;
          this.useForm = true;
          this._PaymentService.changeTotalLyra().subscribe(value => {
            this.loadLyraForm();
          });
        }
      },
      () => (this.showLoading = false)
    );

    return void 0;
  }

  async loadLyraForm(): Promise<void> {
    this.showLoading = true;
    this.quote = this._PaymentService.getCurrentQuote();
    const codigoMoneda = this.quote.response
      ? this.quote.response.codigoMoneda
      : this.quote.withMPD
      ? this.quote.dataQuote.cotizacionConMPD.codigoMoneda
      : this.quote.dataQuote.cotizacionSinMPD.codigoMoneda;
    this.apiPublicKey = codigoMoneda === 1 ? environment.lyraPublicKeySoles : environment.lyraPublicKeyDolares;
    const body: any = FormLyraObjects.getToken(
      this.quote,
      this.clientService,
      this.formMode,
      this.currentPaymentSoatType
    );
    const response = await this._Autoservicios
      .PasarelaFormularioToken({ codigoApp: APPLICATION_CODE }, body)
      .toPromise();
    this.apiFormToken = response.formToken;
    const formPayment: HTMLElement = this.document.getElementById('formPayment');
    if (!formPayment) {
      return void 0;
    }
    this._scripts = FormLyraObjects.getLyraScripts(lyraScripts, SCRIPT_TYPE, this.apiPublicKey);
    formPayment.setAttribute('kr-form-token', this.apiFormToken);
    this._ScriptService.load(this._scripts).then(() => {
      this._initForm();
      this.showLoading = false;
    });
  }

  _initForm(): void {
    this._NgZone.run(() => {
      if (KR && !Object.keys(KR).length) {
        return void 0;
      }
      KR.setFormConfig({
        formToken: this.apiFormToken,
        'kr-placeholder-card-holder-name': this.placeholderCardHolderName
      });
      this._KREvents();
    });
  }

  private _KREvents(): any {
    this._NgZone.run(() => {
      KR.onSubmit(this._onSubmit.bind(this));
      KR.onError(this._onError.bind(this));
      KR.onFormReady(this._onFormReady.bind(this));
    });
  }

  showErrors(control: AbstractControl): boolean {
    return (control.dirty || control.touched) && !isEmpty(control.errors);
  }

  getImageCardByType(type: string): string {
    let imageCard = '';
    switch (type) {
      case 'VISA':
      case 'VISA_ELECTRON':
      case 'VISA_DEBIT':
        imageCard = IMAGE_CARDS.VISA;
        break;
      case 'MASTERCARD':
      case 'MASTERCARD_DEBIT':
        imageCard = IMAGE_CARDS.MASTER_CARD;
        break;
      case 'DINERS_CLUB':
      case 'DINERS':
        imageCard = IMAGE_CARDS.DINNER_CLUB;
        break;
      case 'AMERICAN_EXPRESS':
      case 'AMERICAN_EXPRESS_AMEX':
      case 'AMERICAN EXPRESS':
      case 'AMEX':
        imageCard = IMAGE_CARDS.AMERICAN_EXPRESS;
        break;
      default:
        imageCard = '';
        break;
    }

    return imageCard;
  }

  private _onSubmit(event: any): void {
    this._NgZone.run(() => {
      this._paymentResponse(event);
    });
  }

  private _paymentResponse(event): void {
    if (event.clientAnswer.orderStatus) {
      this.eventResponseTransactionLyra = event;
      let item: IPaymentMethod;
      item = this.emitCard();
      const headers = new HttpHeaders({
        uuid: event.clientAnswer.transactions[0].uuid
      });
      this.quote = this._PaymentService.getCurrentQuote();
      event.clientAnswer.saveCard = this.chbSaveCard ? this.chbSaveCard.checked : false;
      event.clientAnswer.autoDebit = this.chbAutoDebit ? this.chbAutoDebit.checked : false;
      const bodyTransaction: any = FormLyraObjects.getPaymentObject(event, this.quote, this.currentPaymentMode);
      if (this.quote.comesSoat) {
        this._Autoservicios
          .GenerarEmisionSOATLyra({ codigoApp: APPLICATION_CODE }, bodyTransaction, { headers })
          .pipe(takeUntil(this.unsubscribeDestroy$))
          .subscribe(res => {
            try {
              this.responseSubmit(res, item);
            } catch (error) {}
          });
      } else {
        this._Autoservicios
          .PagarReciboLyra({ codigoApp: APPLICATION_CODE }, bodyTransaction, { headers })
          .pipe(takeUntil(this.unsubscribeDestroy$))
          .subscribe(res => {
            try {
              this.prepairDataPostPayment(res);
              this.responseSubmit(res, item);
            } catch (error) {}
          });
      }
    }
  }

  public prepairDataPostPayment(res): void {
    this.quote.response = res;
    this.quote.confirmFormData = {
      paymentMethodLabel: 'Medio de pago:',
      paymentMethodDetail: [],
      panCardType: res.tarjeta.tipo,
      panCard: res.tarjeta.numero,
      chbAutoDebit: this.chbAutoDebit ? this.chbAutoDebit.checked : res.tienePagoAutomatico ? true : false,
      chbSaveCard: this.chbSaveCard ? this.chbSaveCard.checked : res.guardarTarjeta ? true : false,
      tokenCard: res.tarjeta.tarjetaToken
    };
    this.quote.email = res.tarjeta.correoElectronico;

    if (this.quote.confirmFormData.chbSaveCard) {
      this.quote.confirmFormData.paymentMethodDetail.push('Esta tarjeta se ha guardado para futuros pagos y compras');
    }
    if (this.quote.confirmFormData.chbAutoDebit) {
      this.quote.confirmFormData.paymentMethodDetail.push('Se habilitó el pago automático en la siguiente póliza:');
    }

    this._PaymentService.setCurrentQuote(this.quote);
  }

  public useOtherCard(): void {
    this.useForm = true;
    this.loadLyraForm();
  }

  private responseSubmit(res, item): void {
    this.showLoading = false;
    if (res.transaccion.codTransaccion !== '01') {
      this.modalText = res.transaccion.mensajeUsuario;
      this.mfModalAlert.open();
    } else {
      this.payment.emit(item);
    }
  }

  private _onError(event: any): void {
    this._NgZone.run(() => {
      if (event && event.errorCode && event.errorCode.startsWith('CLIENT')) {
        return void 0;
      }
      this._paymentResponse(event.metadata.answer);
    });
  }

  private _onFormReady(): void {
    this._NgZone.run(() => {
      this._captureCheckbox();
      this._hideLyraCheckbox();
      setTimeout(() => {
        FormLyraObjects.setLabelForm(this.document);
      }, 100);
    });
  }

  public _emitSubmit(): void {
    this._NgZone.run(() => {
      this.emitPayButton.disabled = true;
      this.showLoading = true;
      const button: any = this.document.getElementsByClassName('kr-payment-button')[0];
      if (this.ga && this.ga.find(c => c.control === 'buttonPayPolicy')) {
        const ga = this.ga.find(c => c.control === 'buttonPayPolicy');
        this.addGAEvent(ga, this.quote);
      }
      if (this.currentPaymentMode === PaymentMode.BANK) {
        let item: IPaymentMethod;
        item = { paymentMode: PaymentMode.BANK };
        this.payment.emit(item);
      } else if (this.currentPaymentMode === PaymentMode.CARD && this.thereAreSavedCards && !this.useForm) {
        if (!this.selectedCard) {
          this.modalText = 'Seleccione una tarjeta válida';
          this.mfModalAlert.open();
        } else {
          let item: IPaymentMethod;
          item = this.emitCardToken();
          this.quote = this._PaymentService.getCurrentQuote();
          const event = { clientAnswer: { customer: { email: this.clientService.getUserEmail() } } };
          const bodyTransaction: any = FormLyraObjects.getPaymentObject(
            event,
            this.quote,
            PaymentMode.TOKN,
            this.selectedCard
          );
          this._Autoservicios
            .PagarReciboLyra({ codigoApp: APPLICATION_CODE }, bodyTransaction)
            .pipe(takeUntil(this.unsubscribeDestroy$))
            .subscribe(
              res => {
                try {
                  this.prepairDataPostPayment(res);
                  this.responseSubmit(res, item);
                } catch (error) {}
              },
              () => (this.showLoading = false)
            );
        }
      } else {
        KR.validateForm()
          .then(() => {
            if (this.frm.invalid) {
              this.showLoading = false;
              this._handlerErorClassInput(1, true);
            } else {
              button.click();
            }
          })
          .catch(() => {
            this.showLoading = false;
            this._handlerErorClassInput(1, true);
          });
      }
    });
  }

  private _handlerInputHoldName(): void {
    this._divHolderName = this.document.getElementById(this._idDivHolderName) as HTMLDivElement;

    let input: HTMLInputElement = this._divHolderName.querySelector('input');
    const label: HTMLLabelElement = this._divHolderName.querySelector('label');

    fromEvent(input, 'input')
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this._NgZone.run(() => {
          this._handlerErorClassInput(2, false);
          input = this._divHolderName.querySelector('input');
        });
      });

    fromEvent(input, 'blur')
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this._NgZone.run(() => {
          this._handlerErorClassInput(2, !input.value);
          if (input.value) {
            this._Renderer2.addClass(input, 'visible');
            this._Renderer2.addClass(label.parentElement, 'focus');
            this._Renderer2.removeClass(label.parentElement, 'default');
          }
        });
      });
  }

  private _handlerErorClassInput(input: number, add: boolean = false): void {
    if (input !== 1) {
      this._Renderer2[add ? 'addClass' : 'removeClass'](this._divHolderName.firstElementChild, 'kr-error');
    }
  }

  private _hideLyraCheckbox(): void {
    const krChbBox = document.getElementsByClassName('kr-field kr-checkbox kr-visible kr-checkbox-type-default')[0];
    if (krChbBox) {
      krChbBox.setAttribute('style', 'display:none');
      this.chbSaveCardFromLyra = krChbBox.children[0].children[0].children[0].children[0] as HTMLInputElement;
    }
  }

  private _captureCheckbox(): void {
    const chb1 = document.getElementById('chbSaveCards') as HTMLInputElement;
    const chb2 = document.getElementById('chbAutoDebit') as HTMLInputElement;
    if (chb1) {
      this.chbSaveCard = chb1;
      chb1.addEventListener('change', event => {
        if (chb1.checked) {
          if (chb2) {
            chb2.removeAttribute('disabled');
          }
          this.chbSaveCardFromLyra.click();
        } else {
          this.chbSaveCardFromLyra.checked = false;
          if (chb2) {
            chb2.checked = false;
            chb2.setAttribute('disabled', 'disabled');
          }
        }
      });
    }
    if (chb2) {
      this.chbAutoDebit = chb2;
      chb2.addEventListener('change', event => {});
    }
  }

  formatValue(control): void {
    FormComponent.removeSpaces(control);
  }

  emitCard(): IPaymentMethod {
    return CardPayment.getCardInfo(this.eventResponseTransactionLyra, PaymentMode.CARD);
  }

  emitCardToken(): IPaymentMethod {
    return CardPayment.getCardInfoByToken(this.selectedCard, PaymentMode.TOKN);
  }

  onConfirmModal(event): void {
    if (this.useForm || (!this.useForm && this.thereAreSavedCards && this.selectedCard)) {
      this._Location.back();
    }
  }

  // tslint:disable-next-line: max-file-line-count
}
