import { Component, OnInit } from '@angular/core';
import { AuthService } from '@mx/services/auth/auth.service';
import { PaymentService } from '@mx/services/payment.service';
import { ConfirmBankPaymentSoatLang, PaymentLang } from '@mx/settings/lang/payment.lang';
import { Content } from '@mx/statemanagement/models/general.models';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { POLICY_QUOTA_TYPE } from '../../utils/policy';

@Component({
  selector: 'client-bank-payment-soat',
  templateUrl: './bank-payment-soat.component.html'
})
export class BankPaymentSoatComponent implements OnInit {
  quote: IPaymentQuoteCard;
  isNotice: boolean;

  documentData: string;
  content: Content = ConfirmBankPaymentSoatLang;
  labelBankPaymentCode = PaymentLang.Labels.BankPaymentCode;
  labelNoticeNumber = PaymentLang.Labels.NoticeNumber;
  labelDocument = PaymentLang.Labels.DocumentNumber;
  digitalPlatform = PaymentLang.Labels.DigitalPlatform;

  constructor(private readonly authService: AuthService, private readonly paymentService: PaymentService) {}

  ngOnInit(): void {
    this.quote = this.paymentService.getCurrentQuote();
    const auth = this.authService.retrieveEntity();
    this.documentData = `${auth.number}`;
    this.isNotice = this.quote.response.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
  }
}
