import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { Component, HostBinding, OnInit } from '@angular/core';
import { TruncateDecimalsPipe } from '@mx/core/shared/common/pipes/truncatedecimals.pipe';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { PaymentService } from '@mx/services/payment.service';
import { DEFAULT_MPD_DECIMALS } from '@mx/settings/constants/general-values';
import { MethodPaymentLang, PaymentLang } from '@mx/settings/lang/payment.lang';
import { IPaymentQuote, IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'client-card-quote-soat',
  templateUrl: './card-quote-soat.component.html',
  providers: [DecimalPipe, TruncateDecimalsPipe, CurrencyPipe]
})
export class CardQuoteSoatComponent implements OnInit {
  @HostBinding('class') class = 'w-px-lg--36 w-10 order-1 order-lg-2';
  quote: IPaymentQuoteCard;
  collapse = false;
  lang = MethodPaymentLang;
  item: IPaymentQuote;
  hasMPD: boolean;
  useMPD: boolean;
  discount: number;
  mapfreDolars: string;
  labelMapfreDolars: string;

  constructor(
    private readonly paymentService: PaymentService,
    private readonly decimalPipe: DecimalPipe,
    private readonly truncateDecimalsPipe: TruncateDecimalsPipe,
    public currencyPipe: CurrencyPipe
  ) {}

  ngOnInit(): void {
    this.quote = this.paymentService.getCurrentQuote();
    // Verificar si tiene mafpre dolars
    this.hasMPD =
      !isNullOrUndefined(this.quote.dataQuote.cotizacionConMPD) &&
      !!Object.keys(this.quote.dataQuote.cotizacionConMPD).length &&
      (this.quote.dataQuote.cotizacionConMPD && this.quote.dataQuote.cotizacionConMPD.montoMapfreDolaresUsado > 0);
    this.useMPD = this.hasMPD;
    this.switchChange(this.hasMPD);
  }

  switchChange(value): void {
    // Actualiza storage
    this.quote = this.paymentService.getCurrentQuote();
    this.quote.withMPD = value;
    this.useMPD = value;
    if (this.quote.withMPD) {
      this.setValues(this.quote.dataQuote.cotizacionConMPD);
    } else {
      this.setValues(this.quote.dataQuote.cotizacionSinMPD);
    }
    this.quote.currencySymbol = StringUtil.getMoneyDescription(this.quote.currency);
    this.paymentService.setCurrentQuote(this.quote);
    this.labelMapfreDolars = this.discount > 0 ? this.lang.Remaining : this.lang.Available;

    const discountFormat = this.decimalPipe.transform(this.discount, '1.2-2');
    const totalFormat = this.decimalPipe.transform(this.quote.total, '1.2-2');

    // Actualiza vista
    this.item = {
      discount: `${this.discount > 0 ? '- ' : ''} ${this.quote.currencySymbol} ${discountFormat}`,
      mapfreDolars: `${StringUtil.getMoneyDescription(2)} ${this.mapfreDolars}`,
      year: MethodPaymentLang.DefaultPeriod,
      finalPrice:
        this.quote.total === 0 ? PaymentLang.Texts.Free.toUpperCase() : `${this.quote.currencySymbol} ${totalFormat}`
    };
    this.paymentService.changeTotalLyra().next('change');
  }

  setValues(quote): void {
    this.quote.total = quote.montoFinal;
    this.quote.currency = quote.codigoMoneda;
    this.discount = quote.montoAhorro;
    this.mapfreDolars = this.currencyPipe.transform(
      this.truncateDecimalsPipe.transform(quote.montoMapfreDolaresRestantes, DEFAULT_MPD_DECIMALS),
      ' '
    );

    this.paymentService.onUseMapfreDolarsChange().next(this.quote.total === 0);
  }
}
