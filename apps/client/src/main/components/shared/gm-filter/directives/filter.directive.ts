import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[filter-map]'
})
export class FilterMapDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
