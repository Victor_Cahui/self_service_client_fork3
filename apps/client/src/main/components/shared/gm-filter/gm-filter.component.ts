import { DOCUMENT } from '@angular/common';
import {
  Component,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { OrderByPipe } from '@mx/core/shared/common/pipes/orderby.pipe';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { HealthService } from '@mx/services/health/health.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { takeUntil } from 'rxjs/operators';
import { GmFilterViewItem } from './providers/gm-filter-view-item';
import { GmFilterEvents, GmFilterService } from './providers/gm-filter.service';

@Component({
  selector: 'client-gm-filter',
  templateUrl: './gm-filter.component.html',
  providers: [OrderByPipe]
})
export class GmFilterComponent extends GaUnsubscribeBase implements OnInit, OnChanges, OnDestroy {
  @HostBinding('attr.class') attr_class = 'g-map-filter';

  @Input() items: Array<IItemMarker>;
  @Input() title: string;
  @Input() gmFilterViewItem: GmFilterViewItem;
  @Input() centerButton: any;
  @Input() configView: any;
  @Input() showLoading: boolean;
  @Input() policy: IPoliciesByClientResponse;
  @Output() viewDetail: EventEmitter<IItemMarker>;
  @Output() goToAppointment: EventEmitter<IItemMarker> = new EventEmitter();
  @Output() centerButtonClick: EventEmitter<any>;
  @Output() favorite: EventEmitter<IItemMarker>;

  listOriginal: Array<IItemMarker>;
  tmpItems: Array<IItemMarker> = [];
  showList: boolean;
  showFilter: boolean;
  currentIsDesktop: boolean;

  currentTextSearch: any;

  constructor(
    private readonly googleMapService: GoogleMapService,
    private readonly platformService: PlatformService,
    private readonly resizeService: ResizeService,
    private readonly renderer2: Renderer2,
    private readonly gmFilterService: GmFilterService,
    private readonly orderByPipe: OrderByPipe,
    protected gaService: GAService,
    private readonly _Autoservicios: Autoservicios,
    private readonly healthService: HealthService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {
    super(gaService);
    this.viewDetail = new EventEmitter<IItemMarker>();
    this.centerButtonClick = new EventEmitter<any>();
    this.favorite = new EventEmitter<IItemMarker>();

    this.currentIsDesktop = this.platformService.isDesktop;

    this.resizeService.resizeEvent.pipe(takeUntil(this.unsubscribeDestroy$)).subscribe(() => {
      const newIsDesktop = this.platformService.isDesktop;

      if (this.currentIsDesktop !== newIsDesktop) {
        this.showList = newIsDesktop;
        this.showFilter = false;
      }

      this.currentIsDesktop = newIsDesktop;
    });
  }

  ngOnInit(): void {
    this.showList = this.platformService.isDesktop;
    this.handlerClass();
    this.handlerFilter();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.items && changes.items.currentValue !== changes.items.previousValue) {
      setTimeout(() => (this.tmpItems = this.items), 0);
    }
    this.gmFilterService.setItems(this.items);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.handlerClass(false);
  }

  fnSelectItem(item: IItemMarker): void {
    const clinicImagePortraitStorage = this.healthService.getImagePortraitClinic(item.payload.clinicaId.toString());
    if (clinicImagePortraitStorage === null) {
      this._Autoservicios
        .ObtenerImagenClinica({ codigoApp: APPLICATION_CODE }, { codigoImg: item.payload.imagenPortraitUrl })
        .subscribe(r => {
          this.healthService.setImagePortraitClinic(item.payload.clinicaId.toString(), r.base64);
          item.imageUrl = r.base64;
          item.payload.imagenPortraitUrl = r.base64;
        });
      this._Autoservicios
        .ObtenerImagenClinica({ codigoApp: APPLICATION_CODE }, { codigoImg: item.payload.imagenLandscapeUrl })
        .subscribe(r => {
          this.healthService.setImageLandScapeClinic(item.payload.clinicaId.toString(), r.base64);
          item.payload.imagenLandscapeUrl = r.base64;
        });
    } else {
      item.imageUrl = clinicImagePortraitStorage;
      item.payload.imagenPortraitUrl = clinicImagePortraitStorage;
      item.payload.imagenLandscapeUrl = this.healthService.getImageLandScapeClinic(item.payload.clinicaId.toString());
    }
    this.googleMapService.setItemMarker(item);
  }

  handlerFilterView(show?: boolean): void {
    show && this.gmFilterService.emitOpenedFilterRx(true);
    this.showFilter = show;
    if (this.platformService.isDesktop) {
      this.showList = !show;
    }
  }

  handlerSearchView(show?: boolean): void {
    if (!this.platformService.isDesktop) {
      this.showList = show;
    }
  }

  viewDetailEvent(item: IItemMarker): void {
    this.googleMapService.eventEmit(GoogleMapsEvents.VIEW_DETAILS, item);
    if (!this.platformService.isDesktop) {
      this.showList = false;
    }
    setTimeout(() => {
      this.viewDetail.next(item);
    }, 10);
  }

  appointment(marker: IItemMarker): void {
    this.goToAppointment.next(marker);
  }

  search(words: Array<string>): void {
    if (words && !words.length) {
      this.currentTextSearch = undefined;
      const arr = this.gmFilterService.getModifiedItems() || this.items;
      setTimeout(() => {
        this.tmpItems = [...arr];
        this.googleMapService.eventEmit(GoogleMapsEvents.CHANGE_MARKERS, this.tmpItems);
      });

      return void 0;
    }

    const tmpItems = this.fnSearch(words);
    setTimeout(() => (this.tmpItems = tmpItems), 0);
    this.googleMapService.eventEmit(GoogleMapsEvents.CHANGE_MARKERS, tmpItems);
  }

  fnSearch(words: Array<string>): Array<IItemMarker> {
    let tmpWords = [];
    (words || []).forEach(word => {
      const w = word.split(',');
      tmpWords = tmpWords.concat(w);
    });
    // tslint:disable-next-line: no-parameter-reassignment
    words = tmpWords.filter(tw => tw);
    this.currentTextSearch = Array.isArray(words) ? (words.length ? words : undefined) : undefined;

    let tmpItems = this.gmFilterService.getModifiedItems() || this.items;
    tmpItems = words && words.length ? this.searchByWords(tmpItems, words) : tmpItems;
    tmpItems = this.formatRanking(tmpItems, words);
    tmpItems = this.orderByPipe.transform(tmpItems || [], ['-ranking', 'distance', 'title']);

    if (tmpItems.length && tmpItems[0].showFavoriteButton) {
      tmpItems = ArrayUtil.customSort(tmpItems, [true, false], 'isFavorite');
    }

    return tmpItems;
  }

  searchByWords(items: Array<IItemMarker>, words: Array<string>): Array<IItemMarker> {
    return items.filter(item => {
      const title = this.getFormat(item.title);
      const department = this.getFormat(item.department);
      const province = this.getFormat(item.province);
      const district = this.getFormat(item.district);
      const address = this.getFormat(item.address);

      return words.find(word => {
        // tslint:disable-next-line: no-parameter-reassignment
        word = StringUtil.withoutDiacritics(word.toLowerCase() || '');
        const hasTitle = this.hasKey(title, word);
        const hasDepartment = this.hasKey(department, word);
        const hasProvince = this.hasKey(province, word);
        const hasDistrict = this.hasKey(district, word);
        const hasAdress = this.hasKey(address, word);

        return hasTitle || hasDepartment || hasProvince || hasDistrict || hasAdress;
      });
    });
  }

  formatRanking(items: Array<IItemMarker>, words: Array<string>): Array<IItemMarker> {
    return items.map((item, i) => {
      let matches = 0;
      words.forEach(word => {
        // tslint:disable-next-line: no-parameter-reassignment
        word = StringUtil.withoutDiacritics(word.toLowerCase());
        matches = this.getMatch(item.title, word, matches);
        matches = this.getMatch(item.department, word, matches);
        matches = this.getMatch(item.province, word, matches);
        matches = this.getMatch(item.district, word, matches);
        matches = this.getMatch(item.address, word, matches);
      });
      item.ranking = matches;

      return item;
    });
  }

  handlerClass(add = true): void {
    const headerElements = this.document.getElementsByTagName('body');
    if (headerElements && headerElements.length) {
      this.renderer2[add ? 'addClass' : 'removeClass'](headerElements[0], 'g-maps--global');
    }
  }

  handlerFilter(): void {
    this.gmFilterService
      .onEvent()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        if (res) {
          switch (res.event) {
            case GmFilterEvents.FILTER:
              if (res.data) {
                this.gmFilterService.setModifiedItems(res.data);
                const tmpItems = this.fnSearch([]);
                setTimeout(() => (this.tmpItems = tmpItems), 0);
                this.googleMapService.eventEmit(GoogleMapsEvents.CHANGE_MARKERS, tmpItems);
              }
              break;
            case GmFilterEvents.FAVORITE_MARKER:
              if (res.data) {
                res.data.isFavorite = !res.data.isFavorite;
                this.favorite.next(res.data);
              }
              break;
            default:
              break;
          }
        }
      });
  }

  getFormat(word: string): string {
    return StringUtil.withoutDiacritics(word.toLowerCase() || '');
  }

  hasKey(criteria: string, word: string): boolean {
    return !!criteria
      .split(' ')
      .concat(criteria)
      .find(e => e.startsWith(word));
  }

  getMatch(key, word, matches): number {
    // tslint:disable-next-line: restrict-plus-operands
    return StringUtil.withoutDiacritics(key.toLowerCase()).indexOf(word) !== -1 ? matches + 1 : matches;
  }

  sortByProperty(array: Array<IItemMarker>, attrib: string): Array<IItemMarker> {
    return array.sort((a, b) => (a[attrib] > b[attrib] ? 1 : b[attrib] > a[attrib] ? -1 : 0));
  }

  centerButtonFn(): void {
    this.centerButtonClick.next();
  }
}
