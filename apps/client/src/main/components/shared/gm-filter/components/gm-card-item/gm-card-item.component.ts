import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { environment } from '@mx/environments/environment';
import { GeneralService } from '@mx/services/general/general.service';
import { HEART_ICON, HEART_OUT_ICON } from '@mx/settings/constants/images-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { GmFilterLang } from '../../gm-filter.lang';
import { GmFilterEvents, GmFilterService } from '../../providers/gm-filter.service';

@Component({
  selector: 'client-gm-card-item',
  templateUrl: './gm-card-item.component.html'
})
export class GmCardItemComponent extends GaUnsubscribeBase implements OnInit {
  @Input() item: IItemMarker;
  @Input() active: boolean;
  @Input() configView: any;
  @Input() policy: IPoliciesByClientResponse;

  @Output() viewOnMap: EventEmitter<any>;
  @Output() viewDetail: EventEmitter<any>;
  @Output() goToAppointment: EventEmitter<IItemMarker> = new EventEmitter();

  buttonTexts = GmFilterLang.Buttons;
  noInfo = GeneralLang.Texts.WithOutInformation;
  deductibles: string;
  heartIcon = HEART_ICON;
  heartOutIcon = HEART_OUT_ICON;
  activateSearchClinicKey = environment.ACTIVATE_SEARCH_CLINIC_CLD;
  codRamoClinicDigital = 275;
  module = 'CDM';

  constructor(
    private readonly gmFilterService: GmFilterService,
    protected readonly _GeneralService: GeneralService,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.viewOnMap = new EventEmitter<any>();
    this.viewDetail = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.deductibles = this.item ? PolicyUtil.getDeductibleText(this.item.payload) : null;
  }

  viewMaker(): void {
    this.viewOnMap.next();
  }

  detail(): void {
    this.viewDetail.next();
  }

  appointment(): void {
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AGENDA_ESPECIALISTA_CDM),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this.goToAppointment.next();
  }

  favoriteFn(): void {
    this.gmFilterService.eventEmit(GmFilterEvents.FAVORITE_MARKER, this.item);
  }
}
