import { DOCUMENT } from '@angular/common';
import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  HostBinding,
  Inject,
  Input,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { Subscription } from 'rxjs';
import { FilterMapDirective } from '../../directives/filter.directive';
import { GmHandlerShow } from '../../extends/gm-handler-show';
import { GmFilterLang } from '../../gm-filter.lang';
import { GmFilterViewItem } from '../../providers/gm-filter-view-item';

@Component({
  selector: 'client-gm-filter-view',
  templateUrl: './gm-filter-view.component.html'
})
export class GmFilterViewComponent extends GmHandlerShow implements OnInit {
  @HostBinding('attr.class') attr_class = 'g-map-filter--filter g-bg-c--white1 h-100 d-none';

  @ViewChild(FilterMapDirective) filterMap: FilterMapDirective;

  @Input() gmFilterViewItem: GmFilterViewItem;

  buttonTexts = GmFilterLang.Buttons;
  buttonState = true;
  filterButtonStateSub: Subscription;

  componentRef: ComponentRef<any>;
  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    @Inject(DOCUMENT) protected document: Document,
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly fb: FormBuilder,
    protected gaService: GAService
  ) {
    super(render2, elementRef, platformService, resizeService, gaService, document);
  }

  ngOnInit(): void {
    this.loadComponent();
  }

  loadComponent(): void {
    if (this.gmFilterViewItem) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.gmFilterViewItem.component);

      const viewContainerRef = this.filterMap.viewContainerRef;
      viewContainerRef.clear();

      this.componentRef = viewContainerRef.createComponent(componentFactory);
      this.componentRef.instance.fb = this.fb;
      this.componentRef.instance.ga = this.ga;
      this.filterButtonStateSub = this.componentRef.instance.filterButtonState.subscribe(
        state => (this.buttonState = state)
      );
    }
  }

  clearFilter(): void {
    if (this.componentRef) {
      this.componentRef.instance.reset();
    }
  }

  applyFilterEvent(): void {
    if (this.componentRef) {
      this.componentRef.instance.applyFilter.next();
    }
    setTimeout(() => {
      this.close();
    }, 100);
  }

  back(): void {
    if (this.componentRef) {
      this.componentRef.instance.resetState();
    }
    this.close();
  }

  mfOnDestroy(): void {
    if (this.filterButtonStateSub) {
      this.filterButtonStateSub.unsubscribe();
    }
  }
}
