import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderByPipe } from '@mx/core/shared/common/pipes/orderby.pipe';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { InputSearchModule } from '../input-search/input-search.module';
import { ItemNotFoundModule } from '../item-not-found/item-not-found.module';
import { GmCardFilterComponent } from './components/gm-card-filter/gm-card-filter.component';
import { GmCardItemComponent } from './components/gm-card-item/gm-card-item.component';
import { GmFilterViewComponent } from './components/gm-filter-view/gm-filter-view.component';
import { GmListComponent } from './components/gm-list/gm-list.component';
import { FilterMapDirective } from './directives/filter.directive';
import { GmFilterComponent } from './gm-filter.component';
import { GmFilterService } from './providers/gm-filter.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputSearchModule,
    ItemNotFoundModule,
    MfLoaderModule,
    GoogleModule
  ],
  declarations: [
    GmFilterComponent,
    GmListComponent,
    GmCardItemComponent,
    GmCardFilterComponent,
    GmFilterViewComponent,
    OrderByPipe,
    FilterMapDirective
  ],
  exports: [GmFilterComponent, GmListComponent, GmCardItemComponent, GmCardFilterComponent, GmFilterViewComponent],
  providers: [PlatformService, GmFilterService]
})
export class GmFilterModule {}
