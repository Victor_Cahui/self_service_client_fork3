import { DOCUMENT, Location } from '@angular/common';
import { Component, ElementRef, HostBinding, Inject, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { GROUP_TYPE_ID_COMPANY } from '@mx/settings/auth/auth-values';
import { MFP_Mi_Cuenta_5B } from '@mx/settings/constants/events.analytics';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { Observable, Subscription } from 'rxjs';
import { MenuComponent } from '../utils/menu';

@Component({
  selector: 'client-main-header',
  templateUrl: './main-header.component.html'
})
export class MainHeaderComponent implements OnInit, OnDestroy {
  @HostBinding('class') class = 'col-12 g-oim--header g-header min-height--70';
  @HostBinding('class.g-box--shadow1') box: boolean;
  @ViewChild('dropUser') dropUser: ElementRef;
  @ViewChild(MfModalAlertComponent) mfModalConfirm: MfModalAlertComponent;

  profileLang = ProfileLang;
  yes = GeneralLang.Buttons.Yes;
  no = GeneralLang.Buttons.No;
  messageConfirmLogout = ProfileLang.Messages.ConfirmLogout;
  titleHeader: string;
  subTitleHeader: string;
  subIconHeader: string;
  routeback: string;
  nameClient: string;
  businessNameClient: string;
  nameClientTitle: string;
  dataClient: Observable<IClientInformationResponse>;

  routerSubs: Subscription;
  headerSub: Subscription;
  tabSub: Subscription;
  userDataSub: Subscription;

  boxShadow: boolean;
  showModal: boolean;
  subBack: boolean;
  droppedDown: boolean;
  isEnterprise: boolean;
  notifications = 0;

  gaMyProfile: IGaPropertie;
  gaLogout: IGaPropertie;
  routeBack: string;

  constructor(
    private readonly authService: AuthService,
    private readonly clientService: ClientService,
    private readonly location: Location,
    private readonly headerHelperService: HeaderHelperService,
    private readonly _Title: Title,
    private readonly renderer2: Renderer2,
    private readonly router: Router,
    protected localStorageService: LocalStorageService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {
    this.box = true;
    this.gaMyProfile = MFP_Mi_Cuenta_5B(this.profileLang.Buttons.MyProfile);
    this.gaLogout = MFP_Mi_Cuenta_5B(this.profileLang.Buttons.Logout);
  }

  ngOnInit(): void {
    const logguedUser: ILogguedUser = this.localStorageService.getData(LOCAL_STORAGE_KEYS.USER);
    if (logguedUser && logguedUser.groupTypeId) {
      this.isEnterprise = +logguedUser.groupTypeId === GROUP_TYPE_ID_COMPANY;
    }

    this.userDataSub = this.clientService.getUserDataSubject().subscribe(data => {
      if (data) {
        this.setInformation(data);
      } else {
        const dataUser = this.clientService.getUserData();
        if (dataUser) {
          this.setInformation(dataUser);
        }
      }
    });

    this.headerSub = this.headerHelperService.get().subscribe((res: IHeader) => {
      this.titleHeader = res.title;
      this.subTitleHeader = res.subTitle;
      this._Title.setTitle(this.titleHeader);
      this.subBack = res.back;
      this.subIconHeader = res.icon;
      this.routeback = res.routeback;
    });

    this.tabSub = this.headerHelperService.getTab().subscribe((res: boolean) => {
      this.boxShadow = res;
      this.box = this.boxShadow ? false : true;
    });
  }

  setInformation(data: IClientInformationResponse): void {
    if (data.nombreCliente) {
      this.nameClient = data.nombreCliente.split(' ')[0];
      this.nameClientTitle = `${ProfileLang.Texts.Welcome} ${this.nameClient}`;
    }
    if (this.isEnterprise) {
      this.businessNameClient = data.razonSocial;
    }
  }

  ngOnDestroy(): void {
    if (!!this.headerSub) {
      this.headerSub.unsubscribe();
    }
    if (!!this.tabSub) {
      this.tabSub.unsubscribe();
    }
  }

  logoutModal(): void {
    this.showModal = true;
    setTimeout(() => {
      this.mfModalConfirm.open();
    });
  }

  logoutUser(confirmed?: boolean): void {
    if (confirmed) {
      localStorage.setItem('canReload', '1');
      document.getElementsByTagName('body')[0].style.display = 'none';
      this.authService.logout();
    }
  }

  openNavBox(): void {
    MenuComponent.openNav();
  }
  closeNavBox(): void {
    MenuComponent.closeNav();
  }

  closeDropDownUser(): void {
    this.dropUser.nativeElement.checked = !this.dropUser.nativeElement.checked;
  }

  dropDownUser(): void {
    const dropdown = this.document.querySelector('.g-dropdown--click.g-dropwdown--user');
    const dropdownItem = this.document.querySelector('.g-dropdown--show.g-dropdown--user');
    const classList = Array.from(dropdown.classList);
    if (classList.find(c => c === 'active')) {
      this.droppedDown = false;
      this.renderer2.removeClass(dropdown, 'active');
      this.renderer2.removeClass(dropdownItem, 'active');
      this.closeNavBox();
    } else {
      this.droppedDown = true;
      this.renderer2.addClass(dropdown, 'active');
      this.renderer2.addClass(dropdownItem, 'active');
    }
  }

  clickOutside(): void {
    if (this.droppedDown) {
      this.dropDownUser();
      delete this.droppedDown;
    }
  }

  goBack(routeback): void {
    if (this.routeback && this.routeback !== '') {
      this.router.navigate([routeback]);
    } else {
      this.location.back();
    }
  }
}
