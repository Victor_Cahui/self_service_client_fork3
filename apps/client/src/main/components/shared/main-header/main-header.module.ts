import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfModalAlertModule } from '@mx/core/ui';
import { UiModule } from '@mx/core/ui/lib/ui.module';
import { MainHeaderComponent } from './main-header.component';

@NgModule({
  imports: [CommonModule, UiModule, RouterModule, MfModalAlertModule, DirectivesModule, GoogleModule],
  declarations: [MainHeaderComponent],
  exports: [MainHeaderComponent]
})
export class MainHeaderModule {}
