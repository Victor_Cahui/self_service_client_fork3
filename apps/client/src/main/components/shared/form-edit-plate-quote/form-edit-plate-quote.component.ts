import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MfInputComponent } from '@mx/core/ui/lib/components/forms/input/input.component';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { MFP_Cotizacion_9A } from '@mx/settings/constants/events.analytics';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH,
  REG_EX
} from '@mx/settings/constants/general-values';
import { CURRENT_OWN_POLICY, CURRENT_POLICY } from '@mx/settings/constants/policy-values';
import * as source from '@mx/settings/lang/health.lang';
import * as LANG from '@mx/settings/lang/vehicles.lang';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IVerifyPlateRequest, IVerifyPlateResponse } from '@mx/statemanagement/models/vehicle.interface';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-form-edit-plate-quote',
  templateUrl: './form-edit-plate-quote.component.html'
})
export class FormEditPlateQuoteComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(MfInputComponent) inputPlate: MfInputComponent;

  @Input() isModal: boolean;
  @Output() enableButton = new EventEmitter<boolean>();
  @Output() showMessage = new EventEmitter<any>();
  @Output() formSuccess = new EventEmitter<any>();
  @Output() showLoader = new EventEmitter<any>();

  formQuote: FormGroup;
  plate: AbstractControl;
  checkNoPlate: AbstractControl;

  title = LANG.VehiclesLang.Titles.QuoteAndPurchase;
  textQuote = source.HealthLang.Texts.Cotizar;
  labelPlate = LANG.VehiclesLang.Labels.EnterYourPlate;
  labelCheckNoPlate = LANG.VehiclesLang.Labels.NoPlate;
  btnQuote = LANG.VehiclesLang.Buttons.Quote;
  plateMinLength = GENERAL_MIN_LENGTH.numberPlate;
  plateMaxLength = GENERAL_MAX_LENGTH.numberPlate;
  plateRegEx = REG_EX.plate;

  formChangesSub: Subscription;
  vehicleServiceSub: Subscription;
  formQuotePlateSub: Subscription;
  formQuoteSub: Subscription;
  sendDataSub: Subscription;

  environment = environment;
  activatedNoPlate = false;
  isCheckedNoPlate = false;
  auth: IdDocument;
  showLoading: boolean;
  validForm: boolean;

  gaQuoteNow: IGaPropertie;

  constructor(
    private readonly formBuilder: FormBuilder,
    public authService: AuthService,
    private readonly vehicleService: VehicleService,
    private readonly quoteService: PoliciesQuoteService,
    private readonly router: Router,
    protected gaService: GAService
  ) {
    super(gaService);
    this.gaQuoteNow = MFP_Cotizacion_9A(this.btnQuote);
  }

  ngOnInit(): void {
    this.initForm();
    this.auth = this.authService.retrieveEntity();
  }

  initForm(): void {
    if (this.formQuote) {
      return;
    }
    const data = this.quoteService.getVehicleData();
    if (data && data.placaEnTramite && this.isModal) {
      this.isCheckedNoPlate = data.placaEnTramite;
    }
    this.formQuote = this.formBuilder.group({
      plate: [
        { value: data && data.placa && this.isModal ? data.placa : '', disabled: false },
        Validators.pattern(this.plateRegEx)
      ],
      checkNoPlate: [{ value: data && data.placaEnTramite && this.isModal ? data.placaEnTramite : '', disabled: false }]
    });
    this.plate = this.formQuote.get('plate');
    this.checkNoPlate = this.formQuote.get('checkNoPlate');
    this.valueChangesPlate();
  }

  ngOnDestroy(): void {
    if (this.formChangesSub) {
      this.formChangesSub.unsubscribe();
    }
    if (this.vehicleServiceSub) {
      this.vehicleServiceSub.unsubscribe();
    }
    if (this.formQuotePlateSub) {
      this.formQuotePlateSub.unsubscribe();
    }
    if (this.formQuoteSub) {
      this.formQuoteSub.unsubscribe();
    }
    if (this.sendDataSub) {
      this.sendDataSub.unsubscribe();
    }
  }

  // Buscar placa
  searchPlate(): void {
    if (!this.isCheckedNoPlate && (!this.formQuote.controls.plate.value || !this.formQuote.valid)) {
      return;
    }
    if (!environment.ACTIVATE_VEHICLES_QUOTE_BUTTON) {
      return;
    }
    this.quoteService.setChoiseData(null);

    // Si tiene placa
    if (!this.isCheckedNoPlate) {
      // Verifica cambios
      const plate = this.quoteService.getVehiclePlate();
      if (plate === this.plate.value) {
        const data = this.quoteService.getVehicleData();
        if (data.flagEstadoPoliza === CURRENT_OWN_POLICY || data.flagEstadoPoliza === CURRENT_POLICY) {
          this.showModal(data);
        } else {
          this.goDetail();
        }
        // Busca en servicio
      } else {
        this.loader(true);
        const params: IVerifyPlateRequest = {
          codigoApp: APPLICATION_CODE,
          tipoDocumento: this.auth.type,
          documento: this.auth.number,
          placa: this.plate.value
        };
        this.vehicleServiceSub = this.vehicleService.verifyPlate(params).subscribe(
          (res: IVerifyPlateResponse) => {
            if (res) {
              if (res.flagEstadoPoliza === CURRENT_OWN_POLICY || res.flagEstadoPoliza === CURRENT_POLICY) {
                this.showModal(res);
              } else {
                this.quoteService.setVehicleRegistred(res);
                this.goDetail();
              }
            } else {
              this.quoteService.setVehiclePlate(this.plate.value, this.isCheckedNoPlate);
              this.goDetail();
            }
          },
          () => {
            this.loader(false);
          },
          () => {
            this.loader(false);
            this.vehicleServiceSub.unsubscribe();
          }
        );
      }
    } else {
      // Guardar placa en Storage
      this.quoteService.setVehiclePlate(this.plate.value, this.isCheckedNoPlate);
      this.goDetail();
    }
  }

  // Ir a Detalle o volver al form
  goDetail(): void {
    if (this.isModal) {
      this.formSuccess.emit();
    } else {
      this.router.navigate(['/vehicles/quote/detail']);
    }
  }

  showModal(data): void {
    this.showMessage.emit({ show: true, flag: data.flagEstadoPoliza, date: data.fechaVigencia });
  }

  // Loader para modal
  loader(value): void {
    if (!this.isModal) {
      this.showLoading = value;
    }
    this.showLoader.emit(value);
  }

  // Checked
  checkedNoPlate(event): void {
    this.plate.setValue('');
    setTimeout(() => (this.isCheckedNoPlate = event.target.checked));
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  setFocusPlateInput(): void {
    this.inputPlate.input_native.nativeElement.focus();
  }

  valueChangesPlate(): void {
    this.formQuoteSub = this.formQuote.valueChanges.subscribe(() => {
      this.validForm = this.isCheckedNoPlate || (this.formQuote.controls.plate.value && this.formQuote.valid);
      this.enableButton.emit(this.validForm);
    });
    this.formQuotePlateSub = this.formQuote.controls['plate'].valueChanges.subscribe(() => {
      if (this.plate.value.trim().length) {
        this.isCheckedNoPlate = false;
      }
    });
  }

  handlePaste(event): void {
    let clipboard = event.clipboardData.getData('text/plain');
    clipboard = clipboard.replace(/[^a-zA-Z0-9]/g, '');
    setTimeout(() => {
      this.formQuote.controls['plate'].setValue(clipboard);
    });
  }

  handlerkeyDown(event): void {
    if (event.keyCode === 13 && this.validForm) {
      this.searchPlate();
    }
  }
}
