import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { IconCoverageComponent } from './icon-coverage.component';

@NgModule({
  imports: [CommonModule, RouterModule, TooltipsModule],
  exports: [IconCoverageComponent],
  declarations: [IconCoverageComponent]
})
export class IconCoverageModule {}
