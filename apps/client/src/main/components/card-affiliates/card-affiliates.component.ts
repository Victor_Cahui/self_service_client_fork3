import { CurrencyPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { APPLICATION_CODE, SumaAseguradaInfo } from '@mx/settings/constants/general-values';

import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AffiliatesFacade } from '@mx/core/shared/state/affiliates';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { EPS_DESC_TIPO_SUMA_ASEG } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-card-affiliates',
  templateUrl: './card-affiliates.component.html',
  providers: [CurrencyPipe]
})
export class CardAffiliatesComponent implements OnInit {
  @Input() policyNumber?: string;
  collapse = false;
  disableCollapse: boolean;
  lang = GeneralLang;
  list: any[];
  showLoading = true;
  isVisibleSeeAll: boolean;

  constructor(
    private readonly _AffiliatesFacade: AffiliatesFacade,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _CurrencyPipe: CurrencyPipe,
    private readonly _GeneralService: GeneralService
  ) {}

  ngOnInit(): void {
    this._initValues();
    this.getList();
  }

  getList(): void {
    this.showLoading = true;
    const bodyTooltip = this._GeneralService.getValueParams(EPS_DESC_TIPO_SUMA_ASEG);

    this._Autoservicios
      .ObtenerEpsContratanteDatosCardAfiliados(
        { nroContrato: this.policyNumber },
        {
          codigoApp: APPLICATION_CODE
        }
      )
      .subscribe(
        res => {
          this.showLoading = false;
          if (!res) {
            return void 0;
          }
          this.list = res.map(r => ({
            ...r,
            money: this._getMoney(r),
            titleTooltip: SumaAseguradaInfo[r.tipoSumaAsegurada].title,
            bodyTooltip
          }));
        },
        e => {
          this.showLoading = false;
          console.error(e);
        }
      );
  }

  private _getMoney(aff): any {
    if (aff.tipoSumaAsegurada === SumaAseguradaInfo.ILIMITADA.code) {
      return 'ilimitada';
    }

    return this._CurrencyPipe.transform(aff.sumaAsegurada, `${StringUtil.getMoneyDescription(aff.codigoMoneda)} `);
  }

  private _initValues(): void {
    this.isVisibleSeeAll = environment.ACTIVATE_EPS_VIEW_AFFILIATES;
    this._AffiliatesFacade.AddNroPolicy(this.policyNumber);
  }
}
