import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { GoogleMapService } from '@mx/services/google-map/google-map.service';
import { OfficeDetailComponent } from './office-detail.component';

@NgModule({
  imports: [CommonModule, MfLoaderModule],
  entryComponents: [OfficeDetailComponent],
  exports: [OfficeDetailComponent],
  declarations: [OfficeDetailComponent],
  providers: [GeolocationService, GoogleMapService]
})
export class OfficeDetailModule {}
