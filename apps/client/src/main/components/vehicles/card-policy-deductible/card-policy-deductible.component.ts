import { Component, Input, OnInit } from '@angular/core';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { IDeductiblePolicy } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-policy-deductible',
  templateUrl: './card-policy-deductible.component.html'
})
export class CardPolicyDeductibleComponent implements OnInit {
  @Input('item') item: IDeductiblePolicy;
  title: string;

  ngOnInit(): void {
    this.setHeaderTitle();
  }

  setHeaderTitle(): void {
    this.title = `${
      this.item.vehiculo.placaEnTramite ? 'PLACA EN TRÁMITE ' : this.item.vehiculo.placa
    } - ${this.complexModel()}`;
  }

  complexModel(): string {
    return `${this.item.vehiculo.marca} ${this.item.vehiculo.modelo} ${this.item.vehiculo.anio}`;
  }

  trackByFn(index, item): number {
    return item;
  }

  getMoneyDescription(moneyCode: number): string {
    return StringUtil.getMoneyDescription(moneyCode).concat(' ');
  }
}
