import { CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { CAR_ICON } from '@mx/settings/constants/images-values';
import { SOAT_DAYS_DATE_START_VALIDITY } from '@mx/settings/constants/key-values';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { ICardTitle } from '@mx/statemanagement/models/general.models';
import { IClientVehicleDetail } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-quote-buy-detail',
  templateUrl: './quote-buy-detail.component.html',
  providers: [CurrencyPipe]
})
export class QuoteBuyDetailComponent implements OnInit {
  lang = VehiclesLang;
  vehicleData: IClientVehicleDetail;
  vehicleValue: string;
  vehicleDescription: string;
  key = SOAT_DAYS_DATE_START_VALIDITY;
  riskCard: ICardTitle = {
    title: VehiclesLang.Titles.DataYourVehicle,
    icon: CAR_ICON,
    collapse: true
  };
  currentStep: number;
  currentStepSub: Subscription;
  showVigency = false;

  constructor(private readonly quoteService: PoliciesQuoteService, private readonly currencyPipe: CurrencyPipe) {
    this.currentStepSub = this.quoteService.getCurrentStepSubject().subscribe(
      data => {
        this.currentStep = data;
        this.showVigency = this.currentStep === this.quoteService.getLastStep();
        if (this.currentStep === 2) {
          this.getVehicleData();
        }
      },
      () => {},
      () => {
        this.currentStepSub.unsubscribe();
      }
    );
  }

  ngOnInit(): void {
    this.getVehicleData();
  }

  // Datos del Vehículo
  getVehicleData(): void {
    this.vehicleData = this.quoteService.getVehicleData();
    if (this.vehicleData) {
      const currency = `${StringUtil.getMoneyDescription(this.vehicleData.codigoMonedaValorSugerido)} `;
      this.vehicleValue = this.currencyPipe.transform(this.vehicleData.valorSugerido, currency);
      this.vehicleDescription = `${this.vehicleData.marca} ${this.vehicleData.modelo} ${this.vehicleData.anio}`.toLowerCase();
      this.vehicleData.uso = this.vehicleData.uso.toLowerCase();
      this.vehicleData.ciudad = this.vehicleData.ciudad.toLowerCase();
      this.vehicleData.color = (this.vehicleData.color && this.vehicleData.color.toLowerCase()) || '';
    }
  }
}
