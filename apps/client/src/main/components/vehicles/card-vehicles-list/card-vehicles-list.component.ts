import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { ScrollService } from '@mx/services/scroll.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE, URL_FRAGMENTS } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IClientVehicleDetail, IVehiclesListRequest } from '@mx/statemanagement/models/vehicle.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-card-vehicles-list',
  templateUrl: './card-vehicles-list.component.html'
})
export class CardVehiclesListComponent extends UnsubscribeOnDestroy implements OnInit {
  @Input() isPPFM: boolean;

  policyNumber: string;
  policyType: string;
  policyIcon: string;
  vehiclesList: Array<IClientVehicleDetail>;
  titleMyVehicles = VehiclesLang.Titles.MyVehicles;
  labelViewDetail = GeneralLang.Buttons.ViewDetail;
  textInProcess = VehiclesLang.Texts.InProcess;
  labelUse = VehiclesLang.Labels.UseVehicle;
  labelChassis = VehiclesLang.Labels.ChassisNumber;
  labelMotor = VehiclesLang.Labels.MotorNumber;
  labelColor = VehiclesLang.Labels.Color;
  insuredFragment = URL_FRAGMENTS.INSURED;
  idVehicle;
  collapse = false;
  showLoading: boolean;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly vehicleService: VehicleService,
    private readonly scrollService: ScrollService,
    private readonly router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.policyIcon = POLICY_TYPES.MD_AUTOS.icon;

    const param: IVehiclesListRequest = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber
    };
    this.showLoading = true;
    this.vehicleService
      .getVehiclesList(param)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IClientVehicleDetail>) => {
        response.map(v => {
          v.title = `${v.placaEnTramite ? this.textInProcess : v.placa} - ${v.marca} ${v.modelo} ${v.anio}`;
        });
        this.vehiclesList = response;
        this.scrollService.checkScrollTo(this.insuredFragment);
        this.showLoading = false;
      });
  }

  goToDetailVehicle(vehicle: IClientVehicleDetail): void {
    const path = this.getPath(vehicle);
    this.router.navigate([path], { preserveQueryParams: true });
  }

  getPath(vehicle): string {
    const query = `${this.policyNumber}/${vehicle.numeroChasis}/${vehicle.numeroMotor}`;
    this.idVehicle = query;

    return `/vehicles/vehicularInsurance/detail-vehicle/${query}`;
  }
}
