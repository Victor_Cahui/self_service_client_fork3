import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MfModal } from '@mx/core/ui';
import { FAIL_ICON, MAIL_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehicleQuoteBuyLang, VehiclesLang } from '@mx/settings/lang/vehicles.lang';

@Component({
  selector: 'client-modal-error-buy-step-one',
  templateUrl: './modal-error-buy-step-one.component.html'
})
export class ModalErrorBuyStepOneComponent implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;

  @Input() chassis: number;
  @Input() motor: number;

  lang = VehicleQuoteBuyLang;
  vehicleLang = VehiclesLang;
  generalLang = GeneralLang;
  failIcon = FAIL_ICON;
  mailIcon = MAIL_ICON;

  ngOnInit(): void {}
}
