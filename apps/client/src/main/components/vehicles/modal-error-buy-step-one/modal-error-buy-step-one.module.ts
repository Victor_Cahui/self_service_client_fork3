import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfModalModule } from '@mx/core/ui';
import { ModalErrorBuyStepOneComponent } from './modal-error-buy-step-one.component';

@NgModule({
  imports: [CommonModule, MfModalModule, RouterModule],
  declarations: [ModalErrorBuyStepOneComponent],
  exports: [ModalErrorBuyStepOneComponent]
})
export class ModalErrorBuyStepOneModule {}
