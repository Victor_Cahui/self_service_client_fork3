import { OnDestroy, OnInit } from '@angular/core';
import { Router, RouterState } from '@angular/router';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { stringYYYYMMDDToDate } from '@mx/core/shared/helpers/util/date';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE, MAX_NUMBER_ITEMS_HORIZONTAL_TIMELINE } from '@mx/settings/constants/general-values';
import { CAR_ICON, PROFILE_ICON } from '@mx/settings/constants/images-values';
import {
  VEHICLE_ASISST_STATE_DELIVERED,
  VEHICLE_ASISST_STATE_REJECTED,
  YEARS_ANTIQUITY_ASISST_VEHICLE_HISTORY,
  YEARS_ANTIQUITY_ASSIST_DEFAULT
} from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { ASSISTANCE } from '@mx/settings/lang/vehicles.lang';
import { IConfigUICardViewItemList, IItemView, IParameterApp } from '@mx/statemanagement/models/general.models';
import { IRequestItemView, ITimelimeStep } from '@mx/statemanagement/models/timeline.interface';
import {
  IVehicleAssistsHistoryResponse,
  IVehicleAssistsProcessResponse
} from '@mx/statemanagement/models/vehicle.interface';
import { isAccidentsPath, isStolenPath } from '@mx/views/vehicles/vehicles-records/vehicles-records.utils';
import { isEmpty } from 'lodash-es';
import { Observable, Subscription } from 'rxjs';

export abstract class BaseCardAssistanceVehicle extends ConfigurationBase implements OnInit, OnDestroy {
  vehicleSub: Subscription;
  configUI: IConfigUICardViewItemList;
  itemsProcess: Array<IRequestItemView>;
  itemsHistory: Array<IRequestItemView>;
  showLoading: boolean;
  showNotification: boolean;
  yearsRange: number;
  lang = ASSISTANCE;
  parametersSubject: Observable<Array<IParameterApp>>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService: GAService,
    protected generalService: GeneralService,
    protected _Router: Router
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService);
    this.configUI = { hasFilter: false, hasShowMore: true };
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.loadConfiguration();
    this.getParameters();
    this.getItems(this.getParamsForRecords());
  }

  ngOnDestroy(): void {
    if (this.vehicleSub) {
      this.vehicleSub.unsubscribe();
    }
  }

  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(() => {
      this.yearsRange =
        Number(this.generalService.getValueParams(YEARS_ANTIQUITY_ASISST_VEHICLE_HISTORY)) ||
        YEARS_ANTIQUITY_ASSIST_DEFAULT;
    });
  }

  getParamsForRecords(): any {
    return {
      codigoApp: APPLICATION_CODE
    };
  }

  // Configurar vista
  formatHistory(histories: Array<IVehicleAssistsHistoryResponse>): Array<IRequestItemView> {
    let itemList = [];
    itemList = (histories || []).map(item => {
      const itemAssist: IRequestItemView = {
        registerDate: item.fechaReporteAccidente,
        updateDate: item.ultimaFechaActualizacion,
        assistanceNumber: item.numeroAsistencia,
        policyNumber: item.numeroPoliza,
        productName: item.nombreProducto,
        currentState: (item.estadoAsistencia || '').toUpperCase(),
        year: this.calculateYear(item.fechaReporteAccidente),
        itemList: this.setItemLits(item),
        stateColor: item.codigoEstado === VEHICLE_ASISST_STATE_REJECTED ? 'gray' : ''
      };

      return itemAssist;
    });

    return itemList;
  }

  sortHistory(
    histories: Array<IVehicleAssistsHistoryResponse>,
    propToSorting: string,
    sorting: 'ASC' | 'DESC'
  ): Array<IRequestItemView> {
    const arrHistory = this.formatHistory(histories).map(h => ({
      ...h,
      registerDateObj: stringYYYYMMDDToDate(h.registerDate)
    }));

    return ArrayUtil.arraySortByKey(arrHistory, propToSorting, sorting);
  }

  formatProcess(processes: Array<IVehicleAssistsProcessResponse>): Array<IRequestItemView> {
    // tslint:disable-next-line: no-parameter-reassignment
    processes = processes || [];
    this.showNotification = !!processes.find(p => p.mostrarTalleresPreferentes);

    return processes.map((process, index) => {
      process.estados.forEach(estado => {
        estado.nombreEstado =
          estado.codigoEstado === VEHICLE_ASISST_STATE_DELIVERED ? this.lang.Delivery : estado.nombreEstado;

        return estado;
      });
      const item: IRequestItemView = {
        registerDate: process.fechaReporteAccidente,
        assistanceNumber: process.numeroAsistencia,
        policyNumber: process.numeroPoliza,
        productName: process.nombreProducto,
        currentState: process.estados.find(estado => estado.codigoEstado === process.codigoEstado).nombreEstado,
        itemList: this.setItemLits(process),
        isCollapse: true,
        collapse: index > 0,
        showWorkshops: process.mostrarTalleresPreferentes
      };

      if (process.estados.length > MAX_NUMBER_ITEMS_HORIZONTAL_TIMELINE) {
        const executed = process.estados.filter(e => !!e.fechaEstado).length;
        if (executed <= MAX_NUMBER_ITEMS_HORIZONTAL_TIMELINE) {
          process.estados.splice(MAX_NUMBER_ITEMS_HORIZONTAL_TIMELINE);
        } else {
          process.estados = process.estados.slice(
            Math.max(process.estados.length - MAX_NUMBER_ITEMS_HORIZONTAL_TIMELINE, 0)
          );
        }
      }

      item.steps = process.estados.map(estado => {
        const step: ITimelimeStep = {
          description: estado.nombreEstado,
          date: estado.fechaEstado,
          complete: !!estado.fechaEstado
        };

        if (estado.tooltip && estado.tooltip.tipoTooltip) {
          step.tooltip = {
            title: estado.nombreEstado,
            description: estado.tooltip.textoTooltip
          };
          if (estado.tooltip.textoLinkDescargable && estado.tooltip.descargables) {
            step.tooltip.linkText = estado.tooltip.textoLinkDescargable;
            step.tooltip.files = estado.tooltip.descargables.map(descargable => ({
              fileName: descargable.nombreDocumento,
              base64: descargable.base64
            }));
          }
        }

        return step;
      });

      return item;
    });
  }

  abstract getItems(params): void;

  getPageType(): string {
    const state: RouterState = this._Router.routerState;
    const url = state.snapshot.url;

    return isAccidentsPath(url) ? 'accidents' : isStolenPath(url) ? 'stolen' : '';
  }

  getRouteRecords(): IConfigUICardViewItemList {
    return { ...this.configUI, routerLinkViewAll: `/vehicles/${this.getPageType()}/records` };
  }

  private calculateYear(myDate: string): string {
    return !isEmpty(myDate) ? myDate.split('-')[0] : '';
  }

  setItemLits(item: IVehicleAssistsProcessResponse): Array<IItemView> {
    // NOTA: Más de 2 item, revisar maqueta
    return [
      {
        label: this.lang.Attorney,
        text: item.nombreProcurador,
        icon: PROFILE_ICON
      },
      {
        label: item.datosVehiculo,
        icon: CAR_ICON
      }
    ];
  }
}
