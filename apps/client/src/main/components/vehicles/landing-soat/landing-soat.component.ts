import { Component } from '@angular/core';
import { LANDING_SOAT } from '@mx/settings/lang/landing';

@Component({
  selector: 'client-landing-soat',
  templateUrl: './landing-soat.component.html'
})
export class LandingSoatComponent {
  landingContent = LANDING_SOAT;
}
