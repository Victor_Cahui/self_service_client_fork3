import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { LandingSoatComponent } from './landing-soat.component';

@NgModule({
  imports: [CommonModule, LandingModule],
  exports: [LandingSoatComponent],
  declarations: [LandingSoatComponent],
  providers: []
})
export class LandingSoatModule {}
