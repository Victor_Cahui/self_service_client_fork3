import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  DirectivesModule,
  MfButtonModule,
  MfCheckboxModule,
  MfInputModule,
  MfSelectModule,
  MfShowErrorsModule
} from '@mx/core/ui';
import { QuoteBuyAddressComponent } from './quote-buy-address.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfButtonModule,
    MfCheckboxModule,
    MfInputModule,
    MfSelectModule,
    MfShowErrorsModule,
    ReactiveFormsModule
  ],
  declarations: [QuoteBuyAddressComponent],
  exports: [QuoteBuyAddressComponent]
})
export class QuoteBuyAddressModule {}
