import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputWithIconModule } from '@mx/components/shared';
import { DirectivesModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { AffiliateComponent } from './affiliate.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    FormsModule,
    InputWithIconModule,
    MfInputModule,
    MfShowErrorsModule,
    ReactiveFormsModule
  ],
  declarations: [AffiliateComponent],
  exports: [AffiliateComponent]
})
export class AffiliateModule {}
