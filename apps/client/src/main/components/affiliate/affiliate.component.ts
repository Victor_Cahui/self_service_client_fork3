import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FrmProvider } from '@mx/core/shared/providers/services';
import { getUpdatedAffiliate } from '@mx/core/shared/state/affiliates';
import { environment } from '@mx/environments/environment';
import { CIRCLE_CLOSE_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-affiliate',
  templateUrl: './affiliate.component.html'
})
export class AffiliateComponent implements OnInit {
  @Input() item: any = {};
  @Input() policyNumber?: string;
  @Output() getIsEditing: EventEmitter<boolean> = new EventEmitter();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSave: EventEmitter<any> = new EventEmitter();
  closeIcon = CIRCLE_CLOSE_ICON;
  frmDependents: FormGroup;
  frmHolder: FormGroup;
  lang = GeneralLang;
  canEdit = true;
  loading = false;
  canEditByBetaProfile: boolean;
  lblPluralized: string;

  constructor(private readonly _FrmProvider: FrmProvider) {}

  ngOnInit(): void {
    this._initValues();
  }

  toggleDependents(): void {
    if (this.item.canEdit) {
      return void 0;
    }

    this.item = { ...this.item, isVisibleDependent: !this.item.isVisibleDependent };
  }

  edit(): void {
    this._initFrm();
    this.item = { ...this.item, isLoading: false, isVisibleDependent: true };
    this.getIsEditing.emit(true);
    this.canEdit = false;
  }

  save(): void {
    this.item = { ...this.item, isLoading: true, isVisibleDependent: true };
    this.onSave.emit(getUpdatedAffiliate(this.item, this.frmHolder.value, this.frmDependents.value));
    this.loading = true;
  }

  reset(): void {
    this.item = { ...this.item, isLoading: false, isVisibleDependent: true };
    this.getIsEditing.emit(false);
    this.canEdit = true;
  }

  private _initFrm(): void {
    this.frmHolder = this._FrmProvider.AffiliateComponent([this.item]);
    this.frmDependents = this._FrmProvider.AffiliateComponent(this.item.dependientes);
  }

  private _initValues(): void {
    this._initFrm();
    this.canEdit = true;
    this.canEditByBetaProfile = environment.ACTIVATE_EPS_EDIT_AFFILIATE;
    this.loading = false;
    this.lblPluralized = this.item.dependientes.length === 1 ? 'dependiente' : 'dependientes';
  }
}
