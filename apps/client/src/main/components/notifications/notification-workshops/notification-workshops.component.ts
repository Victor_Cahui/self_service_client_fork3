import { Component, OnInit } from '@angular/core';
import { validURL } from '@mx/core/shared/helpers/functions/general-functions';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IWorkshopPDFResponse } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-notification-workshops',
  templateUrl: './notification-workshops.component.html'
})
export class NotificationWorkshopsComponent extends GaUnsubscribeBase implements OnInit {
  notification = VehiclesLang.Messages.WorkshopsNotification;
  workshopsSub: Subscription;
  pdfFiles: IWorkshopPDFResponse;
  isLimaValid: boolean;
  isProvincesValid: boolean;

  constructor(private readonly vehicleService: VehicleService, protected gaService: GAService) {
    super(gaService);
  }

  ngOnInit(): void {
    const params = {
      codigoApp: APPLICATION_CODE
    };
    this.workshopsSub = this.vehicleService.getWorkshopsPDF(params).subscribe(
      (files: IWorkshopPDFResponse) => {
        if (files) {
          this.pdfFiles = files;
          this.isLimaValid = validURL(files.talleresLimaURL);
          this.isProvincesValid = validURL(files.talleresProvinciaURL);
        }
        this.workshopsSub.unsubscribe();
      },
      () => {
        this.workshopsSub.unsubscribe();
      }
    );
  }
}
