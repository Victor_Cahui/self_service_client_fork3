import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCardModule } from '@mx/core/ui';
import { NotificationHelpComponent } from './notification-help.component';

@NgModule({
  imports: [CommonModule, MfCardModule],
  exports: [NotificationHelpComponent],
  declarations: [NotificationHelpComponent]
})
export class NotificationHelpModule {}
