import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui/lib/directives/directives.module';
import { UiModule } from '@mx/core/ui/lib/ui.module';
import { NotificationWorkshopsComponent } from './notification-workshops/notification-workshops.component';
import { NotificationsHomeComponent } from './notifications-home/notifications-home.component';
import { NotificationsPolicyComponent } from './notifications-policy/notifications-policy.component';
import { NotificationsComponent } from './notifications.component';

@NgModule({
  imports: [CommonModule, RouterModule, UiModule, DirectivesModule, GoogleModule],
  exports: [
    NotificationsComponent,
    NotificationsPolicyComponent,
    NotificationsHomeComponent,
    NotificationWorkshopsComponent
  ],
  declarations: [
    NotificationsComponent,
    NotificationsPolicyComponent,
    NotificationsHomeComponent,
    NotificationWorkshopsComponent
  ]
})
export class NotificationsnModule {}
