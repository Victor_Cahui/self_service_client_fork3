import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { BankPaymentModule } from '@mx/components/shared/payments/bank-payment/bank-payment.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardPendingInvoicesComponent } from './card-pending-invoices.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    GoogleModule,
    IconPoliciesModule,
    LinksModule,
    BankPaymentModule,
    MfCardModule,
    MfLoaderModule,
    MfModalModule,
    RouterModule
  ],
  declarations: [CardPendingInvoicesComponent],
  exports: [CardPendingInvoicesComponent]
})
export class CardPendingInvoicesModule {}
