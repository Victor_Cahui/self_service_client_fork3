import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardSctrPeriodItemModule } from '@mx/components/shared/card-sctr-period-item/card-sctr-period-item.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';

import { CardSctrPeriodsComponent } from './card-sctr-periods.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    LinksModule,
    MfLoaderModule,
    GoogleModule,
    CardSctrPeriodItemModule
  ],
  declarations: [CardSctrPeriodsComponent],
  exports: [CardSctrPeriodsComponent]
})
export class CardSctrPeriodsModule {}
