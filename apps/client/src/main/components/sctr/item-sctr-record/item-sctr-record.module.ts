import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { ItemSctrRecordComponent } from './item-sctr-record.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, LinksModule],
  declarations: [ItemSctrRecordComponent],
  exports: [ItemSctrRecordComponent]
})
export class ItemSctrRecordModule {}
