import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { ClientService } from '@mx/services/client.service';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { CIRCLE_CLOSE_ICON, NO_PENDING_PROCEDURES_ICON, PROFILE_ICON } from '@mx/settings/constants/images-values';
import { INSURED_TYPE, POLICY_TYPES, RELATION_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IPolicyInsuredResponse } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-policy-insured-ppfm',
  templateUrl: './card-policy-insured-ppfm.component.html'
})
export class CardPolicyInsuredPPFMComponent implements OnInit {
  policyType = '';
  namePolice = '';
  policyNumber = '';
  cardTitle: string;
  collapse: boolean;
  policyTypes = POLICY_TYPES;
  iconCard = PROFILE_ICON;
  noItems = NO_PENDING_PROCEDURES_ICON;
  messageWithoutInsureds = GeneralLang.Messages.WithoutInsureds;
  insuredList: Array<any> = [];
  theyAreEditing: boolean;
  closeIcon = CIRCLE_CLOSE_ICON;
  frmHolder: FormGroup;
  data = {
    labelViewDetail: GeneralLang.Buttons.ViewDetail
  };
  showLoading: boolean;
  userLoggedIsTitular: boolean;
  userData: IClientInformationResponse;

  constructor(
    private readonly autoservicios: Autoservicios,
    private readonly insuredDependentEditService: InsuredDependentEditService,
    protected clientService: ClientService,
    private readonly router: Router,
    private readonly _FrmProvider: FrmProvider
  ) {}

  ngOnInit(): void {
    this.showLoading = true;
    this.userData = this.clientService.getUserData();
    this._loadData();
  }

  _loadData(): void {
    this.autoservicios.ObtenerAseguradosBeneficiosAdicionales({ codigoApp: APPLICATION_CODE }).subscribe(
      response => {
        this.showLoading = false;
        response && (this.cardTitle = response.titulo);
        response && (this.insuredList = response.asegurados.map(this._mapInsured.bind(this)));
        response &&
          (this.userLoggedIsTitular = response.asegurados.some(
            insured =>
              insured.documento === this.userData.documento &&
              insured.relacionId === RELATION_TYPES.RELATION_HEADLINE.relacionId
          ));
      },
      () => (this.showLoading = false)
    );
  }

  _mapInsured(asegurado: any): any {
    return { ...asegurado, isEdit: false, showLoading: false };
  }

  _initForm(insured: any): void {
    this.frmHolder = this._FrmProvider.InsuredFormComponent([insured]);
  }

  save(insured: any): void {
    insured.showLoading = true;
    const insuredChange = this._getInsuredFrmValue(this.frmHolder.value, insured);
    const body = {
      codAfiliado: insuredChange.codAfiliado,
      telefono: insuredChange.telefono,
      correo: insuredChange.correo,
      polizas: insuredChange.polizas
    };

    this.autoservicios.ActualizarAseguradosPoliza({ codigoApp: APPLICATION_CODE }, body).subscribe(
      () => {
        this._activeEdit(insured, false);
        this._loadData();
      },
      () => (insured.showLoading = false)
    );
  }

  _getInsuredFrmValue(frm: FormGroup, a: any): any {
    return {
      ...a,
      telefono: frm[`phone_${a.documento}`],
      correo: frm[`email_${a.documento}`]
    };
  }

  reset(insured: any): void {
    this._activeEdit(insured, false);
  }

  edit(insured: any): void {
    this._initForm(insured);
    this._activeEdit(insured, true);
  }

  _activeEdit(insured, flagEdit: boolean): void {
    insured.isEdit = flagEdit;
    this.theyAreEditing = flagEdit;
  }

  getTitleGrouped(insuredTypeId: number, cant: number): string {
    let titleGoup = '';
    switch (insuredTypeId) {
      case INSURED_TYPE.INSURED_CODE:
        titleGoup = cant > 1 ? GeneralLang.Titles.InsuredTypes.Insureds : GeneralLang.Titles.InsuredTypes.Insured;
        break;
      case INSURED_TYPE.BENEFICIARY_CODE:
        titleGoup = GeneralLang.Titles.InsuredTypes.Beneficiary;
        break;
      case INSURED_TYPE.DEPENDENT_CODE:
        titleGoup = GeneralLang.Titles.InsuredTypes.Dependent;
        break;
      default:
        break;
    }

    return titleGoup;
  }

  getFullName(insured: IPolicyInsuredResponse): string {
    return `${insured.nombre} ${insured.apellidoPaterno} ${insured.apellidoMaterno}`;
  }

  getRelation(insured: IPolicyInsuredResponse): string {
    return insured.tipoAseguradoId === INSURED_TYPE.INSURED_CODE && `${this.policyType}` === POLICY_TYPES.MD_VIDA.code
      ? ''
      : insured.relacionDescripcion.toUpperCase() === POLICY_TYPES.MD_OTROS.title.toUpperCase()
      ? ''
      : this.capitalizeRelation(insured.relacionDescripcion.toLowerCase());
  }

  capitalizeRelation(relation: string): string {
    return relation.charAt(0).toUpperCase() + relation.slice(1);
  }

  goToDetail(insured: IPolicyInsuredResponse): void {
    this.insuredDependentEditService.setPolicyInsuredResponse(
      insured,
      APPLICATION_CODE,
      this.policyNumber,
      this.namePolice
    );
    setTimeout(() => this.router.navigate(['/health/health-insurance/insured-dependent/edit/1']));
  }

  getEmail(insured: IPolicyInsuredResponse): string {
    return insured.correoElectronico || GeneralLang.Texts.WithOutEmail;
  }

  getPhone(phone: string): string {
    return phone || GeneralLang.Texts.WithOutPhone;
  }
}
