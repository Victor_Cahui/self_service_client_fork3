import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownModule } from '@mx/components/shared/dropdown/dropdown.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { CardEpsBasicInfoComponent } from './card-eps-basic-info.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, DropdownModule, MfLoaderModule],
  declarations: [CardEpsBasicInfoComponent],
  exports: [CardEpsBasicInfoComponent]
})
export class CardEpsBasicInfoModule {}
