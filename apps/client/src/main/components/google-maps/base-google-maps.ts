import { AgmMap, GoogleMapsAPIWrapper, LatLngBounds } from '@agm/core';
import { AfterViewInit, HostBinding, Input, OnDestroy, ViewChild } from '@angular/core';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { IAbrevCoords, ICoords, IGoogleOptions } from '@mx/statemanagement/models/google-map.interface';
import { Subscription } from 'rxjs';
declare var google: any;

export class BaseGoogleMaps implements AfterViewInit, OnDestroy {
  @HostBinding('attr.class') attr_class = 'g-map';
  @ViewChild(AgmMap) agmMap: AgmMap;
  @Input() latitude: number;
  @Input() longitude: number;

  options: IGoogleOptions;
  map: GoogleMapsAPIWrapper;
  self: any;

  zoomMarker: number;

  mapReadySub: Subscription;
  zoomChangeSub: Subscription;
  geolocatonSub: Subscription;
  googleMapSub: Subscription;
  googleMapEventSub: Subscription;
  googleTemporalPositionSub: Subscription;
  gmFilterServiceSub: Subscription;
  generalServiceSub: Subscription;

  constructor(protected googleMapService: GoogleMapService) {
    this.options = {
      latitude: 0,
      longitude: 0,
      zoom: 18,
      minZoom: 6,
      disableDefaultUI: true,
      usePanning: true,
      zoomControl: false,
      streetViewControl: false,
      scrollwheel: null,
      gestureHandling: 'greedy',
      clickableIcons: false
    };
  }

  static isInsidePoligon(coords: ICoords, poligons: Array<Array<IAbrevCoords>>): boolean {
    const x = coords.latitude;
    const y = coords.longitude;
    let inside = false;

    for (const polygon of poligons) {
      for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
        const xi = polygon[i].lat;
        const yi = polygon[i].lng;
        const xj = polygon[j].lat;
        const yj = polygon[j].lng;
        const intersect = yi > y !== yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
        if (intersect) {
          inside = !inside;
        }
      }
    }

    return inside;
  }

  ngAfterViewInit(): void {
    this.mapReadySub = this.agmMap.mapReady.subscribe(
      map => {
        this.map = map;
        this.googleMapService.eventEmit(GoogleMapsEvents.READY);
        map.addListener(GoogleMapsEvents.DRAG_END, () => {
          this.googleMapService.eventEmit(GoogleMapsEvents.DRAG_END);
        });
        map.addListener(GoogleMapsEvents.DRAG_START, () => {
          this.googleMapService.eventEmit(GoogleMapsEvents.DRAG_START);
        });
        this.googleMapService.setMap(map);
      },
      () => {},
      () => {
        this.mapReadySub.unsubscribe();
      }
    );
    this.zoomChangeSub = this.agmMap.zoomChange.subscribe(zoom => (this.options.zoom = zoom));
  }

  ngOnDestroy(): void {
    if (this.googleMapSub) {
      this.googleMapSub.unsubscribe();
    }
    if (this.googleMapEventSub) {
      this.googleMapEventSub.unsubscribe();
    }
    if (this.zoomChangeSub) {
      this.zoomChangeSub.unsubscribe();
    }
    if (this.generalServiceSub) {
      this.generalServiceSub.unsubscribe();
    }
    if (this.mapReadySub) {
      this.mapReadySub.unsubscribe();
    }
    if (this.googleTemporalPositionSub) {
      this.googleTemporalPositionSub.unsubscribe();
    }
    this.googleMapService.setSearchMarker(null);
    this.googleMapService.setSearchMarkerDefault(null);
    this.googleMapService.setSearchPlaceEventMarker(null);
    // this.googleMapService.eventEmit(GoogleMapsEvents.SET_END_MARKER, null);
    // this.googleMapService.eventEmit(GoogleMapsEvents.SET_START_MARKER, null);
    this.onDestroy();
  }

  onDestroy(): void {}

  setPosition(lat: number, lng: number, applyZoom = true): void {
    this.latitude = lat;
    this.longitude = lng;
    this.options.latitude = this.agmMap.latitude;
    this.options.longitude = this.agmMap.longitude;
    setTimeout(() => {
      this.options.latitude = lat;
      this.options.longitude = lng;
    }, 10);

    if (applyZoom) {
      this.options.zoom = this.zoomMarker || 18;
    }
  }

  boundsMap(listCoords: Array<ICoords>): void {
    try {
      const bounds: LatLngBounds = new google.maps.LatLngBounds();
      (listCoords || [])
        .filter(coords => coords.latitude)
        .forEach(coords => {
          bounds.extend(new google.maps.LatLng(coords.latitude, coords.longitude));
        });
      if (this.map) {
        const getSouthWest = bounds.getSouthWest();
        const getNorthEast = bounds.getNorthEast();
        const extendPointSouth = new google.maps.LatLng(getSouthWest.lat() + 0.01, getSouthWest.lng() + 0.01);
        const extendPointNorth = new google.maps.LatLng(getNorthEast.lat() + 0.01, getNorthEast.lng() + 0.01);
        bounds.extend(extendPointSouth);
        bounds.extend(extendPointNorth);
        this.map.fitBounds(bounds);
      }
    } catch (error) {}
  }
}
