import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
import { HealthService } from '@mx/services/health/health.service';
import { HEART_ICON, HEART_OUT_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { ICoords } from '@mx/statemanagement/models/google-map.interface';
import { IClinicResponse } from '@mx/statemanagement/models/health.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-clinic-detail-info',
  templateUrl: './clinic-detail-info.component.html'
})
export class ClinicDetailInfoComponent extends UnsubscribeOnDestroy implements OnInit {
  @Input() clinic: IClinicResponse;
  @Output() favorite: EventEmitter<boolean>;

  hasGPSPermission: boolean;
  canScheduleAppointment: boolean;
  HowToGetThere = GeneralLang.Labels.HowToGetThere;
  scheduleAppointment = GeneralLang.Buttons.goToAppointment;
  HoureChange = HealthLang.Labels.HoureChange;
  generalLang = GeneralLang;
  clinicAddress: string;
  heartIcon = HEART_ICON;
  heartOutIcon = HEART_OUT_ICON;

  constructor(
    private readonly geolocationService: GeolocationService,
    public router: Router,
    public healthService: HealthService,
    public _AppointmentFacade: AppointmentFacade
  ) {
    super();
    this.favorite = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
    this.geolocationService.hasPermission().then(hasPermission => (this.hasGPSPermission = hasPermission));
    this.canScheduleAppointment = this.clinic.puedeAgendarCita;
    this.setClinicAddress();
  }

  goToGoogleMap(): void {
    this.geolocationService
      .getCurrentPosition()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(position => {
        const to: ICoords = { latitude: this.clinic.latitud, longitude: this.clinic.longitud };
        this.geolocationService.drawRouteGM(position, to);
      });
  }

  goToAppointment(): void {
    this.healthService
      .getCurrentPolicy()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(currentPolicy => {
        this._AppointmentFacade.AddEstablishment(this.clinic);
        this._AppointmentFacade.AddPolicy(currentPolicy);
        this.router.navigate(['/digital-clinic/schedule/steps/1']);
      });
  }

  setClinicAddress(): void {
    this.clinicAddress = this.clinic ? `${this.clinic.direccion}` : '';
    this.clinicAddress += this.clinic.distritoDescripcion ? `, Distrito de ${this.clinic.distritoDescripcion}` : '';
    this.clinicAddress += this.clinic.provinciaDescripcion ? `, ${this.clinic.provinciaDescripcion}` : '';
    this.clinicAddress += this.clinic.departamentoDescripcion ? `, ${this.clinic.departamentoDescripcion}` : '';
    this.clinicAddress += this.clinic.distancia ? ` - ${this.clinic.distancia} km` : '';
  }

  favoriteFn(): void {
    this.favorite.next(!this.clinic.esFavorito);
  }
}
