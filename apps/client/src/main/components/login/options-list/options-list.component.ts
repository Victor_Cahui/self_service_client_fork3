import { Component, OnDestroy, OnInit } from '@angular/core';
import { Actions } from '@mx/settings/lang/auth.lang';

@Component({
  selector: 'client-app-options-list',
  templateUrl: './options-list.component.html'
})
export class OptionsListComponent implements OnInit, OnDestroy {
  actions = Actions;

  ngOnInit(): void {}

  ngOnDestroy(): void {}
}
