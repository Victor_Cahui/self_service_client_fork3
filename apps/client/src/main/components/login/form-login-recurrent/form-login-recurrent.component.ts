import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { LoginFormComponent } from '@mx/layouts/login-layout/login-top/login-form/login-form.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { MFP_Iniciar_Sesion_2B, MFP_Iniciar_Sesion_2C } from '@mx/settings/constants/events.analytics';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { AuthInterface, ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { AllValidationErrors } from '@mx/statemanagement/models/form.interface';
import { isEmpty } from 'lodash-es';
import { BaseLogin } from '../base-login';

@Component({
  selector: 'client-form-login-recurrent',
  templateUrl: './form-login-recurrent.component.html'
})
export class FormLoginRecurrentComponent extends BaseLogin implements OnInit {
  status = NotificationStatus.INFO;
  constructor(
    protected router: Router,
    protected policiesService: PoliciesService,
    protected clientService: ClientService,
    protected generalService: GeneralService,
    protected authService: AuthService,
    protected paymentService: PaymentService,
    protected fb: FormBuilder,
    protected localStorage: LocalStorageService,
    protected adminService: UserAdminService,
    protected _Comun: Comun,
    protected _Autoservicios: Autoservicios,
    protected notifyService: NotificationService,
    protected gaService?: GAService
  ) {
    super(
      router,
      policiesService,
      clientService,
      generalService,
      authService,
      paymentService,
      fb,
      localStorage,
      adminService,
      _Comun,
      _Autoservicios,
      notifyService,
      gaService
    );
    this.ga = [MFP_Iniciar_Sesion_2B(), MFP_Iniciar_Sesion_2C()];
  }

  ngOnInit(): void {
    this.authService.logout();
    LoginFormComponent.updateView.next(true);
    this.messageUserInvalid = AuthLang.Messages.PasswordInvalid;
    this.getDocumentTypes();
    this.createForm();
    const data: ILogguedUser = this.localStorage.getData(LOCAL_STORAGE_KEYS.USER);
    this.clientID = {
      tipoDocumento: data ? data.documentType : null,
      documento: data ? data.documentNumber : null
    };
  }

  // Formulario
  createForm(): void {
    this.formLogin = this.fb.group({
      password: ['', Validators.required]
    });
    this.password = this.formLogin.controls['password'];
  }

  resetForm(): void {
    FormComponent.setControl(this.password, '');
  }

  // Login
  login(): void {
    this.clientService.canShowModalElectronicPolicy = true;
    const error: AllValidationErrors = FormComponent.getFormValidationErrors(this.formLogin.controls).shift();
    if (isEmpty(error)) {
      const data: AuthInterface = {
        client_id: '',
        grant_type: '',
        username: this.clientID.documento,
        password: this.password.value
      };
      // Validar Usuario y hacer Login
      this.userValidate(data);
    } else {
      FormComponent.validateAllFormFields(this.formLogin);
    }
  }

  anotherProfile(): void {
    this.localStorage.clear();
    document.getElementsByTagName('body')[0].style.display = 'none';
    this.authService.logout();
  }
}
