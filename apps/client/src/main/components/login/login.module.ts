import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InputIdentityModule } from '@mx/components/shared/input-identity/input-identity.module';
import { InputPasswordModule } from '@mx/components/shared/input-password/input-password.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { UiModule } from '@mx/core/ui/lib/ui.module';
import { ConfirmExpiredTokenComponent } from './confirm-expired-token/confirm-expired-token.component';
import { ConfirmRestorePasswordComponent } from './confirm-restore-password/confirm-restore-password.component';
import { ConfirmSendEmailComponent } from './confirm-send-email/confirm-send-email.component';
import { FormEnableUserComponent } from './form-enable-user/form-enable-user.component';
import { FormLoginRecurrentComponent } from './form-login-recurrent/form-login-recurrent.component';
import { FormLoginTypeComponent } from './form-login-type/form-login-type.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { FormRecoverUserComponent } from './form-recover-user/form-recover-user.component';
import { FormRestorePasswordComponent } from './form-restore-password/form-restore-password.component';
import { OptionsListComponent } from './options-list/options-list.component';
import { TitleBoxComponent } from './title-box/title-box.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    UiModule,
    DirectivesModule,
    InputIdentityModule,
    InputPasswordModule,
    GoogleModule,
    MfCustomAlertModule
  ],
  exports: [
    TitleBoxComponent,
    OptionsListComponent,
    FormLoginComponent,
    FormLoginRecurrentComponent,
    FormEnableUserComponent,
    FormRecoverUserComponent,
    FormRestorePasswordComponent,
    ConfirmSendEmailComponent,
    ConfirmRestorePasswordComponent,
    ConfirmExpiredTokenComponent,
    FormLoginTypeComponent,
    MfCustomAlertModule
  ],
  declarations: [
    TitleBoxComponent,
    OptionsListComponent,
    FormLoginComponent,
    FormLoginRecurrentComponent,
    FormEnableUserComponent,
    FormRecoverUserComponent,
    ConfirmSendEmailComponent,
    FormRestorePasswordComponent,
    ConfirmRestorePasswordComponent,
    ConfirmExpiredTokenComponent,
    FormLoginTypeComponent
  ]
})
export class LoginModule {}
