import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VerifySessionService } from '@mx/core/shared/common/services/verify-session.service';
import { LoginFormComponent } from '@mx/layouts/login-layout/login-top/login-form/login-form.component';
import { LoginWelcomeComponent } from '@mx/layouts/login-layout/login-top/login-welcome/login-welcome.component';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { KEY_ACTIONS } from '@mx/settings/auth/auth-values';
import { APPLICATION_CODE, SUCCESS_CODE } from '@mx/settings/constants/general-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IChangePasswordRequest, IChangePasswordToken } from '@mx/statemanagement/models/auth.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-form-restore-password',
  templateUrl: './form-restore-password.component.html'
})
export class FormRestorePasswordComponent implements OnInit, OnDestroy {
  form: FormGroup;
  lang = AuthLang;
  showLoading: boolean;
  success: boolean;
  token: string;
  action: string;
  btnConfirm = GeneralLang.Buttons.Agree;
  userSub: Subscription;

  constructor(
    private readonly router: Router,
    private readonly adminService: UserAdminService,
    protected activatedRoute: ActivatedRoute,
    private readonly verifySession: VerifySessionService,
    private readonly fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.adminService.clearChangePasswordConfirm();
    this.adminService.clearEmailConfirm();
    LoginWelcomeComponent.updateView.next(true);
    LoginFormComponent.updateView.next(false);
    this.form = this.fb.group({});
    this.token = this.activatedRoute.snapshot.queryParams['token'];
    this.action = this.activatedRoute.snapshot.queryParams['origin'];
    const validTimeToken = this.verifySession.verifyTimeToken(this.token);
    const validToken = this.verifySession.validToken(this.token);
    const validAction = this.action === KEY_ACTIONS.ENABLE || this.action === KEY_ACTIONS.RECOVER;

    if (!validToken || !validAction) {
      this.setError(true);
    } else if (!validTimeToken) {
      this.setError(false);
    }
  }

  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  restorePassword(): void {
    if (!this.form.valid) {
      return;
    }
    this.showLoading = true;
    const data = this.form.getRawValue().changePassword;
    const params: IChangePasswordRequest = {
      applicationCode: APPLICATION_CODE,
      token: this.token,
      password: data.newPassword
    };
    this.userSub = this.adminService.changePassword(params).subscribe(
      res => {
        this.success = res && res.operationCode === SUCCESS_CODE;
        const dataToken: IChangePasswordToken = {
          token: this.token,
          action: this.action,
          success: this.success
        };
        this.adminService.setChangePasswordConfirm(dataToken);
        if (this.success) {
          this.router.navigate(['/confirm-restore-password']);
        } else {
          this.showError();
        }
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.userSub.unsubscribe();
      }
    );
  }

  setError(value: boolean): void {
    const dataToken: IChangePasswordToken = {
      token: this.token,
      action: this.action,
      tryAgain: value
    };
    this.adminService.setChangePasswordConfirm(dataToken);
    this.showError();
  }

  showError(): void {
    LoginWelcomeComponent.updateView.next(false);
    this.router.navigate(['./expired-token']);
  }
}
