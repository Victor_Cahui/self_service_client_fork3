import { Component, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { LOGIN_RECURRENT_SOCIAL_REASON_MAX_LENGTH } from '@mx/settings/constants/general-values';
import { PROFILE_ICON } from '@mx/settings/constants/images-values';

@Component({
  selector: 'client-app-title-box',
  templateUrl: './title-box.component.html'
})
export class TitleBoxComponent implements OnInit, OnDestroy {
  @HostBinding('class') container = 'col col-12 col-lg-10';
  @Input() text?: string;
  @Input() icon = PROFILE_ICON;
  maxLength = LOGIN_RECURRENT_SOCIAL_REASON_MAX_LENGTH;

  ngOnInit(): void {}

  ngOnDestroy(): void {}
}
