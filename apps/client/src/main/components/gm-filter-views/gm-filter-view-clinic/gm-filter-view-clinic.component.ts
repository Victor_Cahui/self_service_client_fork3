import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GmFilterBase, GmFilterEvents, GmFilterService } from '@mx/components/shared';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthService } from '@mx/services/health/health.service';
import { PoliciesService } from '@mx/services/policies.service';
import { UbigeoService } from '@mx/services/ubigeo.service';
import { QUERY_SOURCE } from '@mx/settings/constants/coverage-values';
import { ALL_COVERAGES, APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { ClinicFormFilter } from '@mx/settings/lang/office.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { ICoveragesPolicyRequest, IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IUbigeoResponse } from '@mx/statemanagement/models/ubigeo.interface';
import { pickBy } from 'lodash-es';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'client-gm-filter-view-clinic',
  templateUrl: './gm-filter-view-clinic.component.html'
})
export class GmFilterViewClinicComponent extends GmFilterBase implements OnInit, OnDestroy {
  policyFilterLang = ClinicFormFilter.policyFilter;
  coverageFilterLang = ClinicFormFilter.coverageFilter;
  typeFilterLang = ClinicFormFilter.establishmentTypeFilter;
  location = ClinicFormFilter.UbigeoFilter;
  labelDepartment = ProfileLang.Labels.Department;
  labelProvince = ProfileLang.Labels.Province;
  labelDistrict = ProfileLang.Labels.District;

  formFilterSub: Subscription;
  formFilterPolicySub: Subscription;
  policiesSub: Subscription;
  coveragesSub: Subscription;
  clinicListSub: Subscription;
  getCurrentPolicySub: Subscription;
  applyFilterSub: Subscription;
  applyResetSub: Subscription;
  getPoliciesSub: Subscription;
  departamentSub: Subscription;
  provinceSub: Subscription;
  districtSub: Subscription;

  allTypes: boolean;
  selectedTypes: number;
  checkboxStates: Array<boolean>;

  policyTmp: string;
  coverageTmp: string;

  policiesArray: Array<IPoliciesByClientResponse> = [];
  policiesOptions = [];
  coveragesOptions = [];
  clinicPolicies = [POLICY_TYPES.MD_SALUD.code, POLICY_TYPES.MD_EPS.code, POLICY_TYPES.MD_SCTR.code];

  policies: Array<IPoliciesByClientResponse>;
  currentPolicy: IPoliciesByClientResponse;
  noneItem: IUbigeoResponse;

  auth: IdDocument;

  showLoading: boolean;
  errorHelper: boolean;
  filterNotified: boolean;
  formOriginalStatus: boolean;
  someUbigeo: boolean;
  isFirstTime = true;

  departamento: AbstractControl;
  provincia: AbstractControl;
  distrito: AbstractControl;

  constructor(
    private readonly authService: AuthService,
    private readonly policiesService: PoliciesService,
    public formBuilder: FormBuilder,
    public gmFilterService: GmFilterService,
    public healthService: HealthService,
    public ubigeoService: UbigeoService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    protected gaService: GAService
  ) {
    super(formBuilder, gmFilterService, ubigeoService, gaService);
    this.checkboxStates = [];
    const controls = this.typeFilterLang.items.map(() => new FormControl());
    this.formFilter = this.formBuilder.group({
      policy: new FormControl('', Validators.required),
      coverage: new FormControl(''),
      type: new FormArray(controls),
      sort: new FormControl(''),
      department: new FormControl(''),
      province: new FormControl(''),
      district: new FormControl('')
    });
    this.formFilter.addControl('todos', new FormControl(''));
    this.departamento = this.formFilter.controls['department'];
    this.provincia = this.formFilter.controls['province'];
    this.distrito = this.formFilter.controls['district'];
    this.departamentos = [];
    this.provincias = [];
    this.distritos = [];

    this.auth = this.authService.retrieveEntity();
  }

  ngOnInit(): void {
    this.formFilter.controls['todos'].setValue(true);
    this.selectedTypes = this.typeFilterLang.items.length;
    this.selectedTypeByURI();
    this.valueChanges();
    // self-selection from modal
    this.getCurrentPolicySub = this.healthService.getCurrentPolicy().subscribe(currentPolicy => {
      if (currentPolicy && (this.currentPolicy || ({} as any)).numeroPoliza !== currentPolicy.numeroPoliza) {
        this.formFilter.controls['policy'].setValue(currentPolicy.numeroPoliza, { onlySelf: true });
        this.currentPolicy = currentPolicy;
        this.policyTmp = currentPolicy.numeroPoliza;
        this.reset();
        this.onFilter();
      }
    });

    this.getPoliciesSub = this.healthService.getPolicies().subscribe((policies: Array<IPoliciesByClientResponse>) => {
      if (policies && policies.length) {
        this.policies = policies;
        this.policiesOptions = [];
        policies.forEach(policy => {
          if (policy.codRamo !== 275 && !policy.esBeneficioAdicional) {
            this.policiesOptions.push({ value: policy.numeroPoliza, text: policy.descripcionPoliza });
          }
        });
      }
    });

    this.applyFilterSub = this.applyFilter.subscribe(() => {
      this.onFilter();
    });

    // restore form to default values
    this.applyResetSub = this.applyReset.subscribe(() => {
      this.selectedTypes = this.typeFilterLang.items.length;
      this.allTypes = true;
      this.filterButtonState.next(this.allTypes);
      this.setFormTypesTo(this.allTypes);
      this.formFilter.controls['coverage'].setValue(ALL_COVERAGES);
      this.formFilter.controls['policy'].setValue(this.policyTmp, { onlySelf: true });
      this.clearUbigeo();
    });

    this.getDepartamentos();
    this._getOpenedFilterRx$ = this.gmFilterService.getOpenedFilterRx().subscribe(st => {
      if (st) {
        this.restoreDepartment();
        this.restoreProvince();
        this.restoreDistrict();
      }
    });
  }

  valueChanges(): void {
    this.formFilterSub = this.formFilter.valueChanges.subscribe(form => {
      this.errorHelper = this.selectedTypes === 0;
      const value = this.errorHelper ? false : this.formFilter.valid;
      this.filterButtonState.next(value);
    });
    this.formFilterPolicySub = this.formFilter.controls['policy'].valueChanges.subscribe(policyNumber => {
      if (policyNumber) {
        if ((this.currentPolicy || ({} as any)).numeroPoliza !== policyNumber) {
          this.getCoveragesByPolicy(policyNumber);
        }
        if (this.policies) {
          this.currentPolicy = this.policies.find(policy => policy.numeroPoliza === policyNumber);
        }
      }
    });
  }

  getCoveragesByPolicy(policyNumber: string): void {
    let typeCoverageDefault = ALL_COVERAGES;
    if (this.isFirstTime) {
      typeCoverageDefault = this.activatedRoute.snapshot.queryParams['coverage'] || ALL_COVERAGES;
      this.formFilter.controls['coverage'].setValue(typeCoverageDefault);
      this.coverageTmp = typeCoverageDefault;
      this.isFirstTime = false;
    }
    this.coveragesSub = this.policiesService
      .getCoveragesByPolicy({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: policyNumber,
        origenConsulta: QUERY_SOURCE.CLINIC_SEARCH
      } as ICoveragesPolicyRequest)
      .subscribe(
        (coverages: any) => {
          const isSCTRHealth = PolicyUtil.isSctrHealth(
            this.currentPolicy.tipoPoliza,
            this.currentPolicy.codCia,
            this.currentPolicy.codRamo
          );
          if (!coverages && !coverages.coberturas.length && isSCTRHealth) {
            this.formFilter.controls['coverage'].disable();
          } else {
            this.formFilter.controls['coverage'].enable();
          }
          this.coveragesOptions = (coverages.coberturas || []).map(coverage => ({
            value: coverage.llave,
            text: coverage.tituloInfo
          }));
          this.coveragesOptions.unshift({ value: ALL_COVERAGES, text: this.coverageFilterLang.all });
          const coverageOption = this.coveragesOptions.find(coverage => coverage.value === typeCoverageDefault);
          const coverageTmp = coverageOption ? coverageOption.value : ALL_COVERAGES;
          this.formFilter.controls['coverage'].setValue(coverageTmp);
        },
        () => {
          this.coveragesSub.unsubscribe();
        },
        () => {
          this.coveragesSub.unsubscribe();
        }
      );
  }

  checkAllTypes(): void {
    this.allTypes = !this.allTypes;
    this.selectedTypes = this.allTypes ? this.typeFilterLang.items.length : 0;
    this.setFormTypesTo(this.allTypes);
    if (this.formFilter.controls['policy'].value) {
      // si se ha seleccionado antes una póliza
      this.filterButtonState.next(this.allTypes);
    }
  }

  onFilter(): void {
    this._appliedFrm = { ...this.formFilter.value };
    this._appliedOptionsUbigeo = {
      province: [...this.provincias],
      district: [...this.distritos]
    };
    this.showLoading = true;
    const selectedKeyTypes = this.formFilter.controls['type'].value
      .map((clinicType: boolean, i: number) => {
        this.checkboxStates[i] = clinicType === null ? true : !!clinicType;

        return clinicType ? this.typeFilterLang.items[i].value : null;
      })
      .filter(clinicType => clinicType);
    const numeroPoliza = this.currentPolicy.numeroPoliza;
    const tipoCobertura =
      this.formFilter.controls['coverage'].value === ALL_COVERAGES ? '' : this.formFilter.controls['coverage'].value;
    const tiposEstablecimiento = this.allTypes ? '' : selectedKeyTypes.join('|');
    const departamentoId = this.departamento.value;
    const provinciaId = this.provincia.value;
    const distritoId = this.distrito.value;

    this.showFiltersIndicator(numeroPoliza, tipoCobertura).then(() => {
      this.policyTmp = numeroPoliza;
      this.coverageTmp = tipoCobertura;
      this.healthService.setCurrentPolicy(this.currentPolicy);
      this.changingQueryParams({ coverage: tipoCobertura, policy: numeroPoliza, types: tiposEstablecimiento });
      this.healthService.setFilterClinics({
        numeroPoliza,
        tipoCobertura,
        tiposEstablecimiento,
        departamentoId,
        provinciaId,
        distritoId
      });
    });

    this.formFilterTmp = this.formFilter.value;
  }

  typeClicked(event: any): void {
    event.target.checked ? this.selectedTypes++ : this.selectedTypes--;
    if (this.allTypes) {
      this.allTypes = false;
    } else {
      if (this.selectedTypes === this.typeFilterLang.items.length) {
        this.allTypes = true;
      }
    }
    this.errorHelper = this.selectedTypes === 0;
    this.filterButtonState.next(!!this.selectedTypes);
  }

  // back button, reset the form (last filter applied)
  resetState(): void {
    this.gmFilterService.emitOpenedFilterRx(false);
    if (this.checkboxStates && this.checkboxStates.length) {
      this.selectedTypes = this.checkboxStates.filter(e => e).length;
      this.allTypes = this.selectedTypes === this.typeFilterLang.items.length;
      this.formFilter.controls['type'].setValue(
        this.formFilter.controls['type'].value.map((clinicType: boolean, i: number) => this.checkboxStates[i])
      );
    } else {
      this.allTypes = true;
      this.setFormTypesTo(this.allTypes);
    }
    if (this.policyTmp) {
      this.formFilter.controls['policy'].setValue(this.policyTmp);
    }
    if (this.coverageTmp) {
      this.formFilter.controls['coverage'].setValue(this.coverageTmp);
    }
    this.filterButtonState.next(true);
  }

  setFormTypesTo(value: boolean): void {
    this.formFilter.controls['type'].setValue(this.formFilter.controls['type'].value.map(() => value));
  }

  ngOnDestroy(): void {
    if (this._getOpenedFilterRx$) {
      this.gmFilterService.emitOpenedFilterRx(false);
      this._getOpenedFilterRx$.unsubscribe();
    }
    if (this.formFilterSub) {
      this.formFilterSub.unsubscribe();
    }
    if (this.policiesSub) {
      this.policiesSub.unsubscribe();
    }
    if (this.formFilterPolicySub) {
      this.formFilterPolicySub.unsubscribe();
    }
    if (this.coveragesSub) {
      this.coveragesSub.unsubscribe();
    }
    if (this.getCurrentPolicySub) {
      this.getCurrentPolicySub.unsubscribe();
    }
    if (this.applyFilterSub) {
      this.applyFilterSub.unsubscribe();
    }
    if (this.applyResetSub) {
      this.applyResetSub.unsubscribe();
    }
    if (this.getPoliciesSub) {
      this.getPoliciesSub.unsubscribe();
    }
    if (this.departamentSub) {
      this.departamentSub.unsubscribe();
    }
    if (this.provinceSub) {
      this.provinceSub.unsubscribe();
    }
    if (this.districtSub) {
      this.districtSub.unsubscribe();
    }
  }

  showFiltersIndicator(policy: string, coverage: string): Promise<void> {
    return timer()
      .toPromise()
      .then(() => {
        this.formOriginalStatus =
          policy !== this.policyTmp || !(!coverage || coverage === ALL_COVERAGES) || !this.allTypes;

        this.filterNotified = this.gmFilterService.getFilteredClinicsNotified();
        if (!this.filterNotified && (this.formOriginalStatus || this.someUbigeo)) {
          this.gmFilterService.setFilteredClinicsNotified(true);
          this.gmFilterService.eventEmit(GmFilterEvents.FILTER_NOTIFY, true);
        }
        this.gmFilterService.eventEmit(GmFilterEvents.FILTERS_ACTIVED, this.formOriginalStatus || this.someUbigeo);
      });
  }

  selectedTypeByURI(): void {
    const types: string = this.activatedRoute.snapshot.queryParams['types'];
    if (types) {
      const typesTmp = types.split('|');
      this.selectedTypes = typesTmp.length;
      this.allTypes = typesTmp.length === this.typeFilterLang.items.length;

      const typeForm = this.typeFilterLang.items.map(item => this.allTypes);
      typesTmp.forEach(tt => {
        const index = this.typeFilterLang.items.findIndex(item => item.value === tt);
        if (index > -1) {
          typeForm[index] = true;
        }
      });

      this.formFilter.controls['type'].setValue(typeForm);
    } else {
      this.checkAllTypes();
    }
  }

  changingQueryParams(params: any): void {
    // tslint:disable-next-line: no-parameter-reassignment
    params = pickBy(params);
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params
    });
  }

  clearUbigeo(): void {
    this.departamento.setValue('');
    this.provincia.setValue('');
    this.distrito.setValue('');
    this.setNullUbigeo('provincia');
    this.someUbigeo = false;
  }

  selectDepartment(): void {
    this.setNullUbigeo('provincia');
    this.setControl(this.provincia);
    this.setControl(this.distrito);
    this.getProvincias(this.departamento.value);
  }

  selectProvince(): void {
    this.setNullUbigeo('distrito');
    this.setControl(this.distrito);
    this.getDistritos(this.provincia.value);
  }

  getDepartamentos(departamentoId?: string, provinciaId?: string): void {
    if (!this.departamentos.length) {
      this.departamentSub = this.ubigeoService.getDepartaments().subscribe((res: Array<IUbigeoResponse>) => {
        this.departamentos = this.setUbigeoData(res);
        this.departamento.setValue('');

        if (!!departamentoId) {
          this.getProvincias(departamentoId, provinciaId);
        }
        this.departamentSub.unsubscribe();
      });
    } else {
      if (!!departamentoId) {
        this.getProvincias(departamentoId, provinciaId);
      }
    }
  }

  getProvincias(departamentoId: string, provinciaId?: string): void {
    if (!!departamentoId && !this.provincias.length && !this.provincias.find(item => item.value !== provinciaId)) {
      this.provinceSub = this.ubigeoService.getProvinces(departamentoId).subscribe((res: Array<IUbigeoResponse>) => {
        this.provincias = this.setUbigeoData(res);
        this.provincia.setValue('');

        if (!!provinciaId) {
          this.getDistritos(provinciaId);
        }
        this.provinceSub.unsubscribe();
      });
    }
  }

  getDistritos(provinciaId: string): void {
    if (!!provinciaId && !this.distritos.length && !this.distritos.find(item => item.value !== this.distrito.value)) {
      this.setNullUbigeo('distrito');
      this.districtSub = this.ubigeoService.getDistricts(provinciaId).subscribe((res: Array<IUbigeoResponse>) => {
        this.distritos = this.setUbigeoData(res);
        this.distrito.setValue('');

        this.districtSub.unsubscribe();
      });
    }
  }

  resetUbigeo(): void {
    this.ubigeoService.getProvinces(this.formFilterTmp['department']).subscribe((res: Array<IUbigeoResponse>) => {
      this.provincias = this.setUbigeoData(res);
      this.provincia.setValue(this.formFilterTmp['province']);
    });
    this.ubigeoService.getDistricts(this.formFilterTmp['province']).subscribe((res: Array<IUbigeoResponse>) => {
      this.distritos = this.setUbigeoData(res);
      this.distrito.setValue(this.formFilterTmp['district']);
    });
  }

  setNullUbigeo(name: string): void {
    switch (name) {
      case 'provincia':
        if (this.provincias.length) {
          this.provincias = [];
          this.distritos = [];
        }
        break;
      case 'distrito':
        if (this.distritos.length) {
          this.distritos = [];
        }
        break;
      default:
        break;
    }
  }

  setControl(control: AbstractControl, value?: string): void {
    this.someUbigeo = true;
    if (!!value) {
      control.setValue(value);
    } else {
      control.setValue('');
      control.markAsUntouched();
      control.markAsPristine();
    }
  }
  // tslint:disable-next-line: max-file-line-count
}
