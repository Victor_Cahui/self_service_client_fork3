import { DOCUMENT } from '@angular/common';
import { Inject, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IAbrevCoords } from '@mx/statemanagement/models/google-map.interface';
import { IAllowedAreasResponse } from '@mx/statemanagement/models/service.interface';
import { Subscription } from 'rxjs';

export class GmMsLayoutBase implements OnDestroy, OnInit {
  @ViewChild('summary') summary: any;

  poligons: Array<Array<IAbrevCoords>>;
  showForm = true;
  showSummary: boolean;
  showSearch: boolean;
  useFocusedSearch: boolean;
  useFilterMapService: boolean;
  alloweadAreasSub: Subscription;
  header: IHeader;

  constructor(
    protected servicesMfService: ServicesMFService,
    protected methodMfService: string,
    protected render2: Renderer2,
    protected headerHelperService: HeaderHelperService,
    @Inject(DOCUMENT) protected document: Document
  ) {}

  ngOnInit(): void {
    this.headerHelperService.set(this.header);
    this.getAlloweadAreas();
    if (this.summary) {
      this.summary.handlerBodyClass('g-modal-map--open', true);
    }
  }

  ngOnDestroy(): void {
    this.handlerHeader(true);
    if (this.summary) {
      this.summary.handlerBodyClass('g-modal-map--open', false);
    }
  }

  openSearch(): void {
    this.handlerHeader(false);
    this.showForm = false;
    this.showSearch = true;
  }

  openSummary(data: any): void {
    this.summary.setViewData(data);
    this.showForm = false;
    this.showSummary = true;
  }

  backToForm(): void {
    this.handlerHeader(true);
    this.showSearch = this.showSummary = false;
    this.showForm = true;
  }

  handlerHeader(show: boolean): void {
    const header = this.document.getElementsByTagName('client-main-header')[0];
    this.render2.addClass(header, show ? 'd-block' : 'd-none');
    this.render2.removeClass(header, !show ? 'd-block' : 'd-none');
  }

  // Zonas permitidas
  getAlloweadAreas(): void {
    this.poligons = [];
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.alloweadAreasSub = this.servicesMfService[this.methodMfService](params).subscribe(
      (response: Array<IAllowedAreasResponse>) => {
        if (response && response.length) {
          response.forEach(areas => {
            this.poligons.push(
              areas.coordenadas.map(area => {
                return {
                  lng: area.longitud,
                  lat: area.latitud
                };
              })
            );
          });
        }
      },
      () => {},
      () => {
        this.alloweadAreasSub.unsubscribe();
      }
    );
  }
}
