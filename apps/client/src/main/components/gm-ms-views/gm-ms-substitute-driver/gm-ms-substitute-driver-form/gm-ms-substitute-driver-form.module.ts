import { NgModule } from '@angular/core';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { GmMsSubstituteFormModule } from '../../gm-ms-form.module';
import { GmMsSubstituteDriverFormComponent } from './gm-ms-substitute-driver-form.component';
@NgModule({
  imports: [GmMsSubstituteFormModule, MfCustomAlertModule],
  declarations: [GmMsSubstituteDriverFormComponent],
  exports: [GmMsSubstituteDriverFormComponent]
})
export class GmMsSubstituteDriverFormModule {}
