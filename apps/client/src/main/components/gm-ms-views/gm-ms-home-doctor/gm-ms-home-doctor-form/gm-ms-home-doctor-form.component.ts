import { MapsAPILoader } from '@agm/core';
import { DOCUMENT, Location } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseGoogleMaps } from '@mx/components/google-maps/base-google-maps';
import { GmFilterEvents, GmFilterService } from '@mx/components/shared/gm-filter/providers/gm-filter.service';
import { InputSearchComponent } from '@mx/components/shared/input-search/input-search.component';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { REG_EX, STORAGE } from '@mx/settings/constants/general-values';
import { SERVICE_FORM_STYLE } from '@mx/settings/constants/key-values';
import { HOME_DOCTOR_STEP } from '@mx/settings/constants/router-step';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';
import { SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { HomeDoctorLang, SearchPlaceLang } from '@mx/settings/lang/services.lang';
import {
  GoogleResultAutocomplete,
  IAbrevCoords,
  ICoords,
  IFilterNotificacion,
  ISearchPlace
} from '@mx/statemanagement/models/google-map.interface';
import { IHomeDoctorResponse } from '@mx/statemanagement/models/service.interface';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';
import { GmMsFormBase } from '../../gm-ms-base-form';
declare var google;
@Component({
  selector: 'client-gm-ms-home-doctor-form',
  templateUrl: './gm-ms-home-doctor-form.component.html',
  providers: [PlatformService]
})
export class GmMsHomeDoctorFormComponent extends GmMsFormBase implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(InputSearchComponent) inputSearch: InputSearchComponent;
  @Input() poligons: Array<Array<IAbrevCoords>>;
  @Input() useFilter: true;
  currentServiceSelected: IHomeDoctorResponse;
  filterNotification: IFilterNotificacion = { showNotification: false };
  policyListResponse: Array<IHomeDoctorResponse> = [];
  policyListSub: Subscription;
  googleEventSub: Subscription;
  googleSearchMarkerSub: Subscription;
  gmFilterServiceSub: Subscription;
  service = SERVICES_TYPE.HOME_DOCTOR;
  beneficiaries: Array<any> = [];
  specialties: Array<any> = [];
  warningAlert: boolean;
  geocodeService: any;
  autocompleteService: any;
  placeService: any;
  coincidences: Array<GoogleResultAutocomplete>;
  defaultPlace: ISearchPlace;
  searchText = '';
  currentCoords: ICoords;
  notifyText: string;
  langSearch = SearchPlaceLang;
  stepList = HOME_DOCTOR_STEP;
  selectOnMap = HomeDoctorLang.SelectOnMap;
  showInformationAddress = false;
  showFormAddress = true;
  currentSelected = false;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    protected googleMapService: GoogleMapService,
    protected location: Location,
    protected authService: AuthService,
    protected clientService: ClientService,
    protected formBuilder: FormBuilder,
    @Inject(DOCUMENT) protected document: Document,
    private readonly mapsAPILoader: MapsAPILoader,
    private readonly ngZone: NgZone,
    private readonly gmFilterService: GmFilterService,
    private readonly router: Router,
    protected readonly _StorageService: StorageService
  ) {
    super(
      render2,
      elementRef,
      platformService,
      resizeService,
      generalService,
      googleMapService,
      location,
      authService,
      clientService,
      formBuilder,
      document,
      SERVICE_FORM_STYLE.WORK_HOME_MEDIC
    );
  }

  ngOnInit(): void {
    this.header = HOME_DOCTOR_HEADER;
    this.setInfoModal(this.service);
    this.gmFilterService.resetEvent();
    this.googleMapService.eventEmit(GoogleMapsEvents.CHANGE_OPTIONS_MAPS, { zoom: 12 });
    this.googleSearchMarkerSub = this.googleMapService.getSearchMarker().subscribe(res => {
      if (res) {
        this.coincidences = [];
        if (res.coords && res.coords.latitude && res.valid) {
          this.currentSelected = true;
          this.form.controls['endPoint'].setValue(res.address);
          this.form.controls['endPointCoords'].setValue(res.coords);
          this.form.controls['referenceOne'].setValue(res.addressComplete.refOne);
          this.form.controls['referenceTwo'].setValue(res.addressComplete.refTwo);
          this.form.controls['district'].setValue(res.addressComplete.district);
        } else {
          this.fnShowNotify(res.valid !== undefined && !res.valid);
        }
      }
    });

    this.googleEventSub = this.googleMapService.onEvent().subscribe(res => {
      if (res && res.event === GoogleMapsEvents.MAP_CLICK) {
        this.coincidences = [];
        this.googleMapService.eventEmit(GoogleMapsEvents.ZOOM_CONTROL, true);
      }

      if (res && res.event === GoogleMapsEvents.DRAG_END) {
        this.showInformationAddress = true;
        this.showFormAddress = true;
      }

      if (res && res.event === GoogleMapsEvents.DRAG_START) {
        this.showInformationAddress = false;
        this.showFormAddress = false;
      }

      if (res && res.event === GoogleMapsEvents.READY) {
        this.getInformationSaved();
      }
    });

    this.gmFilterServiceSub = this.gmFilterService.onEvent().subscribe(res => {
      if (res) {
        switch (res.event) {
          case GmFilterEvents.FILTER_NOTIFY:
            this.filterNotification.showNotification = false;
            this.filterNotification.textNotification = res.data.text;
            this.filterNotification.iconNotification = res.data.icon;
            setTimeout(() => (this.filterNotification.showNotification = true), 10);
            break;
          default:
            break;
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.googleEventSub) {
      this.googleEventSub.unsubscribe();
    }
    if (this.googleSearchMarkerSub) {
      this.googleSearchMarkerSub.unsubscribe();
    }
    if (this.gmFilterServiceSub) {
      this.gmFilterServiceSub.unsubscribe();
    }
    this.googleMapService.setSearchPlaceEventMarker(null);
    this.googleMapService.setUseFocusedSearch(false);
    this.googleMapService.eventEmit(GoogleMapsEvents.DEFAULT_MAP);
  }

  private getInformationSaved(): void {
    this.mapsAPILoader.load().then(() => {
      this.loadService();
      setTimeout(() => {
        const existInformationSaved = this._StorageService.getStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_ONE);
        if (existInformationSaved == null) {
          return void 0;
        }
        const data: ISearchPlace = {
          coords: existInformationSaved.endPointCoords,
          address: existInformationSaved.endPoint
        };
        this.form.controls.referenceEndPoint.patchValue(existInformationSaved.referenceEndPoint);
        this.googleMapService.setSearchPlaceEventMarker(GoogleMapsEvents.SET_END_MARKER);
        this.googleMapService.eventEmit(GoogleMapsEvents.SET_END_MARKER, { coords: data.coords, goAddress: true });
      }, 2000);
    });
  }

  public clearDataStorage(): void {
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_ONE);
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO);
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO_DATA);
  }

  loadService(): void {
    if (!this.geocodeService && google) {
      this.geocodeService = new google.maps.Geocoder(this.googleMapService.getMap());
    }
    if (!this.autocompleteService && google && google.maps.places) {
      this.autocompleteService = new google.maps.places.AutocompleteService();
    }
    if (!this.placeService && google && google.maps.places) {
      this.placeService = new google.maps.places.PlacesService(this.googleMapService.getMap());
    }
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      endPoint: [null, [Validators.required, Validators.minLength(this.minLength.sd_start_point)]],
      endPointCoords: [null, [Validators.required]],
      referenceOne: [null],
      referenceTwo: [null],
      district: [null],
      referenceEndPoint: [
        null,
        [Validators.maxLength(this.maxLength.interior_department_number), Validators.pattern(REG_EX.noEmojis)]
      ]
    });
  }

  changeUrgent(value: any): void {
    if (value.target.checked) {
      this.form.controls['hour'].disable();
      this.form.controls['date'].disable();
      this.form.controls['am_pm'].disable();
    } else {
      this.form.controls['hour'].enable();
      this.form.controls['date'].enable();
      this.form.controls['am_pm'].enable();
    }
    this.form.controls['urgent'].setValue(value.target.checked);
    this.disableDate = value.target.checked;
  }

  goToSummary(): void {
    this.openSummary.next({ form: this.form.value, policy: this.currentServiceSelected });
  }

  cancel(): void {
    this.cleanSearch();
    this.clearDataStorage();
    this.form.reset();
    this.close();
  }

  confirmation(): void {
    this._StorageService.saveStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_ONE, this.form.value);
    this.router.navigate(['/mapfre-services/home-doctor/request/steps/2']);
  }

  loadApiGoogleMaps(): void {
    this.mapsAPILoader.load().then(() => {
      this.loadService();
    });
  }

  fnOpenSearch(type?: string): void {
    this.loadApiGoogleMaps();
    this.currentPlaceTypeSearch = type;
    const data: ISearchPlace = {
      coords: this.form.controls['endPointCoords'].value,
      address: this.form.controls['endPoint'].value
    };
    if (data.address) {
      data.valid = true;
      this.googleMapService.eventEmit(GoogleMapsEvents.SET_END_MARKER, data);
    }
    this.googleMapService.setSearchMarkerDefault(data);
    this.googleMapService.setSearchPlaceEventMarker(GoogleMapsEvents.SET_END_MARKER);
    this.googleMapService.emitTemporalPosition();
    this.googleMapService.setUseFocusedSearch(true);
    this.googleMapService.setSearchPlaceTitle(this.lang.LocateEndPoint);
  }

  onFocusSearch(): void {
    this.loadApiGoogleMaps();
  }

  cleanSearch(): void {
    if (isEmpty(this.form.controls['endPointCoords'])) {
      this.coincidences = [];
      this.form.reset();

      return void 0;
    }

    this.googleMapService.eventEmit(GoogleMapsEvents.CHANGE_OPTIONS_MAPS, { zoom: 12 });
    this.coincidences = [];
    this.googleMapService.eventEmit(this.googleMapService.getSearchPlaceEventMarker(), { coords: null, address: '' });
    this.form.controls['endPoint'].setValue(null);
    this.form.controls['endPointCoords'].setValue(null);
    this.form.controls['referenceEndPoint'].setValue(null);
    this.form.reset();
    this.searchText = null;
    this.currentSelected = false;
    this.clearDataStorage();
    this.googleMapService.setSearchMarkerDefault(null);
    this.googleMapService.setSearchPlaceEventMarker(null);
    this.googleMapService.setUseFocusedSearch(false);
    this.googleMapService.eventEmit(GoogleMapsEvents.DEFAULT_MAP);
  }

  onKeyUpSearch(control?: AbstractControl): void {
    let text;
    if (control.invalid) {
      text = (control.value || '').trim();
      if (text.length === 0) {
        this.cleanSearch();
      }

      this.coincidences = [];

      return;
    }

    text = (control.value || '').trim();
    if (text.length >= this.minLength.sd_start_point) {
      this.getPlacePredictions(text);
    }

    if (text.length < this.minLength.sd_start_point) {
      this.coincidences = [];
    }

    this.searchText = control.value;
  }

  addressInformationFormat(refTwo: string, district: string): string {
    return `${!isEmpty(refTwo) ? `${refTwo}, ${district}` : !isEmpty(district) ? district : ''}`;
  }

  selectAutocomplete(item: GoogleResultAutocomplete): void {
    this.coincidences = [];
    this.placeService.getDetails(
      { placeId: item.place_id, fields: ['formatted_address', 'geometry'] },
      (response: any) => {
        this.ngZone.run(() => {
          response.formatted_address = item.description;
          this.handlerResponseGoogle([response]);
        });
      }
    );
  }

  handlerResponseGoogle(response: Array<any>): void {
    if (response && response.length && response[0].formatted_address !== 'Perú') {
      const data: ISearchPlace = {
        coords: this.form.controls['endPointCoords'].value,
        address: this.form.controls['endPoint'].value
      };
      const coords: ICoords = {
        latitude: response[0].geometry.location.lat(),
        longitude: response[0].geometry.location.lng()
      };

      if (BaseGoogleMaps.isInsidePoligon(coords, this.poligons)) {
        this.searchText = response[0].formatted_address;
        this.form.controls['endPoint'].setValue(this.searchText);
        this.form.controls['endPointCoords'].setValue(coords);
        this.googleMapService.setSearchMarkerDefault(data);
        this.googleMapService.setSearchPlaceEventMarker(GoogleMapsEvents.SET_END_MARKER);
        this.googleMapService.eventEmit(GoogleMapsEvents.SET_END_MARKER, { coords });
      } else {
        this.form.controls['endPointCoords'].setValue(null);
        this.fnShowNotify(true);
      }
    } else {
      this.form.controls['endPointCoords'].setValue(null);
      this.fnShowNotify();
    }
  }

  getPlacePredictions(text?: string): void {
    this.loadService();
    this.autocompleteService.getPlacePredictions(
      {
        input: text || this.searchText,
        componentRestrictions: { country: 'pe' }
      },
      (response: Array<any>) => {
        this.ngZone.run(() => {
          this.handlerResponseGoogleAutocomplete(response);
        });
      }
    );
  }

  handlerResponseGoogleAutocomplete(response: Array<GoogleResultAutocomplete>): void {
    this.coincidences = response || [];
  }

  fnShowNotify(inValidZone?: boolean): void {
    this.notifyText = inValidZone ? this.langSearch.InvalidZone : this.langSearch.WarmingPlace;
    this.gmFilterService.eventEmit(GmFilterEvents.FILTER_NOTIFY, {
      text: this.notifyText,
      icon: 'icon-mapfre_051_alerta'
    });
  }

  getGoogleMapService(): GoogleMapService {
    return this.googleMapService;
  }
}

// tslint:disable-next-line: max-file-line-count
