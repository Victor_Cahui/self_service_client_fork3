import { DatePipe, DecimalPipe, DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { FormatDDMMMYYYY } from '@mx/core/shared/helpers/util/date';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { APPLICATION_CODE, FORMAT_DATE_YYYYMMDD } from '@mx/settings/constants/general-values';
import {
  ARROW_LEFT_ICON,
  CROSS_ICON,
  DRIVER_ICON,
  MEDICAL_CONSULTATION_ICON,
  PROFILE_ICON
} from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { ServiceFormLang, ServicesLang } from '@mx/settings/lang/services.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IHomeDoctorRequest, ISummaryView } from '@mx/statemanagement/models/service.interface';
import { GmMsBaseSummary } from '../../gm-ms-base-summary';

@Component({
  selector: 'client-gm-ms-home-doctor-summary',
  templateUrl: './gm-ms-home-doctor-summary.component.html',
  providers: [DatePipe, DecimalPipe]
})
export class GmMsHomeDoctorSummaryComponent extends GmMsBaseSummary implements OnInit {
  data: ISummaryView;
  body: IHomeDoctorRequest;
  arrow_left_icon = ARROW_LEFT_ICON;
  medical_consultation_icon = MEDICAL_CONSULTATION_ICON;
  cross_icon = CROSS_ICON;
  profile_icon = PROFILE_ICON;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    protected clientService: ClientService,
    protected authService: AuthService,
    protected servicesMfService: ServicesMFService,
    protected decimalPipe: DecimalPipe,
    protected datePipe: DatePipe,
    protected router: Router,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(
      render2,
      elementRef,
      platformService,
      resizeService,
      generalService,
      clientService,
      authService,
      servicesMfService,
      decimalPipe,
      datePipe,
      router,
      document
    );

    this.icon = DRIVER_ICON;
    this.modalTitle = ServicesLang.Titles.SubstituteDriverRequestSuccess;
    this.modalMessage = ServicesLang.Messages.SubstituteDriverRequestSuccess;
    this.modalTitleError = GeneralLang.Messages.EstimatedUser;
    this.modalMessageError = ServicesLang.Messages.InvalidDate;
    this.serviceLang = ServiceFormLang;
  }

  setViewData(values): void {
    // Para validar fecha
    this.hourRequest = values.form.hour;
    this.dateRequest = values.form.date;
    this.ampmRequest = values.form.am_pm;

    // Vista
    const shortDate = this.datePipe.transform(this.dateRequest, FORMAT_DATE_YYYYMMDD);
    const specialityId = parseInt(values.form.specialty, 10);
    const specialtyFind = values.policy.especialidades.find(
      especialidad => especialidad.identificador === specialityId
    );
    const specialty = specialtyFind.nombre || '';
    const patientId = parseInt(values.form.beneficiary, 10);
    const patiendFind = values.policy.beneficiarios.find(beneficiario => beneficiario.identificador === patientId);
    const patient = patiendFind.nombre || '';

    this.data = {
      doctor: this.serviceLang.ToBeAssigned,
      policy: values.policy.producto.nombreProducto,
      policyNumber: values.policy.producto.numeroPoliza,
      specialty,
      patient,
      symptom: values.form.symptom,
      date: shortDate ? `${FormatDDMMMYYYY(shortDate)} - ${values.form.hour} ${values.form.am_pm.toUpperCase()}` : '',
      to: values.form.endPoint,
      referenceTo: values.form.referenceEndPoint,
      phone: values.form.phone,
      price:
        values.policy.precio.precio === 0
          ? PaymentLang.Texts.Free.toUpperCase()
          : `${values.policy.precio.monedaSimbolo} ${this.decimalPipe.transform(values.policy.precio.precio, '1.2-2')}`
    };

    // Request
    this.body = {
      fechaRecojo: shortDate,
      esUrgente: values.form.urgent,
      destino: {
        puntoLlegada: values.form.endPoint,
        referenciaLlegada: values.form.referenceEndPoint,
        latitud: values.form.endPointCoords.latitude,
        longitud: values.form.endPointCoords.longitude
      },
      producto: values.policy.producto,
      pago: {
        codigoMoneda: values.policy.precio.codigoMoneda,
        precio: values.policy.precio.precio,
        esGratis: values.policy.precio.precio === 0
      },
      telefono: values.form.phone,
      sintomas: values.form.symptom,
      beneficiario: patientId,
      especialidad: specialityId
    };

    if (shortDate) {
      this.body.horaRecojo = `${values.form.hour} ${values.form.am_pm.toUpperCase()}`;
    }

    this.body.producto.tipoCliente = this.clientService.getUserData().tipoUsuario || '';
  }

  confirmRequest(): void {
    this.showModalError = false;
    this.showModal = false;

    // Fecha válida
    this.setMinDateWithHours();
    if (!this.body.esUrgente) {
      delete this.body.esUrgente;
      this.invalidTime = this.validateMinHours(this.hourRequest, this.dateRequest, this.ampmRequest);
      if (this.invalidTime) {
        const date = new Date();
        const timeText = `${this.datePipe.transform(date, 'shortTime')}`;
        this.modalMessageError = this.modalMessageError.replace('{{dateTime}}', timeText);
        this.showModalError = true;
        setTimeout(() => {
          this.mfModalSuccess.open();
        }, 50);

        return;
      }
    } else {
      delete this.body.horaRecojo;
      delete this.body.fechaRecojo;
    }

    // Enviar Solicitud
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.serviceSub = this.servicesMfService.homeDoctorRequest(this.body, params).subscribe(
      response => {
        if (response) {
          this.showModal = true;
          setTimeout(() => {
            this.mfModalSuccess.open();
          }, 50);
        }
      },
      () => {},
      () => {
        this.serviceSub.unsubscribe();
      }
    );
  }
}
