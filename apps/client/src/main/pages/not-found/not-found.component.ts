import { Component, OnInit } from '@angular/core';
import { IMAGE_NOT_FOUND_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-not-found-component',
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent implements OnInit {
  title404 = GeneralLang.Titles.Title404;
  message404 = GeneralLang.Messages.Message404;
  image404 = IMAGE_NOT_FOUND_ICON;

  ngOnInit(): void {}
}
