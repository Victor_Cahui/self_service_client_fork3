import { Component, OnInit } from '@angular/core';
import { IMAGE_NOT_FOUND_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-under-construction',
  templateUrl: './under-construction.component.html'
})
export class UnderConstructionComponent implements OnInit {
  title = GeneralLang.Titles.TitleUnderConstruction;
  message = GeneralLang.Messages.MessageUnderConstruction;
  image = IMAGE_NOT_FOUND_ICON;

  ngOnInit(): void {}
}
