export const environment = {
  production: true,
  API_URL: 'http://10.160.120.215/',

  interceptors: {
    ensureHttps: false,
    auth: true
  }
};
