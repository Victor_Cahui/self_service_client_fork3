  function openNav() {
    document.querySelector(".g-box-nav").classList.add('g-box-nav--active');
    let body = document.querySelector('body');
    let html = document.querySelector('html');
    body.classList.add('g-no-scroll');
    html.classList.add('g-no-scroll');
    document.querySelector(".g-box-nav--close").classList.add('g-block');

    document.querySelector('.js-hamb-close').classList.add('d-block');
    document.querySelector('.js-hamb-open').classList.add('d-none');

    document.querySelector('.g-oim--header').classList.add('g-oim-header--open-menu');

  }

  function closeNavBox() {
    document.querySelector(".g-box-nav").classList.remove('g-box-nav--active');
    let body = document.querySelector('body');
    let html = document.querySelector('html');
    body.classList.remove('g-no-scroll');
    html.classList.remove('g-no-scroll');
    document.querySelector(".g-box-nav--close").classList.remove('g-block');

    document.querySelector('.js-hamb-open').classList.remove('d-none');
    document.querySelector('.js-hamb-close').classList.remove('d-block');

    document.querySelector('.g-oim--header').classList.remove('g-oim-header--open-menu');


  }