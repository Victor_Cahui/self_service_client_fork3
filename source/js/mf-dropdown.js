// Mobile nav touch hover script
// $(document).ready(function(){
// 	var container = $('.g-dropdown');
// 	var list = $('.g-sub-nav');
// 	var flag = false;

// container.bind('touchstart', function(){
//   if (!flag) {
//     flag = true;
//     setTimeout(function(){ flag = false; }, 260);
// 	 list.toggleClass('g-show-nav');
//   }
//   return false
// });

// container.hover(function(){
// 	 list.addClass('g-show-nav');
// 	}, function(){
// 	 list.removeClass('g-show-nav');
// 	});
// });



$(function() {

  var dropdown_click  = $(".g-dropdown--click"),
      dropdown_close  = $(".g-dropdown--close"),
      dropdown_show   = $(".g-dropdown--show"),
      dropdown_active = "active";

      fn_dropdown_close();
      $(window).resize(function() {
        fn_dropdown_close();
      });

      dropdown_click.on("click", function() {
        $(this).toggleClass(dropdown_active);

        if($(this).hasClass(dropdown_active)) {
          $(this).next().addClass(dropdown_active);
        } else {
          $(this).next().removeClass(dropdown_active);
        }
      });

      dropdown_close.on("click", function() {
        fn_dropdown_close();
      });

      function fn_dropdown_close() {
        dropdown_show.removeClass(dropdown_active);
        dropdown_click.removeClass(dropdown_active);
      }

});
