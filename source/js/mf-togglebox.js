var boxToggle = function(){
  var init = function () {
    var dataBox = '*[data-boxtoggle]';
    var box = document.querySelectorAll(dataBox)[0];
    if (box !== undefined) {
      var dataTitle = '*[data-boxtitle="title"]';
      var boxTop = box.querySelectorAll(dataTitle)[0].offsetHeight;
      var body = document.getElementsByTagName('body')[0];
      var isDesktop = _fnIsDesktop();
      var heightDevice = _fnWindowHeight();
      if (isDesktop) {
        box.classList.remove('show');
        box.style.top = 'auto';
        body.removeAttribute('class');
      } else {
        box.style.top = heightDevice - boxTop + 'px';
      }
    }
  };
  var toggle = function (c) {
    var body = document.getElementsByTagName('body')[0];
    var dataBox = '*[data-boxtoggle="' + c + '"]';
    var box = document.querySelectorAll(dataBox)[0];
    var isDesktop = _fnIsDesktop();
    var isFilterVisible = (box.classList.contains('show')) ? true : false;
    if (isDesktop) {
      body.removeAttribute('class');
      box.classList.remove('show');
      return;
    } else {
      isFilterVisible = !isFilterVisible;
      if (isFilterVisible) {
        body.setAttribute('class', 'body-hidden');
        box.classList.add('show');
      } else {
        body.removeAttribute('class');
        box.classList.remove('show');
      }
    }
  };
  return {init:init, toggle:toggle}
}();
boxToggle.init();

function _fnIsDesktop() {
  return (window.innerWidth > 991) ? true : false;
}

function _fnWindowHeight(){
  return window.innerHeight;
}