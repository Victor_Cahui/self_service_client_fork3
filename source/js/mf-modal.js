$(function() {

  $(".g-modal--link").on("click", function (e) {
    e.preventDefault();
    var id = $(this).data("modal");
    $('[data-modal="'+ id + '"]').fadeIn();
  });

  $(".g-modal--close").on("click", function (e) {
    e.preventDefault();
    $(".g-modal").hide();
  });
  // esta funcion es solo demostrativa en patternlab
  $(".g-modal--other").on("click", function (e) {
    e.preventDefault();
    setTimeout(function(){
      $('[data-modal="open-send"]').fadeIn();
     }, 1000);

  });

  $(".open-modal").each(function() {
    $(this).trigger("click");
  });

});


var fullModal = function () {
  var posY;
  var open = function (modal) {
    // console.log('Open modal... ')
    var body = document.getElementsByTagName('body')[0];
    var isDesktop = _fnIsDesktop();
    var uiView = document.getElementsByClassName('g-ui--view')[0];
    var modalName = document.getElementsByName(modal)[0];
    var headerBox = modalName.getElementsByClassName('g-full-modal--header')[0];
    var footerBox = modalName.getElementsByClassName('g-full-modal--footer')[0];
    var contentBox = modalName.getElementsByClassName('g-full-modal--content')[0];
    posY = _fnGetPosY();
    body.setAttribute('class', 'body-hidden');
    uiView.classList.add('disable-scrolling');
    uiView.style.marginTop = '-' + posY + 'px';
    modalName.classList.add('open');
    if (!isDesktop) {
      contentBox.style.top = headerBox.offsetHeight - 1 + 'px';
      contentBox.style.bottom = footerBox.offsetHeight - 1 + 'px';
    } else {
      contentBox.style.top = 'auto';
      contentBox.style.bottom = 'auto';
    }
  };
  var close = function (modal) {
    var body = document.getElementsByTagName('body')[0];
    var uiView = document.getElementsByClassName('g-ui--view')[0];
    var modalName = document.getElementsByName(modal)[0];
    var contentBox = modalName.getElementsByClassName('g-full-modal--content')[0];
    body.removeAttribute('class');
    uiView.classList.remove('disable-scrolling');
    uiView.removeAttribute('style');
    modalName.classList.remove('open');
    window.scrollTo(0,posY);
    contentBox.style.top = 'auto';
    contentBox.style.bottom = 'auto';
  };
  var resize = function () {
    var isDesktop = _fnIsDesktop();
    var modalName = document.getElementsByClassName('g-full-modal'); //console.log(modalName);
    var modalArray = Array.from(modalName);
    modalArray.forEach(function (e) {
      var isOpen = _fnIsOpen(e);
      var headerBox = e.getElementsByClassName('g-full-modal--header')[0];
      var footerBox = e.getElementsByClassName('g-full-modal--footer')[0];
      var contentBox = e.getElementsByClassName('g-full-modal--content')[0];
      if (!isDesktop && isOpen) {
        contentBox.style.top = headerBox.offsetHeight - 1 + 'px';
        contentBox.style.bottom = footerBox.offsetHeight - 1 + 'px';
      } else {
        contentBox.style.top = 'auto';
        contentBox.style.bottom = 'auto';
      }
    })
  }
  return {open:open, close:close, resize:resize}
}();


document.ontouchmove = function ( event ) {
  var isTouchMoveAllowed = true;
  var target = event.target;

  while ( target !== null ) {
    if ( target.classList && target.classList.contains( 'disable-scrolling' ) ) {
      isTouchMoveAllowed = false;
      break;
    }
    target = target.parentNode;
  }

  if ( !isTouchMoveAllowed ) {
    event.preventDefault();
  }

};

var _fnGetPosY = function () {
  return window.pageYOffset;
}

var _fnWindowHeight = function (){
  var clientHeight = document.documentElement.clientHeight,
      innerHeight = window.innerHeight;
  return clientHeight < innerHeight ? innerHeight : clientHeight;
  // return (typeof window.outerHeight != 'undefined') ? Math.max(window.outerHeight, $(window).height()) : $(window).height()
}

var _fnIsDesktop = function () {
  return (window.innerWidth > 991) ? true : false;
}

var _fnIsOpen = function (e) {
  return (e.classList.contains('open')) ? true : false;
}