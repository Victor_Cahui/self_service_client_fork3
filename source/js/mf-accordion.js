$(function() {

  var item = $('[data-accordion="item"]');

  item.each(function() {
    var open          = $(this).data("open"),
    content           = $('[data-accordion="content"]'),
        on            = $('[data-accordion="on"]'),
        off           = $('[data-accordion="off"]'),
        click         = $('[data-accordion="click"]'),
        activeClass   = "active";

    if(open === true) {
      $(this).find(click).addClass(activeClass);
      $(this).find(on).hide();
      $(this).find(off).show();
      $(this).find(content).show();
    }

  });

  $('[data-accordion="click"]').on("click", function(e) {
    e.preventDefault();

    var container     = $('[data-accordion="container"]'),
        item          = $('[data-accordion="item"]'),
        title         = $('[data-accordion="title"]'),
        content       = $('[data-accordion="content"]'),
        on            = $('[data-accordion="on"]'),
        off           = $('[data-accordion="off"]'),
        parent        = $(this).closest(container),
        group         = parent.data("group"),
        activeClass   = "active";

        if(group === true) {
          parent.find(content).not(this).slideUp();
          parent.find(" ." + activeClass).not(this).removeClass(activeClass);
          parent.find(on).show().next().hide();
        }

      $(this).toggleClass(activeClass);

      if ($(this).hasClass(activeClass)) {
        $(this).closest(item).find(content).slideDown();
        $(this).find(on).hide();
        $(this).find(off).show();
      } else {
        $(this).closest(item).find(content).slideUp();
        $(this).find(off).hide();
        $(this).find(on).show();
      }
  });

});