// export const environment = {
//   production: true,
// tslint:disable-next-line: max-line-length
//   API_URL: 'https://virtserver.swaggerhub.com/multiplicalima/policies/0.1/policies/findByClient?documenttype=DNI&document=46454654&status=1'
// };

export const environment = {
  production: false,
  API_URL: 'https://jsonplaceholder.typicode.com/posts',
  API_URL2: 'http://jsonplaceholder.typicode.com/posts/{id}',

  interceptors: {
    ensureHttps: false,
    auth: true
  }
};
