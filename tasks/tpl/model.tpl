// tslint:disable

import { hasValue } from '@mx/core/shared/helpers';

function removeUndefinedFromObj(obj: any): any {
  const arrObjKeys = Object.keys(obj);

  return arrObjKeys.reduce((acc, k) => hasValue(obj[k]) ? {...acc, [k]: obj[k]} : acc, {});
}

{{#data}}
  {{#hasPathReq}}
  {{#parameters}}
export class {{path.modelName}} {
  {{#path.params}}
  public {{name}}{{^isRequired}}?{{/isRequired}}: {{type}};
  {{/path.params}}

  public static create(obj): {{path.modelName}} {
    return removeUndefinedFromObj({
      {{#path.params}}
      {{name}}: hasValue(obj.{{name}}) ? {{^isObject}}{{#isString}}'' + {{/isString}}{{#isNumber}}+{{/isNumber}}{{#isBoolean}}!!({{/isBoolean}}obj.{{name}}{{#isBoolean}}){{/isBoolean}} : undefined{{/isObject}}{{#isObject}}obj.{{name}} : undefined{{/isObject}}{{#comma}},{{/comma}}{{#hasEnum}} //{{#enum}} {{val}}{{#hasComma}},{{/hasComma}}{{/enum}}{{/hasEnum}}
      {{/path.params}}
    });
  }
}

  {{/parameters}}
  {{/hasPathReq}}
  {{#hasQueryReq}}
  {{#parameters}}
export class {{query.modelName}} {
  {{#query.params}}
  public {{name}}{{^isRequired}}?{{/isRequired}}: {{type}};
  {{/query.params}}

  public static create(obj): {{query.modelName}} {
    return removeUndefinedFromObj({
      {{#query.params}}
      {{name}}: hasValue(obj.{{name}}) ? {{^isObject}}{{#isString}}'' + {{/isString}}{{#isNumber}}+{{/isNumber}}{{#isBoolean}}!!({{/isBoolean}}obj.{{name}}{{#isBoolean}}){{/isBoolean}} : undefined{{/isObject}}{{#isObject}}obj.{{name}} : undefined{{/isObject}}{{#comma}},{{/comma}}{{#hasEnum}} //{{#enum}} {{val}}{{#hasComma}},{{/hasComma}}{{/enum}}{{/hasEnum}}
      {{/query.params}}
    });
  }
}

  {{/parameters}}
  {{/hasQueryReq}}
  {{#hasBodyReq}}
  {{#parameters}}
export class {{body.modelName}} {
  {{#body.params}}
  public {{name}}{{^isRequired}}?{{/isRequired}}: {{type}};
  {{/body.params}}
  {{#body.isObject}}

  public static create(obj): {{body.modelName}} {
    return removeUndefinedFromObj({
      {{#body.params}}
      {{name}}: hasValue(obj.{{name}}) ? {{^isObject}}{{#isString}}'' + {{/isString}}{{#isNumber}}+{{/isNumber}}{{#isBoolean}}!!({{/isBoolean}}obj.{{name}}{{#isBoolean}}){{/isBoolean}} : undefined{{/isObject}}{{#isObject}}obj.{{name}} : undefined{{/isObject}}{{#comma}},{{/comma}}{{#hasEnum}} //{{#enum}} {{val}}{{#hasComma}},{{/hasComma}}{{/enum}}{{/hasEnum}}
      {{/body.params}}
    });
  }
  {{/body.isObject}}
  {{#body.isArray}}

  public static create(obj): {{body.modelName}} {
    {{#body.params}}
    return obj.{{name}}.map(v => {{#isString}}'' + {{/isString}}{{#isNumber}}+{{/isNumber}}{{#isBoolean}}!!({{/isBoolean}}v);
    {{/body.params}}
  }
  {{/body.isArray}}
}

  {{/parameters}}
  {{/hasBodyReq}}
{{/data}}
export default '';
