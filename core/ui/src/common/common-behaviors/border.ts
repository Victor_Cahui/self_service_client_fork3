import { ElementRef } from '@angular/core';

import { coerceBooleanProp } from '../helpers';
import { Constructor } from './constructor';

export interface CanBorder {
  outline: boolean;
}

export interface HasElementRef {
  _elementRef: ElementRef;
}

export type Bordered = boolean;

export function mixinBordered<T extends Constructor<HasElementRef>>(base: T): Constructor<CanBorder> & T {
  return class extends base {
    // tslint:disable-next-line: prefer-readonly
    private _outline: any;

    get outline(): Bordered {
      return this._outline;
    }
    set outline(value: Bordered) {
      const outline = coerceBooleanProp(value);
      if (this._outline !== outline) {
        if (outline) {
          this._elementRef.nativeElement.classList.add('g-btn-outline');
        } else {
          this._elementRef.nativeElement.classList.remove('g-btn-outline');
        }

        this._outline = outline;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);
    }
  };
}
