import { ElementRef } from '@angular/core';

import { Constructor } from './constructor';

export interface CanSize {
  size: Size;
}

export interface HasElementRef {
  _elementRef: ElementRef;
}

export type Size = 'sm' | 'md' | 'lg' | undefined;

// tslint:disable-next-line: only-arrow-functions
export function mixinSize<T extends Constructor<HasElementRef>>(base: T, defaultSize?: Size): Constructor<CanSize> & T {
  return class extends base {
    // tslint:disable-next-line: prefer-readonly
    private _size: Size;

    get size(): Size {
      return this._size;
    }
    set size(value: Size) {
      const size = value || defaultSize;

      if (size !== this._size) {
        if (this._size) {
          this._elementRef.nativeElement.classList.remove(`size-${this._size}`);
        }
        if (size) {
          this._elementRef.nativeElement.classList.add(`size-${size}`);
        }

        this._size = size;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);

      this.size = defaultSize;
    }
  };
}
