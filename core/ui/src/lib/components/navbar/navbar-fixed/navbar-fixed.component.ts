import { Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'client-navbar-fixed',
  template: `
    <div #navbar class="g-navbar--fixed" style="display: none;">
      <div class="g-bg-c--gray21 g-navbar--shadow {{ class }}">
        <ng-content></ng-content>
      </div>
    </div>
  `
})
export class NavbarFixedComponent {
  @ViewChild('navbar') navbar: ElementRef;

  @Output() pageYOffset: EventEmitter<number> = new EventEmitter<number>();
  @Input() offsetTop = 400;
  @Input() class: string;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(): void {
    this.navbar.nativeElement.style.display = window.pageYOffset > this.offsetTop ? 'block' : 'none';
    this.pageYOffset.emit(window.pageYOffset);
  }
}
