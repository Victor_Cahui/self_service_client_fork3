import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCustomAlertComponent } from './custom-alert.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfCustomAlertComponent],
  exports: [MfCustomAlertComponent]
})
export class MfCustomAlertModule {}
