export {
  MfAlertSuccessComponent as MfAlertSuccess
} from '@mx/core/ui/lib/components/alerts/alert-success/alert-success.component';
export { MfAlertSuccessModule } from '@mx/core/ui/lib/components/alerts/alert-success/alert-success.module';
