import { Component, EventEmitter, HostBinding, Input, OnDestroy, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { coerceBooleanProp } from '../../../../common/helpers';

@Component({
  selector: 'mf-card-header',
  templateUrl: './card-header.component.html'
})
export class CardHeaderComponent implements OnDestroy {
  @HostBinding('attr.class') attr_class = 'g-ie-flex w-100';

  @Input() header = '';
  @Input() uppercaseHeader = true;
  @Input() subtitle?;
  @Input() textRigth?;
  @Input() collapseText;
  @Input() collapseClassColor;
  @Input() icon = '';
  @Input() showIconWeb = false;
  @Input() disableCollapse = false;

  @Input() swapLoading: boolean;

  // Hidden header in Desktop
  @Input()
  get hlg(): boolean {
    return this._hlg;
  }
  set hlg(value: boolean) {
    this._hlg = coerceBooleanProp(value);
  }
  protected _hlg = false;

  // Hidden header actions link
  @Input()
  get hal(): boolean {
    return this._hal;
  }
  set hal(value: boolean) {
    this._hal = coerceBooleanProp(value);
  }
  protected _hal = false;

  // Hidden without truncate
  @Input()
  get wot(): boolean {
    return this._wot;
  }
  set wot(value: boolean) {
    this._wot = coerceBooleanProp(value);
  }
  protected _wot = false;

  @Input()
  get act(): boolean {
    return this._act;
  }
  set act(value: boolean) {
    this._act = coerceBooleanProp(value);
  }
  protected _act = false;

  // Hidden collapse
  @Input()
  get hc(): boolean {
    return this._hc;
  }
  set hc(value: boolean) {
    this._hc = coerceBooleanProp(value);
  }
  protected _hc = false;

  @Input()
  get collapsed(): boolean {
    return this._collapsed;
  }
  set collapsed(value: boolean) {
    this._collapsed = coerceBooleanProp(value);
  }
  protected _collapsed = false;

  @Output() expand: EventEmitter<boolean>;

  subResize: Subscription;

  constructor() {
    this.expand = new EventEmitter<boolean>();
  }

  toggle(): void {
    this.collapsed = !this.collapsed;
    this.expand.next(this.collapsed);
  }

  toggleActive(): void {
    if (this._act) {
      this.toggle();
    }
  }

  ngOnDestroy(): void {
    if (!!this.expand) {
      this.expand.unsubscribe();
    }
  }
}
