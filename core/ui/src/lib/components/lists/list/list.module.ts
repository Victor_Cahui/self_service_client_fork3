import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfListComponent } from './list.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfListComponent],
  exports: [MfListComponent]
})
export class MfListModule {}
