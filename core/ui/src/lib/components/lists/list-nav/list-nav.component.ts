import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-list-nav',
  templateUrl: './list-nav.component.html',
  styleUrls: ['./list-nav.component.scss']
})
export class MfListNavComponent implements OnInit {
  @Input() list: Array<any> = [];

  ngOnInit(): void {}

  trackByFn(index, item): any {
    return index;
  }
}
