import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfListNavComponent } from './list-nav.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfListNavComponent],
  exports: [MfListNavComponent]
})
export class MfListNavModule {}
