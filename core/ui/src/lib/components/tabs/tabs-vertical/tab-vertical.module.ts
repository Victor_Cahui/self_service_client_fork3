import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfTabVerticalComponent } from './tab-vertical.component';

@NgModule({
  imports: [CommonModule, RouterModule, GoogleModule],
  declarations: [MfTabVerticalComponent],
  exports: [MfTabVerticalComponent],
  providers: [ResizeService]
})
export class MfTabVerticalModule {}
