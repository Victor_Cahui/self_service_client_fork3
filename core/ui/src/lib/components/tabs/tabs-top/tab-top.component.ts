import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'mf-tab-top',
  templateUrl: './tab-top.component.html'
})
export class MfTabTopComponent extends GaUnsubscribeBase implements AfterViewInit, OnChanges, OnInit {
  @ViewChild('boxMin') boxMin: ElementRef;
  @Input() disabled: boolean;
  @Input() full = true;
  @Input() local?: boolean;
  @Input() relative?: boolean;
  @Input() shadow = true;
  @Input() tabItemsList: Array<any>;
  @Input() selectedTab = 0;
  @Output() selected?: EventEmitter<number> = new EventEmitter<number>();

  showArrowRight: boolean;
  showArrowLeft: boolean;
  citaCovidSpeciality: any;
  resizeEventSub: Subscription;

  constructor(
    protected gaService: GAService,
    private readonly resizeService: ResizeService,
    public activatedRoute: ActivatedRoute
  ) {
    super(gaService);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const selectedTab = changes.selectedTab || ({} as SimpleChange);
    if (selectedTab.previousValue !== selectedTab.currentValue) {
      this.handlerArrow();
      setTimeout(() => {
        this.scrollToTab(selectedTab.currentValue);
      });
    }

    if (changes.tabItemsList && changes.tabItemsList.currentValue) {
      this._initTabs();
    }
  }

  ngAfterViewInit(): void {
    this.handlerArrow();
    this.boxMin.nativeElement.onscroll = () => {
      this.handlerArrow();
    };
    this.resizeService.resizeEvent.pipe(takeUntil(this.unsubscribeDestroy$)).subscribe(() => {
      this.handlerArrow();
    });
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe();
    if (this.relative === undefined) {
      this.relative = true;
    }
  }

  // Ruta
  routeRelative(route): string {
    return !this.relative ? route : `./${route}`;
  }

  // Accion dentro del mismo componente
  onSelect(value, index: number): void {
    this.selectedTab = index;
    this.selected.emit(value);
  }

  scrollToTab(position: number): void {
    const list = this.boxMin.nativeElement.children[0].children;
    if (list.length) {
      const offsetLeft = list[position].offsetLeft;
      this.scrollBy(offsetLeft - 150);
    }
  }

  handlerArrow(): void {
    if (this.boxMin) {
      const max = this.boxMin.nativeElement.firstElementChild.clientWidth;
      const min = this.boxMin.nativeElement.clientWidth;
      const maxScroll = this.boxMin.nativeElement.scrollWidth - min;
      const scrollLeft = this.boxMin.nativeElement.scrollLeft;

      setTimeout(() => (this.showArrowRight = max > min && scrollLeft < maxScroll));
      setTimeout(() => (this.showArrowLeft = max > min && scrollLeft > 0));
    }
  }

  goToRight(): void {
    this.scrollBy(window.scrollX + 200);
  }

  goToLeft(): void {
    this.scrollBy(window.scrollX - 200);
  }

  scrollBy(left: number): void {
    this.boxMin.nativeElement.scrollBy({
      behavior: 'smooth',
      left,
      top: 0
    });
  }

  private _initTabs(): void {
    this.tabItemsList = this.tabItemsList.filter(tab => tab.visible);
  }
}
