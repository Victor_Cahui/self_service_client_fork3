import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonModule } from '../../forms/button';
import { MfModalAlertComponent } from './modal-alert.component';

@NgModule({
  imports: [CommonModule, MfButtonModule],
  declarations: [MfModalAlertComponent],
  exports: [MfModalAlertComponent]
})
export class MfModalAlertModule {}
