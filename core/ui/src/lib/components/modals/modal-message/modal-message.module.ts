import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfModalAlertModule } from '../modal-alert/modal-alert.module';
import { MfModalMessageComponent } from './modal-message.component';

@NgModule({
  imports: [CommonModule, MfModalAlertModule],
  exports: [MfModalMessageComponent],
  declarations: [MfModalMessageComponent]
})
export class MfModalMessageModule {}
