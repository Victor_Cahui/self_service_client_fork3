// tslint:disable: invalid-void
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MfModalAlertComponent as MfModalAlert } from '../modal-alert/modal-alert.component';

@Component({
  selector: 'mf-modal-message',
  templateUrl: './modal-message.component.html'
})
export class MfModalMessageComponent implements OnInit {
  @Input() icon?: string;
  @Input() pathIcon?: string;
  @Input() confirm = false;
  @Input() success = true;
  @Input() messageDone: string;
  @Input() sizeClass = 'g-modal--medium';
  @Input() messageSuccessUpdate: string;
  @Input() confirmButtonText = 'Aceptar';
  @Output() confirmResponse: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild(MfModalAlert) mfModalAlert: MfModalAlert;

  ngOnInit(): void {}

  open(): void {
    this.mfModalAlert.open();
  }

  close(): void {
    this.mfModalAlert.close();
    this.closeModal.emit();
  }

  onConfirm(event): void {
    this.confirmResponse.emit(event);
  }
}
