import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MfModalAlertComponent as MfModalAlert } from '../modal-alert/modal-alert.component';

@Component({
  selector: 'mf-modal-error',
  templateUrl: './modal-error.component.html'
})
export class MfModalErrorComponent implements OnInit {
  @Input() messageDone: string;
  @Input() messageSuccessUpdate: string;
  @Input() confirmButtonText = 'Confirmar';
  @ViewChild(MfModalAlert) mfModalAlert: MfModalAlert;

  ngOnInit(): void {}

  open(): void {
    this.mfModalAlert.open();
  }

  close(): void {
    this.mfModalAlert.close();
  }
}
