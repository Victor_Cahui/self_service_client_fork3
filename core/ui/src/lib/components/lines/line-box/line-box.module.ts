import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfLineBoxComponent } from './line-box.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfLineBoxComponent],
  exports: [MfLineBoxComponent]
})
export class MfLineBoxModule {}
