import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class MfTitleComponent implements OnInit {
  @Input() text1 = 'Jusmar';
  @Input() text2 = 'Ayma';

  ngOnInit(): void {}
}
