import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TooltipsComponent } from './tooltips.component';

@NgModule({
  imports: [CommonModule],
  declarations: [TooltipsComponent],
  exports: [TooltipsComponent]
})
export class TooltipsModule {}
