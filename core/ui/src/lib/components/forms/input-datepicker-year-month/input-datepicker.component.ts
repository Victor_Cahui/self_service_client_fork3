import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Size } from '../../../../common';
import { coerceBooleanProp } from '../../../../common/helpers';

@Component({
  selector: 'mf-input-datepicker-month-year',
  templateUrl: './input-datepicker.component.html',
  providers: [
    // tslint:disable-next-line:no-forward-ref
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MfInputDatepickerMonthYearComponent), multi: true }
  ]
})
export class MfInputDatepickerMonthYearComponent
  implements AfterViewInit, ElementRef, ControlValueAccessor, OnChanges, OnInit {
  nativeElement: any;
  @ViewChild('mfInputDatepickerComponent', { read: ElementRef })
  input_native: ElementRef;

  @HostBinding('attr.currency') currency: boolean;

  @HostBinding('attr.max') max: string;

  @HostBinding('attr.min') min: string;

  @HostBinding('attr.maxlength') maxlength_host: string;

  @HostBinding('attr.minlength') minlength_host: string;
  @Input() minD: Date;
  @Input() maxD: Date;
  @Input() monthYearOnly: boolean;
  get maxlength(): string {
    return this._maxlength;
  }
  set maxlength(maxlength: string) {
    this._maxlength = maxlength;
  }
  protected _maxlength: string;

  @Input()
  get minlength(): string {
    return this._minlength;
  }
  set minlength(minlength: string) {
    this._minlength = minlength;
  }
  protected _minlength: string;

  @Input() OnlyNumber: boolean;

  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }
  protected _value;

  @Input()
  get type(): string {
    return this._type;
  }
  set type(type: string) {
    this._type = type || 'text';
  }
  protected _type = 'text';

  @Input() size: Size = 'md';
  @Input()
  get label(): string {
    return this._label;
  }
  set label(label: string) {
    this._label = label;
    if (!this._label) {
      this._isLabelHidden = true;
    }
  }
  protected _label: string;

  @Input()
  get placeholder(): string {
    return this._label;
  }
  set placeholder(placeholder: string) {
    this.label = placeholder; // analizar
  }

  @Input()
  get isLabelHidden(): boolean {
    return this._isLabelHidden;
  }
  set isLabelHidden(hidden: boolean) {
    this._isLabelHidden = hidden;
  }
  protected _isLabelHidden = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProp(value);

    // Browsers may not fire the blur event if the input is disabled too quickly.
    // Reset from here to ensure that the element doesn't become stuck.
    // tslint:disable-next-line: comment-type
    /*if (this.focused) {
      this.focused = false;
      this.stateChanges.next();
    }*/
  }
  protected _disabled = false;

  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }
  protected _error = false;

  @Input()
  get maskcurrency(): boolean {
    return this._maskCurrency;
  }
  set maskcurrency(value: boolean) {
    // this._maskCurrency = coerceBooleanProp(value);
  }
  protected _maskCurrency = false;

  @Output()
  public onblur: EventEmitter<string> = new EventEmitter<string>();

  onBlurInput($event: any): void {
    if (!this._disabled) {
      if (this._value) {
        this.onblur.emit(this._value);
      }
    }
  }

  propagateChange: any = () => {};

  constructor() {
    this.monthYearOnly = false;
  }

  ngAfterViewInit(): void {
    // outputs `I am span`
    // tslint:disable-next-line: comment-type
    /*  console.log('ngAfterViewInit nativeElement', this.nativeElement);
    console.log('ngAfterViewInit ElementRef', this.input_native.nativeElement); */
  }

  ngOnInit(): void {}

  ngOnChanges(changes: any): void {
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
