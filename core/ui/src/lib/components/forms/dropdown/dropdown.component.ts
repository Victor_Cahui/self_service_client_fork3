import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class MfDropdownComponent implements OnInit {
  @Input() list: Array<any> = [];
  @Input() title: string;

  ngOnInit(): void {}

  trackByFn(index, item): any {
    return index;
  }
}
