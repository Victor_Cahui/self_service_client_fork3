import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MfCheckboxComponent } from './checkbox.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [MfCheckboxComponent],
  exports: [MfCheckboxComponent]
})
export class MfCheckboxModule {}
