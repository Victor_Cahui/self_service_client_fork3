import { Component, EventEmitter, forwardRef, Input, OnChanges, Output } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { hasNotValidValue, isPristine, NumberOrString } from '@mx/core/shared/helpers';
import { dateDiffInHours, dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import {
  ANIO_LIMITE_POLIZA,
  ANIO_LIMITE_SINISTER,
  ESTADO_SINIESTRO_KEY,
  GET_ESTADO_SINIESTRO,
  TIPO_SEGURO
} from '@mx/settings/constants/key-values';
import { mapArrPolicies } from '@mx/views/mapfre-services/schedule-appointment/steps/step-1/schedule-step-1.utils';
import { of } from 'rxjs/internal/observable/of';
import { map } from 'rxjs/internal/operators/map';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Observable } from 'rxjs/Observable';
import { isNullOrUndefined } from 'util';
import { coerceBooleanProp } from '../../../../common/helpers';
import { SelectListGroup, SelectListItem } from './select-item';
import { _mapHour } from './select-item.utils';
import { ConfigCbo } from './select.interface';

@Component({
  selector: 'mf-select',
  templateUrl: './select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => MfSelectComponent),
      multi: true
    }
  ]
})
export class MfSelectComponent extends UnsubscribeOnDestroy implements ControlValueAccessor, OnChanges {
  @Input()
  get value(): any {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    this.selectedValue(this._value);
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }

  @Input()
  set options(listItem: Array<SelectListItem>) {
    if (Array.isArray(listItem)) {
      this._listItem = listItem;
      this.verifyGroup();
    }
  }
  get options(): Array<SelectListItem> {
    return this._listItem;
  }

  get groups(): Array<SelectListGroup> {
    return this._listGroup;
  }

  @Input()
  get label(): string {
    return this._label;
  }
  set label(label: string) {
    this._label = label;
    if (!this._label) {
    }
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(placeholder: string) {
    this._placeholder = placeholder;
    this.selectedDefault = true;
  }

  @Input()
  get readonly(): boolean {
    return this._disabled;
  }
  set readonly(value: boolean) {
    this._disabled = coerceBooleanProp(value);
  }

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProp(value);
  }

  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }

  get existGroup(): boolean {
    return this._existGroup;
  }

  constructor(
    private readonly _Autoservicios: Autoservicios,
    private readonly _StorageService: StorageService,
    private readonly authService: AuthService,
    private readonly _Comun: Comun,
    private readonly _PoliciesService: PoliciesService
  ) {
    super();
  }
  @Input()
  public arrCustom: any[];
  @Input()
  public config: ConfigCbo = {};
  @Input()
  public dependent1: any;
  @Input()
  public dependent2: any;
  @Input()
  public dependent3: any;
  @Input()
  public dependent4: any;
  @Input()
  public dependent5: any;
  @Input()
  public dataPatient: any;
  @Input()
  public isValid: boolean;
  @Input()
  public citaCovid: boolean;
  @Input()
  public type: string;
  @Input()
  public lblCbo = 'text';
  @Input()
  public valueCbo = 'value';
  @Input()
  public validCodiv: boolean;
  @Input()
  public frm: FormGroup;
  @Input()
  public name: string;
  protected _value: any;
  protected _listItem: Array<SelectListItem>;
  protected _listGroup: Array<SelectListGroup>;
  protected _label: string;
  protected _placeholder: string;
  protected _disabled = false;
  protected _error = false;
  protected _existGroup = false;
  @Input() showEnableWhenHasOneItem: boolean;

  @Output() getSelectedOpt: EventEmitter<any> = new EventEmitter();
  @Output() getLoadingState: EventEmitter<any> = new EventEmitter();
  @Output() getSelectedOptDoctor: EventEmitter<any> = new EventEmitter();

  selectedDefault = false;
  private _isFirstTime = true;
  isEmpty = false;

  propagateChange: any = () => {};

  private verifyGroup(): void {
    if (this._listItem) {
      this._existGroup = this._listItem.some(i => !isNullOrUndefined(i.group));

      if (this._existGroup) {
        const listGroup = this._listItem
          .map(item => {
            return item.group;
          })
          .filter(x => !!x);

        let listGroupNew = new Array<SelectListGroup>();
        const groupFlat: any = {};
        listGroup.forEach(group => {
          if (groupFlat[group.text]) {
            return;
          }
          groupFlat[group.text] = true;
          listGroupNew = [...listGroupNew, group];
        });

        this._listGroup = listGroupNew;
      }
    }
  }

  selectedValue(value: string): void {
    if (this._listItem) {
      this._listItem.forEach(item => {
        item.selected = item.value === value;
      });
    }
  }

  optionsByGroup(group: SelectListGroup): Array<SelectListItem> {
    const _newItems = this._listItem.filter(item => {
      return item.group === group;
    });

    return _newItems;
  }

  ngOnChanges(changes: any): void {
    if (!this._disabled) {
      this.propagateChange(this.value);
    }

    const actions = this._getActions();
    if (actions[this.type]) {
      const selectedValueId = this.frm.value[this.name];
      setTimeout(() => {
        if (!changes.options) {
          isPristine(changes) || this._resetCbo();
        }
        actions[this.type]();
        isPristine(changes) && this._setValueInFrm(selectedValueId);
      });
    }
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  change(id): any {
    const objSelected = this.options.find(o => `${o[this.valueCbo]}` === `${id}`);
    if (!objSelected) {
      return void 0;
    }
    this.getSelectedOpt.emit({ isFirstTime: this._isFirstTime, ...objSelected });
    this._isFirstTime = false;
  }

  trackByFn(index, item): any {
    return index;
  }

  byId(item1: SelectListItem, item2: SelectListItem): any {
    return item1.value === item2.value;
  }

  private _getActions(): any {
    return {
      anioLimitePoliza: this._anioLimitePoliza.bind(this),
      anioLimiteSiniestro: this._anioLimiteSiniestro.bind(this),
      insuranceType: this._insuranceType.bind(this),
      period: this._period.bind(this),
      plan: this._plan.bind(this),
      sinisterStatus: this._sinisterStatus.bind(this),
      specialty: this._specialty.bind(this),
      vigencyMonth: this._vigencyMonth.bind(this),
      recordType: this._recordType.bind(this),
      periodMonth: this._periodMonth.bind(this),
      documentoPaciente: this._patient.bind(this),
      numeroPoliza: this._policy.bind(this),
      doctorByDate: this._doctorByDate.bind(this),
      doctorByDoctor: this._doctorByDoctor.bind(this),
      clinicBySpeciality: this._clinicBySpeciality.bind(this),
      horaInicio: this._hoursByDate.bind(this),
      horaInicioByDoctor: this._hoursByDoctor.bind(this),
      clinicsList: this._getClinics.bind(this)
    };
  }

  private _insuranceType(): void {
    this.valueCbo = 'codigo';
    this.lblCbo = 'descripcion';
    const arrFromLocalStorage = this._StorageService.getStorage(TIPO_SEGURO) || [];
    if (arrFromLocalStorage.length) {
      this._successService(TIPO_SEGURO, arrFromLocalStorage);

      return void 0;
    }

    const arrOptions$ = this._Autoservicios.ObtenerCategorias({
      codigoApp: APPLICATION_CODE
    });
    this._doActionsWhenAsynEnds(arrOptions$, TIPO_SEGURO);
  }

  private _anioLimitePoliza(): void {
    this.valueCbo = 'descripcion';
    this.lblCbo = 'descripcion';
    const arrFromLocalStorage = this._StorageService.getStorage(ANIO_LIMITE_POLIZA) || [];
    if (arrFromLocalStorage.length) {
      this._successService(ANIO_LIMITE_POLIZA, arrFromLocalStorage);

      return void 0;
    }

    const arrOptions$ = this._Autoservicios.ListarLimiteAnios(
      {
        codigoApp: APPLICATION_CODE
      },
      { orden: 'DESC' }
    );
    this._doActionsWhenAsynEnds(arrOptions$, ANIO_LIMITE_POLIZA);
  }

  private _anioLimiteSiniestro(): void {
    this.valueCbo = 'codigoAnioSiniestro';
    this.lblCbo = 'descripcionAnioSiniestro';
    const arrFromLocalStorage = this._StorageService.getStorage(ANIO_LIMITE_SINISTER) || [];
    if (arrFromLocalStorage.length) {
      this._successService(ANIO_LIMITE_SINISTER, arrFromLocalStorage);

      return void 0;
    }

    const arrOptions$ = this._Autoservicios.ObtenerAniosSiniestro({
      codigoApp: APPLICATION_CODE
    });
    this._doActionsWhenAsynEnds(arrOptions$, ANIO_LIMITE_SINISTER);
  }

  private _specialty(): void {
    this.valueCbo = 'especialidadId';
    this.lblCbo = 'especialidadDescripcion';

    if ([this.dependent1, this.dependent2].some(hasNotValidValue)) {
      return void (this.disabled = !this.showEnableWhenHasOneItem && true);
    }

    const arrOptions$ = this._Autoservicios
      .ObtenerDetalleCita(
        {
          clinicaId: this.dependent1,
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.dependent2
        },
        {
          tipoCita: this.dependent5 ? this.dependent5 : 'P',
          flagEspecialidad: 'S'
        }
      )
      // .pipe(map(c => (!this.validCodiv ? c.especialidades.especialidadId === 104 : c.especialidades)));
      .pipe(map(c => (c !== null ? c.especialidades : [])));

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _sinisterStatus(): void {
    this.valueCbo = 'code';
    this.lblCbo = 'txt';
    this._successService(ESTADO_SINIESTRO_KEY, GET_ESTADO_SINIESTRO());
  }

  private _plan(): void {
    this.valueCbo = 'codPlan';
    this.lblCbo = 'descPlan';

    if ([this.dependent1].some(hasNotValidValue)) {
      return void (this.disabled = true);
    }

    const arrOptions$ = this._getPlanPeriodService().pipe(
      map(r => r.planes),
      map(r => [{ codPlan: 'default', descPlan: 'Todos los planes' }, ...r])
    );

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _period(): void {
    this.valueCbo = 'codPeriodo';
    this.lblCbo = 'descPeriodo';

    if ([this.dependent1].some(hasNotValidValue)) {
      return void (this.disabled = true);
    }

    const arrOptions$ = this._getPlanPeriodService().pipe(
      map(r =>
        r.periodos.map(p => ({
          ...p,
          codPeriodo: `${p.anioPeriodo}-${p.mesPeriodo}`
        }))
      )
    );

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _getPlanPeriodService(): Observable<any> {
    return this._Autoservicios.ObtenerEpsContratanteAfiliadosFiltros(
      {
        nroContrato: this.dependent1
      },
      {
        codigoApp: APPLICATION_CODE
      }
    );
  }

  // sctr constancias (records)

  private _vigencyMonth(): void {
    this.valueCbo = 'value';
    this.lblCbo = 'texto';

    if ([this.dependent1].some(hasNotValidValue)) {
      return void (this.disabled = true);
    }

    const arrOptions$ = this._getPeriodRecorService().pipe(map(r => r.periodos));

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _recordType(): void {
    this.valueCbo = 'value';
    this.lblCbo = 'texto';

    if ([this.dependent1].some(hasNotValidValue)) {
      return void (this.disabled = true);
    }

    const arrOptions$ = this._getPeriodRecorService().pipe(map(r => r.tipoConstancias));

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _getPeriodRecorService(): Observable<any> {
    const entity = this.authService.retrieveEntity();

    return this._Autoservicios.FiltrosConstancias(
      {
        numeroPoliza: this.dependent1
      },
      {
        codigoApp: APPLICATION_CODE,
        tipoDocumento: entity.type,
        documento: entity.number
      }
    );
  }

  // sctr period receipts

  private _periodMonth(): void {
    this.valueCbo = 'value';
    this.lblCbo = 'texto';

    if ([this.dependent1].some(hasNotValidValue)) {
      return void (this.disabled = true);
    }

    const arrOptions$ = this._getPeriodReceiptsService();

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _getPeriodReceiptsService(): Observable<any> {
    const entity = this.authService.retrieveEntity();

    return this._Autoservicios.ListarPeriodosFiltros(
      {
        numeroPoliza: this.dependent1
      },
      {
        codigoApp: APPLICATION_CODE,
        tipoDocumento: entity.type,
        documento: entity.number
      }
    );
  }

  private _patient(): void {
    this.valueCbo = 'documento';
    this.lblCbo = 'nombreCompleto';

    // this._namePropInResp = 'documento';

    const arrOptions$ = this._PoliciesService.getSortedHealthyPolicies().pipe(
      switchMap((ph: any = []) =>
        this._Autoservicios.ObtenerPacientesXUsuarioLogueado(
          {
            codigoApp: APPLICATION_CODE
          },
          {
            RequestGetPacientesVO: ph.map((p: any) => ({ tipoPoliza: p.tipoPoliza, numeroPoliza: p.numeroPoliza }))
          }
        )
      )
    );

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _policy(): void {
    if (!Array.isArray(this.arrCustom)) {
      return void 0;
    }

    this.valueCbo = 'numeroPoliza';
    this.lblCbo = 'descripcionPolizaFull';

    this._doActionsWhenAsynEnds(of(mapArrPolicies(this.arrCustom)));
  }

  private _doctorByDate(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3, this.dependent4].some(hasNotValidValue)) {
      return void 0;
    }

    this.valueCbo = 'doctorId';
    this.lblCbo = 'nombreCompleto';

    const selectedDate = dateToStringFormatYYYYMMDD(this.dependent3);
    const doctors$: Observable<any> = this._Autoservicios.ObtenerDoctorPorFechaYHora(
      {
        codigoApp: APPLICATION_CODE,
        clinicaId: this.dependent1,
        especialidadId: this.dependent2,
        fecha: selectedDate,
        horaInicio: this.dependent4
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P'
      }
    );

    const arrOptions$ = doctors$.pipe(
      map(m => {
        const doctors = m.map(u => ({
          ...u,
          cmpDescription: `CMP: ${u.codigoColegioMedico}`,
          specialitiesDescription: u.especialidades.map(e => e.especialidadDescripcion).join(', ')
        }));

        return doctors;
      })
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _doctorByDoctor(): void {
    if ([this.dependent1, this.dependent2].some(hasNotValidValue)) {
      return void 0;
    }

    this.valueCbo = 'doctorId';
    this.lblCbo = 'nombreCompleto';

    const doctors$ = this._Autoservicios.ObtenerDoctores(
      {
        codigoApp: APPLICATION_CODE,
        clinicaId: this.dependent1,
        especialidadId: this.dependent2
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P'
      }
    );

    const arrOptions$ = doctors$.pipe(
      map(arr =>
        arr !== null
          ? arr.map(d => ({
              ...d,
              cmpDescription: `CMP: ${d.codigoColegioMedico}`,
              specialitiesDescription: d.especialidades.map(e => e.especialidadDescripcion).join(', ')
            }))
          : []
      )
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _clinicBySpeciality(): void {
    this.valueCbo = 'clinicaId';
    this.lblCbo = 'nombre';

    const clinics$ = this._Autoservicios.ObtenerClinicasAgendables(
      { codigoApp: APPLICATION_CODE },
      {
        latitud: this.dependent1.latitud,
        longitud: this.dependent1.longitud
      }
    );

    this._doActionsWhenAsynEnds(clinics$);
  }

  private _getClinics(): void {
    this.valueCbo = 'clinicaId';
    this.lblCbo = 'nombre';

    const clinics$ = this._Autoservicios.ObtenerClinicasAgendables(
      { codigoApp: APPLICATION_CODE },
      {
        latitud: this.dependent1.latitud,
        longitud: this.dependent1.longitud
      }
    );

    this._doActionsWhenAsynEnds(clinics$);
  }

  private _hoursByDate(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3].some(hasNotValidValue)) {
      return void 0;
    }

    this.valueCbo = 'horaInicio';
    this.lblCbo = 'description';

    const selectedDate = dateToStringFormatYYYYMMDD(this.dependent3);
    const serverDateTime$ = this._Comun.ObtenerDatosServidor();
    const hoursByDate$ = this._Autoservicios.ObtenerHorarios(
      {
        clinicaId: this.dependent1,
        codigoApp: APPLICATION_CODE,
        especialidadId: this.dependent2,
        fecha: selectedDate
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P',
        tipoDocPaciente: this.dataPatient.tipoDocumento,
        docPaciente: this.dataPatient.documento
      }
    );
    const arrOptions$ = serverDateTime$.pipe(
      switchMap(s =>
        hoursByDate$.pipe(
          map(arr =>
            arr !== null
              ? arr
                  .filter(f => {
                    const serverDatetime = `${s.fecha}T${s.hora}`;
                    const appointmentDatetime = `${selectedDate}T${f.horaInicio}`;

                    return dateDiffInHours(appointmentDatetime, serverDatetime) >= 3;
                  })
                  .map(_mapHour.bind(this))
              : []
          )
        )
      )
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _hoursByDoctor(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3, this.dependent4].some(hasNotValidValue)) {
      return void 0;
    }

    this.valueCbo = 'horaInicio';
    this.lblCbo = 'description';

    const selectedDate = dateToStringFormatYYYYMMDD(this.dependent4);
    const serverDateTime$ = this._Comun.ObtenerDatosServidor();
    const hoursByDate$ = this._Autoservicios.ObtenerHorariosPorDoctor(
      {
        clinicaId: this.dependent1,
        codigoApp: APPLICATION_CODE,
        doctorId: this.dependent3,
        especialidadId: this.dependent2,
        fecha: selectedDate
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P',
        tipoDocPaciente: this.dataPatient.tipoDocumento,
        docPaciente: this.dataPatient.documento
      }
    );

    const arrOptions$ = serverDateTime$.pipe(
      switchMap(s =>
        hoursByDate$.pipe(
          map(arr =>
            arr !== null
              ? arr
                  .filter(f => {
                    const serverDatetime = `${s.fecha}T${s.hora}`;
                    const appointmentDatetime = `${selectedDate}T${f.horaInicio}`;

                    return dateDiffInHours(appointmentDatetime, serverDatetime) >= 3;
                  })
                  .map(_mapHour.bind(this))
              : []
          )
        )
      )
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  // core
  private _doActionsWhenAsynEnds(obs$: Observable<any>, storageKeyToSave?: string): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    obs$
      .pipe(takeUntil(this.unsubscribeDestroy$))
      // tslint:disable-next-line: no-unbound-method
      .subscribe(this._successService.bind(this, storageKeyToSave), console.error);
  }
  private _successService(storageKeyToSave: string, arr: any[]): void {
    this.getLoadingState.emit({ isLoadingVisible: false });
    this.isValid = true;
    this.isEmpty = false;

    if (arr && !arr.length) {
      this.options = [];
      this.isEmpty = true;
      this.disabled = true;

      return void 0;
    }

    this.options = arr;
    storageKeyToSave && this._StorageService.saveStorage(storageKeyToSave, this.options);
    this.disabled = false;
    setTimeout(() => {
      if (!this.frm.value[this.name]) {
        this.config.canAutoSelectFirstOpt && this._setValueByFirstOpt(arr);

        if (this.citaCovid) {
          this.getSelectedOptDoctor.emit(this.options);
        }

        return void 0;
      }
      this._setValueInFrm(this.frm.value[this.name]);
      this.change(this.frm.value[this.name]);
    });
  }

  private _setValueByFirstOpt(arr: any[]): void {
    if (arr) {
      let item;
      if (this.type === 'documentoPaciente') {
        const index = arr.findIndex(e => e.relacion === 'TITULAR');
        item = index !== -1 ? arr[index] : arr[0];
      } else {
        item = arr[0];
      }
      arr.length === 1 && (this.disabled = false);
      setTimeout(() => {
        item && this._setValueInFrm(item[this.valueCbo]);
        this.change(this.frm.value[this.name]);
      });
    } else {
      this.disabled = true;
    }
  }

  private _setValueInFrm(selectedValueId: NumberOrString): void {
    if (hasNotValidValue(selectedValueId)) {
      return void 0;
    }

    this.frm.patchValue({
      [this.name]: selectedValueId || ''
    });
  }

  private _resetCbo(): void {
    this.frm.patchValue({
      [this.name]: ''
    });
    this.options = [];
  }
}

// tslint:disable-next-line: max-file-line-count
