import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { OWL_DATE_TIME_LOCALE, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DirectivesModule } from '../../../directives/directives.module';
import { MfInputDatepickerComponent } from './input-datepicker.component';

@NgModule({
  imports: [CommonModule, FormsModule, DirectivesModule, OwlDateTimeModule, OwlNativeDateTimeModule],
  declarations: [MfInputDatepickerComponent],
  providers: [{ provide: OWL_DATE_TIME_LOCALE, useValue: 'es' }],
  exports: [MfInputDatepickerComponent]
})
export class MfInputDatepickerModule {}
