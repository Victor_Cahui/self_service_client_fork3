import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-dropdown-icon',
  templateUrl: './dropdown-icon.component.html',
  styleUrls: ['./dropdown-icon.component.scss']
})
export class MfDropdownIconComponent implements OnInit {
  @Input() list: Array<any> = [];
  @Input() title: string;
  @Input() number: number;
  @Input() icon: string;

  ngOnInit(): void {}

  trackByFn(index, item): any {
    return index;
  }
}
