import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../../directives/directives.module';
import { MfTextAreaComponent } from './text-area.component';

@NgModule({
  imports: [CommonModule, FormsModule, DirectivesModule],
  declarations: [MfTextAreaComponent],
  exports: [MfTextAreaComponent]
})
export class MfTextAreaModule {}
