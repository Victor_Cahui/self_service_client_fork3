import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Size } from '../../../../common';
import { coerceBooleanProp } from '../../../../common/helpers';

@Component({
  selector: 'mf-textarea',
  templateUrl: './text-area.component.html',
  providers: [
    // tslint:disable-next-line:no-forward-ref
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MfTextAreaComponent), multi: true }
  ]
})
export class MfTextAreaComponent implements AfterViewInit, ElementRef, ControlValueAccessor, OnChanges, OnInit {
  nativeElement: any;

  @ViewChild('mfTextAreaComponent', { read: ElementRef })
  input_native: ElementRef;

  @HostBinding('attr.maxlength') maxlength_host: string;
  @HostBinding('attr.minlength') minlength_host: string;

  @Input()
  get pattern(): string {
    return this._pattern;
  }
  set pattern(pattern: string) {
    this._pattern = pattern;
  }
  protected _pattern: string;

  @Input()
  get maxlength(): string {
    return this._maxlength;
  }
  set maxlength(maxlength: string) {
    this._maxlength = maxlength;
  }
  protected _maxlength: string;

  @Input()
  get minlength(): string {
    return this._minlength;
  }
  set minlength(minlength: string) {
    this._minlength = minlength;
  }
  protected _minlength: string;

  @Input()
  get rows(): number {
    return this._rows;
  }
  set rows(rows: number) {
    this._rows = rows;
  }
  protected _rows = 2;

  @Input() spellcheck = false;

  @Input()
  get upperCase(): boolean {
    return this._uppercase;
  }

  set upperCase(value: boolean) {
    this._uppercase = value;
  }
  protected _uppercase = false;

  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
    if (!this._disabled) {
      if (this.upperCase) {
        this._value = value.toUpperCase();
      }
      this.propagateChange(this.value);
    }
  }
  protected _value;

  @Input() size: Size = 'md';
  @Input()
  get label(): string {
    return this._label;
  }
  set label(label: string) {
    this._label = label;
    if (!this._label) {
      this._isLabelHidden = true;
    }
  }
  protected _label: string;

  @Input()
  get placeholder(): string {
    return this._label;
  }
  set placeholder(placeholder: string) {
    this.label = placeholder; // analizar
  }
  @Input()
  get isLabelHidden(): boolean {
    return this._isLabelHidden;
  }

  set isLabelHidden(hidden: boolean) {
    this._isLabelHidden = hidden;
  }
  protected _isLabelHidden = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProp(value);
  }
  protected _disabled = false;

  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }
  protected _error = false;

  @Output() onblur: EventEmitter<string> = new EventEmitter<string>();
  @Output() onpaste: EventEmitter<string> = new EventEmitter<string>();
  @Output() onfocus: EventEmitter<string> = new EventEmitter<string>();
  @Output() onkeypress: EventEmitter<string> = new EventEmitter<string>();

  onBlurInput($event: any): void {
    if (!this._disabled) {
      if (this._value) {
        this.onblur.emit(this._value);
      }
    }
  }

  onFocusInput($event: any): void {
    if (!this._disabled) {
      this.onfocus.emit(this._value);
    }
  }

  onPasteInput(event: any): void {
    if (!this._disabled) {
      this.onpaste.emit(event);
    }
  }

  onKeyPress($event): void {
    if (!this._disabled) {
      this.onkeypress.emit(this._value);
    }
  }

  propagateChange: any = () => {};

  ngAfterViewInit(): void {}

  ngOnInit(): void {}

  ngOnChanges(changes: any): void {
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
