import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  FiltersModule,
  MfAlertWarningModule,
  MfBoxModule,
  MfButtonModule,
  MfCheckboxModule,
  MfDropdownIconModule,
  MfDropdownModule,
  MfInputDatepickerModule,
  MfInputModule,
  MfLineBoxModule,
  MfListModule,
  MfListNavModule,
  MfLoaderModule,
  MfModalAlertModule,
  MfModalModule,
  MfSelectModule,
  MfShowErrorsModule,
  MfTabTopModule,
  MfTitleModule
} from '../public-api';

@NgModule({
  exports: [
    FiltersModule,
    CommonModule,
    MfAlertWarningModule,
    MfBoxModule,
    MfButtonModule,
    MfCheckboxModule,
    MfDropdownIconModule,
    MfDropdownModule,
    MfInputDatepickerModule,
    MfInputModule,
    MfLineBoxModule,
    MfListModule,
    MfListNavModule,
    MfLoaderModule,
    MfModalModule,
    MfModalAlertModule,
    MfSelectModule,
    MfShowErrorsModule,
    MfTabTopModule,
    MfTitleModule
  ]
})
export class UiModule {}
