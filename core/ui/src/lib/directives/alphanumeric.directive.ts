import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[Alphanumeric]'
})
export class AlphanumericDirective {
  regexText = /^[a-zA-Zá-úÁ-Ú0-9\s]*$/g;
  regex: RegExp = new RegExp(this.regexText);
  isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);
  private readonly specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Space'];

  @Input() Alphanumeric: boolean;
  @Input() maxlength: number;

  constructor(private readonly el: ElementRef, private readonly ngControl: NgControl) {}

  @HostListener('keypress', ['$event'])
  onKeyPress(event: KeyboardEvent): any {
    if (this.Alphanumeric) {
      if (this.specialKeys.indexOf(event.code) !== -1) {
        return;
      }
      if (this.el) {
        const current: string = this.el.nativeElement.value ? this.el.nativeElement.value : '';
        const next: string = current.concat(event.key);
        if (next && !String(next).match(this.regex)) {
          event.preventDefault();
        }
      }
    }
  }

  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent): any {
    if (this.Alphanumeric) {
      e.stopPropagation();
      e.preventDefault();
      const result = this.handlerPaste(e);
      if (this.regex.test(result.value)) {
        this.ngControl.control.setValue(result.value.substring(0, this.maxlength));
      }
    }
  }

  handlerPaste($e: KeyboardEvent): any {
    const clipboard =
      $e['clipboardData'] ||
      ($e['originalEvent'] && $e['originalEvent']['clipboardData']
        ? $e['originalEvent']['clipboardData']
        : window['clipboardData']);
    const argument = this.isIEOrEdge ? 'text' : 'text/plain';
    const value = clipboard.getData(argument);
    const valueOnlyAlphanumeric = value.match(this.regexText);
    const valueNew = (valueOnlyAlphanumeric && valueOnlyAlphanumeric.join('')) || '';

    return this.promiseValue($e.srcElement, valueNew);
  }

  promiseValue(domInput, insertValue): any {
    if ('selectionStart' in domInput && 'selectionEnd' in domInput) {
      const lValue = domInput.value.substring(0, domInput.selectionStart);
      const rValue = domInput.value.substring(domInput.selectionEnd, domInput.value.length);

      return {
        value: lValue + insertValue + rValue,
        support: true
      };
    }

    return {
      value: insertValue,
      support: false
    };
  }

  clean(el): void {
    el.value = el.value.match(this.regexText);
  }
}
