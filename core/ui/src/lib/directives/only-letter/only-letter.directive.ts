import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[OnlyLetter]'
})
export class OnlyLetterDirective {
  private readonly regex: RegExp = new RegExp(/^[a-zA-ZñÑá-ú ]*$/g);
  private readonly specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'Space'];

  @Input() OnlyLetter: boolean;

  constructor(private readonly el: ElementRef) {}

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent): any {
    if (this.OnlyLetter) {
      if (this.specialKeys.indexOf(event.key) !== -1) {
        return;
      }
      if (this.el) {
        const current: string = this.el.nativeElement.value ? this.el.nativeElement.value : '';
        const next: string = current.concat(event.key);
        if (next && !String(next).match(this.regex)) {
          event.preventDefault();
        }
      }
    }
  }
}
