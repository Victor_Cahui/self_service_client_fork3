import { NgModule } from '@angular/core';
import { NgDropFilesDirective } from './ng-drop-files.directive';

@NgModule({
  exports: [NgDropFilesDirective],
  declarations: [NgDropFilesDirective]
})
export class NgDropModule {}
