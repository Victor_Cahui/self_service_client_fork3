import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[dropdown-close]'
})
export class DropdownCloseDirective {
  currentIndex = -1;

  constructor(private readonly elementRef: ElementRef, private readonly renderer2: Renderer2) {}

  @HostListener('click', ['$event'])
  onclick(event): void {
    const dropdownElements = this.elementRef.nativeElement.getElementsByTagName('client-dropdown') || [];
    const dropdowns = [];
    for (const drop of dropdownElements) {
      dropdowns.push(drop);
    }

    const newIndex = dropdowns.findIndex(
      (dropdown, index) => dropdown.classList.contains('active') && index !== this.currentIndex
    );
    if (newIndex > -1) {
      if (this.currentIndex > -1) {
        this.renderer2.removeClass(dropdowns[this.currentIndex], 'active');
      }
      this.currentIndex = newIndex;
    }
  }
}
