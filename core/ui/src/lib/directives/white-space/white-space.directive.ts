import { Directive, ElementRef, HostListener, Inject, Input, Optional, Renderer2 } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, DefaultValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[whiteSpace]',
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: DontWhiteSpaceDirective, multi: true }]
})
export class DontWhiteSpaceDirective extends DefaultValueAccessor {
  private _type = 'text';

  private _value: string;

  private readonly _sourceRenderer: Renderer2;
  private readonly _sourceElementRef: ElementRef;

  @Input() whiteSpace: string;

  @Input()
  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value || 'text';
  }

  get value(): any {
    return this._value;
  }

  set value(val: any) {
    this.writeValue(val);

    if (val !== this.value) {
      this._value = val;
    }
    this.onChange(val);
  }

  @HostListener('blur', ['$event.type', '$event.target.value'])
  onBlur(event: string, value: string): void {
    if (this._sourceElementRef.nativeElement.maxLength >= value.length) {
      this.updateValue(event, value);
    }

    this.onTouched();
  }

  @HostListener('input', ['$event.type', '$event.target.value'])
  onInput(event: string, value: string): void {
    if (this._sourceElementRef.nativeElement.maxLength >= value.length) {
      this.updateValue(event, value);
    }
  }

  constructor(
    @Inject(Renderer2) renderer: Renderer2,
    @Inject(ElementRef) elementRef: ElementRef,
    @Optional() @Inject(COMPOSITION_BUFFER_MODE) compositionMode: boolean
  ) {
    super(renderer, elementRef, compositionMode);

    this._sourceRenderer = renderer;
    this._sourceElementRef = elementRef;
  }

  public writeValue(value: any): void {
    if (!this._value) {
      this._value = value;
    }

    this._sourceRenderer.setProperty(this._sourceElementRef.nativeElement, 'value', value);

    if (this._type !== 'text') {
      this._sourceRenderer.setAttribute(this._sourceElementRef.nativeElement, 'value', value);
    }
  }

  private updateValue(event: string, value: string): void {
    this.value = this.whiteSpace !== '' && event !== this.whiteSpace ? value : value.trim().replace(/ +/g, ' ');
  }
}
