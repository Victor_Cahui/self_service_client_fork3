import { NgModule } from '@angular/core';
import { AlphanumericDirective } from './alphanumeric.directive';
import { ClickOutsideDirective } from './click-outside.directive';
import { CollapseMobileDirective } from './collapse-mobile.directive';
import { CollapseDirective } from './collapse.directive';
import { DropdownCloseDirective } from './dropdown-close.directive';
import { DropdownIconDirective } from './dropdown-icon.directive';
import { ImgLoaderDirective } from './img-loader/img-loader.directive';
import { MatchHeightDirective } from './match-heigth/match-height.directive';
import { NumberLetterDirective } from './number-letters/numers-letters.directive';
import { NumberDirective } from './number/number.directive';
import { OnlyLetterDirective } from './only-letter/only-letter.directive';
import { OnlyNumberDirective } from './only-number/only-number.directive';
import { SpinnerPDFDirective } from './spinner/spinner-pdf.directive';
import { SyncControlDirective } from './sync-control/sync-control.directive';
import { DontWhiteSpaceDirective } from './white-space/white-space.directive';

@NgModule({
  exports: [
    OnlyNumberDirective,
    OnlyLetterDirective,
    DontWhiteSpaceDirective,
    NumberDirective,
    NumberLetterDirective,
    MatchHeightDirective,
    CollapseDirective,
    CollapseMobileDirective,
    ImgLoaderDirective,
    SpinnerPDFDirective,
    DropdownIconDirective,
    DropdownCloseDirective,
    AlphanumericDirective,
    ClickOutsideDirective,
    SyncControlDirective
  ],
  declarations: [
    OnlyNumberDirective,
    OnlyLetterDirective,
    DontWhiteSpaceDirective,
    NumberDirective,
    NumberLetterDirective,
    MatchHeightDirective,
    CollapseDirective,
    CollapseMobileDirective,
    ImgLoaderDirective,
    SpinnerPDFDirective,
    DropdownIconDirective,
    DropdownCloseDirective,
    AlphanumericDirective,
    ClickOutsideDirective,
    SyncControlDirective
  ]
})
export class DirectivesModule {}
