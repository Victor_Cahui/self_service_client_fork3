import { Directive, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { ResizeService, SCREEN_NAMES as _ } from '@mx/core/shared/helpers/events/resize.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[img-loader]'
})
export class ImgLoaderDirective implements OnInit, OnDestroy, OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('img-loader') imgLG: string;
  // tslint:disable-next-line:no-input-rename
  @Input('img-xs') imgXS: string;
  // tslint:disable-next-line:no-input-rename
  @Input('img-sm') imgSM: string;
  // tslint:disable-next-line:no-input-rename
  @Input('img-md') imgMD: string;
  // tslint:disable-next-line:no-input-rename
  @Input('img-xl') imgXL: string;

  @Input() imgIsBase64 = false;

  @Output() hasImage: EventEmitter<boolean>;

  private tmpHasImage: boolean;

  eventResizeSub: Subscription;

  constructor(private readonly el: ElementRef, private readonly rs: ResizeService) {
    this.hasImage = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
    this.eventResizeSub = this.rs.getEvent().subscribe(() => {
      this.setImage();
    });
  }

  ngOnChanges(): void {
    this.setImage();
  }

  setImage(): void {
    let pathImage = this.imgLG;
    const screenType = this.rs.getScreenType().toUpperCase();
    const imgType = this[`img${screenType}`];
    if (this.rs.is(_.XS) && this.imgXS) {
      pathImage = this.imgXS;
    } else {
      if (this.rs.is(_.XS) && !this.imgXS) {
        pathImage = this.imgSM;
      }
      if (this.rs.is(_.XS) && !this.imgSM) {
        pathImage = this.imgLG;
      }
    }

    if (this.rs.is(_[screenType]) && imgType) {
      pathImage = imgType;
    }

    if (pathImage) {
      this.el.nativeElement.src = this.imgIsBase64 ? `data:image/png;base64, ${pathImage}` : pathImage;
    }

    if (this.tmpHasImage !== !!pathImage) {
      this.tmpHasImage = !!pathImage;
      this.hasImage.next(this.tmpHasImage);
    }
  }

  ngOnDestroy(): void {
    if (this.eventResizeSub) {
      this.eventResizeSub.unsubscribe();
    }
  }
}
