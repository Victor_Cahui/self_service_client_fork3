import { gitDescribe } from 'git-describe';

export function getGitDescribe() {
  const opts = {
    customArguments: ['--abbrev=7']
  };
  const defaultResp = {
    distance: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT',
    hash: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT',
    version: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT'
  };

  return gitDescribe(__dirname, opts).then(r => {
    if (!r.semver) {
      console.error(`⚠️ You should tag your repository`);
    }

    const { hash, distance } = r;
    const { major = 1, minor = 1, patch = 1 } = r.semver || {};
    const commit = r.semver ? hash.substring(1) : hash;

    return {
      ...defaultResp,
      distance,
      hash: commit,
      version: `${major}.${minor}.${patch}+${distance}.${commit}`
    };
  });
}
