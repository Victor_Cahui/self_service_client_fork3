import computerName from 'computer-name';

import { getTime } from '../core';
import { BASECONFIG } from '../tasks.constants';

export function getDeployPc() {
  const res = {
    deployDate: getTime(),
    env: BASECONFIG.env,
    isDevMpf: BASECONFIG.env === 'devMpf',
    isProd: BASECONFIG.env === 'prod',
    pc: computerName()
  };
  return Promise.resolve(res);
}
