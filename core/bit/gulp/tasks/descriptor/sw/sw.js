/* eslint-disable no-console */
import gulp from 'gulp';
import mergeStream from 'merge-stream';
import yaml from 'gulp-yaml';

import { generateFile, readFile, saveFile } from '../../core';
import { generateConstantsFile, generateServicesModelFile } from './utils';
import { BASECONFIG, DESCRIPTOR } from '../../tasks.constants';

gulp.task('convertYamlToJson', function() {
  let tasks = [];
  DESCRIPTOR.descriptors.forEach(item => {
    tasks.push(
      gulp
        .src(`${DESCRIPTOR.relativePathToYaml}${item.fileName}.yaml`)
        .pipe(yaml({ safe: true, schema: 'DEFAULT_SAFE_SCHEMA', space: 2 }))
        .pipe(gulp.dest(DESCRIPTOR.dest))
    );
  });
  return mergeStream(tasks);
});

gulp.task('wrServiceAndModelTemplates', function(done) {
  DESCRIPTOR.descriptors.forEach((item, index, arr) => {
    const objDescrip = item;
    const idx = index;
    const urlDescriptor = `${DESCRIPTOR.dest}${objDescrip.fileName}.json`;

    readFile(urlDescriptor).then(jsonDescriptor => {
      Promise.all([readFile(DESCRIPTOR.services.tpl), readFile(DESCRIPTOR.models.tpl)])
        .then(([tplService, tplModel]) => {
          return Promise.all([
            generateServicesModelFile(tplService, jsonDescriptor, objDescrip),
            generateServicesModelFile(tplModel, jsonDescriptor, objDescrip)
          ]);
        })
        .then(([tplServiceGenerado, tplModelGenerado]) => {
          return Promise.all([
            saveFile(tplServiceGenerado, {
              path: DESCRIPTOR.services.dest,
              fileName: `${objDescrip.appName}.service.ts`
            }),
            saveFile(tplModelGenerado, { path: DESCRIPTOR.models.dest, fileName: `${objDescrip.appName}.model.ts` })
          ]);
        })
        .then(() => {
          if (idx === arr.length - 1) {
            console.info('-- TERMINADO 🎉🎈 Templates de Servicios y Modelos 🎈🎉');
            done();
          }
        })
        .catch(err => {
          console.error('-- ERROR ❌❌💩 Al generar Templates de Servicios y Modelos 💩❌❌');
          console.error(err);
          done();
        });
    });
  });
});

gulp.task('wrIndexTemplates', function(done) {
  Promise.all([
    readFile(DESCRIPTOR.services.barrelTpl),
    readFile(DESCRIPTOR.models.barrelTpl),
    readFile(DESCRIPTOR.constants.barrelTpl)
  ])
    .then(([tplIndexService, tplIndexModel, tplIndexConstants]) =>
      Promise.all([
        generateFile(tplIndexService, {
          data: DESCRIPTOR.descriptors
        }),
        generateFile(tplIndexModel, {
          data: DESCRIPTOR.descriptors
        }),
        generateFile(tplIndexConstants, {})
      ])
    )
    .then(([tplIndexServiceGenerado, tplIndexModelGenerado, tplIndexConstantsGenerado]) =>
      Promise.all([
        saveFile(tplIndexServiceGenerado, { path: DESCRIPTOR.services.dest, fileName: 'index.ts' }),
        saveFile(tplIndexModelGenerado, { path: DESCRIPTOR.models.dest, fileName: 'index.ts' }),
        saveFile(tplIndexConstantsGenerado, { path: DESCRIPTOR.constants.dest, fileName: 'index.ts' })
      ])
    )
    .then(() => {
      console.info('-- TERMINADO 🍺🍷 Templates de fachadas 🍷🍺');
      done();
    })
    .catch(err => {
      console.error('-- ERROR ❌❌💩 Al generar Templates de fachadas 💩❌❌');
      console.error(err);
      done();
    });
});

gulp.task('wrConstants', function(done) {
  Promise.all([readFile(DESCRIPTOR.constants.tpl)])
    .then(([tplConstants]) =>
      Promise.all([
        generateConstantsFile(tplConstants, {
          BASECONFIG,
          descriptors: DESCRIPTOR.descriptors,
          projects: DESCRIPTOR.projects
        })
      ])
    )
    .then(([tplConstantsGenerado]) =>
      Promise.all([saveFile(tplConstantsGenerado, { path: DESCRIPTOR.constants.dest, fileName: 'constants.ts' })])
    )
    .then(() => {
      console.info('-- TERMINADO 🍺🍷 Constants 🍷🍺');
      done();
    })
    .catch(err => {
      console.error('-- ERROR ❌❌💩 Al generar Templates de Constants 💩❌❌');
      console.error(err);
      done();
    });
});
