// tslint:disable-next-line: comment-type
/*
 * Public API Surface of core
 */

export * from './lib/core.service';
export * from './lib/core.component';
export * from './lib/core.module';
export * from './common/pipes/orderby.pipe';
