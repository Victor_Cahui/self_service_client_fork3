export class FilterUtil {
  public static filterBySeveralParams(list: Array<any>, filter: {}): Array<any> {
    const keys = Object.keys(filter);

    return list.filter(item => {
      return keys.find(key => {
        const reg = new RegExp(filter[key], 'gi');

        return reg.test(item[key]);
      });
    });
  }
  public static filterBySameValueInSeveralParams(
    list: Array<any>,
    searchText: string,
    params: Array<string>
  ): Array<any> {
    return list.filter(item => {
      return params.find(param => {
        const reg = new RegExp(searchText, 'gi');

        return reg.test(item[param]);
      });
    });
  }
}
