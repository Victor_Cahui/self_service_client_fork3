/**
 * Convertir string a entero
 * @param str string a convertir
 * @returns numero
 */
export function toInteger(str: string): number {
  // tslint:disable-next-line:radix
  return !str ? 0 : parseInt(str);
}
