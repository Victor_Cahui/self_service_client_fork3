import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { DatalayerClickDirective } from './datalayer-click.directive';
import { DomChangeDirective } from './dom-change.directive';
import { GAService } from './google.analytics.service';

@NgModule({
  declarations: [DatalayerClickDirective, DomChangeDirective],
  exports: [DatalayerClickDirective, DomChangeDirective],
  imports: [CommonModule],
  providers: [GAService]
})
export class GoogleModule {
  static injector: Injector;

  constructor(private readonly _injector: Injector) {
    GoogleModule.injector = _injector;
  }
}
