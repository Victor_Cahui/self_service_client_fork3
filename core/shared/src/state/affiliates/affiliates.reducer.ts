import * as AffiliatesActions from './affiliates.actions';
import * as AffiliatesState from './affiliates.state';

// tslint:disable-next-line: cyclomatic-complexity
export function affiliatesReducer(
  state: AffiliatesState.State = AffiliatesState.initialState,
  action: AffiliatesActions.Actions
): AffiliatesState.State {
  switch (action.type) {
    case AffiliatesActions.Types.AddCustom:
      return { ...state, ...action.payload };

    case AffiliatesActions.Types.CleanState:
      return AffiliatesState.initialState;

    case AffiliatesActions.Types.AddNroPolicy:
      return AffiliatesState.adapter.removeAll({ ...state, nroPolicy: action.payload });

    case AffiliatesActions.Types.GetAllAffiliates:
      return AffiliatesState.adapter.removeAll({ ...state, isLoading: true });

    case AffiliatesActions.Types.GetAllAffiliatesSuccess:
      const res = action.payload || {};
      const { informacionAdicional = {}, totalPaginas, totalRegistros } = res;
      const allAffiliates = res.listaResultados || [];

      return AffiliatesState.adapter.addAll(allAffiliates, {
        ...state,
        isLoading: false,
        totalAffiliates: informacionAdicional.totalAfiliados,
        totalPaginas,
        totalRegistros
      });

    case AffiliatesActions.Types.GetAllAffiliatesFailure:
      return { ...state, isLoading: false };

    case AffiliatesActions.Types.UpdateAffiliateSuccess:
      return AffiliatesState.adapter.updateOne(
        { id: AffiliatesState.getPrimaryKey(action.payload), changes: action.payload },
        {
          ...state,
          isLoading: false
        }
      );

    case AffiliatesActions.Types.UpdateAffiliateFailure:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
