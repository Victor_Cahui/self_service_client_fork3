import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import * as AffiliatestActions from './affiliates.actions';
import { AffiliatesFacade } from './affiliates.facade';
import { getAffiliateRequest } from './affiliates.utils';

@Injectable()
export class AffiliatesEffects {
  @Effect()
  public GetAllAffiliates$: Observable<Action> = this._Actions$.pipe(
    ofType<AffiliatestActions.GetAllAffiliates>(AffiliatestActions.Types.GetAllAffiliates),
    switchMap((action: any) => {
      return this._Autoservicios
        .ObtenerEpsContratanteAfiliadosDetalle(
          { nroContrato: this._AffiliatesFacade.getState().nroPolicy },
          { codigoApp: APPLICATION_CODE, ...action.payload }
        )
        .pipe(
          map(r => new AffiliatestActions.GetAllAffiliatesSuccess(r)),
          catchError(() => of(new AffiliatestActions.GetAllAffiliatesFailure()))
        );
    }),
    catchError(() => of(new AffiliatestActions.GetAllAffiliatesFailure()))
  );

  @Effect()
  public UpdateAffiliate$: Observable<Action> = this._Actions$.pipe(
    ofType<AffiliatestActions.UpdateAffiliate>(AffiliatestActions.Types.UpdateAffiliate),
    switchMap((action: any) => {
      const entity = this._AuthService.retrieveEntity();

      return this._Autoservicios
        .ActualizarEpsContratanteAfiliados(
          { nroContrato: this._AffiliatesFacade.getState().nroPolicy },
          { codigoApp: APPLICATION_CODE, tipoDocumento: entity.type, documento: entity.number, ...action.payload },
          { afiliadosAActualizar: getAffiliateRequest(action.payload) }
        )
        .pipe(
          map(r => new AffiliatestActions.UpdateAffiliateSuccess(action.payload)),
          catchError(() => of(new AffiliatestActions.UpdateAffiliateFailure()))
        );
    }),
    catchError(() => of(new AffiliatestActions.UpdateAffiliateFailure()))
  );

  constructor(
    private readonly _Actions$: Actions,
    private readonly _AffiliatesFacade: AffiliatesFacade,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios
  ) {}
}
