import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const STATE_KEY = 'affiliates';

export interface Affiliate {
  // TODO: update with real service response
  readonly codAfiliado?: number;
  readonly codPlan?: string;
  readonly correo?: string;
  readonly nroDocumento?: string;
  readonly telefono?: string;
}

export interface PartialState {
  readonly [STATE_KEY]: State;
}

export function getPrimaryKey(a: Affiliate): string {
  return `${a.codAfiliado}-${a.codPlan}`;
}

export const adapter: EntityAdapter<Affiliate> = createEntityAdapter<Affiliate>({
  selectId: getPrimaryKey
});

export const initialState: State = adapter.getInitialState();

export interface State extends EntityState<Affiliate> {
  readonly informacionAdicional?: any;
  readonly isLoading?: boolean;
  readonly listaResultados?: any[];
  readonly nroPolicy?: string;
  readonly selectedAffiliateId?: string | null;
  readonly totalAffiliates?: number;
  readonly totalPaginas?: number;
  readonly totalRegistros?: number;
}
