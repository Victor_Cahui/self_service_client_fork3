import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { dateDiffInHours } from '@mx/core/shared/helpers/util/date';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import * as AppointmentActions from './appointment.actions';

@Injectable()
export class AppointmentEffects {
  @Effect()
  public GetAppointmentForDetail$: Observable<Action> = this._Actions$.pipe(
    ofType<AppointmentActions.GetAppointmentForDetail>(AppointmentActions.Types.GetAppointmentForDetail),
    switchMap((action: any) => {
      return forkJoin([
        this._Autoservicios.ObtenerCita(
          { citaId: action.payload.identificadorSolicitud },
          { codigoApp: APPLICATION_CODE }
        ),
        this._Comun.ObtenerDatosServidor()
      ]).pipe(
        map(r => {
          const serverDate = r[1];
          const appointment = r[0];
          const serverDatetime = `${serverDate.fecha}T${serverDate.hora}`;
          const appointmentDatetime = `${appointment.fecha}T${appointment.horaInicio}`;

          return {
            ...appointment,
            estadoCita: action.payload.cita.estadoCita,
            canModify: dateDiffInHours(appointmentDatetime, serverDatetime) >= 3
          };
        }),
        map(r => new AppointmentActions.GetAppointmentForDetailSuccess(r)),
        catchError(r => of(new AppointmentActions.GetAppointmentForDetailFailure(r)))
      );
    }),
    catchError(r => of(new AppointmentActions.GetAppointmentForDetailFailure(r)))
  );

  constructor(
    private readonly _Actions$: Actions,
    private readonly _Autoservicios: Autoservicios,
    private readonly _Comun: Comun
  ) {}
}
