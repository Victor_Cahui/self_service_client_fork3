import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as AppointmentState from './appointment.state';

const getAppointmentState = createFeatureSelector<AppointmentState.State>(AppointmentState.STATE_KEY);

const getAppointment = createSelector(
  getAppointmentState,
  state => state
);

const getPatient = createSelector(
  getAppointment,
  state => state.paciente
);

const getPolicy = createSelector(
  getAppointment,
  state => state.poliza
);

const getSpeciality = createSelector(
  getAppointment,
  state => state.especialidad
);

const getDoctor = createSelector(
  getAppointment,
  state => state.doctor
);

const getTime = createSelector(
  getAppointment,
  state => state.time
);

const getModality = createSelector(
  getAppointment,
  state => state.tipoCita
);

export const AppointmentSelectors = {
  getAppointment,
  getDoctor,
  getPatient,
  getPolicy,
  getSpeciality,
  getTime,
  getModality
};
