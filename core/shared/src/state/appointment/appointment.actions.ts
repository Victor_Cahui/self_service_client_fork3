// tslint:disable:max-classes-per-file no-unused-variable max-line-length
import { Action } from '@ngrx/store';

export enum Types {
  AddCustom = '[Appointment][API] Schedule Detail - AddCustom',
  GetAppointmentForDetail = '[Appointment][API] Schedule Detail - GetAppointmentForDetail',
  GetAppointmentForDetailSuccess = '[Appointment][API] Schedule Detail - GetAppointmentForDetailSuccess',
  GetAppointmentForDetailFailure = '[Appointment][API] Schedule Detail - GetAppointmentForDetailFailure',
  AddCellphone = '[Appointment][Page] Schedule Steps - AddCellphone',
  AddDate = '[Appointment][Page] Schedule Steps - AddDate',
  AddDoctor = '[Appointment][Page] Schedule Steps - AddDoctor',
  AddEstablishment = '[Appointment][Page] Schedule Home - AddEstablishment',
  AddFrmStep1 = '[Appointment][API] Schedule Steps - AddFrmStep1',
  AddFrmStep2 = '[Appointment][API] Schedule Steps - AddFrmStep2',
  AddHour = '[Appointment][Page] Schedule Steps - AddHour',
  AddPatient = '[Appointment][Page] Schedule Steps - AddPatient',
  AddPolicy = '[Appointment][Page] Schedule Steps - AddPolicy',
  AddSpeciality = '[Appointment][Page] Schedule Steps - AddSpeciality',
  CleanAppointment = '[Appointment][Page] CleanAppointment',
  CleanDate = '[Appointment][Page] Schedule Summary - CleanDate',
  CleanDoctor = '[Appointment][Page] Schedule Summary - CleanDoctor',
  CleanFrmStep2 = '[Appointment][Page] CleanFrmStep2',
  CleanHour = '[Appointment][Page] Schedule Summary - CleanHour',
  CleanPatient = '[Appointment][Page] CleanPatient',
  CleanPolicy = '[Appointment][Page] CleanPolicy',
  CleanSpeciality = '[Appointment][Page] CleanSpeciality',
  CleanStep2 = '[Appointment][Page] CleanStep2',
  Modality = '[Appointment][API] tipoCita'
}

export class GetAppointmentForDetail implements Action {
  public readonly type = Types.GetAppointmentForDetail;
  constructor(public payload: any) {}
}

export class GetAppointmentForDetailSuccess implements Action {
  public readonly type = Types.GetAppointmentForDetailSuccess;
  constructor(public payload: any) {}
}

export class GetAppointmentForDetailFailure implements Action {
  public readonly type = Types.GetAppointmentForDetailFailure;
  constructor(public payload: any) {}
}

export class AddCellphone implements Action {
  public readonly type = Types.AddCellphone;
  constructor(public payload: any) {}
}

export class AddCustom implements Action {
  public readonly type = Types.AddCustom;
  constructor(public payload: any) {}
}

export class AddFrmStep1 implements Action {
  public readonly type = Types.AddFrmStep1;
  constructor(public payload: any) {}
}

export class AddFrmStep2 implements Action {
  public readonly type = Types.AddFrmStep2;
  constructor(public payload: any) {}
}

export class CleanAppointment implements Action {
  public readonly type = Types.CleanAppointment;
}

export class CleanFrmStep2 implements Action {
  public readonly type = Types.CleanFrmStep2;
}

export class CleanStep2 implements Action {
  public readonly type = Types.CleanStep2;
}

export class AddEstablishment implements Action {
  public readonly type = Types.AddEstablishment;
  constructor(public payload: any) {}
}

export class AddPatient implements Action {
  public readonly type = Types.AddPatient;
  constructor(public payload: any) {}
}

export class AddSpeciality implements Action {
  public readonly type = Types.AddSpeciality;
  constructor(public payload: any) {}
}

export class AddPolicy implements Action {
  public readonly type = Types.AddPolicy;
  constructor(public payload: any) {}
}

export class AddDoctor implements Action {
  public readonly type = Types.AddDoctor;
  constructor(public payload: any) {}
}

export class AddModality implements Action {
  public readonly type = Types.Modality;
  constructor(public payload: any) {}
}

export class AddDate implements Action {
  public readonly type = Types.AddDate;
  constructor(public payload: any) {}
}

export class AddHour implements Action {
  public readonly type = Types.AddHour;
  constructor(public payload: any) {}
}

export class CleanPolicy implements Action {
  public readonly type = Types.CleanPolicy;
}

export class CleanPatient implements Action {
  public readonly type = Types.CleanPatient;
}

export class CleanSpeciality implements Action {
  public readonly type = Types.CleanSpeciality;
}

export class CleanDate implements Action {
  public readonly type = Types.CleanDate;
}

export class CleanDoctor implements Action {
  public readonly type = Types.CleanDoctor;
}

export class CleanHour implements Action {
  public readonly type = Types.CleanHour;
}

export type Actions =
  | GetAppointmentForDetail
  | GetAppointmentForDetailSuccess
  | GetAppointmentForDetailFailure
  | AddCellphone
  | AddCustom
  | AddDate
  | AddDoctor
  | AddEstablishment
  | AddFrmStep1
  | AddFrmStep2
  | AddHour
  | AddPatient
  | AddPolicy
  | AddSpeciality
  | CleanAppointment
  | CleanDate
  | CleanDoctor
  | CleanFrmStep2
  | CleanHour
  | CleanPatient
  | CleanPolicy
  | CleanSpeciality
  | CleanStep2
  | AddModality;
