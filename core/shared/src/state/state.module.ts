import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AffiliatesFacade } from './affiliates';
import { AffiliatesEffects } from './affiliates/affiliates.effects';
import { AppFacade } from './app';
import { appMetaReducers, appReducer } from './app.reducer';
import { AppEffects } from './app/app.effects';
import { AppointmentFacade } from './appointment';
import { AppointmentEffects } from './appointment/appointment.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EffectsModule.forRoot([AppEffects, AppointmentEffects, AffiliatesEffects]),
    StoreModule.forRoot(appReducer, { metaReducers: appMetaReducers })
    // StoreDevtoolsModule.instrument({ maxAge: 15, logOnly: environment.production })
  ],
  providers: [AppFacade, AppointmentFacade, AffiliatesFacade]
})
export class StateModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: StateModule
    };
  }

  constructor(
    @Optional()
    @SkipSelf()
    parentModule: StateModule
  ) {
    if (parentModule) {
      // tslint:disable-next-line: no-throw
      throw new Error('StateModule is already loaded. Import it in the AppModule only');
    }
  }
}
