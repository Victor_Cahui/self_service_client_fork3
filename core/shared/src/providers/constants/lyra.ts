import { getTextTranslate, PaymentDocumentType, PaymentMode } from '@mx/components/shared/utils/payment';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { ITarjeta } from '@mx/statemanagement/models/payment.models';

export const lyraScripts: any = {
  form: 'https://api.payzen.lat/static/js/krypton-client/V4.0/stable/kr-payment-form.min.js',
  materialJS: 'https://api.payzen.lat/static/js/krypton-client/V4.0/ext/material.js',
  materialCSS: 'https://api.payzen.lat/static/js/krypton-client/V4.0/ext/material-reset.css'
};

export class FormLyraObjects {
  public static getToken(quote, clientService, paymentMode, tipoDocumentSoat): any {
    return {
      codigoMoneda: quote.response
        ? quote.response.codigoMoneda
        : quote.withMPD
        ? quote.dataQuote.cotizacionConMPD.codigoMoneda
        : quote.dataQuote.cotizacionSinMPD.codigoMoneda,
      monto: quote.response
        ? quote.response.montoTotal
        : quote.withMPD
        ? quote.dataQuote.cotizacionConMPD.montoFinal
        : quote.dataQuote.cotizacionSinMPD.montoFinal,
      descripcionOrden: quote.response
        ? quote.response.tipoDocumentoPago === PaymentDocumentType.RECIBO
          ? quote.response.numRecibo
          : quote.response.numeroAviso
        : `${APPLICATION_CODE}_SOAT_${quote.dataQuote.datosVehiculo.vehiculo.placa}`,
      cliente: {
        correo: clientService.getUserEmail()
      },
      formaDePago: paymentMode,
      tipoDocumentoPago: quote.response ? quote.response.tipoDocumentoPago : tipoDocumentSoat
    };
  }

  public static getPaymentObject(event, quote, currentPaymentMode, seletecCard?: ITarjeta): any {
    let body: any;
    if (quote.comesSoat) {
      body = {
        vehiculo: quote.dataQuote.datosVehiculo.vehiculo,
        contratante: quote.dataQuote.contratante,
        cotizacion: quote.withMPD ? quote.dataQuote.cotizacionConMPD : quote.dataQuote.cotizacionSinMPD,
        vigencia: {
          fechaInicio: quote.startDate ? quote.startDate : quote.expiredDate,
          fechaFin: quote.endDate ? quote.endDate : quote.expiredDate
        },
        pago: {
          tipoPago: currentPaymentMode,
          monto: quote.withMPD
            ? quote.dataQuote.cotizacionConMPD.montoFinal
            : quote.dataQuote.cotizacionSinMPD.montoFinal,
          tipoMoneda: quote.withMPD
            ? quote.dataQuote.cotizacionConMPD.codigoMoneda
            : quote.dataQuote.cotizacionSinMPD.codigoMoneda
        },
        tarjeta: {
          numero: event.clientAnswer.transactions[0].transactionDetails.cardDetails.pan,
          nombre: event.clientAnswer.transactions[0].transactionDetails.cardDetails.cardHolderName,
          correoElectronico: event.clientAnswer.customer.email,
          tipo: event.clientAnswer.transactions[0].transactionDetails.cardDetails.effectiveBrand,
          tipoOperacion: event.clientAnswer.transactions[0].operationType,
          tarjetaToken: event.clientAnswer.transactions[0].paymentMethodToken,
          mesVencimiento: event.clientAnswer.transactions[0].transactionDetails.cardDetails.expiryMonth.toString(),
          anioVencimiento: event.clientAnswer.transactions[0].transactionDetails.cardDetails.expiryYear.toString(),
          guardar: event.clientAnswer.saveCard ? event.clientAnswer.saveCard : false
        },
        transaccionPayment: {
          codTransaccion: event.clientAnswer.shopId,
          estadoOrden: event.clientAnswer.orderStatus
        }
      };
    } else if (currentPaymentMode === PaymentMode.TOKN) {
      body = {
        modalidadPago: currentPaymentMode,
        guardarTarjeta: true,
        tarjeta: {
          numero: seletecCard.numero,
          tipo: seletecCard.tipo,
          tarjetaToken: seletecCard.tarjetaToken,
          correoElectronico: event.clientAnswer.customer.email
        },
        recibo: FormLyraObjects._mapRequestReceipt(quote)
      };
    } else {
      body = {
        modalidadPago: currentPaymentMode,
        codigoTransaccion: event.clientAnswer.shopId,
        estadoOrden: event.clientAnswer.orderStatus,
        tienePagoAutomatico: event.clientAnswer.autoDebit ? event.clientAnswer.autoDebit : false,
        guardarTarjeta: event.clientAnswer.saveCard ? event.clientAnswer.saveCard : false,
        tarjeta: {
          numero: event.clientAnswer.transactions[0].transactionDetails.cardDetails.pan,
          nombre: event.clientAnswer.transactions[0].transactionDetails.cardDetails.cardHolderName,
          correoElectronico: event.clientAnswer.customer.email,
          tipo: event.clientAnswer.transactions[0].transactionDetails.cardDetails.effectiveBrand,
          tipoOperacion: event.clientAnswer.transactions[0].operationType,
          tarjetaToken: event.clientAnswer.transactions[0].paymentMethodToken,
          mesVencimiento: event.clientAnswer.transactions[0].transactionDetails.cardDetails.expiryMonth.toString(),
          anioVencimiento: event.clientAnswer.transactions[0].transactionDetails.cardDetails.expiryYear.toString()
        },
        recibo: FormLyraObjects._mapRequestReceipt(quote)
      };
    }

    return body;
  }

  public static _mapRequestReceipt(quote): any {
    return {
      codCia: quote.response.codCia,
      codRamo: quote.response.codRamo,
      codModalidad: quote.response.codModalidad,
      descripcionPoliza: quote.response.descripcionPoliza,
      tipoPoliza: quote.response.tipoPoliza,
      numeroPoliza: quote.response.numeroPoliza,
      numRecibo: quote.response.numRecibo,
      numeroAviso: quote.response.numeroAviso,
      tipoDocumentoPago: quote.response.tipoDocumentoPago,
      numSpto: quote.response.numSpto,
      cuota: quote.response.cuota,
      montoTotal: quote.response.montoTotal,
      codigoMoneda: quote.response.codigoMoneda,
      tiposPagosId: quote.response.codigoMoneda,
      tiposPagosLlave: quote.response.codigoMoneda,
      fechaExpiracion: quote.response.fechaExpiracion,
      tieneCobertura: quote.response.tieneCobertura,
      beneficiarios: [],
      datosPoliza: quote.response.datosPoliza
    };
  }

  public static getLyraScripts(lyraScriptsInternal, scriptType, apiPublicKey): any {
    return [
      {
        src: lyraScriptsInternal.form,
        type: scriptType.JS,
        attributes: {
          'kr-public-key': apiPublicKey
        }
      },
      { src: lyraScriptsInternal.materialJS, type: scriptType.JS },
      { src: lyraScriptsInternal.materialCSS, type: scriptType.CSS }
    ];
  }

  public static removeForms(document: Document): any {
    const elementsKr = Array.from(document.querySelectorAll('[kr-resource]'));
    const elementsTx = Array.from(document.querySelectorAll('[tx-content]'));
    elementsKr.forEach(element => {
      element.remove();
    });
    elementsTx.forEach(element => {
      element.remove();
    });
    const keysKr: string[] = Object.keys(window).filter(
      key => key.startsWith('KR') || key.startsWith('kr') || key.startsWith('__kr__')
    );
    keysKr.forEach(key => {
      // tslint:disable-next-line: no-dynamic-delete
      delete (window as any)[key];
    });
  }

  public static setLabelForm(document: Document): void {
    const elementsKr = Array.from(document.querySelectorAll('.kr-label'));
    elementsKr.forEach((data: any) => {
      if (!data.firstElementChild) {
        return void 0;
      }
      data.firstElementChild.innerHTML = getTextTranslate(data.firstElementChild.innerHTML);
    });
  }

  public static getTokenizer(codigoMoneda, clientService): any {
    return {
      codigoMoneda,
      cliente: {
        correo: clientService.getUserEmail()
      },
      formaDePago: 'RT'
    };
  }
}
