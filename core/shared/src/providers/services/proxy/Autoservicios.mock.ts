//tslint:disable

import {
  SCTR_PERIOD_STATUS,
  SITUATION_TYPE_SCTR_PERIODS,
  TRAFFIC_LIGHT_COLOR
} from '@mx/components/shared/utils/policy';
import * as model from '@mx/core/shared/providers/models';
import { of } from 'rxjs';
import { Observable } from 'rxjs/Observable';

export class AutoserviciosMock {
  public ListarPeridoSCTR(
    pathReq: model.ReqPathGetListarPeridoSCTR,
    queryReq: model.ReqQueryGetListarPeridoSCTR,
    reqOptions?: any
  ): Observable<any> {
    return of({
      totalRegistros: 4,
      informacionAdicional: { tieneMasPeriodos: true },
      totalPaginas: 2,
      listaResultados: [
        {
          fechaPeriodo: 'ENERO 2020',
          estadoPeriodo: 'DECLARADO',
          cantTrabajadores: '70 trabajadores',
          tipoSituacion: 'RECIBOS PENDIENTES',
          semaforo: 'AMARILLO',
          tieneDeclaracion: false,
          tieneInclusion: false,
          puedeDeclarar: false,
          puedeDesglosar: true,
          fechaInicioVigencia: '2020-01-01',
          fechaFinVigencia: '2020-01-31',
          colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
          frecuenciaPeriodo: '1/4',
          polizas: []
        },
        {
          fechaPeriodo: 'FEBRERO 2020',
          estadoPeriodo: 'DECLARADO',
          cantTrabajadores: '50 trabajadores',
          tipoSituacion: 'PAGADO',
          semaforo: 'AMARILLO',
          tieneDeclaracion: false,
          tieneInclusion: true,
          puedeDeclarar: false,
          puedeDesglosar: true,
          fechaInicioVigencia: '2020-02-01',
          fechaFinVigencia: '2020-02-28',
          colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
          frecuenciaPeriodo: '2/4',
          polizas: []
        },
        {
          fechaPeriodo: 'MARZO 2020',
          estadoPeriodo: 'NO DECLARADO',
          cantTrabajadores: null,
          tipoSituacion: null,
          semaforo: null,
          tieneDeclaracion: true,
          tieneInclusion: false,
          puedeDeclarar: true,
          puedeDesglosar: false,
          fechaInicioVigencia: '2020-03-01',
          fechaFinVigencia: '2020-03-31',
          colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
          frecuenciaPeriodo: '3/4',
          polizas: []
        }
      ]
    });
  }

  public ListarPeridoAdicionalesSCTR(
    pathReq: model.ReqPathGetListarPeridoAdicionalesSCTR,
    queryReq: model.ReqQueryGetListarPeridoAdicionalesSCTR,
    reqOptions?: any
  ): Observable<any> {
    return of({
      totalRegistros: 4,
      informacionAdicional: {
        tieneMasPeriodos: true
      },
      totalPaginas: 2,
      listaResultados: [
        {
          fechaPeriodo: 'ENERO 2020',
          estadoPeriodo: 'DECLARADO',
          cantTrabajadores: '70 trabajadores',
          tipoSituacion: 'RECIBOS PENDIENTES',
          semaforo: 'AMARILLO',
          tieneDeclaracion: false,
          tieneInclusion: false,
          puedeDeclarar: false,
          puedeDesglosar: true,
          fechaInicioVigencia: '2020-01-01',
          fechaFinVigencia: '2020-01-31',
          colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
          frecuenciaPeriodo: '1/4',
          polizas: [
            {
              numeroPoliza: '7022000000853',
              codRamo: 702,
              descripcionRamo: 'SCTR Salud',
              tipoPoliza: 'MD_SCTR',
              codCia: 3,
              numApli: 0,
              numSpto: 0,
              numSptoApli: 0,
              estadoPeriodo: 'DECLARADO',
              cantTrabajadores: '35 trabajadores',
              tieneOpciones: false
            },
            {
              numeroPoliza: '7012000000471',
              codRamo: 701,
              descripcionRamo: 'SCTR Pensión',
              tipoPoliza: 'MD_SCTR',
              codCia: 2,
              numApli: 0,
              numSpto: 0,
              numSptoApli: 0,
              estadoPeriodo: 'DECLARADO',
              cantTrabajadores: '35 trabajadores',
              tieneOpciones: true
            }
          ]
        },
        {
          fechaPeriodo: 'FEBRERO 2020',
          estadoPeriodo: 'DECLARADO',
          cantTrabajadores: '50 trabajadores',
          tipoSituacion: 'PAGADO',
          semaforo: 'AMARILLO',
          tieneDeclaracion: false,
          tieneInclusion: true,
          puedeDeclarar: false,
          puedeDesglosar: true,
          fechaInicioVigencia: '2020-02-01',
          fechaFinVigencia: '2020-02-28',
          colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
          frecuenciaPeriodo: '2/4',
          polizas: [
            {
              numeroPoliza: '7022000000853',
              codRamo: 702,
              descripcionRamo: 'SCTR Salud',
              tipoPoliza: 'MD_SCTR',
              codCia: 3,
              numApli: 0,
              numSpto: 0,
              numSptoApli: 0,
              estadoPeriodo: 'DECLARADO',
              cantTrabajadores: '25 trabajadores',
              tieneOpciones: false
            },
            {
              numeroPoliza: '7012000000471',
              codRamo: 701,
              descripcionRamo: 'SCTR Pensión',
              tipoPoliza: 'MD_SCTR',
              codCia: 2,
              numApli: 0,
              numSpto: 0,
              numSptoApli: 0,
              estadoPeriodo: 'DECLARADO',
              cantTrabajadores: '25 trabajadores',
              tieneOpciones: true
            }
          ]
        },
        {
          fechaPeriodo: 'MARZO 2020',
          estadoPeriodo: 'NO DECLARADO',
          cantTrabajadores: null,
          tipoSituacion: null,
          semaforo: null,
          tieneDeclaracion: true,
          tieneInclusion: false,
          puedeDeclarar: true,
          puedeDesglosar: false,
          fechaInicioVigencia: '2020-03-01',
          fechaFinVigencia: '2020-03-31',
          colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
          frecuenciaPeriodo: '3/4',
          polizas: [
            {
              numeroPoliza: '7022000000853',
              codRamo: 702,
              descripcionRamo: 'SCTR Salud',
              tipoPoliza: 'MD_SCTR',
              codCia: 3,
              numApli: 0,
              numSpto: 0,
              numSptoApli: 0,
              estadoPeriodo: 'NO DECLARADO',
              cantTrabajadores: null,
              tieneOpciones: false
            },
            {
              numeroPoliza: '7012000000471',
              codRamo: 701,
              descripcionRamo: 'SCTR Pensión',
              tipoPoliza: 'MD_SCTR',
              codCia: 2,
              numApli: 0,
              numSpto: 0,
              numSptoApli: 0,
              estadoPeriodo: 'NO DECLARADO',
              cantTrabajadores: null,
              tieneOpciones: true
            }
          ]
        }
      ]
    });
  }

  public ListarRiesgoSCTR(
    pathReq: model.ReqPathGetListarRiesgoSCTR,
    queryReq: model.ReqQueryGetListarRiesgoSCTR,
    reqOptions?: any
  ): Observable<any> {
    return of({
      cantTrabajadores: '50 trabajadores',
      colectivoAsegurado: 'FQR INGENIERIA Y CONSTRUCCION EIRL',
      estadoPeriodo: SCTR_PERIOD_STATUS.DECLARADO,
      fechaFinVigencia: '2020-02-28',
      fechaInicioVigencia: '2020-02-01',
      fechaPeriodo: 'FEBRERO 2020',
      frecuenciaPeriodo: '2/4',
      polizas: [
        {
          cantTrabajadores: '50 trabajadores',
          codCia: 3,
          codRamo: 702,
          descripcionRamo: 'SCTR Salud',
          estadoPeriodo: SCTR_PERIOD_STATUS.DECLARADO,
          numApli: 0,
          numSpto: 0,
          numApliSpto: 0,
          numeroPoliza: '7021800000555',
          tieneOpciones: true,
          tipoPoliza: 'MD_SCTR'
        }
      ],
      puedeDeclarar: false,
      puedeDesglosar: true,
      semaforo: TRAFFIC_LIGHT_COLOR.AMARILLO,
      tieneDeclaracion: false,
      tieneInclusion: true,
      tipoSituacion: SITUATION_TYPE_SCTR_PERIODS.PAGADO
    });
  }

  public ListarDeclaracionResumenSCTR(
    queryReq: model.ReqQueryGetListarDeclaracionResumenSCTR,
    reqOptions?: any
  ): Observable<any> {
    return of({
      informacionOperacion: {
        centroCosto: 'Pruebas Multiplica'
      },
      detalleRiesgos: [
        {
          titulo: 'Poliza SCTR salud : 7022000000919',
          codMoneda: 1,
          sueldoTope: 0.0,
          fechaInicioVigencia: '2020-10-01',
          fechaFinVigencia: '2020-10-31',
          totalTrabajadores: 2,
          totalPlanilla: 10000.0,
          totalPrima: 150.0,
          riesgos: [
            {
              numRiesgo: 1,
              descRiesgo: 'TRABAJADORES ALTO RIESGO',
              nroTrabajadores: 2,
              montoPlanilla: 10000.0,
              montoPrima: 150.0
            }
          ]
        }
      ]
    });
  }

  public ListarInclusionResumenSCTR(
    queryReq: model.ReqQueryGetListarInclusionResumenSCTR,
    reqOptions?: any
  ): Observable<any> {
    return of({
      informacionOperacion: {
        centroCosto: 'Pruebas de Inclusion'
      },
      detalleRiesgos: [
        {
          titulo: 'Poliza SCTR pensión : 7012000034923',
          codMoneda: 1,
          sueldoTope: 0.0,
          fechaInicioVigencia: '2020-12-01',
          fechaFinVigencia: '2020-12-31',
          totalTrabajadores: 4,
          totalPlanilla: 9000.0,
          totalPrima: 100.0,
          riesgos: [
            {
              numRiesgo: 1,
              descRiesgo: 'TRABAJADORES ALTO RIESGO',
              nroTrabajadores: 3,
              montoPlanilla: 1000.0,
              montoPrima: 50.0
            }
          ]
        }
      ]
    });
  }
}
