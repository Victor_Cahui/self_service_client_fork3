// tslint:disable

import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as model from '@mx/core/shared/providers/models';
import { ApiService } from '@mx/core/shared/helpers/api';
import { appConstants } from '@mx/core/shared/providers/constants';

interface ReqOptions {
  default?: any;
  headers?:
    | HttpHeaders
    | {
        [header: string]: string | string[];
      };
  observe?: 'body'; // response, body
  params?:
    | HttpParams
    | {
        [param: string]: string | string[];
      };
  preloader?: boolean;
  reportProgress?: boolean;
  responseType?: 'json';
  retry?: number;
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class Sctr {
  constructor(private readonly _ApiService: ApiService) {}

  public ListadoActividadesRiesgo(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.sctr}/sctr/actividadesriesgo`, { ...reqOptions });
  }

  public HacerCotizacion(bodyReq: model.ReqBodyPostHacerCotizacion, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.sctr}/sctr/cotizacion`,
      model.ReqBodyPostHacerCotizacion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public HacerCotizacionReferida(
    bodyReq: model.ReqBodyPostHacerCotizacionReferida,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.sctr}/sctr/cotizacion/referido`,
      model.ReqBodyPostHacerCotizacionReferida.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EmitirPagoBancario(bodyReq: model.ReqBodyPostEmitirPagoBancario, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.sctr}/sctr/emision/pagoBancario`,
      model.ReqBodyPostEmitirPagoBancario.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EmitirTarjetas(bodyReq: model.ReqBodyPostEmitirTarjetas, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.sctr}/sctr/emision/tarjeta`,
      model.ReqBodyPostEmitirTarjetas.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EmitirCorreo(bodyReq: model.ReqBodyPostEmitirCorreo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.sctr}/sctr/emision/correos`,
      model.ReqBodyPostEmitirCorreo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerClausula(pathReq: model.ReqPathGetObtenerClausula, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.sctr}/sctr/emision/clausula/${pathReq.subActividadId}`, {
      ...reqOptions
    });
  }

  public ObtenerListaDocumentos(
    queryReq: model.ReqQueryGetObtenerListaDocumentos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.sctr}/sctr/emision/documentos`, {
      params: { ...model.ReqQueryGetObtenerListaDocumentos.create(queryReq) },
      ...reqOptions
    });
  }

  public EmitirDocumentos(pathReq: model.ReqPathGetEmitirDocumentos, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.sctr}/sctr/emision/documento/${pathReq.numeroPoliza}/${pathReq.codigoDocumento}`,
      { ...reqOptions }
    );
  }

  public ObtenerConstanciasYAsegurados(
    pathReq: model.ReqPathGetObtenerConstanciasYAsegurados,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.sctr}/sctr/constancia/${pathReq.codigoApp}/${pathReq.nroConstancia}/${pathReq.tipoDocumento}/${pathReq.documento}`,
      { ...reqOptions }
    );
  }
}
