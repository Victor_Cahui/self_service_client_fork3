// tslint:disable

import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as model from '@mx/core/shared/providers/models';
import { ApiService } from '@mx/core/shared/helpers/api';
import { appConstants } from '@mx/core/shared/providers/constants';

interface ReqOptions {
  default?: any;
  headers?:
    | HttpHeaders
    | {
        [header: string]: string | string[];
      };
  observe?: 'body'; // response, body
  params?:
    | HttpParams
    | {
        [param: string]: string | string[];
      };
  preloader?: boolean;
  reportProgress?: boolean;
  responseType?: 'json';
  retry?: number;
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class Seguridad {
  constructor(private readonly _ApiService: ApiService) {}

  public Login(bodyReq?: model.ReqFormDataPostLogin, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/login2`,
      model.ReqFormDataPostLogin.create(bodyReq),
      { ...reqOptions }
    );
  }

  public LoginPaso1(bodyReq?: model.ReqFormDataPostLoginPaso1, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/login`,
      model.ReqFormDataPostLoginPaso1.create(bodyReq),
      { ...reqOptions }
    );
  }

  public LoginPaso2(bodyReq?: model.ReqFormDataPostLoginPaso2, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/claims/GenerateClaimsByGroupType`,
      model.ReqFormDataPostLoginPaso2.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ClaimsList(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(`${appConstants.apis.seguridad}/api/claims/list`, { ...reqOptions });
  }

  public XXX(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(`${appConstants.apis.seguridad}/api/seguridad/acceso/logout`, { ...reqOptions });
  }

  public ObtenerTipoUsuario(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(`${appConstants.apis.seguridad}/api/person/GetUsers`, { ...reqOptions });
  }

  public CambioContrasenaUsuarioPersona(
    bodyReq?: model.ReqBodyPostCambioContrasenaUsuarioPersona,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/seguridad/ChangePasswordPersonClientUser`,
      model.ReqBodyPostCambioContrasenaUsuarioPersona.create(bodyReq),
      { ...reqOptions }
    );
  }

  public CambiarContraseniaConContraseniaAntigua(
    bodyReq?: model.ReqBodyPostCambiarContraseniaConContraseniaAntigua,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/seguridad/ChangePasswordPersonClientUserWithOldPassword`,
      model.ReqBodyPostCambiarContraseniaConContraseniaAntigua.create(bodyReq),
      { ...reqOptions }
    );
  }

  public CambiarContraseniaConToken(
    bodyReq?: model.ReqBodyPostCambiarContraseniaConToken,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/seguridad/acceso/ChangePassword`,
      model.ReqBodyPostCambiarContraseniaConToken.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RecuperarContraseniaMail(
    bodyReq: model.ReqBodyPostRecuperarContraseniaMail,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/seguridad/acceso/SendEmailRecoverPassword`,
      model.ReqBodyPostRecuperarContraseniaMail.create(bodyReq),
      { ...reqOptions }
    );
  }

  public HabilitarUsuario(bodyReq: model.ReqBodyPostHabilitarUsuario, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/seguridad/InsertPersonClientUserAuto`,
      model.ReqBodyPostHabilitarUsuario.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerMenuClienteEmpresa(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.seguridad}/api/home/clientCompany/3`, { ...reqOptions });
  }

  public ObtenerAplicacionesClienteEmpresa(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.seguridad}/api/home/applications/clientCompany/3`, {
      ...reqOptions
    });
  }

  public ObtenerAplicacionesLegacyClienteEmpresa(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.seguridad}/api/home/applications/legacy/clientCompany/3`, {
      ...reqOptions
    });
  }

  public GenerarTokenDummy(bodyReq: model.ReqBodyPostGenerarTokenDummy, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/redirect/token/generate`,
      model.ReqBodyPostGenerarTokenDummy.create(bodyReq),
      { ...reqOptions }
    );
  }

  public TransformarTokenDummy(
    bodyReq: model.ReqBodyPostTransformarTokenDummy,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.seguridad}/api/redirect/token/transform`,
      model.ReqBodyPostTransformarTokenDummy.create(bodyReq),
      { ...reqOptions }
    );
  }

  public GenerarTokenMapfre(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(`${appConstants.apis.seguridad}/api/claims/GenerateTokenMapfre`, { ...reqOptions });
  }
}
