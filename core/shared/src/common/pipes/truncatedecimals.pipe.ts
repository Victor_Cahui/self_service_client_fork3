import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateDecimals'
})
export class TruncateDecimalsPipe implements PipeTransform {
  transform(value: number, truncate: number): number {
    const pattern = new RegExp(`^-?\\d+(?:\\.\\d{0,${truncate}})?`);

    return Number(value.toString().match(pattern)[0]);
  }
}
